<?php
	require_once __DIR__ . "/ActiveDataModel.php";

	class ChannelOrderItem extends ActiveDataModel {

		const PACKING_STATUS_ID = 9;

		/**
		 * Returns the primary key value of the row of data
		 *
		 *
		 * @return int
		 */
		public function getPk() {
			return $this->ChannelOrderItemID;
		}

		/**
		 * @return mixed
		 */
		public function setPk( $pk ) {
			$this->ChannelOrderItemID = $pk;
		}

		/**
		 * Stores all possible attributes for a model
		 *
		 *
		 * @return mixed
		 */
		public function attributeNames() {
			return array(
				'ChannelOrderItemID',
				'ChannelOrderID',
				'OrderItemID',
				'ExternalOrderItemID',
				'ChannelOrderItemUPC',
				'ChannelOrderItemSKU',
				'ChannelOrderItemStatusID',
				'ChannelOrderItemShippedInd',
				'ChannelOrderItemReturnedInd',
				'ChannelOrderItemRefundedInd',
                'ChannelOrderItemRefundRMA',
			);
		}

		/**
		 * @return string
		 */
		protected function tableName() {
			return __CLASS__;
		}
	}