<?php
	require_once __DIR__ . "/ActiveDataModel.php";

	class OrderChannelOrder extends ActiveDataModel {

		/**
		 * @return string
		 */
		protected function tableName() {
			return __CLASS__;
		}

		/**
		 * Returns the primary key value of the row of data
		 *
		 *
		 * @return int
		 */
		public function getPk() {
			return $this->OrderChannelOrderID;
		}

		/**
		 * @return mixed
		 */
		public function setPk( $pk ) {
			$this->OrderChannelOrderID = $pk;
		}

		/**
		 * Stores all possible attributes for a model
		 *
		 *
		 * @return mixed
		 */
		public function attributeNames() {
			return array(
				'OrderChannelOrderID',
			    'OrderID',
			    'ChannelOrderID',
			);
		}

	}