<?php
	require_once __DIR__ . "/ActiveDataModel.php";

	class ChannelOrder extends ActiveDataModel {

		private $_orderItems = array();

		/**
		 * @return array
		 */
		public function getOrderItems() {
			return $this->_orderItems;
		}

		/**
		 * @param array $orderItems
		 */
		public function setOrderItems( $orderItems ) {
			$this->_orderItems = $orderItems;
		}

		/**
		 * Stores all possible attributes for a model
		 *
		 *
		 * @return mixed
		 */
		public function attributeNames() {
			return array(
				'ChannelOrderID',
				'ChannelCode',
				'ChannelOrderStatusID',
				'ExternalOrderID',
				'ChannelOrderAcknowledgedInd',
				'ChannelOrderPlacedTS',
				'ChannelOrderTransmittedTS',
			);
		}

		/**
		 * Returns the primary key value of the row of data
		 *
		 *
		 * @return int
		 */
		public function getPk() {
			return $this->ChannelOrderID;
		}

		/**
		 * @return mixed
		 */
		public function setPk( $pk ) {
			$this->ChannelOrderID = $pk;
		}

		/**
		 * @return string
		 */
		protected function tableName() {
			return __CLASS__;
		}

		/**
		 * @return array|bool|null
		 */
		public function getInternalOrderData() {
			if (!$this->getPk()) {
				return false;
			}

			$order_num = $this->getExternalOrderID();

			$sql = <<<SQL
                        SELECT
                o.*
            FROM
                ChannelOrder co
            JOIN OrderChannelOrder oco ON oco.ChannelOrderID = co.ChannelOrderID
            JOIN Orders o ON o.ID = oco.OrderID
            WHERE
                co.ExternalOrderID = '$order_num'
                LIMIT 1
SQL;

			$internal_order_res = mysqli_query($this->getDBConnection(), $sql);
			if (!$internal_order_res) {
				return false;
			}

			return mysqli_fetch_assoc($internal_order_res);
		}
	}