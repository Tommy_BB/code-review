<?php
	require_once __DIR__ . "/ActiveDataModel.php";

	class ChannelOrderItemStatusLog extends ActiveDataModel {

		/**
		 * Returns the primary key value of the row of data
		 *
		 *
		 * @return int
		 */
		public function getPk() {
			return $this->ChannelOrderItemStatusLogID;
		}

		/**
		 * @return mixed
		 */
		public function setPk( $pk ) {
			$this->ChannelOrderItemStatusLogID = $pk;
		}

		/**
		 * Stores all possible attributes for a model
		 *
		 *
		 * @return mixed
		 */
		public function attributeNames() {
			return array(
				'ChannelOrderItemStatusLogID',
			    'ChannelOrderItemID',
			    'ChannelOrderItemStatusID',
			    'ChannelOrderItemStatusTS',
			);
		}

		/**
		 * @return string
		 */
		protected function tableName() {
			return __CLASS__;
		}
	}