<?php

	/**
	 * Simple model abstraction to allow for the mapping of db data
	 *
	 *
	 *         Class Model
	 */
	abstract class Model {
		/** @var array $errors Stores all errors for the model */
		protected $errors;

		/** @var array $_attributes */
		private $_attributes;

		public function __construct() {
			// Preset all available attributes with null values
			foreach ($this->attributeNames() as $name) {
				$this->_attributes[ $name ] = null;
			}
		}

		/**
		 * Searches for a model attribute then returns to normal operation
		 *
		 *
		 *
		 * @param $attributeName
		 *
		 * @return null
		 */
		public function __get( $attributeName ) {
			if (isset($this->_attributes[ $attributeName ])) {
				return $this->_attributes[ $attributeName ];
			}

			try {
				return $this->$attributeName;
			} catch (Exception $e) {
				return null;
			}
		}

		/**
		 *
		 *
		 * @param $attributeName
		 * @param $value
		 */
		public function __set( $attributeName, $value ) {
			if (in_array($attributeName, $this->attributeNames())) {
				$this->_attributes[ $attributeName ] = $value;
			} else {
				$this->{$attributeName} = $value;
			}
		}

		/**
		 * Stores all possible attributes for a model
		 *
		 *
		 * @return mixed
		 */
		abstract public function attributeNames();

		/**
		 * @param $name
		 * @param $arguments
		 *
		 * @return null
		 */
		public function __call( $name, $arguments ) {
			if (strpos($name, 'get') !== false) {
				$name = str_replace('get', '', $name);

				return $this->$name;
			}

			if (strpos($name, 'set') !== false) {
				$name        = str_replace('set', '', $name);
				$this->$name = $arguments[0];
			}
		}

		/**
		 *
		 * @return mixed
		 */
		public function getAttributes() {
			return $this->_attributes;
		}

		/**
		 * Sets attributes for a model instance, checks to ensure the child expects the attribute by default
		 *
		 *
		 *
		 * @param    $attributeCollection
		 *
		 */
		public function setAttributes( $attributeCollection ) {
			foreach ($attributeCollection as $attributeName => $value) {
				if (!$this->isAttribute($attributeName)) {
					continue;
				}
				$this->_attributes[ $attributeName ] = $value;
			}
		}

		/**
		 * Ensures an attribute exists, used before massively assigning
		 *
		 *
		 *
		 * @param $attributeName
		 *
		 * @return bool
		 */
		protected function isAttribute( $attributeName ) {
			return in_array($attributeName, $this->attributeNames(), $strict = true);
		}

		/**
		 * Validates a model's attributes to see if a required attribute is missing
		 *
		 *
		 * @return bool
		 */
		public function validate() {
			foreach ($this->_attributes as $attributeName => $value) {
				if (in_array($attributeName, $this->requiredAttributes()) and !isset($value)) {
					return false;
				}
			}

			return true;
		}

		/**
		 * Used for required attribute validation
		 *
		 *
		 * @return array
		 */
		protected function requiredAttributes() {
			return array();
		}

		/**
		 * @param $message
		 */
		public function addError( $message ) {
			array_push($this->errors, $message);
		}

		/**
		 * @return array
		 */
		public function getErrors() {
			return $this->errors;
		}

		/**
		 * @return int
		 */
		public function hasErrors() {
			return (count($this->errors) > 0) ? true : false;
		}
	}
