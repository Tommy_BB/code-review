<?php
	require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "Model.php";
	require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../zmvc/ZRegistry.php";

	use ZMVC\ZRegistry;

	abstract class ActiveDataModel extends Model {
		const ERROR_INSERT_FAILED = 1;
		const ERROR_UPDATE_FAILED = 2;

		/**
		 * @var integer $_id Pk value for the current row of data
		 */
		private $_id;

		/**
		 * @var array $_metaData
		 */
		private $_metaData;

		public function __construct() {
			parent::__construct();

			$this->setDBConnection();
			$this->setColumnDefaults();
		}

		protected function setDBConnection() {
			$registryDBConnection = ZRegistry::getInstance()->get('db');
			if (empty($registryDBConnection)) {
				/** @var $db_host */
				/** @var $db_user */
				/** @var $db_pass */
				include __DIR__ . "/../../../../classes/db.php";
				$db_cnx = mysqli_connect($db_host, $db_user, $db_pass);
				mysqli_select_db($db_cnx, "zshops");

				ZRegistry::getInstance()->add('db', $db_cnx);
			}
		}

		/**
		 *
		 */
		protected function setColumnDefaults() {
			$modelMetaDataAccessor = get_class($this) . 'MetaData';

			$modelMetaData = ZRegistry::getInstance()->get($modelMetaDataAccessor);
			if (empty($modelMetaData)) {
				ZRegistry::getInstance()->add($modelMetaDataAccessor, $this->getColumnMetaData());
			}

			foreach (ZRegistry::getInstance()->get($modelMetaDataAccessor) as $columnName => $metaData) {
				if ($this->getIsNewRecord()) {
					$this->$columnName = $metaData['column_default'];
				}
			}
		}

		/**
		 * Returns the table's column meta data
		 *
		 *
		 * @return array
		 */
		protected function getColumnMetaData() {
			if (empty($this->_metaData)) {
				$metaQryTmpl = "SELECT column_name, ordinal_position, data_type, column_type, column_key, column_default FROM information_schema.columns WHERE table_name = '{table}'";
				$metaQry     = strtr(
					$metaQryTmpl, array(
						'{table}' => $this->tableName(),
					)
				);
				$metaResult  = mysqli_query($this->getDBConnection(), $metaQry);
				$metaData    = array();
				while ($metaResultData = mysqli_fetch_array($metaResult)) {
					$columnName = $metaResultData['column_name'];
					unset($metaResultData['column_name']);

					if (strpos($metaResultData['column_default'], 'CURRENT') !== false) {
						$metaResultData['column_default'] = '0000-00-00 00:00:00';
					}

					$metaData[ $columnName ] = $metaResultData;
				}

				$this->_metaData = $metaData;
			}

			return $this->_metaData;
		}

		/**
		 * @return string
		 */
		abstract protected function tableName();

		protected function getDBConnection() {
			return ZRegistry::getInstance()->get('db');
		}

		/**
		 * @return bool
		 */
		public function getIsNewRecord() {
			$pkValue = $this->getPk();

			return empty($pkValue);
		}

		/**
		 * Returns the primary key value of the row of data
		 *
		 *
		 * @return int
		 */
		abstract public function getPk();

		/**
		 * Either inserts or updates the row of data, able to preform some before and after 'save' operations
		 *
		 *
		 */
		public function save( $validate = true ) {

			$this->beforeSave();
			if ((false === $validate or $this->validate())) {
				$isSaveSuccessful = ($this->getIsNewRecord()) ? $this->insert() : $this->update();
			}
			$this->afterSave();


			return $isSaveSuccessful;
		}

		/**
		 * Process additional logic before the 'save' event
		 *
		 *
		 */
		protected function beforeSave() {

		}

		/**
		 *
		 * @return bool|mysqli_result
		 */
		private function insert() {
			$insertQryTmpl = 'INSERT INTO {table} ({attributes}) VALUES ({values})';

			foreach ($this->getAttributes() as $attributeName => $value) {
				$columnMetaData = $this->_metaData[$attributeName];
				if ((is_null($value) or $columnMetaData['column_default'] == '0000-00-00 00:00:00') and $columnMetaData['column_type'] == 'timestamp') {
					continue;
				}

				$attributeList .= "{$attributeName},";

				$value = addslashes($value);
				$valueList .= "'{$value}',";
			}

			$attributeList = substr($attributeList, 0, -1);
			$valueList = substr($valueList, 0, -1);

			$insertQry = strtr(
				$insertQryTmpl, array(
					'{table}'      => $this->tableName(),
					'{attributes}' => $attributeList,
					'{values}'     => $valueList,
				)
			);

			$isInsertSuccessful = mysqli_query($this->getDBConnection(), $insertQry);
			if ($isInsertSuccessful) {
				$this->setPk(mysqli_insert_id($this->getDBConnection()));
			} else {
				$this->addError(
					'Failed to insert record into {table}. Message: {message}.', array(
						'{table}'   => $this->tableName(),
						'{message}' => mysqli_error($this->getDBConnection()),
					)
				);
			}

			return $isInsertSuccessful;
		}

		/**
		 * @return mixed
		 */
		abstract public function setPk( $pk );

		/**
		 *
		 * @return bool|mysqli_result
		 */
		private function update() {
			$updateQryTmpl = 'UPDATE {table} SET {attributes} WHERE {primaryKey} = {id}';
			$attributeList = "";
			foreach ($this->getAttributes() as $attributeName => $value) {
				$value = addslashes($value);
				$attributeList .= "{$attributeName} = '{$value}',";
			}
			$updateQry = strtr(
				$updateQryTmpl, array(
					'{table}'      => $this->tableName(),
					'{attributes}' => substr($attributeList, 0, - 1),
					'{id}'         => $this->getPk(),
					'{primaryKey}' => $this->getPrimaryKey(),
				)
			);

			$isUpdateSuccessful = mysqli_query($this->getDBConnection(), $updateQry);
			if (!$isUpdateSuccessful) {
				$this->addError(
					'Failed to update record from {table}. Message: {message}.', array(
						'{table}'   => $this->tableName(),
						'{message}' => mysqli_error($this->getDBConnection()),
					)
				);
			}

			return $isUpdateSuccessful;
		}

		/**
		 * Returns the primary key name of this model
		 *
		 *
		 * @return string
		 */
		protected function getPrimaryKey() {
			$tableMetaData = $this->getColumnMetaData();
			$primaryKey    = "";
			foreach ($tableMetaData as $columnName => $columnMetaData) {
				if ($columnMetaData['column_key'] == 'PRI') {
					$primaryKey = $columnName;
				}
			}

			return $primaryKey;
		}

		/**
		 * Process additional logic after the 'save' event
		 *
		 *
		 */
		protected function afterSave() {

		}

		/**
		 * @param $pk
		 *
		 * @return array|null
		 */
		protected function setAttributesByPk( $pk ) {
			if (!$this->getIsNewRecord()) {
				$selectQry = 'SELECT * FROM {table} WHERE {primaryKey} = {id} LIMIT 1';
				$selectQry = strtr(
					$selectQry, array(
						'{primaryKey}' => $this->getPrimaryKey(),
						'{id}'         => $pk,
						'{table}'      => $this->tableName(),
					)
				);

				$result = mysqli_query($this->getDBConnection(), $selectQry);
				if ($result) {
					$data = mysqli_fetch_assoc($result);
					$this->setAttributes($data);

					return true;
				}
			}

			return false;
		}
	}

