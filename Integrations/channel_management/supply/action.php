<?php
//                        .-.
//                        : :        
// .--. .-..-..---. .---. : :  .-..-.
//`._-.': :; :: .; `: .; `: :_ : :; :
//`.__.'`.__.': ._.': ._.'`.__;`._. ;
//            : :   : :         .-. :
//            :_;   :_;         `._.'

	error_reporting(E_ALL);
	require_once(__DIR__ . "/../../inventory/zindigo_inventory_manager.php");
	require_once(__DIR__ . "/../abstract_channel_action.php");
	require_once(__DIR__ . "/classes/Supply_Channel_Order_Manager.php");

	class Supply_Channel_Action extends Abstract_Channel_Action {

		public function pull() {
			// 1. LOAD PARSER
			//
			$this->prepare_channel_parser();

			// 2. FETCH FEED
			//
			try {
				$file_content = $this->fetch_feed($this->_channel_config);
			} catch (Exception $e) {
				die("No data to parse for feed type: {$this->_feed_type}.\n");
			}

			// 3. PROCEESS FEED
			//
			$this->process_feed($file_content);
		}

		/**
		 * Process the content of a feed pulled from a channel
		 *
		 * @param type $feed_type
		 * @param type $raw_feed_data
		 */

		public function process_feed( $raw_feed_data ) {
			// 3 PARSE AND PROCESS
			//
			$parsed_data = $this->parse_feed($raw_feed_data);
		}

		public function push( $param1 ) {

			if (!isset($this->_channel_config['vendors'])) {
				throw new Exception("No vendors specified in channel config file");
			}
			// LOAD FEED FORMATTER
			// try to load the feed formatter for the channel
			//
			$channel_class_prefix        = ucfirst(strtolower($this->_channel_name));
			$channel_feed_formatter      = $channel_class_prefix . '_Feed_Formatter';
			$channel_feed_formatter_path = __DIR__ . "/{$this->_channel_name}/feed_formatter.php";

			if (file_exists($channel_feed_formatter_path)) {
				include $channel_feed_formatter_path;
			} else {
				print "channel {$this->_channel_name} feed formatter doesn't exist\n";
				throw new Exception("channel {$this->_channel_name} feed formatter doesn't exist");
			}

			// GET DATA
			//
			$payloads = array();

			foreach ($this->_channel_config['vendors'] as $vendor_id => $vendor_config) {
				$vendor_info = Zindigo_Inventory_Manager::get_vendor_info($vendor_id);
				if (!isset($vendor_info)) {
					throw new Exception("unknown vendor: $vendor_id");
				}

				switch (strtolower($this->_feed_type)) {
					case 'pick_ticket_request':
						$payloads = array_merge($payloads, Supply_Channel_Order_Manager::get_orders_for_packing($vendor_id));
						break;
					case 'rma_request':
						$payloads = array_merge($payloads, Supply_Channel_Order_Manager::get_orders_for_returns($vendor_id));
						break;
                    case 'catalog':
                        $payloads = array_merge($payloads, Supply_Channel_Order_Manager::get_inventory_payload($vendor_id));
                        break;
					default:
						print "Cannot push unknown feed type: $this->_feed_type\n";
						print "push [pick_ticket_request, rma_request]\n\n";
						exit;
						break;
				}
			}

			if (empty($payloads)) {
				print "No payloads to send for feed type, {$this->_feed_type}\n";
				exit;
			}

			$this->create_channel_interaction_record($this->_feed_type);

			foreach ($payloads as $payload) {
				// GENERATE FEED
				//
				$file_prefix    = (isset($this->_channel_config['file_name_prefix'][ $this->_feed_type ])) ? $this->_channel_config['file_name_prefix'][ $this->_feed_type ] : null;
				$generated_file = $channel_feed_formatter::render($this->_feed_type, $payload, $file_prefix);

				// UPLOAD FEED
				//
				$remote_dir      = $this->_channel_config['directories']['outgoing'];
				$remote_filename = basename($generated_file);

				if ($param1 == 'debug') {
					print "\nDEBUG MODE\n";
					print "exiting before transmitting to {$remote_dir}{$remote_filename}\n";
					print "less $generated_file\n";
					die();
				}

                $temp_order_data = $payload->getInternalOrderData();

                if(isset($this->_channel_config['send_pdfs']) && $this->_channel_config['send_pdfs'] !== false)
                {
                    if ($this->_file_transporter->put_file((__DIR__ ."/../../../../documents/".$temp_order_data['OrderNum']."_Packing.pdf"), "/attachments/", "__".$temp_order_data['OrderNum']."_Packing.pdf", $this->_channel_config)) {
                        print "\nSENT Packing Label\n";
                    }

                    if ($this->_file_transporter->put_file((__DIR__ ."/../../../../documents/".$temp_order_data['OrderNum']."_Fedex.pdf"), "/attachments/", "__".$temp_order_data['OrderNum']."_Fedex.pdf", $this->_channel_config)) {
                        print "\nSENT Fedex Label\n";
                    }
                }


				if ($this->_file_transporter->put_file($generated_file, $remote_dir, $remote_filename, $this->_channel_config)) {
					print "\nFEED SENT\n";
				} else {
					die("CRITICAL ERROR: sftp_put operation failed to transmit feed to remote server\n");
				}


				// LOG INTERACTION IN DB
				//
				$this->update_channel_interaction_record(file_get_contents($generated_file), $remote_dir . $remote_filename, "");

				// perform after push busines logic
				//
				$this->after_push($this->_feed_type, $payload);
			}
		}

		/**
		 * Trigger business logic that should only happen after successfully pushing a feed
		 *
		 * @param type $feed_type
		 * @param type $payload
		 */

		private function after_push( $feed_type, $payload ) {
			// POST PUSH ACTIONS
			//
			switch (strtolower($feed_type)) {
				case 'inventory':
				case 'product':
				case 'price':
				case 'order_status':
				case 'refund_requests':
				default:
					break;
			}
		}

		protected function getChannelType() {
			return 'supply';
		}
	}
