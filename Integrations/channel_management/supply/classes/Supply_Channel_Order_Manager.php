<?php
	require_once __DIR__ . "/../../classes/ChannelOrder.php";
	require_once __DIR__ . "/../../classes/ChannelOrderItem.php";

	class Supply_Channel_Order_Manager {

		/**
		 * @param $vendor_id
		 */
		public static function get_orders_for_returns( $vendor_id ) {
			global $ulk;

			$vendor_id = addslashes($vendor_id);

			$sql = <<<SQL
			SELECT
				vendor.Name AS VendorName,
				ord.OrderNum AS OrderNumber,
				ord.OrderDate AS OrderDate,
				ret.RMA AS RMA,
				inventory.UPC AS UPC,
				return_detail.ReturnedQty AS Quantity,
                coi.ChannelOrderItemID As ChannelOrderItemID
			FROM Returns ret
			INNER JOIN ReturnDetails return_detail ON ret.ID = return_detail.ReturnID
			INNER JOIN OrderDetails order_detail ON return_detail.LineID = order_detail.ID
			INNER JOIN Orders ord ON ord.ID = order_detail.OrderID
			INNER JOIN Products product ON product.ID = order_detail.ProductID
			INNER JOIN Inventory inventory ON inventory.ProductID = product.ID AND inventory.SKU = order_detail.SKU
			INNER JOIN Vendors vendor ON vendor.ID = product.VendorID
			INNER JOIN OrderChannelOrder oco ON oco.OrderID = ord.ID
			INNER JOIN ChannelOrder co ON co.ChannelOrderID = oco.ChannelOrderID
			INNER JOIN ChannelOrderItem coi ON coi.ChannelOrderID = co.ChannelOrderID
			WHERE PendingRefund = 1 AND ReturnsAccepted = 0 AND vendor.ID = '$vendor_id' AND order_detail.ItemStatus NOT IN (2, 5, 6) AND coi.ChannelOrderItemReturnedInd <> 1
			GROUP BY return_detail.LineID
SQL;

			$returns = array();

			$return_order_res = mysqli_query($ulk->db_cnx, $sql);
			if ($return_order_res) {
				while ($return_order_data = mysqli_fetch_assoc($return_order_res)) {
					$return = $return_order_data;
					if (!isset($returns[$return['OrderNumber']])) {
						$returns[$return['OrderNumber']] = array(
							'VendorName' => $return['VendorName'],
						    'OrderNumber' => $return['OrderNumber'],
						    'OrderDate' => $return['OrderDate'],
						    'RMA' => $return['RMA'],
						    'Items' => array(),
						);
					}

					$item_details = array(
						'UPC' => $return['UPC'],
					    'Quantity' => $return['Quantity'],
                        'InternalItemID' => $return['ChannelOrderItemID'],
					);

					$returns[$return['OrderNumber']]['Items'][] = $item_details;
				}
			}

			return $returns;
		}

		/**
		 * Orders are marked as "packing" when the order is released via the admin interface
		 * @param $vendor_id
		 *
		 * @return array|bool
		 */
		public static function get_orders_for_packing( $vendor_id ) {
			global $ulk;

			/**
			 * Get all orders that have child items in a "packing" state and the parent order is in a "onhold" state
			 */

            /*$sql = <<<SQL
                        SELECT
                ord.*
            FROM
                ChannelOrder ord
            INNER JOIN ChannelOrderItem item ON item.ChannelOrderID = ord.ChannelOrderID
            INNER JOIN Vendors v ON v.ChannelCode = ord.ChannelCode
            WHERE
                item.ChannelOrderItemStatusID = 9
            AND ord.ChannelOrderStatusID = 6
            AND v.ID = {$vendor_id}
            GROUP BY
                ord.ChannelOrderID
SQL;*/

            /* edited this to only pull things that have been released. */
            $sql = <<<SQL
            SELECT
                ord.*
            FROM
                ChannelOrder ord
            INNER JOIN ChannelOrderItem item ON item.ChannelOrderID = ord.ChannelOrderID
            INNER JOIN Vendors v ON v.ChannelCode = ord.ChannelCode
			INNER JOIN OrderChannelOrder oco ON oco.ChannelOrderID = ord.ChannelOrderID
			INNER JOIN Orders o ON o.ID = oco.OrderID
            WHERE
                item.ChannelOrderItemStatusID = 9
            AND ord.ChannelOrderStatusID = 6
            AND v.ID = {$vendor_id}
			AND o.OrderStatus IN(3,5)
            GROUP BY
                o.ID
SQL;

			$packing_orders = array();

			$packing_order_res = mysqli_query($ulk->db_cnx, $sql);
			if ($packing_order_res) {

				while ($packing_order_data = mysqli_fetch_assoc($packing_order_res)) {
					$packing_order = self::map_order_to_model($packing_order_data);

					/**
					 * Get all of the child order items from the packing order
					 */
					$packing_order_id = $packing_order->getPk();
					$sql = <<<SQL
					SELECT * FROM ChannelOrderItem
					WHERE ChannelOrderID = '$packing_order_id'
SQL;

					$packing_order_item_res = mysqli_query($ulk->db_cnx, $sql);
					if (!$packing_order_res) {
						continue;
					}

					/**
					 * Assign the order items to the packing order
					 */
					$packing_order_items = array();
					while ($packing_order_item_data = mysqli_fetch_assoc($packing_order_item_res)) {
						$packing_order_item = self::map_order_item_to_model($packing_order_item_data);
						$packing_order_items[] = $packing_order_item;
					}
					$packing_order->setOrderItems($packing_order_items);

					$packing_orders[] = $packing_order;
				}
			}

			return $packing_orders;
		}

        public static function get_inventory_payload($vendorid) {
            global $ulk;
            $find_sql = <<<SQL
                SELECT
                    i.UPC
                FROM
                    StoresCategories sc
                JOIN StoresProducts sp ON sp.StoreCategoryID = sc.ID
                JOIN Inventory i ON i.ProductID = sp.ProductID
                JOIN Products p ON p.ID = sp.ProductID
                WHERE
                    p.VendorID = {$vendorid}
                AND i.UPC IS NOT NULL
                AND i.UPC <> ''
                GROUP BY
                    i.InventoryID
SQL;
            $channel_order_item_res = mysqli_query($ulk->db_cnx, $find_sql);
            $inventory_payload = array();
            if($channel_order_item_res)
            {
                while($row = mysqli_fetch_assoc($channel_order_item_res))
                {
                    $inventory_payload[] = $row['UPC'];
                }
            }

            return $payload = array($inventory_payload);
        }

		public static function map_order_to_model( $order_data ) {
			$order = new ChannelOrder();
			$order->setAttributes(array(
				'ChannelOrderID'              => $order_data['ChannelOrderID'],
				'ChannelCode'                 => $order_data['ChannelCode'],
				'ChannelOrderStatusID'        => $order_data['ChannelOrderStatusID'],
				'ExternalOrderID'             => $order_data['ExternalOrderID'],
				'ChannelOrderAcknowledgedInd' => $order_data['ChannelOrderAcknowledgedInd'],
				'ChannelOrderPlacedTS'        => $order_data['ChannelOrderPlacedTS'],
				'ChannelOrderTransmittedTS'   => $order_data['ChannelOrderTransmittedTS'],
			));

			return $order;
		}

		public static function map_order_item_to_model( $order_item_data ) {
			$order_item = new ChannelOrderItem();
			$order_item->setAttributes(array(
				'ChannelOrderItemID'          => $order_item_data['ChannelOrderItemID'],
				'ChannelOrderID'              => $order_item_data['ChannelOrderID'],
				'OrderItemID'                 => $order_item_data['OrderItemID'],
				'ExternalOrderItemID'         => $order_item_data['ExternalOrderItemID'],
				'ChannelOrderItemUPC'         => $order_item_data['ChannelOrderItemUPC'],
				'ChannelOrderItemSKU'         => $order_item_data['ChannelOrderItemSKU'],
				'ChannelOrderItemStatusID'    => $order_item_data['ChannelOrderItemStatusID'],
				'ChannelOrderItemShippedInd'  => $order_item_data['ChannelOrderItemShippedInd'],
				'ChannelOrderItemReturnedInd' => $order_item_data['ChannelOrderItemReturnedInd'],
				'ChannelOrderItemRefundedInd' => $order_item_data['ChannelOrderItemRefundedInd'],
			));

			return $order_item;
		}
	}