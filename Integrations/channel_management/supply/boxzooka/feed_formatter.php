<?php
    require_once __DIR__ . "/../../classes/ChannelOrderItem.php";
    require_once __DIR__ . "/../../classes/ChannelOrderItemStatusLog.php";

	class Boxzooka_Feed_Formatter {

		public static function render( $feed_type, $payload, $file_prefix = null ) {

			print "generating $feed_type";

			switch (strtolower($feed_type)) {
				default:
					throw new Exception("Undefined feed type: $feed_type");
			}

			$file_prefix      = (isset($file_prefix)) ? $file_prefix : $feed_type;
			$channel_temp_dir = "/tmp/boxzooka";
			$file_path        = "$channel_temp_dir.xml";

			if (!file_exists($channel_temp_dir)) {
				print "making channel tmp directory $channel_temp_dir\n";
				mkdir($channel_temp_dir, 0777);
			}

			$xml->formatOutput = true;
			if ($xml->save($file_path)) {
				return $file_path;
			}
		}


        private static function generate_product_feed( $products ) {
            $xml = new DOMDocument("1.0");
            $catalogRequest = $xml->createElement('CatalogRequest');
            $catalog_header = $xml->createElement('CustomerAccess');

            $header_node_data = array(
                'CustomerID' => 1234, //int
                'CustomerKey' => 'XXX' //text
            );

            foreach ($header_node_data as $tag_name => $tag_value) {
                $header_node = $xml->createElement($tag_name, $tag_value);
                $catalog_header->appendChild($header_node);
            }



            $break = 0;

            /*
             *
             * <?xml version=”1.0” encoding=”UTF-8”?>
  <CatalogRequest>
     <CustomerAccess>
       <CustomerID>1234567890</CustomerID>
       <CustomerKey>XXXXXXXXXXXXXXXXXXXXXXXXXXXX</CustomerKey>
     </CustomerAccess>
     <Version>1.5</Version>
     <Items>
       <Item>
          <Sku>111111111</Sku>
          <ItemName>Short Sleeve Crewe</ItemName>
          <DeclaredCustomsValue>34.99</DeclaredCustomsValue>
          <CurrencyCode>USD</CurrencyCode>
          <Category>Men/Shirts/Sport/Short Sleeve</Category>
          <Color>Green</Color>
          <Description>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed tempus
          diam non pede. Vivamus porttitor dui ac est fringilla porttitor. In luctus iaculis
          nunc. Praesent ac ligula. Cras rutrum cursus risus. Nunc tincidunt sapien dignissim
          massa. Nam facilisis. Mauris sit amet nisl a ante porta faucibus...</Description>
          <ShortDescription>Men’s Short Sleeve Crew T-Shirt</ShortDescription>
          <Materials>Cotton,Polyester</Materials>
          <Size>Large</Size>
          <BrandOrManufacturer>SportFit</BrandOrManufacturer>
          <UpcVendorBarcode>222222222222</UpcVendorBarcode>
          <CountryOfOrigin>US</CountryOfOrigin>
          <ItemUrl>http://www.domain.com/products/13345855/</ItemUrl>
          <ImageUrl>http://www.domain.com/products/13345855/image.jpg</ImageUrl>
          <Weight>0.43</Weight>
          <WeightUnit>LBS</WeightUnit>
          <DimWidth>6</DimWidth>
          <DimLength>6</DimLength>
          <DimHeight>6</DimHeight>
          <DimUnit>IN</DimUnit>
          <UCC>333333333333</UCC>
</Item>
            </Items>
            </CatalogRequest>

             */
        }

	}
