<?php

	return array(
		'debug_mode'       => false,
		'aggregate_stores' => true,
		'server'           => '',
		'username'         => '',
		'password'         => '',
		'rsa_key_path'     => '',
		'protocol'         => 'ftp',
		'directories'      => array(
			'incoming' => '/ecommerce',
			'outgoing' => '/ecommerce',
		),
		'file_name_ext'    => '.xml',
		'file_name_prefix' => array(
		),
		'archive_dir'      => array(
		),
		'vendors'          => array(
			101 => array(),
		),
	);