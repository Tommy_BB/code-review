<?php
    require_once __DIR__ . "/../../classes/ChannelOrderItem.php";
    require_once __DIR__ . "/../../classes/ChannelOrderItemStatusLog.php";

	class Savino_Feed_Formatter {

		/**
		 * Render a bluefly xml feed
		 *
		 * @param type $feed_type
		 * @param type $vendor_info
		 * @param type $payload
		 *
		 * @return type
		 * @throws Exception
		 */
		public static function render( $feed_type, $payload, $file_prefix = null ) {

			print "generating $feed_type";

			$document_guid = static::generate_document_guid($feed_type, $payload, "{$file_prefix}");

			/** @var DomDocument $xml */
			switch (strtolower($feed_type)) {
				case 'pick_ticket_request':
					$xml = static::generate_pick_ticket_feed($payload);
					break;
				case 'rma_request':
					$xml = static::generate_rma_feed($payload);
					break;
                case 'catalog':
                    $xml = static::generate_catalog_feed($payload);
                    break;
				default:
					throw new Exception("Undefined feed type: $feed_type");
			}

			$file_prefix      = (isset($file_prefix)) ? $file_prefix : $feed_type;
			$channel_temp_dir = "/tmp/savino";
			$file_path        = "$channel_temp_dir/$document_guid.xml";

			if (!file_exists($channel_temp_dir)) {
				print "making channel tmp directory $channel_temp_dir\n";
				mkdir($channel_temp_dir, 0777);
			}

			$xml->formatOutput = true;
			if ($xml->save($file_path)) {
				return $file_path;
			}
		}

        public static function SendEmailError($therror, $to, $from, $subject, $cc = "", $bcc = "")
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'api:key-3wt7-hoklyp34o8856d8y9zzwns3px17');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v2/zindigo.com/messages');

            $mailarray['from'] = strip_tags($from);
            $mailarray['to'] = strip_tags($to);
            if($cc <> "")   $mailarray['cc'] = strip_tags($cc);
            if($bcc <> "")   $mailarray['bcc'] = strip_tags($bcc);
            $mailarray['subject'] = strip_tags($subject);
            $mailarray['html'] = $therror;
            curl_setopt($ch, CURLOPT_POSTFIELDS, $mailarray);

            $result = curl_exec($ch);
            error_log("SENDMAIL: ".$result);
            curl_close($ch);
        }

        public static function generate_catalog_feed( $inventory_payload ) {
            global $ulk;

            $xml = new DOMDocument("1.0");
            $catalog = $xml->createElement('catalog');
            $catalog_header = $xml->createElement('catalog_header');

            // Header node contents
            $header_node_data = array(
                'company' => $return['VendorName'],
                'year' => '2014',
                'season' => 'WS14 D5',
                'total' => count($inventory),
            );

            foreach ($header_node_data as $tag_name => $tag_value) {
                $header_node = $xml->createElement($tag_name, $tag_value);
                $catalog_header->appendChild($header_node);
            }

            $catalog_body = $xml->createElement('catalog_body');

            foreach($inventory_payload as $curinv)
            {
                $catalog_sku = $xml->createElement('sku');
                $catalog_upc = $xml->createElement('upc',$curinv);

                $catalog_sku->appendChild($catalog_upc);
                $catalog_body->appendChild($catalog_sku);
            }

            $catalog->appendChild($catalog_header);
            $catalog->appendChild($catalog_body);
            $xml->appendChild($catalog);
            return $xml;

        }

		public static function generate_rma_feed( $return ) {
			global $ulk;

			$xml = new DOMDocument("1.0");
			$receiving_advice_node = $xml->createElement('receiving_advice');

			// Build the header node
			$receiving_advice_header_node = $xml->createElement('receiving_advice_header');

			// Header node contents
			$header_node_data = array(
				'company' => $return['VendorName'],
			    'warehouse' => 'FDSNJ',
			    'master_invoice_number' => $return['OrderNumber'],
			    'master_invoice_date' => $return['OrderDate'],
			    'ra_number' => $return['RMA'],
			    'currency' => 'USD',
			    'total_pieces' => count($return['Items']),
			);
			foreach ($header_node_data as $tag_name => $tag_value) {
				$header_node = $xml->createElement($tag_name, $tag_value);
				$receiving_advice_header_node->appendChild($header_node);
			}

			// Build the body node
			$receiving_advice_body_node = $xml->createElement('receiving_advice_body');

			// Body node contents
			$pieces_node = $xml->createElement('pieces');


			foreach ($return['Items'] as $return_item) {

                /* UPDATE RMA SO WE CAN RELAY IT BACK TO VENDOR IF ACCEPTED */
                $find_sql = <<<SQL
				SELECT coi.* FROM
				ChannelOrder co JOIN
				ChannelOrderItem coi ON coi.ChannelOrderID = co.ChannelOrderID
				WHERE coi.ChannelOrderItemID = '{$return_item[InternalItemID]}'
SQL;
                $channel_order_item_res = mysqli_query($ulk->db_cnx, $find_sql);
                if ($channel_order_item_res) {
                    $channel_order_item_details = mysqli_fetch_assoc($channel_order_item_res);
                    $channel_order_item = new ChannelOrderItem();
                    $channel_order_item->setAttributes($channel_order_item_details);
                    $channel_order_item->setChannelOrderItemReturnedInd(1);
                    $channel_order_item->save();
                }

				$sku_node = $xml->createElement('sku');

				$return_item_node_data = array(
					'barcode_upc' => $return_item['UPC'],
				    'qty' => $return_item['Quantity'],
				    'slot' => 'ecommerce',
				);
				foreach ($return_item_node_data as $tag_name => $tag_value) {
					$item_node = $xml->createElement($tag_name, $tag_value);
					$sku_node->appendChild($item_node);
				}

				$pieces_node->appendChild($sku_node);
			}
			$receiving_advice_body_node->appendChild($pieces_node);

			// Construct the rest of the return document
			$receiving_advice_header_node->appendChild($receiving_advice_body_node);
			$receiving_advice_node->appendChild($receiving_advice_header_node);

			$xml->appendChild($receiving_advice_node);

			return $xml;
		}

		/**
		 * Generate the packing ticket xml feed
		 *
		 * @param $packing_orders
		 */
		public static function generate_pick_ticket_feed( ChannelOrder $packing_order ) {
			global $ulk;

			$internal_order_data = $packing_order->getInternalOrderData();

            $vendor_order_sql = <<<EQOSQL
            SELECT
                SUM(od.Price)AS VendorTotal
            FROM
                OrderDetails od
            JOIN Products p ON p.ID = od.ProductID
            WHERE
                OrderID = {$internal_order_data['ID']}
            AND p.VendorID = 53
EQOSQL;

            $vendor_order_price = mysqli_query($ulk->db_cnx,$vendor_order_sql);
            $vendor_order_price = mysqli_fetch_assoc($vendor_order_price);




			// Get the order details
			$order_details = array();
			$result        = $ulk->getOrderDetails($internal_order_data['ID']);
			while ($order_detail_data = mysqli_fetch_assoc($result)) {
				$order_details[] = $order_detail_data;
			}

			$general_order_details = current($order_details);

			$xml             = new DOMDocument("1.0");
			$xml_pick_ticket = $xml->createElement('pickticket');

			// Process the header node
			$xml_pick_ticket_header  = $xml->createElement('pickticket_header');
			$pick_ticket_header_data = array(
				'company'           => $general_order_details['Name'],
				'warehouse'         => 'FDSNJ',
				'picking_reference' => $packing_order->getExternalOrderID(),
				'total_pieces'      => count($packing_order->getOrderItems()),
				'pick_type'         => 'TO_SHIP',
				'shipping_via'      => $general_order_details['ShipMethod'],
				'invoice_amount'    => $vendor_order_price['VendorTotal'],
			);
			foreach ($pick_ticket_header_data as $name => $value) {
				$pick_ticket_header_node = $xml->createElement($name, $value);
				$xml_pick_ticket_header->appendChild($pick_ticket_header_node);
			}

			// Process the ship_to node
			$xml_pick_ticket_ship_to  = $xml->createElement('ship_to');
			$pick_ticket_ship_to_data = array(
				'client_code'  => $internal_order_data['UserID'],
				'contact_name' => $internal_order_data['Ship_FName'] . ' ' . $internal_order_data['Ship_LName'],
				'name'         => $internal_order_data['Ship_FName'] . ' ' . $internal_order_data['Ship_LName'],
				'address'      => $internal_order_data['Ship_Address1'],
				'city'         => $internal_order_data['Ship_City'],
				'country'      => $internal_order_data['Ship_Country'],
				'state'        => $internal_order_data['Ship_State'],
				'zip'          => $internal_order_data['Ship_Zip'],
			);
			foreach ($pick_ticket_ship_to_data as $name => $value) {
				$pick_ticket_ship_to_node = $xml->createElement($name, $value);
				$xml_pick_ticket_ship_to->appendChild($pick_ticket_ship_to_node);
			}
			$xml_pick_ticket_header->appendChild($xml_pick_ticket_ship_to);

			// Process the body node
			$pick_ticket_body = $xml->createElement('pickticket_body');
			$pieces_node      = $xml->createElement('pieces');
			foreach ($order_details as $order_detail) {
                if($order_detail['VendorID'] != 53)
                    continue;
                //notify gina this order is order is messed up then skip also we need to pass in vendor id for matching as this was never ment for zindigo multi orders....
                if($order_detail['UPC'] == null)
                {
                    static::SendEmailError("NO UPC FOR ORDER ".$internal_order_data['ID'], "gina@zindigo.com", "emails@zindigo.com", "Error No Upc For Savino","tommy@zindigo.com");
                    continue;
                }

				$sku_node  = $xml->createElement('sku');
				$upc_node  = $xml->createElement('barcode_upc', $order_detail['UPC']);
				$qty_node  = $xml->createElement('qty', $order_detail['ProductQuantity']);
				$slot_node = $xml->createElement('slot', 'ecommerce');

				$sku_node->appendChild($upc_node);
				$sku_node->appendChild($qty_node);
				$sku_node->appendChild($slot_node);
				$pieces_node->appendChild($sku_node);
			}
			$pick_ticket_body->appendChild($pieces_node);
			$xml_pick_ticket_header->appendChild($pick_ticket_body);

			// Append the header to the xml document
			$xml_pick_ticket->appendChild($xml_pick_ticket_header);
			$xml->appendChild($xml_pick_ticket);

			// Mark the order as "pending"
			$packing_order->ChannelOrderStatusID = 5;
			$packing_order->save();

			return $xml;
		}

		public static function generate_document_guid( $feed_type, $payload, $prefix ) {
			$payload_pk = "";
			switch ($feed_type) {
				case 'rma_request':
					$payload_pk = $payload['RMA'];
					break;
				case 'pick_ticket_request':
					$payload_pk = $payload->getPk();
					break;
			}


            if($feed_type == "catalog")
                return $prefix . '_' . date("m_d_Y") . '_id';

			if ($payload_pk) {
				$prefix .= "_{$payload_pk}";
			}



			return $prefix . '_' . date("Y_m_d_H:i");
		}
	}
