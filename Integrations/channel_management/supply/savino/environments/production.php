<?php

	return array(
		'debug_mode'       => false,
		'aggregate_stores' => true,
		'server'           => 'ftp.sdb.it',
		'username'         => 'edi_fjb',
		'password'         => 'Owgaso',
		'rsa_key_path'     => '',
		'protocol'         => 'ftp',
		'directories'      => array(
			'incoming' => '/ecommerce',
			'outgoing' => '/ecommerce',
		),
		'file_name_ext'    => '.xml',
		'file_name_prefix' => array(
			'pick_ticket_receipt' => 'shipment_confirmation',
			'pick_ticket_request' => 'pick_ticket_request',
			'inventory'           => 'inventory',
			'rma_request'         => 'receipt_advice',
			'rma_receipt'         => 'receipt_confirmation',
            'catalog'             => 'catalog',
		),
		'archive_dir'      => array(
			'rma_receipt'         => '/ecommerce/archived/rma_receipts/',
			'pick_ticket_receipt' => '/ecommerce/archived/pick_ticket_receipts/',
			'inventory'           => '/ecommerce/archived/inventory/',
		),

		// list of vendor ids that this channel services
		// 53 = Raoul
		'vendors'          => array(
			53 => array(),
		),
	);