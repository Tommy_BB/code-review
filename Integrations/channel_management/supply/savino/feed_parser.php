<?php
	require_once __DIR__ . "/../../classes/ChannelOrderItem.php";
	require_once __DIR__ . "/../../classes/ChannelOrderItemStatusLog.php";

	class Savino_Feed_Parser {

		public static function parse( $feed_type, $raw_xml_string ) {
			print "parsing $feed_type\n";
			$dom = simplexml_load_string($raw_xml_string);

			switch (strtolower($feed_type)) {
				case 'rma_receipt':
					$parse_and_process_result = self::parse_and_process_rma_receipt($dom);
					break;

				case 'inventory':
					$parse_and_process_result = self::parse_and_process_inventory($dom);
					break;

				case 'pick_ticket_receipt':
					$parse_and_process_result = self::parse_and_process_pick_tickets($dom);
					break;

				default:
					print "feed type $feed_type doesn't exist\n";
					print "pull savino [inventory|rma_receipt|pick_ticket_receipt]\n\n";
					break;
			}

			return $parse_and_process_result;
		}

        public static function SendEmailError($thebody, $to, $from, $subject, $cc = "", $bcc = "")
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'api:key-3wt7-hoklyp34o8856d8y9zzwns3px17');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v2/zindigo.com/messages');

            $mailarray['from'] = strip_tags($from);
            $mailarray['to'] = strip_tags($to);
            if($cc <> "")   $mailarray['cc'] = strip_tags($cc);
            if($bcc <> "")   $mailarray['bcc'] = strip_tags($bcc);
            $mailarray['subject'] = strip_tags($subject);
            $mailarray['html'] = $thebody;
            curl_setopt($ch, CURLOPT_POSTFIELDS, $mailarray);

            $result = curl_exec($ch);
            error_log("SENDMAIL: ".$result);
            curl_close($ch);
        }

		/**
		 * @param $dom
		 *
		 * @return bool
		 */
		public static function parse_and_process_and_process_rma_receipt( $dom ) {
			global $ulk;

			$receiving_confirmation_header_node = $dom->receiving_confirmation_header;
			$receiving_confirmation_body_node = $receiving_confirmation_header_node->receiving_confirmation_body;

			// Data received from the header node
			$ra_number = (string) $receiving_confirmation_header_node->ra_number;

			foreach ($receiving_confirmation_body_node->pieces->sku as $sku_node) {
				$item_upc = (string) $sku_node->barcode_upc;
				$item_qty_returned = (string) $sku_node->over_under_qty;

				// Return accepted status indicator
				//
				$return_accepted = ($item_qty_returned == 0) ? 1 : 0;

				// Update the return status
				//
				$sql = <<<SQL
				UPDATE ReturnDetails SET ReturnsAccepted = '$return_accepted', PendingRefund = 0 WHERE ID IN (
					SELECT return_detail.ID FROM Returns ret
					INNER JOIN ReturnDetails return_detail ON return_detail.ReturnID = ret.ID
					INNER JOIN OrderDetails order_detail ON order_detail.ID = return_detail.LineID
					INNER JOIN Products product ON product.ID = order_detail.ProductID
					INNER JOIN Inventory inventory ON inventory.ProductID = product.ID
					WHERE RMA = '$ra_number' AND inventory.UPC = '$item_upc'
					LIMIT 1
				)
SQL;
				mysqli_query($ulk->db_cnx, $sql);
				print "updating status: \t RMA: {$ra_number} \t UPC: {$item_upc} \t ReturnsAccepted: {$return_accepted}\n";

				// Return status 6 == Accepted, 5 = Rejected
				//
				$item_return_status = ($item_qty_returned == 0) ? 6 : 5;

				// Update the order detail status
				//
				$sql = <<<SQL
				UPDATE OrderDetails SET ItemStatus = '$item_return_status' WHERE ID IN (
					SELECT order_detail.ID FROM Returns ret
					INNER JOIN ReturnDetails return_detail ON return_detail.ReturnID = ret.ID
					INNER JOIN OrderDetails order_detail ON order_detail.ID = return_detail.LineID
					INNER JOIN Products product ON product.ID = order_detail.ProductID
					INNER JOIN Inventory inventory ON inventory.ProductID = product.ID
					WHERE RMA = '$ra_number' AND inventory.UPC = '$item_upc'
					LIMIT 1
				)
SQL;
				mysqli_query($ulk->db_cnx, $sql);
				print "updating status: \t RMA: {$ra_number} \t UPC: {$item_upc} \t ItemStatus: {$item_return_status}\n";
			}

			return true;
		}

		/**
		 * @param $dom
		 *
		 * @return bool
		 */
		public static function parse_and_process_inventory( $dom ) {
			global $ulk;

			foreach ($dom->product as $curprod) {
				// Update the inventory by the UPC
				$upc = (string) $curprod->upc;
                $qty = (string) $curprod->qty;
                print "UPDATING ".$upc." TO QTY". $qty."\n";
				$ulk->updateInventoryByUPC($qty, mysqli_escape_string($ulk->db_cnx, $upc));
			}


            //lets do a out of inventory check

            $inventorySqlCheck = <<<EQOSQL
                SELECT
                    SUM(i.Qty) AS TotalProductQty,
                    p.Name AS ProductName,
                    p.VendorSKUBase AS ProdSKU,
                    sc.Name AS CategoryName,
                    i.*
                FROM
                    StoresCategories sc
                JOIN StoresProducts sp ON sp.StoreCategoryID = sc.ID
                JOIN Inventory i ON i.ProductID = sp.ProductID
                JOIN Products p ON p.ID = sp.ProductID
                WHERE
                    sc.StoreID = 114
                GROUP BY i.ProductID
                HAVING TotalProductQty <= 0
                ORDER BY
                    sc.ID ASC
EQOSQL;

            $invResult = mysqli_query($ulk->db_cnx,$inventorySqlCheck);
            if($invResult && mysqli_num_rows($invResult) > 0)
            {
                $outLog ="\n".str_pad("NAME",40)."\t"
                    .str_pad("STYLE",20)."\n";
                $outLog .= str_repeat("-",130)."\n";
                while($invRows = mysqli_fetch_assoc($invResult))
                {
                    $outLog .="\n".str_pad($invRows['ProductName'],40)."\t"
                        .str_pad($invRows['ProdSKU'],20)."\n";
                }
                $message = "<html><body>"
                    . "<h1>Out Of Inventory Log</h1>"
                    . "<pre>{$outLog}</pre></html>";
                static::SendEmailError($message, "gina@zindigo.com", "inventory@zindigo.com", "Raoul Out Of Inventory Report", "tommy@zindigo.com");
            }

			return true;
		}

		/**
		 * @param $dom
		 *
		 * @return bool
		 */
		public static function parse_and_process_pick_tickets( $dom ) {
			global $ulk;
			require_once __DIR__ . '/../../../../../classes/orders.php';
			require_once __DIR__ . '/../../../../../classes/email.php';

			print "Parsing pick ticket with invoice number: {$dom->confirm_shipping_header->invoice_number}\n";

			$invoice_number = $dom->confirm_shipping_header->invoice_number;

			// Retrieve the channel order
			$sql = <<<SQL
			SELECT ChannelOrderID
			FROM ChannelOrder WHERE ExternalOrderID = '$invoice_number'
			LIMIT 1
SQL;
			$channel_order_res = mysqli_query($ulk->db_cnx, $sql);
			if (!$channel_order_res) {
				print "Could not locate parent channel order for invoice number: {$invoice_number}\n";

				return false;
			}

			$channel_order_details = mysqli_fetch_assoc($channel_order_res);
			$channel_order_id = $channel_order_details['ChannelOrderID'];

			if (empty($dom->confirm_shipping_header->confirm_shipping_body->pieces)) {
				print "Nothing to parse for invoice number: {$dom->confirm_shipping_header->invoice_number}\n";

				return false;
			}

			$items_to_ship = $channel_order_item_find_sqls = array();
			foreach ($dom->confirm_shipping_header->confirm_shipping_body->pieces->sku as $piece) {
				$upc = (string) $piece->barcode_upc;
				//this qty is if they are short a qty then requested
				$qty = (integer) $piece->short_qty;
				$tracking_number = (string) $piece->tracking_number;

				if (empty($tracking_number)) {
					print "No tracking number found for upc: {$upc}, skipping.\n";
					continue;
				}

				$order_id = $ulk->getOrderID($dom->confirm_shipping_header->invoice_number);
				if (empty($order_id)) {
					print "No order id could be found for upc: {$upc}, skiping.\n";
					continue;
				}
                $order_item = $orders->getOrderItemFromUPCFixed($upc, $order_id);
                $shipment = mysqli_query($ulk->db_cnx,"SELECT * FROM Shipments WHERE LineID = '".$order_item['ID']."' AND TrackingNumber = '".mysqli_escape_string($ulk->db_cnx,$tracking_number)."'");
                $item_already_shipped = (mysqli_num_rows($shipment) != 0);
				if ($item_already_shipped) {
                    print "Item has already shipped for upc: {$upc}, skipping.\n";
                    continue;
                }

				$order_item_id = $order_item['ID'];
				$items_to_ship[ $order_item_id ]['ShipQty'] = (($qty > 0) ? ($order_item['Qty'] - $qty) : $order_item['Qty'] );
				$items_to_ship[ $order_item_id ]['OrderQty'] = $order_item['Qty'];
				$items_to_ship[ $order_item_id ]['TrackingNumber'] = $tracking_number;
				$items_to_ship[ $order_item_id ]['ShipMethod'] = 1;

				$channel_order_item_find_sqls[] = <<<SQL
				SELECT * FROM
				ChannelOrderItem
				WHERE ChannelOrderID = '$channel_order_id' AND OrderItemID = '$order_item[ID]'
SQL;
			}

			if (empty($items_to_ship)) {
				print "No items to ship for invoice number: {$dom->confirm_shipping_header->invoice_number}\n";
				return false;
			}

			// Retrieve the store id from the order details
			$order_details_res = $ulk->getOrderDetails($order_id);
			$order_details = mysqli_fetch_assoc($order_details_res);
			if (empty($order_details)) {
				print "Could not find a valid store id for invoice number: {$dom->confirm_shipping_header->invoice_number}\n";

				return false;
			}
			$store_id = $order_details['StoreID'];

			// Retrieve the store details
			$store_details = $ulk->getStore($order_details['StoreID']);
			if (empty($store_details)) {
				print "Could not find a valid store for invoice number: {$dom->confirm_shipping_header->invoice_number}\n";
				return false;
			}

			// Process the credit card
			$payment_amount = $orders->captureOrderItems($order_id, $store_id, $items_to_ship);

			// Marks the items as shipped
			$orders->shipOrderItems($items_to_ship);

			$_SESSION['orderinfo'] = (object) array(
				'ship_fname' => $order_details['FName'],
				'ship_lname' => $order_details['LName'],
			);
			$email = $order_details['Email'];

			$message = "";
			$post = array(
				"orderaction"    => "CustomerShipNotice",
				"orderid"        => $order_id,
				"ordernote"      => "",
				"storeid"        => $store_id,
				"pmtamount"      => $payment_amount,
				'TrackingNumber' => $tracking_number,
				'sitems'         => $items_to_ship,
				"to"             => $email,
				//"to"             => "tommy@zindigo.com",
				"cc"             => null,
				"bcc"            => null,
				"orderid"        => $order_id,
				"storeid"        => $store_id,
				"storename"      => $store_details['Name'],
				"storeemail"     => $store_details['Email']
			);

			$email = new EMAIL;
			$email->EmailCustomerUpdate($post, $message, $ulk);

			print "Email sent for invoice number: {$dom->confirm_shipping_header->invoice_number}\n";

			// Add the status record in order to track the changes
			foreach ($channel_order_item_find_sqls as $find_sql) {
				$channel_order_item_res = mysqli_query($ulk->db_cnx, $find_sql);
				if ($channel_order_item_res) {
					$channel_order_item_details = mysqli_fetch_assoc($channel_order_item_res);

					$channel_order_item = new ChannelOrderItem();
					$channel_order_item->setAttributes($channel_order_item_details);

					$order_item = $orders->getOrderItemFromUPCFixed($channel_order_item->getChannelOrderItemUPC(), $order_id);

					$channel_order_item->setChannelOrderItemStatusID($order_item['ItemStatus']);
					$channel_order_item->save();

					$channel_order_item_status_log = new ChannelOrderItemStatusLog();
					$channel_order_item_status_log->setAttributes(array(
						'ChannelOrderItemID'       => $channel_order_item->getPk(),
						'ChannelOrderItemStatusID' => $order_item['ItemStatus'],
					));
					$channel_order_item_status_log->save();
				}
			}

			return true;
		}
	}
