<?php

	return array(
		'debug_mode'       => false,
		'aggregate_stores' => true,
		'server'           => 'marketing.zindigo.com',
		'username'         => '',
		'password'         => '',
		'protocol'         => 'sftp',
		'directories'      => array(
			'incoming' => '/incoming',
			'outgoing' => '/outgoing',
		),
		'file_name_ext'    => '.csv',
		'file_name_prefix' => array(
			'inventory'           => 'KU_'
		),
		'archive_dir'      => array(
			'inventory'           => '/outgoing/',
		),

		// list of vendor ids that this channel services
		//106 Remac
        //43 Kayunger
        //44 Pheobe
		'vendors'          => array(
			106 => array(),
            43 => array(),
            44 => array(),
		),
	);