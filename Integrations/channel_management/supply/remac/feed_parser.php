<?php
	require_once __DIR__ . "/../../classes/ChannelOrderItem.php";
	require_once __DIR__ . "/../../classes/ChannelOrderItemStatusLog.php";

	class Remac_Feed_Parser {

        public static $csv_fields = array("STYLE","NUMERIC_SIZE","LETTER_SIZE","UPC","QTY","EMPTY");

		public static function parse( $feed_type, $raw_csv ) {
			print "parsing $feed_type\n";

            //raw csv file into a csv array
            $csv_lines = explode(PHP_EOL, $raw_csv);
            $end_csv = array();
            foreach ($csv_lines as $csv_line) {
                $end_csv[] = str_getcsv($csv_line);
            }

			switch (strtolower($feed_type)) {

				case 'inventory':
					$parse_and_process_result = self::parse_and_process_inventory($end_csv);
					break;

				default:
					print "feed type $feed_type doesn't exist\n";
					print "pull remac [inventory]\n\n";
					break;
			}

			return $parse_and_process_result;
		}

		/**
		 * @param $dom
		 *
		 * @return bool
		 */
		public static function parse_and_process_inventory( $the_csv ) {
			global $ulk;
            foreach($the_csv as $csv_line)
            {
                $upc = $csv_line[array_search('UPC', self::$csv_fields)];
                $qty = $csv_line[array_search('QTY', self::$csv_fields)];
                if($upc != null && trim($upc) != "")
                $ulk->updateInventoryByUPC($qty, mysqli_escape_string($ulk->db_cnx, $upc));
            }
			return true;
		}
	}
