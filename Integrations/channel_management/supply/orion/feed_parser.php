<?php
	require_once __DIR__ . "/../../classes/ChannelOrderItem.php";
	require_once __DIR__ . "/../../classes/ChannelOrderItemStatusLog.php";

	class Orion_Feed_Parser {

		public static function parse( $feed_type, $raw_csv ) {
			print "parsing $feed_type\n";

            //raw csv file into a csv array
            $csv_lines = explode(PHP_EOL, $raw_csv);
            $end_csv = array();
            foreach ($csv_lines as $csv_line) {
                $end_csv[] = str_getcsv($csv_line,"^");
            }


			switch (strtolower($feed_type)) {

				case 'inventory':
                    //every file we have has a header row with information we do not need to parse
                    array_shift($end_csv);
                    //every file we have has a footer row with information we do not need to parse
                    array_pop($end_csv);
					$parse_and_process_result = self::parse_and_process_inventory($end_csv);
					break;

                case 'pick_ticket_receipt':
                    //every file we have has a header row with information we do not need to parse
                    array_shift($end_csv);
                    $parse_and_process_result = self::parse_and_process_order($end_csv);
                    break;

				default:
					print "feed type $feed_type doesn't exist\n";
					print "pull orion [inventory]\n\n";
					break;
			}

			return $parse_and_process_result;
		}

        public static function calculate_item_sku($vendor_id, &$item, $reverse_color_size = false) {
            if ($reverse_color_size) {
                return "{$vendor_id}_{$item["STYLE"]}-{$item["SIZE_NAME"]}-{$item["COLOR"]}";
            } else {
                return "{$vendor_id}_{$item["STYLE"]}-{$item["COLOR"]}-{$item["SIZE_NAME"]}";
            }
        }

        public static function get_orderitem_by_sku($ordernum,$sku,$color, $sizeName) {
            global $ulk;
            $sql = <<<SQL
            SELECT od.OrderID, od.ID, od.Qty, UPPER(od.SKU) as sku FROM Orders o JOIN OrderDetails od ON od.OrderID = o.ID
            WHERE o.OrderNum = '{$ordernum}'
            HAVING (sku LIKE'%{$sku}%' AND sku LIKE '%-{$color}%' AND sku LIKE '%-{$sizeName}%')
SQL;

            echo $sql . "\n";

            if (!mysqli_multi_query($ulk->db_cnx, $sql)) {
                die(mysqli_error($ulk->db_cnx));
            }

            $rs = mysqli_store_result($ulk->db_cnx);

            $ulk->cleanConnection();
            $tresult = mysqli_fetch_assoc($rs);
            mysqli_free_result($rs);

            if (isset($tresult['ID'])) {
                return $tresult;
            }

        }

        public static function update_channel_value($inventoryid, $value)
        {
            global $ulk;
            $sql = "REPLACE INTO ChannelValues (InventoryID, ChannelValue, ChannelCode) Values ('".$inventoryid."', '".$value."', 'ORION')";
            mysqli_query($ulk->db_cnx,$sql);
        }

        public static function get_item_inventory_by_sku($vendor_id, $calculated_sku) {
            global $ulk;

            $vendor_id		= (int) $vendor_id;
            $calculated_sku = addslashes(strtoupper(trim($calculated_sku)));

            $sql = "SELECT i.InventoryID, i.ProductID, i.UPC, i.Qty, UPPER(CONCAT(`p`.`VendorID`,'_',`p`.`VendorSKUBase`,v1.SKU, v2.SKU )) as sku
			FROM `Inventory` `i`
			INNER JOIN `Products` `p` ON `i`.`ProductID` = `p`.`ID`
			JOIN `Values` v1 ON v1.ID= i.Value1
			JOIN `Values` v2 ON v2.ID= i.Value2
			WHERE p.VendorID = '$vendor_id'
			HAVING sku ='$calculated_sku' LIMIT 1;";

            if (!mysqli_multi_query($ulk->db_cnx, $sql)) {
                die(mysqli_error($ulk->db_cnx));
            }

            $rs = mysqli_store_result($ulk->db_cnx);

            $ulk->cleanConnection();
            $tresult = mysqli_fetch_assoc($rs);
            mysqli_free_result($rs);

            if (isset($tresult['InventoryID'])) {
                return $tresult;
            }
        }

        public static function hasSKU($the_sku) {
            global $ulk;

            $skuSQL = <<<SQL
            SELECT * FROM Products WHERE VendorSKUBase = '{$the_sku}'
SQL;
            $skuResult = mysqli_query($ulk->db_cnx,$skuSQL);
            if($skuResult && mysqli_num_rows($skuResult) >= 1)
                return true;

            return false;

        }

        public static function normalizeSKs($jsonarray) {
            global $ulk;

            $allColors = <<<SQL
                SELECT DISTINCT
                    v1.SKU,
                    v1.ID
                FROM
                    zshops.Inventory i
                JOIN Products p ON i.ProductID = p.ID
                JOIN `Properties` pr ON pr.ProductID = i.ProductID
                JOIN `Values` v1 ON v1.PropertyID = pr.ID
                WHERE
                    pr. NAME = 'Color'
                AND p.VendorID = 102
SQL;

            $result = mysqli_query($ulk->db_cnx, $allColors) or die("error: ".mysqli_error($ulk->db_cnx));
            while ($row = mysqli_fetch_array($result)) {
                $lookup_sku = strtoupper($row['SKU']);
                $foundColor = self::find_color_in_array($lookup_sku,$jsonarray);

                if(!$foundColor)
                {
                    $break = 0;
                }
            }

        }

        public static function zeroSKUQty($thesku)
        {
            global $ulk;
            if(strlen($thesku) < 5) return;
            $sql = <<<SQL
            UPDATE Inventory SET Qty = 0 WHERE SKU LIKE '%102_{$thesku}%'
SQL;

            mysqli_query($ulk->db_cnx,$sql);

        }

        public static function update_item_qty($inventory_id, $qty) {
            global $ulk;

            $product_status_id = 1;

            $sql = "UPDATE Inventory SET "
                . "Qty = '$qty', "
                . "ProductStatus = $product_status_id, "
                . "DateUpdated = CURRENT_TIMESTAMP(), "
                . "SyncDate = NOW() "
                . " WHERE InventoryID = '$inventory_id';";

//		$sql = "UPDATE Inventory SET Qty = '$qty' WHERE InventoryID = $inventory_id;";
            if (!mysqli_multi_query($ulk->db_cnx, $sql)) {
                die(mysqli_error($ulk->db_cnx));
            }
            return mysqli_affected_rows($ulk->db_cnx);
        }

		/**
		 * @param $dom
		 *
		 * @return bool
		 */
		public static function parse_and_process_inventory( $the_csv ) {
			global $ulk;

            //lets make an array of all codes
            $fixSKUs = $jsonarray = array();
            $fp = fopen(__DIR__ . "/lib/colorcodes.csv", 'r');
            while (($data = fgetcsv($fp, 99999999, ",")) !== FALSE)
            {
                $colorcode = $data[0];
                $colorname = $data[1];
                if(strlen(trim($colorname)) <= 0) continue;
                //$colorname = str_replace("/", " ", $colorname);
                //$colorname = str_replace("LT. ", "", $colorname);

                $jsonarray[] = array("colorcode" => trim($colorcode), "colorname" => trim($colorname));
            }

            //self::normalizeSKs($jsonarray);
            //return true;

            $header_layout = array("Identifier", "Company", "Division", "Item", "Color", "Size Number", "Size description", "Warehouse", "On Hand", "Cost", "Length", "Width", "Height", "Weight");

            $notownedSKUs = array();
            foreach($the_csv as $csv_line)
            {
                $thesku = $csv_line[array_search('Item', $header_layout)];
                $qty = $csv_line[array_search('On Hand', $header_layout)];
                $color = $csv_line[array_search('Color', $header_layout)];
                $size = $csv_line[array_search('Size description', $header_layout)];

                //check we have the product or else skip
                if($thesku != null)
                {
                    $updatedSKU = false;
                    $thesku = substr($thesku, 0, -2);

                    $haveSKU = self::hasSKU($thesku);
                    if(!$haveSKU){
                        if(!in_array($thesku,$notownedSKUs)) {
                            $notownedSKUs[] = $thesku;
                        }
                        continue;
                    }


                    $foundColor = self::find_color_in_array($color,$jsonarray);
                    if($foundColor == "BLA")
                        $foundColor = "BLACK";

                    $theitem = array("STYLE" => $thesku, "COLOR" => $foundColor, "SIZE_NAME" => $size);

                    $calculated_sku = self::calculate_item_sku(102,$theitem);
                    $reverse_calculated_sku = self::calculate_item_sku(102,$theitem,true);

                    $current_inventory_data = self::get_item_inventory_by_sku(102,$calculated_sku);

                    if ($current_inventory_data['InventoryID']) {
                        self::update_item_qty($current_inventory_data['InventoryID'], $qty);
                        self::update_channel_value($current_inventory_data['InventoryID'],$color);
                        echo "Updating ". $calculated_sku . " to qty " . $qty . "\r\n";
                        $updatedSKU = true;
                    } else {
                        $current_inventory_data = self::get_item_inventory_by_sku(102,$reverse_calculated_sku);
                        if ($current_inventory_data['InventoryID']) {
                            self::update_item_qty($current_inventory_data['InventoryID'], $qty);
                            self::update_channel_value($current_inventory_data['InventoryID'],$color);
                            echo "Updating ". $reverse_calculated_sku . " to qty " . $qty . "\r\n";
                            $updatedSKU = true;
                        }
                    }

                    $endofString = substr($calculated_sku, -3);

                    if(!$updatedSKU && $haveSKU && $endofString != "-XL")
                    {
                        if(!in_array(($thesku.$foundColor),$fixSKUs)) {
                            $fixSKUs[] = ($thesku.$foundColor);
                            //lets 0 out any inventory for this since we cant get integrity for it
                            self::zeroSKUQty($thesku);
                            echo "Putting ". $thesku . " to qty 0\r\n";
                        }
                    }
                }
            }

            //now lets email a report
            if(!empty($fixSKUs) || !empty($notownedSKUs)) {

                $outLog = "";
                if(!empty($fixSKUs))
                {
                    $outLog .= "\n" . str_pad("SKU COLORS TO FIX", 40)."\n";
                    $outLog .= str_repeat("-", 130) . "\n";
                    foreach($fixSKUs as $tofix)
                    {
                        $outLog .= "\n" . str_pad($tofix, 40) . "\n";
                    }

                }

                if(!empty($notownedSKUs))
                {
                    $outLog .= "\n\n" . str_pad("SKUS NOT IN BACKEND", 40)."\n";
                    $outLog .= str_repeat("-", 130) . "\n";
                    foreach($notownedSKUs as $tofix)
                    {
                        $outLog .= "\n" . str_pad($tofix, 40) . "\n";
                    }

                }

                if($outLog != "") {
                    $message = "<html><body>"
                        . "<h1>Inventory Update Log</h1>"
                        . "<pre>{$outLog}</pre></html>";
                    static::SendEmailError($message, "gina@zindigo.com", "inventory@zindigo.com", "Olive Oak Inventory Report", "tommy@zindigo.com");
                }
            }

			return true;
		}

        public static function SendEmailError($thebody, $to, $from, $subject, $cc = "", $bcc = "")
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'api:key-3wt7-hoklyp34o8856d8y9zzwns3px17');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v2/zindigo.com/messages');

            $mailarray['from'] = strip_tags($from);
            $mailarray['to'] = strip_tags($to);
            if($cc <> "")   $mailarray['cc'] = strip_tags($cc);
            if($bcc <> "")   $mailarray['bcc'] = strip_tags($bcc);
            $mailarray['subject'] = strip_tags($subject);
            $mailarray['html'] = $thebody;
            curl_setopt($ch, CURLOPT_POSTFIELDS, $mailarray);

            $result = curl_exec($ch);
            echo "SENDMAIL: ".$result . "\n";
            curl_close($ch);
        }


        public static function find_color_in_array( $color,$array ) {
            foreach($array as $anitem)
            {
                if (strcasecmp($anitem['colorcode'], $color) == 0) {
                    return $anitem['colorname'];
                }
            }
            return $color;
        }

        /**
         * @param $dom
         *
         * @return bool
         */
        public static function parse_and_process_order( $the_csv ) {
            global $ulk;
            require_once __DIR__ . '/../../../../../classes/orders.php';
            require_once __DIR__ . '/../../../../../classes/email.php';

            $fp = fopen(__DIR__ . "/lib/colorcodes.csv", 'r');
            while (($data = fgetcsv($fp, 99999999, ",")) !== FALSE)
            {
                $colorcode = $data[0];
                $colorname = $data[1];
                if(strlen(trim($colorname)) <= 0) continue;
                $jsonarray[] = array("colorcode" => trim($colorcode), "colorname" => trim($colorname));
            }


            $header_layout = array("Identifier", "Comapny", "Division", "Order Number", "Invoice Number", "Customer", "Order Status", "Ship Name", "Ship Address 1", "Ship Address 2", "Ship City", "Ship State", "Ship Zip", "Ship Country", "Status Date", "Carrier");
            $detail_layout = array("Identifier", "Line Number", "Item", "Color Code", "Item Description", "Quanitity Ordered", "Quantity Shipped", "Tracking Number", "Size Number", "Size Name");
            $items_to_cancel = $csv_order_items = $channel_order_item_find_sqls = array();


            $canceling = false;
            $shipItems = false;

            foreach($the_csv as $csv_line)
            {
                //header for current order file
                if($csv_line[0] == "HD")
                {
                    //find internal order number based on order
                    $order_number = $csv_line[array_search('Order Number', $header_layout)];

                    $sql = <<<SQL
			SELECT ChannelOrderID
			FROM ChannelOrder WHERE ExternalOrderID = '$order_number'
			LIMIT 1
SQL;
                    $channel_order_res = mysqli_query($ulk->db_cnx, $sql);
                    if (!$channel_order_res) {
                        print "Could not locate parent channel order for invoice number: {$order_number}\n";

                        return false;
                    }

                    $channel_order_details = mysqli_fetch_assoc($channel_order_res);
                    $channel_order_id = $channel_order_details['ChannelOrderID'];

                    $canceling = $csv_line[array_search('Order Status', $header_layout)] == "CNL" ? true : false;
                    $shipItems = $csv_line[array_search('Order Status', $header_layout)] == "SHP" ? true : false;
                }

                //items in the current order
                if($csv_line[0] == "DT")
                {
                    $sku = $csv_line[array_search('Item', $detail_layout)];
                    $sku = substr($sku, 0, -2);
                    $shipped = $csv_line[array_search('Quantity Shipped', $detail_layout)];
                    $tracking_number = $csv_line[array_search('Tracking Number', $detail_layout)];
                    $color = $csv_line[array_search('Color Code', $detail_layout)];
                    $sizeName = $csv_line[array_search('Size Name', $detail_layout)];

                    $foundColor = self::find_color_in_array($color,$jsonarray);
                    if($foundColor == "BLA")
                        $foundColor = "BLACK";

                    $order_item = self::get_orderitem_by_sku($order_number,$sku,$foundColor, $sizeName);

                    $shipment = mysqli_query($ulk->db_cnx,"SELECT * FROM Shipments WHERE LineID = '".$order_item['ID']."' AND TrackingNumber = '".mysqli_escape_string($ulk->db_cnx,$tracking_number)."'");
                    $item_already_shipped = (mysqli_num_rows($shipment) != 0);

                    if ($item_already_shipped) {
                        print "Item has already shipped for SKU: {$sku}, skipping.\n";
                        continue;
                    }

                    $order_id = $order_item['OrderID'];
                    $order_item_id = $order_item['ID'];

                    if(!$canceling && $shipItems) {
                        $items_to_ship = array();
                        $items_to_ship[ $order_item_id ]['ShipQty'] = $shipped;
                        $items_to_ship[ $order_item_id ]['OrderQty'] = $order_item['Qty'];
                        $items_to_ship[ $order_item_id ]['TrackingNumber'] = $tracking_number;
                        $items_to_ship[ $order_item_id ]['ShipMethod'] = 1;

                        $order_details_res = $ulk->getOrderDetails($order_id);
                        $order_details = mysqli_fetch_assoc($order_details_res);
                        if (empty($order_details)) {
                            print "Could not find a valid store id for invoice number: {$order_id}\n";

                            return false;
                        }
                        $store_id = $order_details['StoreID'];

                        // Retrieve the store details
                        $store_details = $ulk->getStore($order_details['StoreID']);
                        if (empty($store_details)) {
                            print "Could not find a valid store for invoice number: {$order_id}\n";
                            return false;
                        }

                        if(!is_array($csv_order_items[$order_id]['Items']))
                            $csv_order_items[$order_id]['Items'] = array();

                        $csv_order_items[$order_id]['Order'] = $order_details;
                        $csv_order_items[$order_id]['Store'] = $store_details;
                        $csv_order_items[$order_id]['Items'] = $csv_order_items[$order_id]['Items']  + $items_to_ship;
                        $csv_order_items[$order_id]['Tracking'] = $tracking_number;
                    }




                    if($canceling && !$shipItems)
                    {
                        //$items_to_cancel[$order_item_id] = ;
                    }

                    $channel_order_item_find_sqls[] = <<<SQL
				SELECT * FROM
				ChannelOrderItem
				WHERE ChannelOrderID = '$channel_order_id' AND OrderItemID = '$order_item[ID]'
SQL;
                }

            }

            //cancel items here because we cannot ship after
            if(!empty($items_to_cancel))
            {
                $refundamt = 0;
                $taxamount = 0;
                $_SESSION['cart'] = new Cart;
                //$ulk->popCartfromOrder($order_number);
                foreach($items_to_cancel as $lineitem => $amount)
                {
                    /*
                    $lineamts = $_POST['LineTotal'];
                    $refundamt += $lineamts[$lineitemid];
                    $taxamount += $_SESSION['cart']->items[$products[$lineitemid]]->get_taxamount();
                    $_SESSION['cart']->remove($products[$lineitemid]);

                    $res=$ulk->CancelOrderItem($lineitemid);

                    $citems[$lineitemid]['ShipQty'] = $orderqtys[$lineitemid];
                    */
                }

            }


            foreach($csv_order_items as $the_orderid => $the_items) {

                $order_id = $the_orderid;

                $items_to_ship = $the_items['Items'];
                $order_details = $the_items['Order'];
                $store_details = $the_items['Store'];
                $tracking_number = $the_items['tracking'];
                $store_id = $order_details['StoreID'];
                $email = $order_details['Email'];

                //this is how where we accumulate all order items into an array and process them as a singular object

                $payment_amount = $orders->captureOrderItems($order_id, $store_id, $items_to_ship);

                $orders->shipOrderItems($items_to_ship);

                $_SESSION['orderinfo'] = (object) array(
                    'ship_fname' => $order_details['FName'],
                    'ship_lname' => $order_details['LName'],
                );
                $email = $order_details['Email'];

                $message = "";
                $post = array(
                    "orderaction"    => "CustomerShipNotice",
                    "orderid"        => $order_id,
                    "ordernote"      => "",
                    "storeid"        => $store_id,
                    "pmtamount"      => $payment_amount,
                    'TrackingNumber' => $tracking_number,
                    'sitems'         => $items_to_ship,
                    "to"             => $email,
                    "cc"             => null,
                    "bcc"            => null,
                    "orderid"        => $order_id,
                    "storeid"        => $store_id,
                    "storename"      => $store_details['Name'],
                    "storeemail"     => $store_details['Email']
                );


                $email = new EMAIL;
                $email->EmailCustomerUpdate($post, $message, $ulk);

            }
            //end

            // Add the status record in order to track the changes
            foreach ($channel_order_item_find_sqls as $find_sql) {
                $channel_order_item_res = mysqli_query($ulk->db_cnx, $find_sql);
                if ($channel_order_item_res) {
                    $channel_order_item_details = mysqli_fetch_assoc($channel_order_item_res);

                    $channel_order_item = new ChannelOrderItem();
                    $channel_order_item->setAttributes($channel_order_item_details);

                    $order_item = $orders->getOrderItemFromUPCFixed($channel_order_item->getChannelOrderItemUPC(), $order_id);

                    $channel_order_item->setChannelOrderItemStatusID($order_item['ItemStatus']);
                    $channel_order_item->save();

                    $channel_order_item_status_log = new ChannelOrderItemStatusLog();
                    $channel_order_item_status_log->setAttributes(array(
                        'ChannelOrderItemID'       => $channel_order_item->getPk(),
                        'ChannelOrderItemStatusID' => $order_item['ItemStatus'],
                    ));
                    $channel_order_item_status_log->save();
                }
            }

            return true;
        }

	}
