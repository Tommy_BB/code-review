<?php
    require_once __DIR__ . "/../../classes/ChannelOrderItem.php";
    require_once __DIR__ . "/../../classes/ChannelOrderItemStatusLog.php";
    include_once(__DIR__ . "/../../../../../classes/orders.php");
	class Orion_Feed_Formatter {

		/**
		 * Render a capacity xml feed
		 *
		 * @param type $feed_type
		 * @param type $vendor_info
		 * @param type $payload
		 *
		 * @return type
		 * @throws Exception
		 */
		public static function render( $feed_type, $payload, $file_prefix = null ) {

			print "generating $feed_type";

			$document_guid = static::generate_document_guid($feed_type, $payload, "{$file_prefix}");

			/** @var DomDocument $xml */
			switch (strtolower($feed_type)) {
                case 'pick_ticket_request':
                    $csv = static::generate_pick_ticket_feed($payload);
                    break;
				default:
					throw new Exception("Undefined feed type: $feed_type");
			}

			$file_prefix      = (isset($file_prefix)) ? $file_prefix : $feed_type;
			$channel_temp_dir = "/tmp/orion";
			$file_path        = "$channel_temp_dir/$document_guid.txt";

			if (!file_exists($channel_temp_dir)) {
				print "making channel tmp directory $channel_temp_dir\n";
				mkdir($channel_temp_dir, 0777);
			}

            if(file_put_contents($file_path, $csv))
            {
                return $file_path;
            }
		}

        public static function find_color_in_array( $color,$array ) {
            //$color = strtoupper($color);
            foreach($array as $anitem)
            {
                if (strcasecmp($anitem['colorname'], $color) == 0) {
                    return $anitem['colorcode'];
                }
            }
            return $color;
        }

		/**
		 * Generate the order ticket csv tab feed
		 *
		 * @param $packing_orders
		 */
		public static function generate_pick_ticket_feed( ChannelOrder $packing_order ) {
			global $ulk, $orders, $basepath;
            $internal_order_data = $packing_order->getInternalOrderData();

            // Get the order details
            $order_details = array();
            $result        = $ulk->getOrderDetails($internal_order_data['ID']);
            while ($order_detail_data = mysqli_fetch_assoc($result)) {
                $order_details[] = $order_detail_data;
            }
            $general_order_details = current($order_details);

            $general_order_details['VendorID'] = 102;
            $general_order_details['FranID'] = 99;


            $jsonarray = array();
            $fp = fopen(__DIR__ . "/lib/colorcodes.csv", 'r');
            while (($data = fgetcsv($fp, 99999999, ",")) !== FALSE)
            {
                $colorcode = $data[0];
                $colorname = $data[1];
                if(strlen(trim($colorname)) <= 0) continue;
                $jsonarray[] = array("colorcode" => trim($colorcode), "colorname" => trim($colorname));
            }



            //We need a control number
            $csv = "";

            //Trandmission ROW
            $control_number = 01;
            $csv .= "TR^".$control_number.PHP_EOL;

            //HD^8471^ECOM2X^8471^2X-WEB-B2C^^WILLIE BRIDGES ^4825 12 AVE SOUTH^^MINNEAPOLIS^MN^55417^US^20140615^20140615^ECPAY^FDES^
            $header_row = array(
                "Identifier" => array("override" => "HD"),
                "EcomNumber" => "OrderNum",
                "Customer" => array("override" => "ZINDIBC"), //wtf is this
                "PONumber" => "OrderNum",
                "Order Type" => "",
                "ShiptoNumber" => "",
                "Ship To Name" => array('multiple' => array('Ship_FName', 'Ship_LName')),
                "Ship Address 1" => "Ship_Address1",
                "Ship Address 2" => "Ship_Address2",
                "Ship City" => "Ship_City",
                "Ship State" => "Ship_State",
                "Ship Zip" => "Ship_Zip",
                "Ship Country" => "Ship_Country",
                "Start Date" => array("override" => date("Ymd")),
                "Cancel Date" => array("override" => date("Ymd", strtotime('+7 days'))),
                "Terms" => array("override" => "S27"),
                "Carrier" => array("override" => "FDEG"),
                "Notes" => ""
            );
                foreach ($header_row as $header => $mapped) {
                    if (is_array($mapped)) {

                        //this is for overrides that cannot be mapped / are static instances
                        if (isset($mapped['override'])) {
                            $csv .= $mapped['override'] . "^";
                        }

                        //mapping multiple columns and concatting them together
                        if (isset($mapped['multiple'])) {
                            $addedon = "";
                            foreach ($mapped['multiple'] as $multiples) {
                                if (isset($general_order_details[$multiples])) {
                                    $addedon .= $general_order_details[$multiples] . " ";
                                }
                            }
                            if ($addedon !== "") {
                                //lets remove any additional spaces we may have added and append to the csv
                                $csv .= rtrim($addedon). "^";
                            }
                        }

                    } else if ($mapped != "") {
                        if (isset($general_order_details[$mapped])) {
                            if($mapped == "Ship_Zip")
                            {
                                $csv .= substr($general_order_details[$mapped],0,5) . "^";
                            } else
                            $csv .= $general_order_details[$mapped] . "^";
                        }
                        else
                            $csv .= $mapped . "^";
                    } else {
                        $csv .= "^";
                    }

                    //support for END OF LINE for this record
                    if ($header === "Notes") {
                        $csv .= PHP_EOL;
                    }
                }

            //DT^1^03^001^3102030403L^34505^ESSENTIAL Boxer Brief 3-Pack^3102030403L_34505^1^36.00^0.00^3.06^0.00
            $detail_row = array(
                "Identifier" => array("override" => "DT"),
                "Line" => array("counter" => ""),
                "Company" => array("override" => "01"), //??
                "Division" => array("override" => "OLI"),//??
                "Item" => "StyleNumber", //Product.VendorSKU
                "Color Code" => array("colorcode" => ""), //
                "Size Number" => array("sizenum" => ""),
                "Size decription" => array("sizename" => ""),
                "Item Desc" => "", // ??
                "Web SKU" => "", //SKU
                "Qty Ordered" => "ProductQuantity",
                "Item Price" => "ProductPrice",
                "Discount" => array("override" => "0.00"), //nope
                "Tax" => array("override" => "0.00"), //nope
                "Freight" => array("override" => "0.00"), //nope
            );


            $lineItems = 0;
            $packingItems = array();
            $shipItems = array();
            foreach ($order_details as $order_detail) {
                if($order_detail['VendorID'] != 102) continue;

                $shipItems[$order_detail['OrderDetailID']] = $order_detail['ProductQuantity'];
                $packingItems[$order_detail['OrderDetailID']]['ShipQty'] = $order_detail['ProductQuantity'];
                $packingItems[$order_detail['OrderDetailID']]['ShipMethod'] =  $order_detail['ShipMethod'];
                $packingItems[$order_detail['OrderDetailID']]['TrackingNumber'] = "";


                $lineItems++;
                foreach ($detail_row as $header => $mapped) {
                    if (is_array($mapped)) {

                        //this is for overrides that cannot be mapped / are static instances

                        if (isset($mapped['colorcode'])) {
                            $csv .= static::getProductColorCode($order_detail['ProductSKU']);
                        }

                        if (isset($mapped['sizenum'])) {
                            $sizeArr = static::getSizeDescription($order_detail['PropertyName'], $order_detail['PropertyValue'],$order_detail);
                            $csv .= $sizeArr['SizeNum'];
                        }

                        if (isset($mapped['sizename'])) {
                            $sizeArr = static::getSizeDescription($order_detail['PropertyName'], $order_detail['PropertyValue'],$order_detail);
                            $csv .= $sizeArr['SizeName'];
                        }

                        if (isset($mapped['override'])) {
                            $csv .= $mapped['override'];
                        }

                        //this is the number of counting
                        if (isset($mapped['counter'])) {
                            $csv .= $lineItems;
                        }

                        //mapping multiple columns and concatting them together
                        if (isset($mapped['multiple'])) {
                            $addedon = "";
                            foreach ($mapped['multiple'] as $multiples) {
                                if (isset($order_detail[$multiples])) {
                                    $addedon .= $order_detail[$multiples] . " ";
                                }
                            }
                            if ($addedon !== "") {
                                //lets remove any additional spaces we may have added and append to the csv
                                $csv .= rtrim($addedon);
                            }
                        }

                    } else if ($mapped != "") {
                        if (isset($order_detail[$mapped])) {
                            if($mapped == "StyleNumber") {
                                $csv .= $order_detail[$mapped] . "EC";
                            } else if($mapped == "Taxes" || $mapped == "Shipping" || $mapped == "ProductPrice")
                            {
                                $csv .= number_format($order_detail[$mapped],2);
                            } else
                            $csv .= $order_detail[$mapped];
                        }
                        else
                            $csv .= $mapped;
                    }

                    //support for END OF LINE for this record
                    if ($header === "Freight") {
                        $csv .= PHP_EOL;
                    } else {
                        $csv .= "^";
                    }
                }
            }


            $csv .= "TT^".$lineItems.PHP_EOL;
            $packages = null;
            $storesarr = $ulk->getAllOrdersProducts($general_order_details['ID'], $general_order_details['VendorID'], $general_order_details['FranID']);
            $storesarr = mysqli_fetch_assoc($storesarr);



            $chunkCount = array_chunk($order_details, 5);
            $totalweight = count($chunkCount) * 1;

            //lets hardcode the package to ship
            $packages[0]['length'] = $storesarr['PackageLength'];
            $packages[0]['width'] = $storesarr['PackageWidth'];
            $packages[0]['height'] = $storesarr['PackageHeight'];
            $packages[0]['dimunits']= "IN";
            $packages[0]['weightunits']= "LB";
            $packages[0]['weight'] = $totalweight;

                //$totalweight =  ((count($order_details) / 5) <= 0 ? 1 : (count($order_details) / 5) * 1));


            $orders = new orders();
            $basepath = __DIR__ . "/../../../../../";
            include_once __DIR__ . "/../../../../../classes/cloudfiles.php";
            include_once(__DIR__ . "/../../../../../classes/shipping.php");

            $shipcalc = new ShipCalc();


            $shipMethods = array(
                "FEDEX GROUND" => "FEDEX_GROUND",
                "FEDEX FIRST OVERNIGHT" => "FIRST_OVERNIGHT",
                "FIRST OVERNIGHT" => "FIRST_OVERNIGHT",
                "FEDEX PRIORITY OVERNIGHT" => "PRIORITY_OVERNIGHT",
                "PRIORITY OVERNIGHT" => "PRIORITY_OVERNIGHT",
                "FEDEX STANDARD OVERNIGHT" => "STANDARD_OVERNIGHT",
                "STANDARD OVERNIGHT" => "STANDARD_OVERNIGHT",
                "FEDEX 2DAY" => "FEDEX_2_DAY",
                "2DAY" => "FEDEX_2_DAY",
                "FEDEX 2 DAY" => "FEDEX_2_DAY",
                "FEDEX EXPRESS SAVER" => "FEDEX_EXPRESS_SAVER",
                "EXPRESS SAVER" => "FEDEX_EXPRESS_SAVER",
                "FEDEX INTERNATIONAL FIRST" => "INTERNATIONAL_FIRST",
                "INTERNATIONAL FIRST" => "INTERNATIONAL_FIRST",
                "FEDEX INTERNATIONAL PRIORITY" => "INTERNATIONAL_PRIORITY",
                "INTERNATIONAL PRIORITY" => "INTERNATIONAL_PRIORITY",
                "FEDEX INTERNATIONAL ECONOMY" => "INTERNATIONAL_ECONOMY",
                "INTERNATIONAL ECONOMY" => "INTERNATIONAL_ECONOMY",
                "FEDEX HOME DELIVERY" => "FEDEX_HOME_DELIVERY",
                "HOME DELIVERY" => "FEDEX_HOME_DELIVERY",
                "SMART POST" => "SMART_POST",
                "FEDEX OVERNIGHT FREIGHT" => "FEDEX_FIRST_FREIGHT",
                "FEDEX 1DAY FREIGHT" => "FEDEX_1_DAY_FREIGHT",
                "FEDEX 2DAY FREIGHT" => "FEDEX_2_DAY_FREIGHT",
                "FEDEX 3DAY FREIGHT" => "FEDEX_3_DAY_FREIGHT"

            );

            $currentShipMethod = $general_order_details['ShipMethod'];

            $internalShipMethod = isset($shipMethods[$currentShipMethod]) ? $shipMethods[$currentShipMethod] : "FEDEX_GROUND";

            include_once __DIR__ . "/../../../../../classes/cloudfiles.php";
            $trackingnumber = $shipcalc->fedex_ship($general_order_details['ID'],$general_order_details['VendorID'], $internalShipMethod,$packages, $totalweight, $shipItems);

            $file = __DIR__ . "/../../../../../documents/".$trackingnumber.".pdf";
            $newfile = __DIR__ . "/../../../../../documents/".$general_order_details['OrderNum']."_Fedex.pdf";

            //lets copy our tracking pdf to what they want it named
            if (!copy($file, $newfile)) {
                die("Failed making orion copy of tracking");
            }

            $query = "INSERT INTO PendingShipments (`OrderID`, `TrackingNumber`) VALUES (".$general_order_details['ID'].", '".$trackingnumber."')";
            mysqli_query($ulk->db_cnx,$query);

            //lets mark the trackingnum in the db and update later after they send ship confirmation

            //lets make a packing slip and a shipping slip
            self::createpackingSlip($general_order_details['ID'], $general_order_details['VendorID'],$trackingnumber,$packingItems);

            $file = "/tmp/".$trackingnumber."_pack.pdf";
            $newfile = __DIR__ . "/../../../../../documents/".$general_order_details['OrderNum']."_Packing.pdf";

            //lets copy our tracking pdf to what they want it named
            if (!copy($file, $newfile)) {
                die("Failed making orion copy of packing");
            }


            //now lets move everything


            // Mark the order as "pending"
            $packing_order->ChannelOrderStatusID = 5;
            $packing_order->save();

            return $csv;

		}

        public static function getSizeDescription($pronames, $props, $order) {

            $sizeStack = explode(",",$pronames);
            $sizeSpot = -1;
            $incrementer = 0;

            $sizeNumAttr = array("XS" => 1,"S" => 2,"M" => 3,"L" => 4,"XL" => 5);

            foreach($sizeStack as $stack)
            {
                if(strcmp(strtolower(trim($stack)), "size") == 0)
                {
                    $sizeSpot = $incrementer;
                }
                $incrementer++;
            }

            if($sizeSpot == -1)
            {
                return "";
            }

            $sizeNameStack = explode(",",$props);

            $findsizeName = $sizeNameStack[$sizeSpot];
            $findsizeName = strtoupper($findsizeName);

            return array("SizeNum" => $sizeNumAttr[$findsizeName], "SizeName" => $findsizeName);

        }

        public static function getProductColorCode($skuBase) {
            global $ulk;
            $sql = "SELECT cv.ChannelValue FROM Inventory i JOIN ChannelValues cv ON cv.InventoryID = i.InventoryID WHERE i.SKU = '".strtoupper($skuBase)."'";
            $result = mysqli_query($ulk->db_cnx,$sql);
            $resultValues = mysqli_fetch_assoc($result);

            return $resultValues['ChannelValue'];

        }

        public static function SendEmailError($therror, $to, $from, $subject, $cc = "", $bcc = "")
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'api:key-3wt7-hoklyp34o8856d8y9zzwns3px17');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v2/zindigo.com/messages');

            $mailarray['from'] = strip_tags($from);
            $mailarray['to'] = strip_tags($to);
            if($cc <> "")   $mailarray['cc'] = strip_tags($cc);
            if($bcc <> "")   $mailarray['bcc'] = strip_tags($bcc);
            $mailarray['subject'] = strip_tags($subject);
            $mailarray['html'] = $therror;
            curl_setopt($ch, CURLOPT_POSTFIELDS, $mailarray);

            $result = curl_exec($ch);
            error_log("SENDMAIL: ".$result);
            curl_close($ch);
        }

		public static function generate_document_guid( $feed_type, $payload, $prefix ) {
			$payload_pk = "";
			if ($payload_pk) {
				$prefix .= "_{$payload_pk}";
			}

			return $prefix . "12". date("Ymd_His");
		}

        public static function createpackingSlip($orderid,$vendorid,$trackingnumber, $items)
        {
            global $ulk, $vendors, $orders;
            $order = $ulk->GetOrder($orderid);
            $vendor = $vendors->getVendor($vendorid);
            $tracknum = $trackingnumber;
            ob_start();
            include __DIR__ . "/../../../../../admin/packlist.php";
            $content = ob_get_clean();
            ob_end_flush();
            ob_end_clean();
            include_once __DIR__ . "/../../../../../classes/pdf/html2pdf.class.php";
            include_once __DIR__ . "/../../../../../classes/cloudfiles.php";
            $cfclass = new cloudfiles();

            try
            {

                $html2pdf = new HTML2PDF('P', 'A4', 'en');
                $html2pdf->setDefaultFont('Helvetica');
                $html2pdf->writeHTML($content, isset ($_GET['vuehtml']));
                $html2pdf->Output('/tmp/' . $trackingnumber . '_pack.pdf', 'F');
                $cfclass->saveFile('documents', '/tmp/' . $trackingnumber . '_pack.pdf', $trackingnumber . '_pack.pdf');

            }
            catch (HTML2PDF_exception $e)
            {
                die($e);
            }

        }
	}
