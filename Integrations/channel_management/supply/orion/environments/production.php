<?php

	return array(
		'debug_mode'       => false,
		'aggregate_stores' => true,
		'server'           => 'marketing.zindigo.com',
		'username'         => '',
		'password'         => '',
		'protocol'         => 'sftp',
		'directories'      => array(
			'inventory' => '/incoming',
            'pick_ticket_request' => '/incoming',
            'incoming' => '/incoming',
            'outgoing' => '/outgoing',
		),
		'file_name_ext'    => '.txt',
		'file_name_prefix' => array(
			'inventory'           => 'INV',
            'pick_ticket_request'           => 'ORD',
            'pick_ticket_receipt'           => 'ORS',
		),
		'archive_dir'      => array(
			'inventory'           => '/archive/',
            'pick_ticket_receipt' => '/archive/',
		),
        'send_pdfs' => true,
		'vendors'          => array(
			102 => array(),
		),
	);