<?php

return array(
    'debug_mode'       => true,
    'aggregate_stores' => true,
    'server'           => 'ftp.capacityllc.com',
    'username'         => '',
    'password'         => '',
    'protocol'         => 'local',
    'directories'      => array(
        'incoming' => '/Applications/XAMPP/xamppfiles/htdocs/trunk/stores-module/Portal/storeprod/packages/channel_management/supply/orion/local/incoming',
        'outgoing' => '/Applications/XAMPP/xamppfiles/htdocs/trunk/stores-module/Portal/storeprod/packages/channel_management/supply/orion/local/outgoing',
        'inventory' => '/Applications/XAMPP/xamppfiles/htdocs/trunk/stores-module/Portal/storeprod/packages/channel_management/supply/orion/local/outgoing/inventory',
        'pick_ticket_receipt' => '/Applications/XAMPP/xamppfiles/htdocs/trunk/stores-module/Portal/storeprod/packages/channel_management/supply/orion/local/outgoing/Order Status',
        'item_master' => '/Applications/XAMPP/xamppfiles/htdocs/trunk/stores-module/Portal/storeprod/packages/channel_management/supply/orion/local/outgoing/Items Master',
    ),
    'file_name_ext'    => '.txt',
    'file_name_prefix' => array(
        'inventory'           => 'INV',
        'pick_ticket_receipt' => 'ORS',
        'item_master'         => 'ITM',
    ),
    'archive_dir'      => array(
    ),

    // list of vendor ids that this channel services
    // 53 = Raoul
    'vendors'          => array(
        102 => array(),
    ),
);