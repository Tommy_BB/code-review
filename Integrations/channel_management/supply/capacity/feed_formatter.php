<?php
    require_once __DIR__ . "/../../classes/ChannelOrderItem.php";
    require_once __DIR__ . "/../../classes/ChannelOrderItemStatusLog.php";

	class Capacity_Feed_Formatter {

		/**
		 * Render a capacity xml feed
		 *
		 * @param type $feed_type
		 * @param type $vendor_info
		 * @param type $payload
		 *
		 * @return type
		 * @throws Exception
		 */
		public static function render( $feed_type, $payload, $file_prefix = null ) {

			print "generating $feed_type";

			$document_guid = static::generate_document_guid($feed_type, $payload, "{$file_prefix}");

			/** @var DomDocument $xml */
			switch (strtolower($feed_type)) {
                case 'pick_ticket_request':
                    $csv = static::generate_pick_ticket_feed($payload);
                    break;
				default:
					throw new Exception("Undefined feed type: $feed_type");
			}

			$file_prefix      = (isset($file_prefix)) ? $file_prefix : $feed_type;
			$channel_temp_dir = "/tmp/capacity";
			$file_path        = "$channel_temp_dir/$document_guid.txt";

			if (!file_exists($channel_temp_dir)) {
				print "making channel tmp directory $channel_temp_dir\n";
				mkdir($channel_temp_dir, 0777);
			}

            if(file_put_contents($file_path, $csv))
            {
                return $file_path;
            }
		}

		/**
		 * Generate the order ticket csv tab feed
		 *
		 * @param $packing_orders
		 */
		public static function generate_pick_ticket_feed( ChannelOrder $packing_order ) {
			global $ulk;
            $internal_order_data = $packing_order->getInternalOrderData();

            // Get the order details
            $order_details = array();
            $result        = $ulk->getOrderDetails($internal_order_data['ID']);
            while ($order_detail_data = mysqli_fetch_assoc($result)) {
                $order_details[] = $order_detail_data;
            }
            $general_order_details = current($order_details);

            $csv = "";

            $pick_ticket_header_data = array(
                'PickTicketID' => 'OrderNum', //mandatory
                'MasterOrderID' => 'OrderNum',
                'ReleaseFlag' => array("override" => "N"), //mandatory Y/N
                'Reship850Flag' => array("override" => "N"),
                'OrderType' => '',
                'SpecialProcessingFlag' => array("override" => "N"),
                'CapacityImportID' => '',
                'PONumber' => '',
                'RetailerCode' => array("override" => "ZIN"),
                'StoreNumber' => '',
                'ShipToLocation' => '',
                'ClientCode' => array("override" => "BJ"), //mandatory
                'DropshipID' => '',
                'ExternalCustomerID' => '',
                'ShipBusinessName' => 'Ship_Company', //mandatory
                'ShipName' => array('multiple' => array('Ship_FName', 'Ship_LName')), //mandatory
                'ShipAddress1' => 'Ship_Address1', //mandatory
                'ShipAddress2' => 'Ship_Address2',
                'ShipCity' => 'Ship_City', //mandatory
                'ShipState' => 'Ship_State', //mandatory
                'ShipZip' => 'Ship_Zip', //mandatory
                'ShipCountry' => 'Ship_Country',
                'ShipPhone' => 'Ship_Phone',
                'ShipEmail' => array("override" => "emails@zindigo.com"),
                'ShipResidentialFlag' => 'Residential', //mandatory
                'ShipMethod' => 'ShipMethod', //mandatory
                'ThirdPartyAccount' => '',
                'SignatureRequired' => array("override" => "N"),
                'ShipComment' => '',
                'GiftWrapFlag' => array("override" => "N"),
                'GiftFrom' => '',
                'GiftTo' => '',
                'GiftMessage' => '',
                'BillBusinessName' => 'Company',
                'BillName' => array('multiple' => array('FName', 'LName')),
                'BillAddress1' => 'Address1',
                'BillAddress2' => 'Address2',
                'BillCity' => 'City',
                'BillState' => 'State',
                'BillZip' => 'Zip',
                'BillCountry' => 'Country',
                'BillPhone' => 'Phone',
                'PaymentTerms' => '',
                'OrderDate' => 'OrderDate',
                'RequestedShipDate' => '',
                'CancelDate' => '',
                'RequestedDeliveryDate' => '',
                'DeliveryByDate' => '',
                'ShippingAmount' => '',
                'ShippingTaxRate' => '',
                'OrderDiscountRate' => '',
                'OrderDiscountAmount' => '',
                'ItemLineNumber' => '',
                'ItemProductID' => 'StyleNumber', //mandatory
                'ItemUnitAmount' => '',
                'ItemDiscountRate' => '',
                'ItemTaxRate' => '',
                'ItemOrderQuantity' => 'ProductQuantity',
                'ItemShipQuantity' => 'ProductQuantity',
                'BuyerItemNumber' => '',
                'ItemProductDesc' => '',
                'CODFlag' => '',
                'CODAmount' => '',
                'CODAddShipCharges' => '',
                'CODCashierCheck' => '',
                'SaturdayDelivery' => '',
                'EndOfLine' => 'EOL'
            );


            //build the header
            foreach($pick_ticket_header_data as $header => $mapped)
            {
                $csv .= $header . "\t";
            }
            $csv .= PHP_EOL;
            $j = 0;
            foreach ($order_details as $order_detail) {

                if($order_detail['VendorID'] != 109)
                    continue;

                foreach($pick_ticket_header_data as $header => $mapped) {
                    if(is_array($mapped)) {

                        //this is for overrides that cannot be mapped / are static instances
                        if(isset($mapped['override'])) {
                            $csv .= $mapped['override'] . "\t";
                        }

                        //mapping multiple columns and concatting them together
                        if(isset($mapped['multiple'])) {
                            $addedon = "";
                            foreach($mapped['multiple'] as $multiples)
                            {
                                if(isset($order_detail[$multiples])) {
                                    $addedon .= $order_detail[$multiples] . " ";
                                }
                            }
                            if($addedon !== "") {
                                //lets remove any additional spaces we may have added and append to the csv
                                $csv .= rtrim($addedon);
                                $csv .= "\t";
                            }
                        }

                     } else  if($mapped != "") {
                        if(isset($order_detail[$mapped]))
                            $csv .= strip_tags($order_detail[$mapped]) . "\t";
                        else
                            $csv .= strip_tags($mapped) . "\t";
                     } else {
                        $csv .= "\t";
                    }

                    //support for END OF LINE for this record
                    if($header === "EndOfLine")
                    {
                        $csv .= PHP_EOL;
                    }
                }
            }

            $packing_order->ChannelOrderStatusID = 5;
            $packing_order->save();

            return $csv;

		}

		public static function generate_document_guid( $feed_type, $payload, $prefix ) {
			return $prefix . '_' . date("Y_m_d_H_i_s");
		}
	}
