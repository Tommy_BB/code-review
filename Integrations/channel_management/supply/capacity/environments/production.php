<?php

	return array(
		'debug_mode'       => false,
		'aggregate_stores' => true,
		'server'           => 'ftp.capacityllc.com',
		'username'         => '',
		'password'         => '',
		'protocol'         => 'sftp',
		'directories'      => array(
			'inventory' => '/OUT/Zindigo/Inventory',
            'pick_ticket_receipt' => '/OUT/Zindigo/ShipConfirm',
            'pick_ticket_request' => '/IN/Zindigo',
            'outgoing'		=> '/IN/Zindigo',
		),
		'file_name_ext'    => '.txt',
		'file_name_prefix' => array(
			'inventory'           => 'Daily_Inventory_BJ',
            'pick_ticket_request' => 'Zindigo_Order',
            'pick_ticket_receipt' => 'ShipConfirm'
		),
        'archive_dir' => array(),
        'delete_feed' => array(
            'pick_ticket_receipt' => true,
            'inventory' => true
        ),

		// list of vendor ids that this channel services
		'vendors'          => array(
			109 => array(),
		),
	);