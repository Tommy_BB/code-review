<?php

return array(
    'debug_mode'       => true,
    'aggregate_stores' => true,
    'server'           => 'ftp.capacityllc.com',
    'username'         => '',
    'password'         => '',
    'protocol'         => 'local',
    'directories'      => array(
        'incoming' => '/Applications/XAMPP/xamppfiles/htdocs/trunk/stores-module/Portal/storeprod/packages/channel_management/supply/capacity/local/incoming',
        'outgoing' => '/Applications/XAMPP/xamppfiles/htdocs/trunk/stores-module/Portal/storeprod/packages/channel_management/supply/capacity/local/outgoing',
        'pick_ticket_receipt' => '/Applications/XAMPP/xamppfiles/htdocs/trunk/stores-module/Portal/storeprod/packages/channel_management/supply/capacity/local/outgoing',
    ),
    'file_name_ext'    => '.txt',
    'file_name_prefix' => array(
        'inventory'           => 'Daily_Inventory_BJ',
        'pick_ticket_request' => 'Zindigo_Order',
        'pick_ticket_receipt' => 'ShipConfirm'
    ),
    'archive_dir'      => array(
    ),

    // list of vendor ids that this channel services
    // 53 = Raoul
    'vendors'          => array(
        109 => array(),
    ),
);