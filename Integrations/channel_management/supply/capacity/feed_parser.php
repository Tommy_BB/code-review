<?php
	require_once __DIR__ . "/../../classes/ChannelOrderItem.php";
	require_once __DIR__ . "/../../classes/ChannelOrderItemStatusLog.php";

	class Capacity_Feed_Parser {

        public static $csv_inv_fields = array("SKU","AltSKU","Description","OnHand","Commited","Available","Price","Category");

		public static function parse( $feed_type, $raw_csv ) {
			print "parsing $feed_type\n";

            //raw csv file into a csv array
            $csv_lines = explode(PHP_EOL, $raw_csv);
            $end_csv = array();
            foreach ($csv_lines as $csv_line) {
                $end_csv[] = str_getcsv($csv_line,"\t");
            }

			switch (strtolower($feed_type)) {

				case 'inventory':
                    array_shift($end_csv);
					$parse_and_process_result = self::parse_and_process_inventory($end_csv);
					break;
                case 'pick_ticket_receipt':
                    array_shift($end_csv);
                    $parse_and_process_result = self::parse_and_process_receipt($end_csv);
                    break;

				default:
					print "feed type $feed_type doesn't exist\n";
					print "pull capacity [inventory]\n\n";
					break;
			}

			return $parse_and_process_result;
		}

		/**
		 * @param $dom
		 *
		 * @return bool
		 */
		public static function parse_and_process_inventory( $the_csv ) {
            require_once __DIR__ . "/../../../../../classes/products.php";
			global $ulk;
            $products = new products();
            $duped_upcs = array();
            $parsed_upcs = array();
            foreach($the_csv as $csv_line)
            {
                $thesku = $csv_line[array_search('SKU', self::$csv_inv_fields)];
                $qty = $csv_line[array_search('Available', self::$csv_inv_fields)];
                $upc = $csv_line[array_search('AltSKU', self::$csv_inv_fields)];
                if($thesku != null && trim($thesku) != "" && trim($qty) != "")
                {
                    $qty = (int)$qty;
                    if($qty < 0)
                        $qty = 0;
                    $products->UpdateInventoryBySKU("109_".$thesku, $qty);
                    echo "Updating ". $thesku . " with upc " . $upc . " to qty " . $qty . "\r\n";
                    //$ulk->updateInventoryByUPC($qty, mysqli_escape_string($ulk->db_cnx, $upc));
                    if(in_array($upc,$parsed_upcs) && !in_array($upc,$duped_upcs))
                    {
                        $duped_upcs[] = $upc;
                    }
                    $parsed_upcs[] = $upc;
                }
            }

			return true;
		}

        /**
         * @param $dom
         *
         * @return bool
         */
        public static function parse_and_process_receipt( $the_csv ) {
            global $ulk;

            require_once __DIR__ . '/../../../../../classes/orders.php';
            require_once __DIR__ . '/../../../../../classes/email.php';

            $header_layout = array(
                "PickTicketID",
                "MasterOrderID",
                "OrderStatus",
                "OrderStatusDescription",
                "OrderType",
                "OrderSource",
                "CapacityImportID",
                "CapacityPickTicketID",
                "PONumber",
                "RetailerCode",
                "StoreNumber",
                "ShipToLocation",
                "ClientCode",
                "DropshipID",
                "ShipDate",
                "ItemLineNumber",
                "CapacityProductID",
                "CapacityOrderQuantity",
                "ItemShipQuantity",
                "CapacityShippedQuantity",
                "CapacityUOM",
                "CapacityCasePack",
                "ItemProductID",
                "ItemSerialNumber",
                "ShipMethod",
                "ShipCarrier",
                "ShipTracking",
                "ShipWeight",
                "ShipCartons",
                "ShipCost",
                "InvoiceNumber",
                "ProductSubtotalAmount",
                "TotalDiscountAmount",
                "TotalChargeAmount",
                "TotalTaxAmount",
                "TotalInvoiceAmount"
            );

            //first parse the csv and then accumulate it into an array then reparse the array with the inner items and charge the order?

            $csv_order_items = array();
            foreach($the_csv as $csv_line)
            {
                $invoice_number = $csv_line[array_search('MasterOrderID', $header_layout)];
                if(!$invoice_number)
                {
                    print "Could not find: {$invoice_number}\n";
                    continue;
                }
                $sql = <<<SQL
                    SELECT ChannelOrderID
                    FROM ChannelOrder WHERE ExternalOrderID = '$invoice_number'
                    LIMIT 1
SQL;
                $channel_order_res = mysqli_query($ulk->db_cnx, $sql);
                if (!$channel_order_res || $channel_order_res == null || mysqli_num_rows($channel_order_res) <= 0) {
                    print "Could not locate parent channel order for invoice number: {$invoice_number}\n";
                    return false;
                }

                $channel_order_details = mysqli_fetch_assoc($channel_order_res);
                $channel_order_id = $channel_order_details['ChannelOrderID'];
                $items_to_ship = $channel_order_item_find_sqls = array();

                $thesku = (string) $csv_line[array_search('ItemProductID', $header_layout)];
                $qty = (integer) $csv_line[array_search('ItemShipQuantity', $header_layout)];
                $tracking_number = (string) $csv_line[array_search('ShipTracking', $header_layout)];

                if (empty($tracking_number)) {
                    print "No tracking number found for upc: {$thesku}, skipping.\n";
                    continue;
                }

                $order_id = $ulk->getOrderID($invoice_number);
                if (empty($order_id)) {
                    print "No order id could be found for upc: {$thesku}, skiping.\n";
                    continue;
                }

                $order_item = $orders->getOrderItemFromSKU($thesku, $order_id);
                $shipment = mysqli_query($ulk->db_cnx,"SELECT * FROM Shipments WHERE LineID = '".$order_item['ID']."' AND TrackingNumber = '".mysqli_escape_string($ulk->db_cnx,$tracking_number)."'");
                $item_already_shipped = (mysqli_num_rows($shipment) != 0);

                if ($item_already_shipped) {
                    print "Item has already shipped for upc: {$thesku}, skipping.\n";
                    continue;
                }


                $order_item_id = $order_item['ID'];
                $items_to_ship[ $order_item_id ]['ShipQty'] = $qty;
                $items_to_ship[ $order_item_id ]['OrderQty'] = $order_item['Qty'];
                $items_to_ship[ $order_item_id ]['TrackingNumber'] = $tracking_number;
                $items_to_ship[ $order_item_id ]['ShipMethod'] = 1;

                $channel_order_item_find_sqls[] = <<<SQL
				SELECT * FROM
				ChannelOrderItem
				WHERE ChannelOrderID = '$channel_order_id' AND OrderItemID = '$order_item[ID]'
SQL;
                if (empty($items_to_ship)) {
                    print "No items to ship for invoice number: {$invoice_number}\n";
                    return false;
                }

                // Retrieve the store id from the order details
                $order_details_res = $ulk->getOrderDetails($order_id);
                $order_details = mysqli_fetch_assoc($order_details_res);
                if (empty($order_details)) {
                    print "Could not find a valid store id for invoice number: {$invoice_number}\n";

                    return false;
                }
                $store_id = $order_details['StoreID'];

                // Retrieve the store details
                $store_details = $ulk->getStore($order_details['StoreID']);
                if (empty($store_details)) {
                    print "Could not find a valid store for invoice number: {$invoice_number}\n";
                    return false;
                }
                //lets append the orderid with the order items

                if(!is_array($csv_order_items[$order_id]['Items']))
                    $csv_order_items[$order_id]['Items'] = array();

                $csv_order_items[$order_id]['Order'] = $order_details;
                $csv_order_items[$order_id]['Store'] = $store_details;
                $csv_order_items[$order_id]['Items'] = $csv_order_items[$order_id]['Items']  + $items_to_ship;
                $csv_order_items[$order_id]['Tracking'] = $tracking_number;

                //this is what marks them all as completed

                foreach ($channel_order_item_find_sqls as $find_sql) {
                    $channel_order_item_res = mysqli_query($ulk->db_cnx, $find_sql);
                    if ($channel_order_item_res) {
                        $channel_order_item_details = mysqli_fetch_assoc($channel_order_item_res);

                        $channel_order_item = new ChannelOrderItem();
                        $channel_order_item->setAttributes($channel_order_item_details);

                        $order_item = $orders->getOrderItemFromSKU($channel_order_item->getChannelOrderItemSKU(), $order_id);

                        $channel_order_item->setChannelOrderItemStatusID($order_item['ItemStatus']);
                        $channel_order_item->save();

                        $channel_order_item_status_log = new ChannelOrderItemStatusLog();
                        $channel_order_item_status_log->setAttributes(array(
                            'ChannelOrderItemID'       => $channel_order_item->getPk(),
                            'ChannelOrderItemStatusID' => $order_item['ItemStatus'],
                        ));
                        $channel_order_item_status_log->save();
                    }
                }



            }

            foreach($csv_order_items as $the_orderid => $the_items) {
                $order_id = $the_orderid;

                $items_to_ship = $the_items['Items'];
                $order_details = $the_items['Order'];
                $store_details = $the_items['Store'];
                $tracking_number = $the_items['tracking'];
                $store_id = $order_details['StoreID'];
                $email = $order_details['Email'];

                //this is how where we accumulate all order items into an array and process them as a singular object

                $payment_amount = $orders->captureOrderItems($order_id, $store_id, $items_to_ship);

                $orders->shipOrderItems($items_to_ship);

                $_SESSION['orderinfo'] = (object) array(
                    'ship_fname' => $order_details['FName'],
                    'ship_lname' => $order_details['LName'],
                );
                $email = $order_details['Email'];

                $message = "";
                $post = array(
                    "orderaction"    => "CustomerShipNotice",
                    "orderid"        => $order_id,
                    "ordernote"      => "",
                    "storeid"        => $store_id,
                    "pmtamount"      => $payment_amount,
                    'TrackingNumber' => $tracking_number,
                    'sitems'         => $items_to_ship,
                    "to"             => $email,
                    "cc"             => null,
                    "bcc"            => null,
                    "orderid"        => $order_id,
                    "storeid"        => $store_id,
                    "storename"      => $store_details['Name'],
                    "storeemail"     => $store_details['Email']
                );


                $email = new EMAIL;
                $email->EmailCustomerUpdate($post, $message, $ulk);



            }



            /*
            foreach($the_csv as $csv_line)
            {
                $invoice_number = $csv_line[array_search('MasterOrderID', $header_layout)];
                if(!$invoice_number)
                {
                    print "Could not find: {$invoice_number}\n";
                    return false;
                }
                $sql = <<<SQL
                    SELECT ChannelOrderID
                    FROM ChannelOrder WHERE ExternalOrderID = '$invoice_number'
                    LIMIT 1
SQL;
                $channel_order_res = mysqli_query($ulk->db_cnx, $sql);
                if (!$channel_order_res || $channel_order_res == null || mysqli_num_rows($channel_order_res) <= 0) {
                    print "Could not locate parent channel order for invoice number: {$invoice_number}\n";
                    return false;
                }

                $channel_order_details = mysqli_fetch_assoc($channel_order_res);
                $channel_order_id = $channel_order_details['ChannelOrderID'];
                $items_to_ship = $channel_order_item_find_sqls = array();

                $thesku = (string) $csv_line[array_search('ItemProductID', $header_layout)];
                $qty = (integer) $csv_line[array_search('ItemShipQuantity', $header_layout)];
                $tracking_number = (string) $csv_line[array_search('ShipTracking', $header_layout)];

                if (empty($tracking_number)) {
                    print "No tracking number found for upc: {$thesku}, skipping.\n";
                    continue;
                }

                $order_id = $ulk->getOrderID($invoice_number);
                if (empty($order_id)) {
                    print "No order id could be found for upc: {$thesku}, skiping.\n";
                    continue;
                }

                $order_item = $orders->getOrderItemFromSKU($thesku, $order_id);
                $shipment = mysqli_query($ulk->db_cnx,"SELECT * FROM Shipments WHERE LineID = '".$order_item['ID']."' AND TrackingNumber = '".mysqli_escape_string($ulk->db_cnx,$tracking_number)."'");
                $item_already_shipped = (mysqli_num_rows($shipment) != 0);
                if ($item_already_shipped) {
                    print "Item has already shipped for upc: {$thesku}, skipping.\n";
                    continue;
                }

                $order_item_id = $order_item['ID'];
                $items_to_ship[ $order_item_id ]['ShipQty'] = $qty;
                $items_to_ship[ $order_item_id ]['OrderQty'] = $order_item['Qty'];
                $items_to_ship[ $order_item_id ]['TrackingNumber'] = $tracking_number;
                $items_to_ship[ $order_item_id ]['ShipMethod'] = 1;

                $channel_order_item_find_sqls[] = <<<SQL
				SELECT * FROM
				ChannelOrderItem
				WHERE ChannelOrderID = '$channel_order_id' AND OrderItemID = '$order_item[ID]'
SQL;
                if (empty($items_to_ship)) {
                    print "No items to ship for invoice number: {$invoice_number}\n";
                    return false;
                }

                // Retrieve the store id from the order details
                $order_details_res = $ulk->getOrderDetails($order_id);
                $order_details = mysqli_fetch_assoc($order_details_res);
                if (empty($order_details)) {
                    print "Could not find a valid store id for invoice number: {$invoice_number}\n";

                    return false;
                }
                $store_id = $order_details['StoreID'];

                // Retrieve the store details
                $store_details = $ulk->getStore($order_details['StoreID']);
                if (empty($store_details)) {
                    print "Could not find a valid store for invoice number: {$invoice_number}\n";
                    return false;
                }

                // Process the credit card
                $payment_amount = $orders->captureOrderItems($order_id, $store_id, $items_to_ship);

                // Marks the items as shipped
                $orders->shipOrderItems($items_to_ship);

                $_SESSION['orderinfo'] = (object) array(
                    'ship_fname' => $order_details['FName'],
                    'ship_lname' => $order_details['LName'],
                );
                $email = $order_details['Email'];

                $message = "";
                $post = array(
                    "orderaction"    => "CustomerShipNotice",
                    "orderid"        => $order_id,
                    "ordernote"      => "",
                    "storeid"        => $store_id,
                    "pmtamount"      => $payment_amount,
                    'TrackingNumber' => $tracking_number,
                    'sitems'         => $items_to_ship,
                    "to"             => $email,
                    "cc"             => null,
                    "bcc"            => null,
                    "orderid"        => $order_id,
                    "storeid"        => $store_id,
                    "storename"      => $store_details['Name'],
                    "storeemail"     => $store_details['Email']
                );

                $email = new EMAIL;
                $email->EmailCustomerUpdate($post, $message, $ulk);

                print "Email sent for invoice number: {$invoice_number}\n";

                // Add the status record in order to track the changes
                foreach ($channel_order_item_find_sqls as $find_sql) {
                    $channel_order_item_res = mysqli_query($ulk->db_cnx, $find_sql);
                    if ($channel_order_item_res) {
                        $channel_order_item_details = mysqli_fetch_assoc($channel_order_item_res);

                        $channel_order_item = new ChannelOrderItem();
                        $channel_order_item->setAttributes($channel_order_item_details);

                        $order_item = $orders->getOrderItemFromSKU($channel_order_item->getChannelOrderItemSKU(), $order_id);

                        $channel_order_item->setChannelOrderItemStatusID($order_item['ItemStatus']);
                        $channel_order_item->save();

                        $channel_order_item_status_log = new ChannelOrderItemStatusLog();
                        $channel_order_item_status_log->setAttributes(array(
                            'ChannelOrderItemID'       => $channel_order_item->getPk(),
                            'ChannelOrderItemStatusID' => $order_item['ItemStatus'],
                        ));
                        $channel_order_item_status_log->save();
                    }
                }
            }

            */
            return true;
        }
	}
