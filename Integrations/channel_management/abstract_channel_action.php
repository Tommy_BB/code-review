<?php

	error_reporting(E_ALL);

	require_once(__DIR__ . "/../file_transport/file_transporter.php");

	abstract class Abstract_Channel_Action {

		public $_channel_name;
		public $_channel_code;
		public $_channel_config;
		public $_channel_feed_parser;
		public $_channel_feed_formatter;
		public $_feed_type;
		public $_file_transporter;
		public $_channel_interaction_id;
		private $_environment;

		public function __construct( $channel_name, $feed_type, $environment ) {
			$this->init($channel_name, $feed_type, $environment);
		}

		private function init( $channel_name, $feed_type, $environment ) {
			$this->_feed_type    = trim($feed_type);
			$this->_channel_name = trim($channel_name);
			$this->_channel_code = strtoupper($this->_channel_name);
			$this->_environment  = $environment;
			$this->load_config($environment);
			$this->load_file_transporter();
		}

		private function load_config( $environment ) {
			if (!file_exists(__DIR__ . "/{$this->getChannelType()}/{$this->_channel_name}/environments/{$environment}.php")) {
				print "unknown channel";
				throw new Exception("unknown channel '{$this->_channel_name}'\n");
			}

			$config_file = "/{$this->getChannelType()}/{$this->_channel_name}/environments/{$environment}.php";
			$config_path = __DIR__ . $config_file;
			print "loading $config_file\n";
			$this->_channel_config = include_once($config_path);

			if (!$this->_channel_config) {
				throw new Exception("Error: channel config not found");
			}
		}

		/**
		 * Starts a channel action and returns an instance of itself
		 *
		 * Note: each channel specifies a configuration file
		 * describing the location of the data
		 * and the naming convention for each type of feed
		 *
		 * @param type $action
		 * @param type $channel_name
		 * @param type $feed_type
		 * @param type $environment
		 *
		 * @return static
		 * @throws Exception
		 */
		public static function start( $action, $channel_name, $feed_type, $environment, $param1 = null ) {

			print "starting $action $channel_name $feed_type $environment $param1\n";

			if ($action == 'push' || $action == 'pull') {
				$environment  = (empty($environment)) ? 'development' : $environment;
				$the_instance = new static($channel_name, $feed_type, $environment);
				$the_instance->$action($param1);

				return $the_instance;
			}

			throw new Exception("unknown action '$action', try (push|pull)\n");
		}

		abstract protected function getChannelType();

		private function load_file_transporter() {
			$protocol = $this->_channel_config['protocol'];
			if (isset($protocol)) {
				print "creating $protocol file transporter\n";
				$this->_file_transporter = File_Transporter::forge($protocol);
			}
		}

		public function prepare_channel_parser() {

			if (isset($this->_channel_feed_parser)) {
				return true;
			}

			// 1. LOAD CHANNEL SPECIFIC PARSER
			//
			$channel_class_prefix       = ucfirst(strtolower($this->_channel_name));
			$this->_channel_feed_parser = $channel_class_prefix . '_Feed_Parser';
			$class_path                 = __DIR__ . "/{$this->getChannelType()}/{$this->_channel_name}/feed_parser.php";

			if (file_exists($class_path)) {
//			print "loading $class_path\n";
				include $class_path;
			} else {
				print "channel {$this->_channel_name} feed parser not found\n";
				throw new Exception("channel {$this->_channel_name} feed parser not found");
			}
		}

		/**
		 * Fetch a feed from a channel using the file transporter instance
		 *
		 * @param type $connection_config
		 *
		 * @return type
		 * @throws Exception
		 */
		public function fetch_feed( $connection_config ) {

			// FIGURE OUT WHERE TO LOOK
			//
			print "pulling {$this->_feed_type} from {$this->_channel_name}\n";

			// check configuration for type specific directory path
			//
			if (isset($this->_channel_config['directories'][ $this->_feed_type ])) {
				// use feed specific directory
				$remote_dir = $this->_channel_config['directories'][ $this->_feed_type ];
			} else {
				// use fallback incoming directory
				$remote_dir = $this->_channel_config['directories']['incoming'];
			}

			$remote_pattern = $this->_channel_config['file_name_prefix'][ $this->_feed_type ] . '*';

			// FIND LATEST FILE MATCHING PATTERN
			//
			print "listing $remote_dir $remote_pattern\n";
			$filename = $this->_file_transporter->get_latest_filename($remote_dir, $remote_pattern, $connection_config);

			if (!$filename) {
				throw new Exception("Error: no file matching pattern '$remote_pattern' found.");
			}

			// CREATE AN INTERACTION LOG
			//
			$this->create_channel_interaction_record($this->_feed_type);

			// FETCH FEED
			//
			$file_content = $this->_file_transporter->get_file_contents($filename, $connection_config);

			// LOG FEED IN DB
			//
			$this->update_channel_interaction_record($file_content, $remote_dir . $filename, "");


            $delete_feed = $connection_config['delete_feed'][ $this->_feed_type ];
            if (isset($delete_feed) && $delete_feed !== false) {
                print "deleting {$this->_feed_type} filename {$filename}\n";
                $this->_file_transporter->delete_file($filename);
            }

			// ARCHIVE REMOTE FILE (if necessary)

			$archive_dir = $connection_config['archive_dir'][ $this->_feed_type ];
			if (isset($archive_dir)) {
				print "moving {$this->_feed_type} from $filename to {$archive_dir}{$filename}\n";
				$this->_file_transporter->move_file($filename, $archive_dir . $filename, $channel_config);
			}



            //undo above when done

			print "[DONE]\n";

			return $file_content;
		}

		public function in_debug_mode() {
			return (isset($this->_channel_config['debug_mode']) && $this->_channel_config['debug_mode']);
		}

		/**
		 * Deliver a local file to the remote connection using the file transporter instance
		 *
		 * @param type $generated_file
		 * @param type $connection_config
		 *
		 * @return type
		 */
		public function deliver_feed( $generated_file, $connection_config ) {

			// UPLOAD FEED
			//
			if (isset($this->_channel_config['directories'][ $this->_feed_type ])) {
				// use feed specific directory
				$remote_dir = $this->_channel_config['directories'][ $this->_feed_type ];
			} else {
				// use fallback outgoing directory
				$remote_dir = $this->_channel_config['directories']['outgoing'];
			}

			$remote_filename = basename($generated_file);

			if ($this->in_debug_mode()) {
				print "\n\nDEBUG MODE\n";
				print "exiting before transmitting to {$remote_dir}{$remote_filename}\n";
				print "less $generated_file\n";
				print "\n" . $this->random_explitive() . "\n";
				die();
			}

			if ($this->_file_transporter->put_file($generated_file, $remote_dir, $remote_filename, $connection_config)) {
				print "\nFEED SENT\n";

				return $remote_dir . $remote_filename;
			} else {
				die("CRITICAL ERROR: sftp_put operation failed to transmit feed to remote server\n");
			}
		}

		public function parse_feed( $content ) {

			// 1. LOAD PARSER
			//
			$this->prepare_channel_parser();


			// 2. PARSE
			//
			print "parsing feed {$this->_feed_type} using {$this->_channel_feed_parser}\n";
			$parser      = $this->_channel_feed_parser;
			$parsed_data = $parser::parse($this->_feed_type, $content);
			print "[DONE]\n";

			return $parsed_data;
		}

		public function create_channel_interaction_record( $interaction_type, $interaction_payload = "", $remote_path = "", $local_path = "" ) {
			global $ulk;

			$interaction_type_escaped    = addslashes($interaction_type);
			$local_path_escaped          = addslashes($local_path);
			$remote_path_escaped         = addslashes($remote_path);
			$interaction_payload_escaped = addslashes($interaction_payload);

			$sql    = "INSERT INTO ChannelInteraction ("
			          . "ChannelCode,"
			          . "InteractionType,"
			          . "InteractionStartedTS,"
			          . "InteractionCompletedTS,"
			          . "InteractionRemotePath,"
			          . "InteractionLocalPath,"
			          . "InteractionPayload"
			          . ") VALUES ("
			          . "'{$this->_channel_code}',"
			          . "'$interaction_type_escaped',"
			          . "CURRENT_TIMESTAMP(),"
			          . "NULL,"
			          . "'$remote_path_escaped',"
			          . "'$local_path_escaped',"
			          . "'$interaction_payload_escaped'"
			          . ")";
			$result = mysqli_query($ulk->db_cnx, $sql);
			if ($result) {
				$this->_channel_interaction_id = mysqli_insert_id($ulk->db_cnx);
				print "starting interaction id: {$this->_channel_interaction_id}\n";

				return $this->_channel_interaction_id;
			} else {
				print "error: " . mysqli_error($ulk->db_cnx) . "\n";
			}

			return false;
		}

		public function update_channel_interaction_record( $interaction_payload, $remote_path, $local_path ) {
			global $ulk;

			if (!isset($this->_channel_interaction_id)) {
				return false;
			}

			$local_path_escaped          = addslashes($local_path);
			$remote_path_escaped         = addslashes($remote_path);
			$interaction_payload_escaped = addslashes($interaction_payload);

			$sql = "UPDATE ChannelInteraction SET "
			       . "InteractionCompletedTS = CURRENT_TIMESTAMP(),"
			       . "InteractionRemotePath = '$remote_path_escaped',"
			       . "InteractionLocalPath = '$local_path_escaped',"
			       . "InteractionPayload = '$interaction_payload_escaped'"
			       . "WHERE ChannelInteractionID = '{$this->_channel_interaction_id}'";
			if (mysqli_query($ulk->db_cnx, $sql)) {
				return true;
			} else {
				print "error: " . mysqli_error($ulk->db_cnx) . "\n";
			}

			return false;
		}

		private function random_explitive() {
			$vocabulary = array('Dang', 'Bam', 'Go Team', 'Sheboigan', 'Snap', 'Wham', 'Oh Yeah', 'Disco');
			$offset     = round(rand(0, count($vocabulary) - 1));

			return $vocabulary[ $offset ] . '!';
		}


	}
