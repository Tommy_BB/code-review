<?php
    require_once __DIR__ . "/../../classes/ChannelOrderItem.php";
    require_once __DIR__ . "/../../classes/ChannelOrderItemStatusLog.php";

	class Bluefly_Feed_Formatter {


		public static function generate_document_guid( $prefix ) {
			return $prefix . date("Y-m-d_H-i");
		}

		/**
		 * Render a bluefly xml feed
		 *
		 * @param type $feed_type
		 * @param type $vendor_info
		 * @param type $payload
		 *
		 * @return type
		 * @throws Exception
		 */
		public static function render( $feed_type, $payload, $file_prefix = null ) {

			print "bluefly XML feed formatter is rendering $feed_type feed...\n";

			$file_prefix = (isset($file_prefix)) ? $file_prefix : $feed_type;
			$document_guid = static::generate_document_guid($file_prefix);
			$xml_feed = static::generate_header($document_guid);
			switch (strtolower($feed_type)) {
				case 'inventory':
					$xml_feed .= static::generate_inventory_feed($payload);
					break;
				case 'price':
					$xml_feed .= static::generate_pricing_feed($payload);
					break;
				case 'product':
					$xml_feed .= static::generate_product_feed($payload);
					break;
				case 'order_status':
					$xml_feed .= static::generate_order_status_feed($payload);
					break;
				case 'refund_requests':
					$xml_feed .= static::generate_refund_request_feed($payload);
					break;
                case 'custom_oos':
                    $xml_feed .= static::custom_oos();
                    break;
				default:
					throw new Exception("Undefined feed type: $feed_type");
			}
			$xml_feed .= "</feed>\n";
			$channel_temp_dir = "/tmp/bluefly";
			$file_path = "$channel_temp_dir/$document_guid.xml";

			if (!file_exists($channel_temp_dir)) {
				print "making channel tmp directory $channel_temp_dir\n";
				mkdir($channel_temp_dir, 0777);
			}
			if (file_put_contents($file_path, $xml_feed)) {
				return $file_path;
			}
		}

        /*
         * This is where we send a custom out of stock feed we define variables in there.
         */
        private static function custom_oos()
        {
            $xml = "<Listings>\n";
            $remove_prods = array();
            $fp = fopen(__DIR__ . "/lib/BFSKUS.csv", 'r');
            while (($data = fgetcsv($fp, 99999999, ",")) !== FALSE)
            {
                $remove_prods[] = $data[0];
            }
            /*

            $messedupSKUS = array(
                "102_LV5F61243-Heather Grey",
                "102_LV6S9495",
            );

            $fixProperties = array(
                "-XS",
                "-S",
                "-M",
                "-L",
                "-XL"
            );

            $remove_prods = array();

            foreach($messedupSKUS as $messed){
                foreach($fixProperties as $prerps)
                {
                    $remove_prods[] = $messed . $prerps;
                }
            }
            */

            $count = 0;
            foreach($remove_prods as $thisprod) {
                $count++;
                $xml .= <<<EOLISTING
    <Listing action="update" MessageID="MessageID$count">
        <ProductReference>
            <ProductSKU>{$thisprod}</ProductSKU>
        </ProductReference>
        <ListingSource>
            <ListingID>
                <Identifier>{$thisprod}</Identifier>
            </ListingID>
        </ListingSource>
        <ListingOnlineFlag>true</ListingOnlineFlag>
        <InventoryData>
            <Quantity>0</Quantity>
            <FulfillmentLatency>2</FulfillmentLatency>
        </InventoryData>
    </Listing>\n
EOLISTING;
            }

            $xml .= "</Listings>";

            return $xml;
        }

		/**
		 * @param $accepted_returns
		 *
		 * @return string
		 */
		private static function generate_refund_request_feed( $accepted_returns ) {
            global $ulk;
			$refund_request_items_xml = "";
			$count = 1;
			foreach ($accepted_returns as $return) {

                /* UPDATE RMA SO WE CAN RELAY IT BACK TO VENDOR IF ACCEPTED */
                $find_sql = <<<SQL
				SELECT coi.* FROM
				ChannelOrder co JOIN
				ChannelOrderItem coi ON coi.ChannelOrderID = co.ChannelOrderID JOIN
				OrderDetails od ON od.ID = coi.OrderItemID
				WHERE co.ExternalOrderID = '{$return[ExternalOrderID]}' AND coi.ExternalOrderItemID = '{$return[ExternalOrderItemID]}' AND od.SKU = '{$return[ProductSKU]}'
SQL;
                $channel_order_item_res = mysqli_query($ulk->db_cnx, $find_sql);
                if ($channel_order_item_res) {
                    $channel_order_item_details = mysqli_fetch_assoc($channel_order_item_res);
                    $channel_order_item = new ChannelOrderItem();
                    $channel_order_item->setAttributes($channel_order_item_details);
                    $channel_order_item->setChannelOrderItemReturnedInd(1);
                    $channel_order_item->save();
                }

				$requested_date = date('Y-m-d\Th:m:s\Z', strtotime($return['RequestedDate']));

				$refund_request_items_xml .= <<<XML
<RefundRequest MessageID="MessageID{$count}">
	<RMA>$return[ChannelRMA]</RMA>
	<RefundDate>$requested_date</RefundDate>
	<OrderID>$return[ExternalOrderID]</OrderID>
	<Currency>$return[Currency]</Currency>
	<RefundType>$return[RefundType]</RefundType>
	<RefundRequestItem>
		<ListingID>
			<EffectiveSupplierID>Zindigo</EffectiveSupplierID>
			<Identifier>$return[ProductSKU]</Identifier>
		</ListingID>
		<OrderItemID>$return[ExternalOrderItemID]</OrderItemID>
		<RefundQuantity>$return[Quantity]</RefundQuantity>
	</RefundRequestItem>
</RefundRequest>
XML;
				++$count;
			}

			return <<<XML
	<RefundRequests>
		$refund_request_items_xml
	</RefundRequests>
XML;
		}

		/**
		 * Generate an xml feed header for a zindigo brand in bluefly
		 *
		 * @param type $vendor_info
		 * @param type $inventory_items
		 *
		 * @return XML string
		 */
		private static function generate_header( $document_guid ) {

			$document_creation_date = date('Y-m-d\Th:m:s\Z');  //2014-02-21T09:40:20Z

			$xml = <<< EOHEADER
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.bluefly.com/xml/marketplaceapi/R1">
  <Header>
    <DocumentGUID>$document_guid</DocumentGUID>
    <DocumentCreationDate>$document_creation_date</DocumentCreationDate>
    <SenderID>ZINDIGO</SenderID>
    <SenderName>ZINDIGO</SenderName>
    <SourceChannelID>ZINDIGO</SourceChannelID>
    <ReceiverID>Bluefly</ReceiverID>
    <DestinationChannelID>bluefly</DestinationChannelID>
  </Header>\n
EOHEADER;

			return $xml;
		}

		/**
		 * Generate a bluefly inventory xml feed
		 *
		 * @param type $vendor_info
		 * @param type $inventory_items
		 *
		 * @return XML string
		 */
		private static function generate_inventory_feed( $inventory_items ) {

			if (!isset($inventory_items)) {
				return "";
			}

			$xml = "<Listings>\n";
			$count = 0;
			foreach ($inventory_items as $item) {
				if ($item['Qty'] < 1) {
					//continue;
				}
                if(strlen(trim($item['ProductSKU'])) <= 0)
                {
                    continue;
                }

				$count ++;
				$xml .= <<<EOLISTING
<Listing action="update" MessageID="MessageID$count">
	<ProductReference>
		<ProductSKU>{$item['ProductSKU']}</ProductSKU>
	</ProductReference>
	<ListingSource>
		<ListingID>
			<Identifier>{$item['ProductSKU']}</Identifier>
		</ListingID>
	</ListingSource>
	<ListingOnlineFlag>true</ListingOnlineFlag>
	<InventoryData>
		<Quantity>{$item['Qty']}</Quantity>
		<FulfillmentLatency>2</FulfillmentLatency>
	</InventoryData>
</Listing>\n
EOLISTING;
			}

			$xml .= "</Listings>";

			return $xml;
		}


		/**
		 * Generate a bluefly pricing xml feed
		 *
		 * @param type $vendor_info
		 * @param type $inventory_items
		 *
		 * @return XML string
		 */
		private static function generate_pricing_feed( $inventory_items ) {
            global $discounts;
			if (!isset($inventory_items)) {
				return "";
			}
            $checkedsaleprices = array();

//<Listings>
//    <Listing action="update" MessageID="MessageID1">
//      <ProductReference>
//        <ProductSKU>ProductSKU</ProductSKU>
//      </ProductReference>
//      <ListingSource>
//        <ListingID>
//          <EffectiveSupplierID>Sender ID</EffectiveSupplierID>
//          <Identifier>identifier0 - Supplier SKU</Identifier>
//        </ListingID>
//        <SupplierName>Sender Name</SupplierName>
//      </ListingSource>
//      <ListingOnlineFlag>false</ListingOnlineFlag>
//      <PriceData>
//        <ListPrice currency="usd">45.00</ListPrice>
//        <MSRP currency="usd">40.00</MSRP>
//        <SalePrice>
//          <Price currency="usd">42.00</Price>
//          <StartDate>2012-01-04T18:13:51.0Z</StartDate>
//          <EndDate>2012-01-04T18:13:51.0Z</EndDate>
//        </SalePrice>
//      </PriceData>
//    </Listing>		


			$xml = "<Listings>\n";
			$count = 0;
            $start_date = date('Y-m-d\Th:m:s\Z', time());
            $end_date = date('Y-m-d\Th:m:s\Z', strtotime(date('Y').'-12-31 23:59:59'));
			foreach ($inventory_items as $item) {

				if (!isset($item['ProductSKU']) || empty($item['ProductSKU'])) {
					continue;
				}
                if(!isset($checkedsaleprices[$item['ProductID']]) && is_numeric($item['ProductID']) && $item['ProductID'] != "") {

                    $saleprice = $discounts->getSalePrice($item['ProductID'], $item['StoreID']);
                    $checkedsaleprices[$item['ProductID']] = $saleprice;
                }
                else
                    $saleprice = $checkedsaleprices[$item['ProductID']];
                $count++;

                if (is_numeric($saleprice) && $saleprice != "" && $saleprice < $item['ListPrice']) {
                    $xml .= <<<EOLISTING
                    <Listing action="update" MessageID="MessageID$count">
	                    <ProductReference>
		                <ProductSKU>{$item['ProductSKU']}</ProductSKU>
	                </ProductReference>
	                <ListingSource>
		                <ListingID>
			                <EffectiveSupplierID>Zindigo</EffectiveSupplierID>
			                <Identifier>{$item['ProductSKU']}</Identifier>
		                </ListingID>
                        <SupplierName>Sender Name</SupplierName>
	                </ListingSource>
	                <ListingOnlineFlag>true</ListingOnlineFlag>
                        <PriceData>
                            <ListPrice currency="usd">{$item['ListPrice']}</ListPrice>
                            <MSRP currency="usd">{$item['ListPrice']}</MSRP>
                            <SalePrice>
                                <Price currency="usd">{$saleprice}</Price>
                                <StartDate>{$start_date}</StartDate>
                                <EndDate>{$end_date}</EndDate>
                            </SalePrice>
                        </PriceData>
                    </Listing>\n
EOLISTING;
                } else {
                    $xml .= <<<EOLISTING
                    <Listing action="update" MessageID="MessageID$count">
	                    <ProductReference>
		                <ProductSKU>{$item['ProductSKU']}</ProductSKU>
	                </ProductReference>
	                <ListingSource>
		                <ListingID>
			                <EffectiveSupplierID>Zindigo</EffectiveSupplierID>
			                <Identifier>{$item['ProductSKU']}</Identifier>
		                </ListingID>
                        <SupplierName>Sender Name</SupplierName>
	                </ListingSource>
	                <ListingOnlineFlag>true</ListingOnlineFlag>
                        <PriceData>
                            <ListPrice currency="usd">{$item['ListPrice']}</ListPrice>
                            <MSRP currency="usd">{$item['ListPrice']}</MSRP>
                        </PriceData>
                    </Listing>\n
EOLISTING;
                }
			}

			$xml .= "</Listings>";

			return $xml;
		}


		/**
		 * Generate a bluefly product xml feed
		 *
		 * @param type $inventory_items
		 *
		 * @return XML string
		 */
		private static function generate_product_feed( $products ) {

			if (!isset($products)) {
				return "";
			}

			$xml = "<Products>\n";
			foreach ($products as $product) {

                //we will not send custom items
                if((int)$product['isCustom'] == 1) continue;

				$product['ProductDesc'] = strip_tags(html_entity_decode($product['ProductDesc']));

				$xml_variant1 = $product['PropertyName1'] ? "<VariationAttributeName localized=\"false\">{$product['PropertyName1']}</VariationAttributeName>" : "";
				$xml_variant2 = $product['PropertyName2'] ? "<VariationAttributeName localized=\"false\">{$product['PropertyName2']}</VariationAttributeName>" : "";

				$xml .= <<<END_OF_PRODUCT_HEADER
    <ProductContentMessage SKU="{$product['StyleSKU']}">
      <ProductContent>
        <OnlineFlag>true</OnlineFlag>
        <ProductData>
          <ProductIDs/>
          <VariationType>Parent</VariationType>
          <Variations>
            <VariationTheme>
				$xml_variant1
				$xml_variant2
            </VariationTheme>
END_OF_PRODUCT_HEADER;

				foreach ($product['variants'] as $variant) {
					if (!isset($variant['ProductSKU']) || empty($variant['ProductSKU'])) {
						continue;
					}

					$xml_variant1_val = $product['PropertyName1'] ? "<Attribute domain=\"CommonTaxonomy\" name=\"{$product['PropertyName1']}\">{$variant['PropertyValue1']}</Attribute>" : "";
					$xml_variant2_val = $product['PropertyName2'] ? "<Attribute domain=\"CommonTaxonomy\" name=\"{$product['PropertyName2']}\">{$variant['PropertyValue2']}</Attribute>" : "";

					$variant['Material'] = (isset($variant['Material'])) ? $variant['Material'] : 'See Details';

					$xml .= <<<END_OF_VARIANT
			<Variation>
			  <VariationSKU>{$variant['ProductSKU']}</VariationSKU>
			  <VariationContent>
				<OnlineFlag>true</OnlineFlag>
				<ProductData>
				  <ProductIDs>
					<UPC>{$variant['UPC']}</UPC>
				  </ProductIDs>
				  <VariationType>Product</VariationType>
				  <Title>
					<value lang="en-us">{$product['ProductName']}</value>
				  </Title>
				  <Description>
					<value lang="en-us">{$product['ProductDesc']}</value>
				  </Description>
                  <ShortDescription>
					<value lang="en-us">{$product['ProductDesc']}</value>
				  </ShortDescription>
END_OF_VARIANT;

                    if(isset($product['Productspecs']) && $product['Productspecs'] != "")
                    {
                        $bulletList = explode(PHP_EOL, $product['Productspecs']);
                        if(count($bulletList) > 0)
                        {
                            $xml .= <<<END_OF_VARIANT

                                <BulletPoints>
                                    <values lang="en-us">

END_OF_VARIANT;
                            foreach($bulletList as $abullet)
                            {
                                $abullet = str_ireplace("\u2022 ","",$abullet);
                                $abullet = str_ireplace("&bull; ","",$abullet);
                                $abullet = str_ireplace("• ","",$abullet);
                                $abullet = str_ireplace("\u2022","",$abullet);
                                $abullet = str_ireplace("&bull;","",$abullet);
                                $abullet = str_ireplace("•","",$abullet);
                                $abullet = str_ireplace(array("\r", "\n"),"",$abullet);
                                $abullet = str_ireplace("&","and",$abullet);
                                $abullet = str_ireplace("<br/>","and",$abullet);
                                $abullet = str_ireplace("<br />","and",$abullet);
                                $abullet = str_ireplace("<br>","and",$abullet);
                                $abullet = utf8_encode($abullet);
                                $xml .= <<<END_OF_VARIANT
                                        <value>{$abullet}</value>

END_OF_VARIANT;
                            }
                            $xml .= <<<END_OF_VARIANT
                                    </values>
                                </BulletPoints>

END_OF_VARIANT;
                        }
                    }

                    $returnDays = $product['FinalSale'] == 1 ? 0 : 21;

                    $xml .= <<<END_OF_VARIANT
				  <Brand>{$product['Manufacturer']}</Brand>
				  <CountryOfOrigin>{$product['Country_Code']}</CountryOfOrigin>
				  <ReturnPolicy>
				        <MaxReturnDays>{$returnDays}</MaxReturnDays>
				  </ReturnPolicy>
				  $xml_variant1_val
				  $xml_variant2_val
				  <Attribute domain="CommonTaxonomy" name="material">{$variant['Material']}</Attribute>
				</ProductData>
				<ImageURLData>
END_OF_VARIANT;


                    /*
					$image_type = 'Main';
					foreach ($product['images'] as $image) {
						$xml .= "\n<ImageURL imageType='$image_type'>http://stores.zindigo.com/{$image['ImagePath']}</ImageURL>\n";

						$image_type = ($image_type == 'Main') ? 2 : $image_type ++;
					}
                    */

                    //alwats put the default image first then the rest if they have >= 2more
                    $usedImages = array();
                    $firstKey = key($product['images']);
                    $usedImages[] = $firstKey;
                    $xml .= "\n<ImageURL imageType='Main'>http://stores.zindigo.com/{$firstKey}</ImageURL>\n";
                    $prodImages = explode(",", $product['ProductImages']);
                    $prodImages = array_reverse($prodImages);

                    if(count($prodImages) > 0)
                    {
                        $imgCount = 2;
                        foreach ($prodImages as $image) {
                            if(!in_array($image,$usedImages) && $imgCount <= 3)
                            {
                                $usedImages[] = $image;
                                $xml .= "\n<ImageURL imageType='". $imgCount ."'>http://stores.zindigo.com/{$image}</ImageURL>\n";
                                $imgCount++;
                            }
                        }
                    }


					$xml .= "</ImageURLData>\n"

					        . "<CategoryAssignmentData>\n";

					// product categories
					//
					foreach ($product['categories'] as $category) {
						$xml .= "\n<CategoryAssignment>"
						        . "<CategoryID>{$category['ZindigoCategoryID']}</CategoryID>\n"
						        . "<CategoryName lang='en-us'>{$category['ZindigoCategoryName']}</CategoryName>\n"
						        . "</CategoryAssignment>\n";
					}

					$xml .= "
				</CategoryAssignmentData>
			  </VariationContent>
			</Variation>\n";
				}

				$xml .= <<<END_OF_PRODUCT
          </Variations>
          <Title>
            <value lang="en-us">{$product['ProductName']}</value>
          </Title>
        </ProductData>
        <ImageURLData>
END_OF_PRODUCT;
				$image_type = 'Main';
				foreach ($product['images'] as $image) {
					$xml .= "\n<ImageURL imageType='$image_type'>http://stores.zindigo.com/{$image['ImagePath']}</ImageURL>";
					$image_type = ($image_type == 'Main') ? 2 : $image_type ++;
				}
				$xml .= <<<END_OF_PRODUCT
        </ImageURLData>
        <CategoryAssignmentData>
          <CategoryAssignment>
            <CategoryID>{$product['ZindigoCategoryID']}</CategoryID>
            <CategoryName lang="en-us">{$product['ZindigoCategoryName']}</CategoryName>
            <SortOrder>{$product['CategorySortOrder']}</SortOrder>
          </CategoryAssignment>
        </CategoryAssignmentData>
      </ProductContent>
    </ProductContentMessage>

END_OF_PRODUCT;
			}

			$xml .= "</Products>\n";

			return $xml;
		}


		/**
		 * Generate a bluefly order status update xml feed
		 *
		 * @param type $order_status_items
		 *
		 * @return XML string
		 */
		private static function generate_order_status_feed( $order_status_items ) {
            global $ulk;
			if (!isset($order_status_items)) {
				return "";
			}
			$date = date('Y-m-d\Th:m:s\Z');

			$xml = "<OrderStatus>\n";
			$count = 0;

			foreach ($order_status_items as $order) {
				$count ++;
				$xml .= <<<EOL
	                    <OrderStatus MessageID="MessageID$count">
		                    <OrderID>{$order['ExternalOrderID']}</OrderID>
		                        <StatusUpdateDate>$date</StatusUpdateDate>
EOL;
                $fulfilling = false;
                $closedfulfilling = false;
				foreach ($order['items'] as $item) {
					switch ($item['InternalItemStatusName']) {
						case 'Cancelled':
                            if($fulfilling) {
                                $xml .= <<<EOL
                        \n</OrderFulfillment>
EOL;
                                $fulfilling = false;
                            }
							$xml .= <<<EOCANCEL
		                            <OrderCancellation>
			                            <CancellationItem>
			                                <OrderItemCode>{$item['ExternalOrderItemID']}</OrderItemCode>
			                                <ListingID>
			                                    <Identifier>{$item['ChannelOrderItemUPC']}</Identifier>
			                                </ListingID>
			                                <CancelReason>{$item['CustomerComments']}</CancelReason>
			                            </CancellationItem>
		                            </OrderCancellation>
EOCANCEL;
							break;
						case 'Shipped':
						case 'ShippedPartial':
                            if(!$fulfilling) {
                                $xml .= <<<EOL
\n<OrderFulfillment>
    <State>Shipped</State>
			<CarrierCode>{$item['Shipper']}</CarrierCode>
			<CarrierName>{$item['Shipper']}</CarrierName>
			<ShippingMethod>Standard</ShippingMethod>
			<TrackingNumber>{$item['TrackingNumber']}</TrackingNumber>
EOL;
                                $fulfilling = true;
                            }

							$xml .= <<<EOSHIP
			\n<FulfillmentItem>
			  <OrderItemCode>{$item['ExternalOrderItemID']}</OrderItemCode>
			  <ListingID>
				<Identifier>{$item['ChannelOrderItemUPC']}</Identifier>
			  </ListingID>
			  <Quantity>{$item['ShippedQty']}</Quantity>
			  <State>{$item['InternalItemStatusName']}</State>
			</FulfillmentItem>
EOSHIP;
							break;
						default:
							break;

					}
				}
                if($fulfilling) {
                    $xml .= <<<EOL
                        \n</OrderFulfillment>
EOL;
                    $fulfilling = false;
                }
				$xml .= <<<EOL
	\n</OrderStatus>\n
EOL;
			}

			//<TrackingURL>{$item['TrackingURL']}{$item['TrackingNumber']}</TrackingURL>
			//<!--Use one of the blocks: <OrderFulfillment>,
			//<OrderCancellation>, or <OrderState> -->

			$xml .= "\n</OrderStatus>\n";

			return $xml;
		}


	}
