<?php
require_once __DIR__ . "/../../classes/ChannelOrderItem.php";
require_once __DIR__ . "/../../classes/ChannelOrderItemStatusLog.php";

class Bluefly_Feed_Parser {
	
	/**
	 * Map the bluefly shipping method to internal shipping method
	 * @param type $bluefly_shipping_method
	 */
	private static function lookup_shipping_service_name($bluefly_shipping_method) {
		
		$shipping_methods['standard']	= 'Standard';
		$shipping_methods['expedited']	= 'Rush';
		$shipping_methods['express']	= 'Next Business Day';

		$lowercase_shipping_method = strtolower($bluefly_shipping_method);
		if (!empty($lowercase_shipping_method) && isset($shipping_methods[$lowercase_shipping_method])) {
			$internal_service_name = $shipping_methods[$lowercase_shipping_method];
			return $internal_service_name;
		} 
		return 'Standard';
	}
	
	private static function parse_return($dom) {
        global $ulk;
		$parsed_returns = array();
		try {
			if (!isset($dom->RMAs)) {
				print "no orders found in document";
			} 

			if (!isset($dom->RMAs->RMAMessage)) {
				print "no SalesOrder element";
			}
			
			foreach ($dom->RMAs->RMAMessage as $rma) {
				$parsed_returns[(string) $rma->RMA] = array(
					'ExternalOrderID' => (string) $rma->OrderID,
					'RMANumber' => (string) $rma->RMA,
					'RMADate' => (string) $rma->RMADate,
				    'TrackingNumber' => (string) $rma->TrackingNumber,
				);
				
				foreach ($rma->RMAItem as $rma_item) {
                    /* UPDATE RMA SO WE CAN RELAY IT BACK TO VENDOR IF ACCEPTED */
                    $find_sql = <<<SQL
				SELECT coi.* FROM
				ChannelOrder co JOIN
				ChannelOrderItem coi ON coi.ChannelOrderID = co.ChannelOrderID JOIN
				OrderDetails od ON od.ID = coi.OrderItemID
				WHERE co.ExternalOrderID = '$rma->OrderID' AND coi.ExternalOrderItemID = '$rma_item->OrderItemCode' AND od.SKU = '{$rma_item->ListingID->Identifier}'
SQL;
                    $channel_order_item_res = mysqli_query($ulk->db_cnx, $find_sql);
                    if ($channel_order_item_res) {
                        $channel_order_item_details = mysqli_fetch_assoc($channel_order_item_res);
                        $channel_order_item = new ChannelOrderItem();
                        $channel_order_item->setAttributes($channel_order_item_details);
                        $channel_order_item->setChannelOrderItemRefundRMA($rma->RMA);
                        $channel_order_item->save();
                    }

					$parsed_returns[(string) $rma->RMA]['items'][(string) $rma_item->OrderItemCode] = array(
							'ExternalOrderItemID' => (string) $rma_item->OrderItemCode,
							'ReturnReason' => (string) $rma_item->ReturnReason,
							'QTY' => (string) $rma_item->Quantity,
							'ItemIdentifier' => (string) $rma_item->ListingID->Identifier
						);
				}
				
			}
		} catch (Exception $ex) {
			print "Exception ".$ex->getMessage();
		}
		
		return $parsed_returns;
	}
	
	private static function parse_orders($dom) {
		$parsed_orders = array();
		try {

			if (!isset($dom->Orders)) {
				return "no orders found in document";
			} 

			if (!isset($dom->Orders->SalesOrder)) {
				return "no SalesOrder element";
			}

			foreach ($dom->Orders->SalesOrder as $order) {
				//				var_dump($order->SalesOrder);
//						print "OrderID: {$order->OrderID}\n";

				if (strstr((string) $order->BillingData->BuyerName,' '))
				{
					list($billing_fname,$billing_lname)  = explode(" ",(string) $order->BillingData->BuyerName);
				} else {
					$billing_fname	= "";
					$billing_lname	= (string) $order->BillingData->BuyerName;
				}


				if (strstr((string) $order->FulfillmentData->FulfillmentAddress->Name,' ')) {
					list($shipping_fname,$shipping_lname)  = explode(" ",(string) $order->FulfillmentData->FulfillmentAddress->Name);
				} else {
					$shipping_fname = "";
					$shipping_lname = (string) $order->FulfillmentData->FulfillmentAddress->Name;
				}


				if (empty($shipping_lname)) {
					$shipping_fname = (string) $order->FulfillmentData->FulfillmentAddress->Name;
				}

				$new_order = array();
				$new_order['store_id']			= 6;
				$new_order['user_id']			= null;
				$new_order['external_order_id']	= addslashes((string)$order->OrderID);
				$new_order['order_date']		= addslashes((string)$order->OrderDate);
				$new_order['currency']			= addslashes((string)$order->Currency);
				$new_order['transaction_id']	= addslashes((string)$order->OrderID);
				$new_order['company']			= "";
				$new_order['billing_fname']		= addslashes((string)$billing_fname);
				$new_order['billing_lname']		= addslashes((string)$billing_lname);
				$new_order['billing_address1']	= addslashes((string)$order->BillingData->BillingAddress->AddressFieldOne);
				$new_order['billing_address2']	= addslashes((string)$order->BillingData->BillingAddress->AddressFieldTwo);
				$new_order['billing_county']	= '';
				$new_order['billing_city']		= addslashes((string)$order->BillingData->BillingAddress->City);
				$new_order['billing_state']		= addslashes((string)$order->BillingData->BillingAddress->StateOrRegion);
				$new_order['billing_zip']		= addslashes((string)$order->BillingData->BillingAddress->PostalCode);
				$new_order['billing_country']	= addslashes((string)$order->BillingData->BillingAddress->CountryCode);
				$new_order['billing_phone']		= addslashes((string)$order->BillingData->BuyerPhoneNumber);
				$new_order['billing_fax']		= '';
				//$new_order['billing_email']		= addslashes((string)$order->BillingData->BuyerEmailAddress);
                $new_order['billing_email'] = 'Marketplace.Orders@bluefly.com';

				$new_order['shipping_company']		= '';
				$new_order['shipping_residential']	= 1;
				$new_order['shipping_fname']		= addslashes((string)$shipping_fname);
				$new_order['shipping_lname']		= addslashes((string)$shipping_lname);
				$new_order['shipping_address1']		= addslashes((string)$order->FulfillmentData->FulfillmentAddress->AddressFieldOne);
				$new_order['shipping_address2']		= addslashes((string)$order->FulfillmentData->FulfillmentAddress->AddressFieldTwo);
				$new_order['shipping_county']		= '';
				$new_order['shipping_comments']		= '';
				$new_order['shipping_city']			= addslashes((string)$order->FulfillmentData->FulfillmentAddress->City);
				$new_order['shipping_state']		= addslashes((string)$order->FulfillmentData->FulfillmentAddress->StateOrRegion);
				$new_order['shipping_zip']			= addslashes((string)$order->FulfillmentData->FulfillmentAddress->PostalCode);
				$new_order['shipping_country']		= addslashes((string)$order->FulfillmentData->FulfillmentAddress->CountryCode);
				$new_order['shipping_phone']		= addslashes(((strlen((string)$order->FulfillmentData->FulfillmentAddress->PhoneNumber) < 7) ? "8772128450" : (string)$order->FulfillmentData->FulfillmentAddress->PhoneNumber));
				$new_order['coupon_discount']		= '';
				$new_order['repid']				= '';
				$new_order['termsagree']		= '1';
				$new_order['pageid']			= 'BLUEFLY';



				foreach ($order->OrderItem as $item) {
					$new_order['items'][] = array(
						'external_order_item_id'	=> (string) $item->OrderItemCode,
						'upc'			=> (string) $item->ListingID->Identifier,
						'sku'			=> "",
						'price'			=> (string)$item->ItemPrices->Price[0],
						'tax'			=> (string)$item->ItemPrices->Price[1],
						'name'			=> (string) $item->Title,
						'gift_message'	=> (string) $item->GiftMessageText,
						'qty'			=> (string) $item->Quantity
					);
					// they pass the shipping method on the item level
					$new_order['shipping_method'] = self::lookup_shipping_service_name((string) $item->ShippingMethod);
				}

				$parsed_orders[(string)$order->OrderID] = $new_order;

			}
		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
		return $parsed_orders;

	}
	
	public static function parse($feed_type, $raw_xml_string) {
		print "parsing $feed_type\n";
		$dom = simplexml_load_string($raw_xml_string);
		
		switch (strtolower($feed_type)) {
			case 'rmas':
			break;

			case 'orders':
				return self::parse_orders($dom);
			break;

			case 'return':
				return self::parse_return($dom);
			break;

			default:
				print "feed type $feed_type doesn't exist\n";
				print "pull bluefly [orders|rmas|refunds]\n\n";
			break;
		}
	}

}
