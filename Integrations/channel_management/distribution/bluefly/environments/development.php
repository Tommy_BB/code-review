<?php


return array(
	'debug_mode'		=> false,
	'aggregate_stores'	=> true,
	'server'		=> 'sftp.bluefly.com',
	'username'		=> 'zindigo',
	'rsa_key_path'	=> __DIR__ . '/ssh-keys/id_rsa',
	'protocol'		=> 'sftp',
	'directories'	=> array(
		'incoming'		=> 'qa/out/',
		'outgoing'		=> 'qa/in/',
		'archive'		=> '../archive/',
	),
	'file_name_ext'	=> '.xml',
	'file_name_prefix'	=> array(
		'inventory'			=> 'Inventory_',
		'price'				=> 'Price_',
		'product'			=> 'Product_',
		'return'			=> 'Return_',
		'refund_requests'	=> 'RefundRequest_',
		'refund'			=> 'Refund_',
		'orders'			=> 'Order_',
		'order_status'		=> 'OrderStatus_',
	),
	'archive_dir'	=> array(
		'return'		=> '../rma_archive/',
		'orders'		=> '../archive/',
	),
	'stores'		=> array(

        //raoul
        '134' => array(
            'category' => '*'
        ),


        //singledress
        '135' => array(
            'category' => '*'
        ),


        //NESH
        '136' => array(
            'category' => '*'
        ),
        //Petit Pois
        '137' => array(
            'category' => '*'
        ),
        //Walter Baker
        '138' => array(
            'category' => '*'
        ),
        //Stella Jamie
        '139' => array(
            'category' => '*'
        ),
        //Nicole Romano
        '129' => array(
            'category' => '*'
        ),
        //Stella Valle
        '140' => array(
            'category' => '*'
        ),

        //Karen Zambos
        '142' => array(
            'category' => '*'
        ),

        //Kaya
        '150' => array(
            'category' => '*'
        ),

	)
);