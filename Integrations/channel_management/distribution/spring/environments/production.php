<?php

return array(
    'debug_mode'		=> false,
    'aggregate_stores'	=> true,
    'server'		=> 'sftp.bluefly.com',
    'username'		=> 'tommy@zindigo.com',
    'protocol'		=> 'curl',
    'directories'	=> array(
    ),
    'stores'		=> array(
        '101' => array(
            'category' => '*',
            'disabled_ids' => '2435,2436,2437,2408,2528,1233'
        ),
    )
);
