<?php
    require_once __DIR__ . "/../../classes/ChannelOrderItem.php";
    require_once __DIR__ . "/../../classes/ChannelOrderItemStatusLog.php";

	class Spring_Feed_Formatter {

		public static function render( $feed_type, $payload, $file_prefix = null ) {

			print "spring feed formatter is rendering $feed_type feed...\n";

			switch (strtolower($feed_type)) {
				case 'product':
					static::generate_product_feed($payload);
					break;
				default:
					throw new Exception("Undefined feed type: $feed_type");
			}
            return "";
		}

        private static function http_build_query_for_curl( $arrays, &$new = array(), $prefix = null ) {

            if ( is_object( $arrays ) ) {
                $arrays = get_object_vars( $arrays );
            }

            foreach ( $arrays AS $key => $value ) {
                $k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
                if ( is_array( $value ) OR is_object( $value )  ) {
                    http_build_query_for_curl( $value, $new, $k );
                } else {
                    $new[$k] = $value;
                }
            }
        }

        private static function convert_to_normal_text($text) {

            $normal_characters = "a-zA-Z0-9\s`~!@#$%^&*()_+-={}|:;<>?,.\/\"\'\\\[\]";
            $normal_text = preg_replace("/[^$normal_characters]/", '', $text);

            return $normal_text;
        }

        private static function generate_product_feed( $products ) {
            global $discounts;
            //this is an array to never loop again
            $checkedsaleprices = array();
            $productsoneImg = array();

            //vendorid 2956
            foreach ($products as $product) {

                $usedImages = array();
                $firstKey = key($product['images']);
                $usedImages[] = "http://stores.zindigo.com/".$firstKey;
                $prodImages = explode(",", $product['ProductImages']);
                $prodImages = array_reverse($prodImages);
                if(count($prodImages) > 0)
                {
                    foreach ($prodImages as $image) {
                        if(!in_array("http://stores.zindigo.com/".$image,$usedImages))
                        {
                            $usedImages[] = "http://stores.zindigo.com/".$image;
                        }
                    }
                }
                if(count($usedImages) <=1 )
                {
                    $productsoneImg[] = $product['StyleSKU'];
                    $brwak = 0;
                }


                if(!isset($checkedsaleprices[$product['ProductsID']]) && is_numeric($product['ProductsID']) && $product['ProductsID'] != "") {
                    $saleprice = $discounts->getSalePrice($product['ProductsID'], 101);
                    $checkedsaleprices[$product['ProductsID']] = $saleprice;
                } else {
                    $saleprice = $checkedsaleprices[$product['ProductsID']];
                }

                $original_price = $product['ListPrice'];

                if (is_numeric($saleprice) && $saleprice != "" && $saleprice < $product['ListPrice']) {
                    $price = $saleprice;
                } else {
                    $price = $product['ListPrice'];
                }



                //begin variant
                $variants = array();
                foreach($product['variants'] as $variant)
                {
                    $curColor = "";
                    $curSizing = "";

                    if(isset($variant['PropertyName1']) && $variant['PropertyName1'] != null)
                    {
                        if(stripos($variant['PropertyName1'],"color") !== false)
                            $curColor = $variant['PropertyValue1'];
                        if(stripos($variant['PropertyName1'],"size") !== false)
                            $curSizing = $variant['PropertyValue1'];
                    }

                    if(isset($variant['PropertyName2']) && $variant['PropertyName2'] != null)
                    {
                        if(stripos($variant['PropertyName2'],"color") !== false)
                            $curColor = $variant['PropertyValue2'];
                        if(stripos($variant['PropertyName2'],"size") !== false)
                            $curSizing = $variant['PropertyValue2'];
                    }

                    $attributes = array();
                    if($curColor != "")
                        $attributes['color'] = $curColor;
                    if($curSizing != "")
                        $attributes['size'] = $curSizing;
                    $addAttributes = array();

                    if(count($attributes) > 0)
                    {
                        $addAttributes = array("attributes" => $attributes);
                    }

                    $variants[] = array(
                        "sku" => $variant['ProductSKU'],
                        "original_price" => $original_price,
                        "price" => $price,
                        "available_inventory" => (int)$variant['Qty'],
                        "size" => $curColor . " " . $curSizing,
                        $addAttributes
                    );
                }

                //end variant

                $product['ProductName'] = self::convert_to_normal_text($product['ProductName']);
                $product['ProductDesc'] = self::convert_to_normal_text($product['ProductDesc']);

                //utf8_encode_deep($product['ProductName']);

                $currentProdArray = array(
                    "vendor_id" => 2956,
                    "product" => array(
                        "id" => $product['ProductsID'],
                        "title" => $product['ProductName'],
                        "description" => $product['ProductDesc'],
                        "tags" => array("nesh"),
                        "active" => true,
                        "purchasable" => true,
                        "external_brand" => "NESH NYC",
                        "affiliate_url" => "http://www.neshnyc.com/index.php?storeid=101&page=product&prodid=".$product['ProductsID'],
                        "photo_urls" => $usedImages,
                        "variants" => $variants

                    )
                );

                $dataBuilding = json_encode($currentProdArray);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://vfe-demo.jellolabs.com:9000/api/1/product");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $dataBuilding);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_USERPWD, "Im8Ah487CAyN8HGtc3Pl:");
                $headers = array();
                $headers[] = "Content-Type: application/json";
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $result = curl_exec($ch);
                $myres = json_decode($result);
                curl_close($ch);

                if(!isset($myres->update_stats))
                {
                    $errorProducts[] = $product['ProductsID'];
                } else {
                    $parsedProducts[] = $product['ProductsID'];
                }
            }
            return "";
        }


	}
