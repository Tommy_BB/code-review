<?php
	require_once __DIR__ . "/../../../../classes/email.php";

	/**
	 * Class Return_Manager
	 */
	class Return_Manager {

		private $_channel_code;
		private $_imported_returns;

		/**
		 * @param $channel_code
		 */
		public function __construct( $channel_code ) {
			$this->_channel_code = $channel_code;
			$this->_imported_returns = array();
		}

		/**
		 * @param $parsed_data
		 */
		public function import_returns( $parsed_data ) {

			foreach ($parsed_data as $rma) {
				foreach ($rma['items'] as $channel_order_item_id => $item_data) {
					$external_order_id = $rma['ExternalOrderID'];
					$this->import_item($external_order_id, $item_data);
				}
			}
		}

		/**
		 * @param $external_order_id
		 * @param $item_data
		 */
		private function import_item( $external_order_id, $item_data ) {
			global $ulk;

			$external_order_item_id = $item_data['ExternalOrderItemID'];
			$quantity = $item_data['QTY'];
			$return_reason = $item_data['ReturnReason'];

			$item = $this->get_order_item_details($external_order_id, $external_order_item_id);
			$item_id = $item['order_item_id'];

			$itemxml = $itemxml . '<item><lineid>' . $item_id . '</lineid><returnqty>' . $quantity . '</returnqty><returnreason>' . mysqli_real_escape_string($ulk->db_cnx, $return_reason)
			           . '</returnreason></item>';

			$item['storeid'] = $item['StoreID']; //different cases used in array passed to EmailCustomerUpdate
			$item['orderID'] = $item['OrderID']; //different cases used in array passed to EmailCustomerUpdate
			$orderqty = $item['RemainingQty'];
			$order["ordernum"] = $item['OrderNum'];
			$orderid = $item['OrderID'];

			$store = $ulk->getStore($item['storeid']);

			$sendemail = array();

			if ($item['ChangeOrderURL'] <> "") {
				$placeurl = $item['ChangeOrderURL'];
				if ($returnqty >= $orderqty) {
					$returnqty = $orderqty;
					$item['Qty'] = $returnqty;

					if (!array_key_exists($item['vendorID'], $sendemail)) {
						$sendemail[ $item['vendorID'] ] = array(
							"to"          => $item['VendorEmail'],
							"storeid"     => $item['storeid'],
							"orderID"     => $item['orderID'],
							"orderaction" => "VendorReturnNotification",
							"vendorID"    => $item['vendorID']
						);
						$products[ $item['vendorID'] ] = array(
							$item['SKU']
						);
					} else {
						array_push($products[ $item['vendorID'] ], $item['SKU']);
					}
				} else {
					$item['Qty'] = $returnqty;

					if (!array_key_exists($item['vendorID'], $sendemail)) {
						$sendemail[ $item['vendorID'] ] = array(
							"to"          => $item['VendorEmail'],
							"storeid"     => $item['storeid'],
							"orderID"     => $item['orderID'],
							"orderaction" => "VendorReturnNotification",
							"vendorID"    => $item['vendorID']
						);
						$products[ $item['vendorID'] ] = array(
							$item['SKU']
						);
					} else {
						array_push($products[ $item['vendorID'] ], $item['SKU']);
					}
				}
			} else {

				if (!array_key_exists($item['vendorID'], $sendemail)) {
					$sendemail[ $item['vendorID'] ] = array(
						"to"          => $item['VendorEmail'],
						"storeid"     => $item['storeid'],
						"orderID"     => $item['orderID'],
						"orderaction" => "VendorReturnNotification",
						"vendorID"    => $item['vendorID']
					);
					$products[ $item['vendorID'] ] = array(
						$item['SKU']
					);
				} else {
					array_push($products[ $item['vendorID'] ], $item['SKU']);
				}
			}


			$xml = "<root>" . $itemxml . "</root>";
			$strQury = "Call IssueRMA(" . $orderid . ",'" . $xml . "',@rma);Select @rma;";

			if (mysqli_multi_query($ulk->db_cnx, $strQury)) {
				mysqli_next_result($ulk->db_cnx);

				$rs2 = mysqli_store_result($ulk->db_cnx);
				$row = mysqli_fetch_row($rs2);

				$rma = $row[0];
				mysqli_free_result($rs2);

				$tracking_number = $item_data['TrackingNumber'];
				if (!empty($tracking_number)) {
					$updateQry = <<<SQL
					UPDATE Returns
					SET TrackingNumber = '$tracking_number'
					WHERE RMA = '$rma'
SQL;
					mysqli_query($ulk->db_cnx, $updateQry);
				}

				$email = new EMAIL();

				// Notify the vendor of the return
				foreach ($sendemail as $key => $record) {

					//$record['to'] = 'tommy@zindigo.com';
					$record['rma'] = $rma;

					$record['products'] = serialize($products[ $key ]);
					$email->EmailVendorUpdate($record, null, $ulk);
				}
			}
		}

		/**
		 * @param $item_id
		 *
		 * @return array|bool|null
		 */
		private function get_order_item_details( $external_order_id, $external_order_item_id ) {
			global $ulk;

			$external_order_id = mysqli_real_escape_string($ulk->db_cnx, $external_order_id);
			$external_order_item_id = mysqli_real_escape_string($ulk->db_cnx, $external_order_item_id);

			$sql = <<<SQL
			SELECT
				OrderItemID
			FROM ChannelOrderItem item
			INNER JOIN ChannelOrder ord ON ord.ChannelOrderID = item.ChannelOrderID
			WHERE item.ExternalOrderItemID = '$external_order_item_id' AND ord.ExternalOrderID = '$external_order_id'
			LIMIT 1
SQL;
			$order_item_search_res = mysqli_query($ulk->db_cnx, $sql);
			$order_item_search_data = mysqli_fetch_assoc($order_item_search_res);

			$order_item_id = $order_item_search_data['OrderItemID'];
			if (!$order_item_id) {
				return false;
			}

			$item_data = $ulk->getOrderItem($order_item_id);
			$item_data['order_item_id'] = $order_item_id;

			return $item_data;
		}
	}