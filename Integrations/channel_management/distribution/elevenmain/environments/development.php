<?php

	return array(
		'debug_mode'       => false,
		'aggregate_stores' => false,
		'protocol'         => 'ftp',
		'directories'      => array(
			'archive'      => '../../archive/',
			'orders'       => 'orders/open_orders/',
			'order_status' => 'orders/shipping_file/',
			'product'      => 'products/auto-submit-replace/',
		),
		'file_name_ext'    => '.csv',
		'file_name_prefix' => array(
			'product'      => 'products',
			'orders'       => 'open_orders',
			'order_status' => 'shipping',
		),
		'archive_dir'      => array(
			'rma'    => '../archive/',
			'orders' => '../archive/',
		),
		'stores'           => array(
			// Threads of friendship
			'128' => array(
				'category' => '*',
				'server'   => 'feeds.11main.com',
				'username' => 'zind_5DEakb',
				'password' => 'CDKmL4FE',
				'passive'  => true,
			),
		)
	);