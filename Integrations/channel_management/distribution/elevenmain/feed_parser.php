<?php

	class Elevenmain_Feed_Parser {

		public static function parse( $feed_type, $raw_feed_contents ) {
			print "parsing $feed_type\n";

			switch (strtolower($feed_type)) {
				case 'rmas':

					break;

				case 'orders':
					return self::parse_orders($raw_feed_contents);
					break;

				case 'refunds':

					break;

				default:
					print "feed type $feed_type doesn't exist\n";
					print "pull elevenmain [orders|rmas|refunds]\n\n";
					break;
			}
		}

		private static function parse_orders( $raw_csv_string ) {

			$parsed_orders = array();

			$csv_order_data = self::convert_raw_csv_to_array($raw_csv_string);
			foreach ($csv_order_data as $order_data) {

				$new_order_item = array(
					'external_order_item_id' => addslashes($order_data['ItemId']),
					'upc'                    => '',
					'sku'                    => addslashes($order_data['ItemId']),
					'price'                  => addslashes($order_data['ItemPrice']),
					'tax'                    => addslashes($order_data['SalesTax']),
					'name'                   => addslashes($order_data['ItemName']),
					'gift_message'           => '',
					'qty'                    => addslashes($order_data['Quantity'])
				);

				// We receiving order information for every item so we need to ensure that the records are distinctive
				//
				if (!isset($parsed_orders[ $order_data['OrderID'] ])) {

					$new_order = array();

					// Dereference the billing name
					//
					$billing_name_parts = explode(" ", $order_data['BillName']);
					$billing_first_name = $billing_name_parts[0];
					unset($billing_name_parts[0]);
					$billing_last_name = implode(" ", $billing_name_parts);

					// Dereference the shipping name
					//
					$shipping_name_parts = explode(" ", $order_data['ShipName']);
					$shipping_first_name = $shipping_name_parts[0];
					unset($shipping_name_parts[0]);
					$shipping_last_name = implode(" ", $shipping_name_parts);

					$new_order['store_id'] = 128;
					$new_order['user_id'] = null;
					$new_order['external_order_id'] = addslashes($order_data['OrderID']);
					$new_order['order_date'] = addslashes($order_data['PurchaseDate']);
					$new_order['currency'] = 'USD';
					$new_order['transaction_id'] = addslashes($order_data['OrderID']);
					$new_order['company'] = "";
					$new_order['billing_fname'] = addslashes($billing_first_name);
					$new_order['billing_lname'] = addslashes($billing_last_name);
					$new_order['billing_address1'] = addslashes($order_data['BillAddress1']);
					$new_order['billing_address2'] = addslashes($order_data['BillAddress2']);
					$new_order['billing_county'] = '';
					$new_order['billing_city'] = addslashes($order_data['BillCity']);
					$new_order['billing_state'] = addslashes($order_data['BillState']);
					$new_order['billing_zip'] = addslashes($order_data['BillZip']);
					$new_order['billing_country'] = addslashes($order_data['BillCountry']);
					$new_order['billing_phone'] = addslashes($order_data['BillPhone']);
					$new_order['billing_fax'] = '';
					$new_order['billing_email'] = addslashes($order_data['BillEmail']);

					$new_order['shipping_company'] = '';
					$new_order['shipping_residential'] = 1;
					$new_order['shipping_fname'] = addslashes($shipping_first_name);
					$new_order['shipping_lname'] = addslashes($shipping_last_name);
					$new_order['shipping_address1'] = addslashes($order_data['ShipAddress1']);
					$new_order['shipping_address2'] = addslashes($order_data['ShipAddress2']);
					$new_order['shipping_county'] = '';
					$new_order['shipping_city'] = addslashes($order_data['ShipCity']);
					$new_order['shipping_state'] = addslashes($order_data['ShipState']);
					$new_order['shipping_zip'] = addslashes($order_data['ShipZip']);
					$new_order['shipping_country'] = addslashes($order_data['ShipCountry']);
					$new_order['shipping_phone'] = addslashes($order_data['ShipPhone']);
					$new_order['coupon_discount'] = addslashes($order_data['CouponDiscount']);
					$new_order['repid'] = '';
					$new_order['termsagree'] = '1';
					$new_order['pageid'] = '11MAIN';
					$new_order['shipping_method'] = self::lookup_shipping_service_name(addslashes($order_data['ShipmentMethod']));

					$new_order['items'][] = $new_order_item;

					$parsed_orders[ $order_data['OrderID'] ] = $new_order;
				} else {
					$parsed_orders[ $order_data['OrderID'] ]['items'][] = $new_order_item;
				}
			}

			return $parsed_orders;
		}

		/**
		 * @param $raw_csv_string
		 *
		 * @return array
		 */
		private static function convert_raw_csv_to_array( $raw_csv_string ) {

			$csv_data = $headers = array();

			$raw_csv_lines = explode("\n", $raw_csv_string);

			$index = 0;
			foreach ($raw_csv_lines as $raw_csv_line) {
				if (empty($raw_csv_line)) {
					continue;
				}

				$raw_csv_columns = explode(',', $raw_csv_line);
				$new_csv_entry = array();

				$col_index = 0;
				foreach ($raw_csv_columns as $csv_column_value) {
					$csv_column_value = self::sanitize_csv_value($csv_column_value);
					if (0 === $index) {
						$headers[] = str_replace(" ", "", $csv_column_value);
					} else {
						$new_csv_entry[ $headers[ $col_index ] ] = $csv_column_value;
						++ $col_index;
					}
				}

				if ($index > 0) {
					$csv_data[] = $new_csv_entry;
				}

				++ $index;
			}

			return $csv_data;
		}

		/**
		 * @param $value
		 *
		 * @return mixed
		 */
		private static function sanitize_csv_value( $value ) {
			return str_replace(array("\n", "\r", "\r\n"), '', $value);
		}

		/**
		 * Map the elevenmain shipping method to internal shipping method
		 *
		 * @param type $elevenmain_shipping_method
		 */
		private static function lookup_shipping_service_name( $elevenmain_shipping_method ) {

			$shipping_methods['standard'] = 'Standard';
			$shipping_methods['expedited'] = 'Rush';
			$shipping_methods['express'] = 'Next Business Day';

			$lowercase_shipping_method = strtolower($elevenmain_shipping_method);
			if (!empty($lowercase_shipping_method) && isset($shipping_methods[ $lowercase_shipping_method ])) {
				$internal_service_name = $shipping_methods[ $lowercase_shipping_method ];

				return $internal_service_name;
			}

			return 'Standard';
		}

		private static function lookup_product_upc_by_product_sku_base( $product_sku_base ) {

		}
	}
