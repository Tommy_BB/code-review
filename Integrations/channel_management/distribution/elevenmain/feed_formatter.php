<?php

class Elevenmain_Feed_Formatter {
	
	public static $csv_field_delimiter = ',';
	
	public static function generate_document_guid($prefix) 
	{
//		return $prefix.'_'.date("Y-m-d");
		return $prefix.'_'.date("Y-m-d_Hi");
	}

	/**
	 * Render a 11main xml feed 
	 * 
	 * @param type $feed_type
	 * @param type $vendor_info
	 * @param type $payload
	 * @return type
	 * @throws Exception
	 */
	public static function render($feed_type, $payload, $file_prefix=null) {
		$csv_data = "";
		print "feed formatter is generating $feed_type feed";
		$file_prefix = (isset($file_prefix)) ? $file_prefix : $feed_type;
		$document_guid = self::generate_document_guid($file_prefix);
//		$csv_data = self::generate_header($document_guid);
		switch (strtolower($feed_type)) {
//			case 'inventory':
//				$csv_data .= self::generate_inventory_feed($payload);
//				break;
//			case 'price':
//				$csv_data .= self::generate_pricing_feed($payload);
//				break;
			case 'product':
				$csv_data .= self::generate_product_feed($payload);
				break;
//			case 'order_status':
//				$csv_data .= self::generate_order_status_feed($payload);
//				break;
			default: throw new Exception("11main Formatter - Undefined feed type: $feed_type");
		}
		
		$channel_temp_dir = "/tmp/11main";
		$file_path = "$channel_temp_dir/$document_guid.csv";
		
		if (!file_exists($channel_temp_dir)) {
			print "making channel tmp directory $channel_temp_dir\n";
			mkdir($channel_temp_dir,0777);
		}
		
		
		if (file_put_contents($file_path,$csv_data)) {
			return $file_path;
		}
	}
	
	
	/**
	 * Generate a 11main product CSV feed 
	 * 
	 * @param type $vendor_info			
	 * @param type $inventory_items
	 * @return XML string
	 */
	private static function generate_product_feed($products) {

		if (!isset($products)) {
			return "";
		}
		
		$n = 0;
		
		// identifier we pass as their ItemID
		//
		$identifier_name = 'StyleSKU';
		
		foreach ($products as $product) {
			if ($n==0) {
				
				
				// pick property names to show in column
				//
				$heading_property1 = $product['PropertyName1'];
				$heading_property2 = $product['PropertyName2'];
				$heading_property1_label = $product['PropertyName1'] ? $product['PropertyName1'] : 'Variation1';
				$heading_property2_label = $product['PropertyName2'] ? $product['PropertyName2'] : 'Variation2';
				
				$data = "ItemID"
						.self::$csv_field_delimiter."Title"
						.self::$csv_field_delimiter."Description"
						.self::$csv_field_delimiter.$heading_property1_label
						.self::$csv_field_delimiter.$heading_property2_label
						.self::$csv_field_delimiter."Price"
						.self::$csv_field_delimiter."Image1"
						.self::$csv_field_delimiter."Quantity"
						.self::$csv_field_delimiter."UPC"
						.self::$csv_field_delimiter."SKU"
						.self::$csv_field_delimiter."GroupItemID\n";
			}
			$n++;
			
			
//			$variant1_present = (isset($product['variants'][0]['properties'][$heading_property1])) ? 'yes' : '';
//			$variant2_present = (isset($product['variants'][0]['properties'][$heading_property2])) ? 'yes' : '';
//			$variant1_present = ($heading_property1==$heading_property1_label) ? 'yes' : '';
//			$variant2_present = ($heading_property2==$heading_property2_label) ? 'yes' : '';
			
			$product['ProductDesc'] = strip_tags(html_entity_decode($product['ProductDesc']));

			// reset the variant indicator labels
			//
			$variant1_present = '';
			$variant2_present = '';
			
			// first build the list of variants if they exist
			//
			$vdata = "";
			foreach ($product['variants'] as $variant) {
				
				if ($variant['StyleSKU'] == $variant['ProductSKU']) {
					continue;
				}

				$variant_property_val1 = (isset($variant['properties'][$heading_property1])) ? $variant['properties'][$heading_property1] : '';
				$variant_property_val2 = (isset($variant['properties'][$heading_property2])) ? $variant['properties'][$heading_property2] : '';
				
				// flip the variant detectors so we can output 'yes' or 'no' when we output the product
				//
				$variant1_present = ($variant_property_val1) ? 'yes' : $variant1_present;
				$variant2_present = ($variant_property_val2) ? 'yes' : $variant2_present;
				
				$vdata .= $variant['ProductSKU'].self::$csv_field_delimiter.self::$csv_field_delimiter.self::$csv_field_delimiter;
//				$data .= $product['ProductName'].self::$csv_field_delimiter;
//				$data .= $product['ProductDesc'].self::$csv_field_delimiter;
				$vdata .= $variant['PropertyValue1'].self::$csv_field_delimiter;
				$vdata .= $variant['PropertyValue2'].self::$csv_field_delimiter;
				$vdata .= round($product['ListPrice'],2).self::$csv_field_delimiter;
				$vdata .= 'http://stores.zindigo.com/'.$variant['ImagePath'].self::$csv_field_delimiter;
				$vdata .= $variant['Qty'].self::$csv_field_delimiter;
				$vdata .= $variant['UPC'].self::$csv_field_delimiter;
				$vdata .= $variant['ProductSKU'].self::$csv_field_delimiter;
				$vdata .= $product[$identifier_name]."\n";
			}
			
			// now build the parent record row
			//
			$data .= $product[$identifier_name].self::$csv_field_delimiter;
			$data .= $product['ProductName'].self::$csv_field_delimiter;
			$data .= $product['ProductName'].self::$csv_field_delimiter;
//			$data .= $product['ProductDesc'].self::$csv_field_delimiter;
			$data .= $variant1_present.self::$csv_field_delimiter;
			$data .= $variant2_present.self::$csv_field_delimiter;
			$data .= round($product['ListPrice'],2).self::$csv_field_delimiter;
			$data .= 'http://stores.zindigo.com/'.$product['ImagePath'].self::$csv_field_delimiter;
			$data .= $product['Qty'].self::$csv_field_delimiter;
			$data .= $product['UPC'].self::$csv_field_delimiter;
			$data .= $product['ProductSKU'].self::$csv_field_delimiter;
			$data .= "\n";
			$data .= $vdata;		// output variants
			
			
			
		}
			

		return $data;
	}	


}
