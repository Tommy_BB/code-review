<?php

return array(
    'debug_mode'		=> false,
    'aggregate_stores'	=> true,
    'server'		=> '',
    'username'		=> '',
    'protocol'		=> 'curl',
    'directories'	=> array(
    ),
    'stores'		=> array(
        '114' => array(
            'category' => '*'
        ),
    )
);
