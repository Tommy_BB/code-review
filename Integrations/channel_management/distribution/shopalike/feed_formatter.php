<?php
    require_once __DIR__ . "/../../classes/ChannelOrderItem.php";
    require_once __DIR__ . "/../../classes/ChannelOrderItemStatusLog.php";

	class Shopalike_Feed_Formatter {

        public static function generate_document_guid() {
            return "ShopaLike";
        }


		public static function render( $feed_type, $payload, $file_prefix = null ) {

			print "spring feed formatter is rendering $feed_type feed...\n";
            $xml_feed = "";
            $document_guid = static::generate_document_guid();
			switch (strtolower($feed_type)) {
				case 'product':
                    $xml_feed = static::generate_product_feed($payload);
					break;
				default:
					throw new Exception("Undefined feed type: $feed_type");
			}

            $channel_temp_dir = __DIR__."/feed/";
            $file_path = "$channel_temp_dir/$document_guid.xml";

            if (!file_exists($channel_temp_dir)) {
                print "making channel tmp directory $channel_temp_dir\n";
                mkdir($channel_temp_dir, 0777);
            }
            if (file_put_contents($file_path, $xml_feed)) {
                return $file_path;
            }
		}


        private static function xmlEscape($string) {
            return str_replace(array('&', '<', '>', '\'', '"'), array('&amp;', '&lt;', '&gt;', '&apos;', '&quot;'), $string);
        }

        private static function convert_to_normal_text($text) {

            $normal_characters = "a-zA-Z0-9\s`~!@#$%^&*()_+-={}|:;<>?,.\/\"\'\\\[\]";
            $normal_text = preg_replace("/[^$normal_characters]/", '', $text);

            return $normal_text;
        }

        private static function generate_product_feed( $products ) {
            global $discounts;
            $checkedsaleprices = array();

            $xml = new DOMDocument("1.0");
            $xml->formatOutput = true;
            $xml->preserveWhiteSpace = false;
            $xmlProducts = $xml->createElement('Products');

            foreach($products as $product)
            {
                //only send products with quantity
                if($product['TotalQty'] <= 0) continue;

                if(!isset($checkedsaleprices[$product['ProductsID']]) && is_numeric($product['ProductsID']) && $product['ProductsID'] != "") {
                    $saleprice = $discounts->getSalePrice($product['ProductsID'], 101);
                    $checkedsaleprices[$product['ProductsID']] = $saleprice;
                } else {
                    $saleprice = $checkedsaleprices[$product['ProductsID']];
                }

                $original_price = $product['ListPrice'];

                if (is_numeric($saleprice) && $saleprice != "" && $saleprice < $product['ListPrice']) {
                    $price = $saleprice;
                } else {
                    $price = $product['ListPrice'];
                }


                $xmlProduct = $xml->createElement('Product');

                $id = $xml->createElement("id", $product['ProductsID']);
                $xmlProduct->appendChild($id);

                $sku = $xml->createElement("sku", $product['StyleSKU']);
                $xmlProduct->appendChild($sku);

                $url = $xml->createElement("url", self::xmlEscape("http://www.raoul.com/index.php?storeid=114&page=productslist&prodid={$product['ProductsID']}"));
                $xmlProduct->appendChild($url);

                $product['ProductName'] = self::convert_to_normal_text($product['ProductName']);
                $name = $xml->createElement("name", $product['ProductName']);
                $xmlProduct->appendChild($name);

                $brand = $xml->createElement("brand", $product['Manufacturer']);
                $xmlProduct->appendChild($brand);

                $product['ProductDesc'] = self::convert_to_normal_text($product['ProductDesc']);
                $description = $xml->createElement("description", $product['ProductDesc']);
                $xmlProduct->appendChild($description);


                $categories = $xml->createElement("category", (($product['Cat1'] != "") ? $product['Cat1'] . " " : "") . (($product['CategoryName'] != "") ? $product['CategoryName'] : ""));
                $xmlProduct->appendChild($categories);

                $originalPrice = $xml->createElement("originalprice", number_format($original_price,2));
                $xmlProduct->appendChild($originalPrice);

                $pricing = $xml->createElement("price", number_format($price,2));
                $xmlProduct->appendChild($pricing);

                //images
                $images = $xml->createElement("images");
                $usedImages = array();
                $firstKey = key($product['images']);
                $usedImages[] = "http://stores.zindigo.com/".$firstKey;

                $currentImage = $xml->createElement("url", "http://stores.zindigo.com/".$firstKey);
                $images->appendChild($currentImage);

                $prodImages = explode(",", $product['ProductImages']);
                $prodImages = array_reverse($prodImages);
                if(count($prodImages) > 0)
                {
                    foreach ($prodImages as $image) {
                        if(!in_array("http://stores.zindigo.com/".$image,$usedImages))
                        {
                            $usedImages[] = "http://stores.zindigo.com/".$image;
                            $currentImage = $xml->createElement("url", "http://stores.zindigo.com/".$image);
                            $images->appendChild($currentImage);
                        }
                    }
                }

                $xmlProduct->appendChild($images);





                //for variants
                $lastColor = "";
                $lastSize = "";
                $createdSizes = false;
                $createdColors = false;
                foreach($product['variants'] as $variant) {
                    $curColor = "";
                    $curSizing = "";

                    if (isset($variant['PropertyName1']) && $variant['PropertyName1'] != null) {
                        if (stripos($variant['PropertyName1'], "color") !== false)
                            $curColor = $variant['PropertyValue1'];
                        if (stripos($variant['PropertyName1'], "size") !== false)
                            $curSizing = $variant['PropertyValue1'];
                    }

                    if (isset($variant['PropertyName2']) && $variant['PropertyName2'] != null) {
                        if (stripos($variant['PropertyName2'], "color") !== false)
                            $curColor = $variant['PropertyValue2'];
                        if (stripos($variant['PropertyName2'], "size") !== false)
                            $curSizing = $variant['PropertyValue2'];
                    }

                    $curColor = trim($curColor);


                    if((int)$variant['Qty'] > 0  && $curColor != "" && $lastColor != $curColor)
                    {
                        if(!$createdColors)
                        {
                            $colors = $xml->createElement("colors");
                            $createdColors = true;
                        }
                        $curCol = $xml->createElement("color", $curColor);
                        $colors->appendChild($curCol);
                        $lastColor = $curColor;
                    }

                    if((int)$variant['Qty'] > 0  && $curSizing != "" && $lastSize != $curSizing)
                    {
                        if(!$createdSizes)
                        {
                            $sizes = $xml->createElement("sizes");
                            $createdSizes = true;
                        }
                        $curSize = $xml->createElement("size",$curSizing);
                        $sizes->appendChild($curSize);
                        $lastSize = $curSizing;
                    }
                }

                if($createdColors)
                $xmlProduct->appendChild($colors);

                if($createdSizes)
                $xmlProduct->appendChild($sizes);


                $xmlProducts->appendChild($xmlProduct);

            }

            $xml->appendChild($xmlProducts);
            return $xml->saveXML();
        }


	}
