<?php
require_once __DIR__ . "/../../../../classes/email.php";

class Distribution_Channel_Order_Manager {

    private $_channel_code;
    private $_imported_orders;
    private $_channel_interaction_id;

    public function __construct( $channel_code, $channel_interaction_id = null ) {
        $this->_channel_code = $channel_code;
        $this->_imported_orders = array();
        $this->_channel_interaction_id = $channel_interaction_id;
    }


    /**
     * Find the zindigo channel store to which an ordered product belongs.
     * There are two possibile ways in which this can happen:
     *
     * a) a channel store exists which contains this product, or
     * b) a channel store exists which mirrors another store that contains the product, in which case the channel store id would be returned
     *
     * @global type $ulk
     *
     * @param type  $upc
     *
     * @return boolean
     */

    public function get_store_product_by_upc_or_sku( $product_identifier ) {
        global $ulk;

        $product_identifier_sanitized = addslashes($product_identifier);
        /*
        $sql = "SELECT
                sc.StoreID,
                s2.ID as ReferenceStoreID,
                i.ProductID,
                p.VendorID,
                CONCAT(p.VendorSKUBase,v1.SKU,v2.SKU) as ProductSKU,
                i.InventoryID,
                i.UPC,
                i.Qty,
                i.Value1,
                i.Value2,
                v1.PropertyID as PropertyID1,
                v2.PropertyID as PropertyID2
        FROM Inventory i
        LEFT JOIN Products p on p.ID = i.ProductID
        LEFT JOIN StoresProducts sp on sp.ProductID = i.ProductID
        LEFT JOIN StoresCategories sc on sc.ID = sp.StoreCategoryID
        LEFT JOIN Stores s on s.ID = sc.StoreID
        LEFT JOIN Stores s2 on s2.ProdStoreID = s.ID
        LEFT JOIN `Values` v1 on v1.ID = i.Value1
        LEFT JOIN `Values` v2 on v2.ID = i.Value2
        WHERE s.StoreChannelCode = '{$this->_channel_code}' OR s2.StoreChannelCode = '{$this->_channel_code}'
        HAVING
            UPC = '$product_identifier_sanitized' or ProductSKU = '$product_identifier_sanitized'
        LIMIT 1";
        */
        $sql = "SELECT
					sc.StoreID,
					s2.ID as ReferenceStoreID,
					i.ProductID,
					p.VendorID,
					p.ProductSKUBase as ProductSKU,
					i.InventoryID,
					i.UPC,
					i.Qty,
					i.Value1,
					i.Value2,
					v1.PropertyID as PropertyID1,
					v2.PropertyID as PropertyID2
			FROM Inventory i
			LEFT JOIN Products p on p.ID = i.ProductID
			LEFT JOIN StoresProducts sp on sp.ProductID = i.ProductID
			LEFT JOIN StoresCategories sc on sc.ID = sp.StoreCategoryID
			LEFT JOIN Stores s on s.ID = sc.StoreID
			LEFT JOIN Stores s2 on s2.ProdStoreID = s.ID
			LEFT JOIN `Values` v1 on v1.ID = i.Value1
			LEFT JOIN `Values` v2 on v2.ID = i.Value2
			WHERE s.StoreChannelCode = '{$this->_channel_code}' OR s2.StoreChannelCode = '{$this->_channel_code}'
			AND (
				UPC = '$product_identifier_sanitized' or i.SKU = '$product_identifier_sanitized')
			LIMIT 1";

        if ($result = mysqli_query($ulk->db_cnx, $sql)) {
            $tresult = mysqli_fetch_assoc($result);
            if (isset($tresult['StoreID'])) {
                return $tresult;
            }

        } else {
            print "error: " . mysqli_error($ulk->db_cnx) . "\n";
        }

        return false;
    }

    public function find_channel_order_items_with_modified_status($storeid) {
        global $ulk;
        $sql = "SELECT
					oco.*,
					co.ExternalOrderID, 
					coi.ExternalOrderItemID,
					o.CustomerComments,
					os.OrderStatus,
					coi.OrderItemID,
					coi.ChannelOrderItemID,
					coi.ChannelOrderItemUPC,
					sh.TrackingNumber,
					sh.ShippedQty,
					sh.ShippedDate,
					shm.Shipper,
					shm.TrackingURL,
					s1.ID as InternalItemStatusID,
					s2.ID as ExternalItemStatusID,
					s1.ItemStatus as InternalItemStatusName,
					s2.ItemStatus as ExternalItemStatusName
				FROM ChannelOrder co 
				LEFT JOIN ChannelOrderItem coi on coi.ChannelOrderID = co.ChannelOrderID
				LEFT JOIN OrderChannelOrder oco on oco.ChannelOrderID = co.ChannelOrderID
				LEFT JOIN Orders o on o.ID = oco.OrderID
				LEFT JOIN OrderStatus os on os.ID = o.OrderStatus
				LEFT JOIN OrderDetails as od ON od.ID = coi.OrderItemID
				LEFT JOIN Shipments as sh ON sh.LineID = od.ID
				LEFT JOIN ShipMethods as shm ON shm.ShipMethodID = sh.ShipMethodID
				LEFT JOIN ItemStatus as s1 ON s1.ID = od.ItemStatus
				LEFT JOIN ItemStatus as s2 ON s2.ID = coi.ChannelOrderItemStatusID
				WHERE 
				co.ChannelCode = '{$this->_channel_code}' 
				AND coi.ChannelOrderItemStatusID <>  od.ItemStatus
				AND od.ItemStatus <> 7
				AND o.StoreID = {$storeid}
				ORDER BY co.ExternalOrderID, oco.OrderID, od.ItemStatus ";

        if ($result = mysqli_query($ulk->db_cnx, $sql)) {
            $rows = array();
            while ($tresult = mysqli_fetch_assoc($result)) {
                if (!isset($rows[ $tresult['ExternalOrderID'] ])) {
                    $rows[ $tresult['ExternalOrderID'] ] = $tresult;
                }
                $rows[ $tresult['ExternalOrderID'] ]['items'][] = $tresult;
            }

            return $rows;

        } else {
            print "error: " . mysqli_error($ulk->db_cnx) . "\n";
        }

        return false;
    }

    public function update_channel_order_items_using_internal_item_status( $channel_orders ) {
        global $ulk;
        foreach ($channel_orders as $order) {
            foreach ($order['items'] as $item) {
                $sql = "UPDATE ChannelOrderItem "
                    . " SET ChannelOrderItemStatusID = '{$item['InternalItemStatusID']}' "
                    . " WHERE ChannelOrderItemID = '{$item['ChannelOrderItemID']}'";
                if (!mysqli_query($ulk->db_cnx, $sql)) {
                    print "failed to update channel order item status: " . mysqli_error($ulk->db_cnx) . "\n";
                } else {
                    print "updated channel order item "
                        . "	{$item['ChannelOrderItemID']} status from "
                        . " {$item['ExternalItemStatusName']} => {$item['InternalItemStatusName']}\n";
                }
            }
        }

        return;
    }


    private function split_orders_by_store( $orders ) {
        $store_orders = array();
        foreach ($orders as $oid => $order) {

            $order['vendors'] = array();
            foreach ($order['items'] as $order_item) {
                if (isset($order_item['upc']) or isset($order_item['sku'])) {

                    $order_item_upc_or_sku = (empty($order_item['upc'])) ? $order_item['sku'] : $order_item['upc'];


                    $manuaFix = array(
                        //bad => good
                        "102_LV6S70676-XS" => "102_LV6S70676-XS-Black",
                        "102_LV6S9495-XS" => "102_LV6S9495-XS-Black/White",
                        "102_LV5F2659-M" => "102_LV5F2659-M-Black",
                        "102_LV6S61748-S" => "102_LV6S61748-S-CREAMY WHITE",
                        "102_LV5F40646-M" => "102_LV5F40646-M-Black Combo",
                        "102_LV6S9500-L" => "102_LV6S9500-L-Black",
                        "97_166S113-M" => "97_166S113-M-Black",
                        "97_166S113-L" => "97_166S113-L-Black",
                        "102_LV5F61243-Heather Grey-S" => "102_LV5F61243-S-Granite",
                        "102_LV6S9495-L" => "102_LV6S9495-L-Black/White",
                        "102_LV5F9421-M-Blk Wht Combo" => "102_LV5F9421-M-BLK/WHT COMBO",
                        "102_LV6S2729-M" => "102_LV6S2729-M-Cobalt Combo",
                        "102_LV6S34119-M" => "102_LV6S34119-M-MED. HEATHER GREY/IVORY",
                        "102_LV5F61369-M-Granite Cream" => "102_LV5F61369-M-GRANITE/CREAM",
                        "102_LV5F40479-S-Emerald Coast" => "102_LV5F40479-S-Emerald Coast",
                        "102_LV6S70640-L" => "102_LV6S70640-L-MINT/NAVY/IVORY",
                        "102_LV6S70645-S" => "102_LV6S70645-S-White",
                        "102_LV6S2731-M" => "102_LV6S2731-M-Black/White",
                        "102_LV6S70645-M" => "102_LV6S70645-M-White",
                        "102_LV6S34119-L" => "102_LV6S34119-L-MED. HEATHER GREY/IVORY",
                        "102_LV6S70669-M" => "102_LV6S70669-M-LT.BLUE",
                        "102_LV6S61666-S" => "102_LV6S61666-S-Navy/White",
                        "102_LV6S2744-XS" => "102_LV6S2744-XS-Baked Clay",
                        "101_P-116-M" => "101_P-116-M-Black_White",
                        "78_FG1108L-S" => "78_FG1108L-Black/ White-S",
                        "97_693A591-M" => "97_693A591-M-Black",
                        "103_W52096-B-M" => "103_W52096-B-M-Black",
                        "103_W52135-M" => "103_W52135-M-Constellation"

                    );

                    if(isset($manuaFix[$order_item_upc_or_sku]))
                    {
                        print "Manually mapping " . $order_item_upc_or_sku . " to " . $manuaFix[$order_item_upc_or_sku] . "\n";


                        $order_item_upc_or_sku = $manuaFix[$order_item_upc_or_sku];
                    }

                    $item_store_info = $this->get_store_product_by_upc_or_sku($order_item_upc_or_sku);

                    $item_store_id = ($item_store_info['ReferenceStoreID']) ? $item_store_info['ReferenceStoreID'] : $item_store_info['StoreID'];

                    if (!$item_store_id) {
                        print "WARNING: failed to find the store by order item upc {$order_item['upc']}\n";
                        var_dump($order_item);
                        continue;
                    }

                    $order['store_id'] = $item_store_id;
                    $order_item['product_vendor'][ $item_store_info['ProductID'] ] = $item_store_info['VendorID'];
                    $order_item['InventoryID'] = $item_store_info['InventoryID'];
                    $order_item['UPC'] = $item_store_info['UPC'];
                    $order_item['ProductID'] = $item_store_info['ProductID'];
                    $order_item['ProductSKU'] = $item_store_info['ProductSKU'];
                    $order_item['Value1'] = $item_store_info['Value1'];
                    $order_item['Value2'] = $item_store_info['Value2'];
                    $order_item['PropertyID1'] = $item_store_info['PropertyID1'];
                    $order_item['PropertyID2'] = $item_store_info['PropertyID2'];
                    if (!isset($store_orders[ $item_store_id ][ $oid ])) {
                        $store_orders[ $item_store_id ][ $oid ] = $order;
                        $store_orders[ $item_store_id ][ $oid ]['vendors'] = array();
                        $store_orders[ $item_store_id ][ $oid ]['items'] = array();
                    }
                    $store_orders[ $item_store_id ][ $oid ]['vendors'][ $item_store_info['VendorID'] ] = $item_store_info['VendorID'];
                    $store_orders[ $item_store_id ][ $oid ]['items'][] = $order_item;
                }
            }
        }

        return $store_orders;

    }

    /**
     * Import parsed channel order data and create orders in our system
     *
     * @param type $parsed_data
     *
     * @return int number of zindigo orders created
     */
    public function import_orders( $parsed_data ) {

        $num_orders_parsed = count($parsed_data);
        $num_orders_created = 0;
        print "importing $num_orders_parsed parsed orders\n";

        $store_orders = $this->split_orders_by_store($parsed_data);
        foreach ($store_orders as $store_id => $tmp_orders) {
            print "\n\nstore: $store_id\n";
            foreach ($tmp_orders as $oid => $order) {
                print "\n	got order: $oid {$order['billing_email']}\n";
                foreach ($order['items'] as $item) {
                    print "		item: {$item['upc']} {$item['ProductSKU']} X {$item['qty']} {$item['name']}\n";
                }

                $order_id = $this->create_order($order);
                if ($order_id) {
                    $this->_imported_orders[ $order_id ] = $order_id;
                    $num_orders_created ++;
                }
            }
        }

        print "created $num_orders_created orders\n";

        return $num_orders_created;
    }

    public function create_channel_order_record( $order_id, $order ) {
        global $ulk;
        $sql = "INSERT INTO ChannelOrder ("
            . "ChannelCode,"
            . "ExternalOrderID,"
            . "ChannelOrderTransmittedTS,"
            . "ChannelOrderPlacedTS"
            . ") VALUES ("
            . "'{$this->_channel_code}',"
            . "'{$order['external_order_id']}',"
            . "NOW(),"
            . "'{$order['order_date']}'"
            . ")";
        if (!mysqli_multi_query($ulk->db_cnx, $sql)) {
            die("error: " . mysqli_error($ulk->db_cnx) . "\n");
        }
        $result = mysqli_query($ulk->db_cnx, $sql);
        if (!$result) {
            die("failed to create channel order record: " . mysqli_error($ulk->db_cnx) . "\n");
        }
        $channel_order_id = mysqli_insert_id($ulk->db_cnx);

        // map new record to internal order record
        //
        $this->create_order_channel_order_record($order_id, $channel_order_id);
        $this->create_channel_order_interaction_record($channel_order_id, $this->_channel_interaction_id);

        return $channel_order_id;
    }

    public function create_order_channel_order_record( $order_id, $channel_order_id ) {
        global $ulk;
        $sql = "INSERT INTO OrderChannelOrder ("
            . "OrderID,"
            . "ChannelOrderID"
            . ") VALUES ("
            . "'$order_id',"
            . "'$channel_order_id'"
            . ")";
        $result = mysqli_query($ulk->db_cnx, $sql);
        if (!$result) {
            die("failed to map channel order to order: " . mysqli_error($ulk->db_cnx) . "\n");
        }

        return true;
    }

    public function create_channel_order_interaction_record( $channel_order_id, $channel_interaction_id ) {
        global $ulk;
        $sql = "INSERT INTO ChannelOrderInteraction ("
            . "ChannelOrderID,"
            . "ChannelInteractionID"
            . ") VALUES ("
            . "'$channel_order_id',"
            . "'$channel_interaction_id'"
            . ")";
        $result = mysqli_query($ulk->db_cnx, $sql);
        if (!$result) {
            die("failed to map channel order to order: " . mysqli_error($ulk->db_cnx) . "\n");
        }

        return true;
    }


    public function create_channel_order_item_record( $channel_order_id, $order_item_id, $order_item ) {
        global $ulk;
        $sql = "INSERT INTO ChannelOrderItem
					(
						`ChannelOrderID`, 
						`OrderItemID`, 
						`ExternalOrderItemID`, 
						`ChannelOrderItemUPC`, 
						`ChannelOrderItemSKU`, 
						`ChannelOrderItemStatusID`
					) VALUES (
						'$channel_order_id', 
						'$order_item_id', 	
						'{$order_item['external_order_item_id']}', 
						'{$order_item['upc']}', 
						'{$order_item['sku']}', 
						'1'
						)";
        $result = mysqli_query($ulk->db_cnx, $sql);
        if (!$result) {
            die("error: " . mysqli_error($ulk->db_cnx) . "\n");
        }

        return mysqli_insert_id($ulk->db_cnx);
    }


    public function lookup_shipping_expenses( $service_name, $amount ) {
        global $ulk;
        $service_name = addslashes($service_name);
        $amount = addslashes($amount);
        $sql = "SELECT * FROM ShipRates WHERE ServiceName = '$service_name' AND OrderAmountMin < '$amount' and OrderAmountMax > '$amount'";
        $result = mysqli_query($ulk->db_cnx, $sql);
        if (!$result) {
            die("failed to lookup shipping expenses for $service_name: " . mysqli_error($ulk->db_cnx) . "\n");
        }
        $row = mysqli_fetch_assoc($result);

        return $row;

    }

    public function create_order( $new_order ) {
        global $ulk;

        error_reporting(E_ALL);
        $sql = "CALL CreateOrder("
            . "'{$new_order['store_id']}', "
            . "'{$new_order['user_id']}', "
            . "'{$new_order['transaction_id']}', "
            . "'{$new_order['company']}', "
            . "'{$new_order['billing_fname']}', "
            . "'{$new_order['billing_lname']}', "
            . "'{$new_order['billing_address1']}', "
            . "'{$new_order['billing_address2']}', "
            . "'{$new_order['billing_city']}', "
            . "'{$new_order['billing_state']}', "
            . "'{$new_order['billing_zip']}', "
            . "'{$new_order['billing_county']}', "
            . "'{$new_order['billing_country']}', "
            . "'{$new_order['billing_phone']}', "
            . "'{$new_order['billing_fax']}', "
            . "'{$new_order['billing_email']}', "
            . "'{$new_order['shipping_company']}', "
            . "'{$new_order['shipping_residential']}', "
            . "'{$new_order['shipping_fname']}', "
            . "'{$new_order['shipping_lname']}', "
            . "'{$new_order['shipping_address1']}', "
            . "'{$new_order['shipping_address2']}', "
            . "'{$new_order['shipping_city']}', "
            . "'{$new_order['shipping_state']}', "
            . "'{$new_order['shipping_zip']}', "
            . "'{$new_order['shipping_county']}', "
            . "'{$new_order['shipping_country']}', "
            . "'{$new_order['shipping_phone']}', "
            . "'{$new_order['shipping_comments']}', "
            . "'{$new_order['coupon_discount']}', "
            . "'{$new_order['coupon_discount']}', "
            . "'{$new_order['repid']}', "
            . "'{$new_order['termsagree']}', "
            . "'{$new_order['pageid']}', "
            . "@orderid,@ordernum);Select @orderid,@ordernum;";

        if (!mysqli_multi_query($ulk->db_cnx, $sql)) {
            die("error: " . mysqli_error($ulk->db_cnx) . "\n");
        }
        mysqli_next_result($ulk->db_cnx);

        $rs2 = mysqli_store_result($ulk->db_cnx);
        $row = mysqli_fetch_row($rs2);
        $ulk->cleanConnection();
        mysqli_free_result($rs2);

        $orderid = $row[0];
        $ordernum = $row[1];

        $channel_order_id = $this->create_channel_order_record($orderid, $new_order);

        if ($orderid) {
            print "\nSUCCESS! created order $orderid [$ordernum] and channel order: $channel_order_id\n";
        } else {
            print "\nFAILED to create channel order {$new_order['transaction_id']}\n";

            return false;
        }

        $order_total = 0.00;
        $order_total_taxes = 0.00;
        $vendor_total = array();

        foreach ($new_order['items'] as $item_id => $order_item) {
            // aggregate total amounts by line item
            //
            $line_total = round($order_item['price'] * $order_item['qty'], 2);
            $line_total_tax = round($order_item['tax'] * $order_item['qty'], 2);
            $order_total += $line_total;
            $order_total_taxes += $line_total_tax;

            // aggregate total amount by vendor
            //
            $current_product_vendor_id = $order_item['product_vendor'][ $order_item['ProductID'] ];
            if (!isset($vendor_total[ $current_product_vendor_id ])) {
                $vendor_total[ $current_product_vendor_id ] = 0.00;
            }
            $vendor_total[ $current_product_vendor_id ] += $line_total;

            // create orderdetails record for each item
            //
            $item_sql = "INSERT INTO OrderDetails SET "
                . " OrderID = $orderid,"
                . " ItemStatus = 1,"
                . " ProductID = '{$order_item['ProductID']}',"
                . " SKU = '{$order_item['upc']}',"
                . " ItemUPC = '{$order_item['UPC']}',"
                . " ItemInventoryID = '{$order_item['InventoryID']}',"
                . " Qty = '{$order_item['qty']}',"
                . " Price = '{$order_item['price']}',"
                . " Currency = '{$new_order['currency']}',"
                . " TaxAmount = '{$order_item['tax']}',"
                . " Conversion = '{$order_item['price']}',"
                . " LineTotal = '{$line_total}';";

            $item_result = mysqli_query($ulk->db_cnx, $item_sql);
            if (!$item_result) {
                die("failed to create order details: " . mysqli_error($ulk->db_cnx) . "\n");
            }
            $order_details_id = mysqli_insert_id($ulk->db_cnx);
            print "	created order details: $order_details_id\n";

            // create channelorderitem
            //
            $channel_order_item_id = $this->create_channel_order_item_record($channel_order_id, $order_details_id, $order_item);
            print "	created channel order item: $channel_order_item_id\n";

            // create orderdetailsproperties record for each item's property values
            //
            $prop_sql = "";
            if($order_item['PropertyID2'] != null) {
                $prop_sql = "INSERT INTO OrderDetailProperties (OrderDetailID,PropertyID,ValueID) VALUES "
                    . "($order_details_id,{$order_item['PropertyID1']},{$order_item['Value1']}),"
                    . "($order_details_id,{$order_item['PropertyID2']},{$order_item['Value2']})";
            } else if($order_item['PropertyID1'] != null) {
                $prop_sql = "INSERT INTO OrderDetailProperties (OrderDetailID,PropertyID,ValueID) VALUES "
                    . "($order_details_id,{$order_item['PropertyID1']},{$order_item['Value1']})";
            }


            mysqli_query($ulk->db_cnx, $prop_sql);


            //this is where we manage the inventory from the order
            $invQuery = "Update Inventory set Qty = if((Qty-{$order_item['qty']}) < 0, 0, Qty - {$order_item['qty']}) where InventoryID = (Select InventoryID from vw_Inventory where SKU = '{$order_item['upc']}' limit 1)";
            $inv_result = mysqli_query($ulk->db_cnx, $invQuery);
            if (!$inv_result) {
                die("failed to update inventory: " . mysqli_error($ulk->db_cnx) . "\n");
            }

            print "	created order item properties: $channel_order_item_id\n";
        }

        // update currency, status and total
        //
        $sql = "UPDATE Orders SET OrderStatus = 6, "
            . "Currency = '{$new_order['currency']}',"
            . "Taxes = '$order_total_taxes', "
            . "TotalAmount = '$order_total' "
            . "WHERE ID = '$orderid'";
        if (mysqli_query($ulk->db_cnx, $sql)) {
            print "	updated order status, currency, and and total for orderid: $orderid\n";
        }


        // create a shipping expense record for each vendor indicating the specified shipping preferences
        // todo: replace with lookup code to determine appropriate shipping expense

        foreach ($new_order['vendors'] as $vendor_id) {
            $shipping_data = $this->lookup_shipping_expenses($new_order['shipping_method'], $vendor_total[ $vendor_id ]);

            $ship_sql = "INSERT INTO ShippingExpenses "
                . " ("
                . "OrderID,"
                . "VendorID,"
                . "ShippingExpenses,"
                . "ShipMethod,"
                . "TransitDays,"
                . "AgentShippingExpenses"
                . ") VALUES ("
                . "$orderid, "
                . "$vendor_id, "
                . "'{$shipping_data['ShipAmount']}',"
                . "'{$shipping_data['ServiceType']}',"
                . "'{$shipping_data['DeliveryDaysMax']}',"
                . " '0.00'"
                . ") ";
            mysqli_query($ulk->db_cnx, $ship_sql);

            //mike wants an order notification
            $email = new EMAIL();
            $storedata =$ulk->getStore($new_order['store_id']);
            $email->EmailVendorNotices("orders@zindigo.com", null, null, $orderid, $new_order['store_id'], $storedata, $ulk);

        }
        return $orderid;
    }


}
