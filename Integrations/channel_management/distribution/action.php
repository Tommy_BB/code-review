<?php

//   .-. _       .-.       _ .-.          .-.  _             
//   : ::_;     .' `.     :_;: :         .' `.:_;            
// .-' :.-. .--.`. .'.--. .-.: `-. .-..-.`. .'.-. .--. ,-.,-.
//' .; :: :`._-.': : : ..': :' .; :: :; : : : : :' .; :: ,. :
//`.__.':_;`.__.':_; :_;  :_;`.__.'`.__.' :_; :_;`.__.':_;:_;
//                                                           

//error_reporting(E_ALL);
	require_once(__DIR__ . "/../../inventory/zindigo_inventory_manager.php");
	require_once(__DIR__ . "/../abstract_channel_action.php");
	require_once(__DIR__ . "/order_manager.php");
	require_once(__DIR__ . "/return_manager.php");

	class Distribution_Channel_Action extends Abstract_Channel_Action {

		public $_order_manager;
		public $_payload;
		public $_store_id;

		/**
		 * Fetch a feed from a channel and process it
		 *
		 * @param type $store_id
		 *
		 * @throws Exception
		 */

		public function pull( $store_id = null ) {

			if ($this->_channel_config['aggregate_stores']) {

				// FETCH FEED
				//
				$file_content = $this->fetch_feed($this->_channel_config);

				// PROCEESS FEED
				//
				$this->process_feed($file_content);

			} else {

				// FETCH FEED
				//
				$file_content = $this->fetch_feed($this->_channel_config['stores'][ $store_id ]);

				// PROCEESS FEED
				//
				$this->process_feed($file_content);
			}
		}

		/**
		 * Process the content of a feed pulled from a channel
		 *
		 * @param type $raw_feed_data
		 */

		public function process_feed( $raw_feed_data ) {
			// PARSE
			//
			$parsed_data = $this->parse_feed($raw_feed_data);

			// PROCESS PAYLOAD
			//
			print "processing data\n";
			switch ($this->_feed_type) {
				case 'return':
					$return_manager = new Return_Manager($this->_channel_code);
					$return_manager->import_returns($parsed_data);
					break;
				case 'orders':
					$this->_order_manager = new Distribution_Channel_Order_Manager($this->_channel_code, $this->_channel_interaction_id);
					$this->_order_manager->import_orders($parsed_data);
					break;
				default :
					die("Unsupported feed type: $this->_feed_type\n");
					break;
			}
		}

		public function push( $store_id = null ) {

			if ($this->_channel_config['aggregate_stores']) {

				// GENERATE
				//
				$generated_file = $this->generate_feed($store_id);

				// DELIVER
				//
				$remote_path = $this->deliver_feed($generated_file, $this->_channel_config);

				// LOG INTERACTION IN DB
				//
				$this->update_channel_interaction_record(file_get_contents($generated_file), $remote_path, "");

				// TRIGGER POST PUSH BUSINESS LOGIC
				//
				$this->after_push();
			} else {

				if (!isset($store_id)) {
					return false;
				}

				// GENERATE
				//
				$generated_file = $this->generate_feed($store_id);

				// DELIVER
				//
				$remote_path = $this->deliver_feed($generated_file, $this->_channel_config['stores'][ $store_id ]);

				// LOG INTERACTION IN DB
				//
				$this->update_channel_interaction_record(file_get_contents($generated_file), $remote_path, "");

				// TRIGGER POST PUSH BUSINESS LOGIC
				//
				$this->after_push();

			}

		}

		/**
		 * Generate a feed file in a format that can be pushed to the channel
		 * Returns the path to the generated file.
		 *
		 * @param type $param1
		 *
		 * @return file path
		 * @throws Exception
		 */

		private function generate_feed( $store_id = null ) {

			if (!isset($this->_channel_config['stores'])) {
				throw new Exception("No stores specified in the channel config file");
			}

			// LOAD FEED FORMATTER
			// try to load the feed formatter for the channel
			//
			$channel_class_prefix = ucfirst(strtolower($this->_channel_name));
			$channel_feed_formatter = $channel_class_prefix . '_Feed_Formatter';
			$channel_feed_formatter_path = __DIR__ . "/{$this->_channel_name}/feed_formatter.php";


			if (file_exists($channel_feed_formatter_path)) {
				include_once $channel_feed_formatter_path;
			} else {
				print "channel {$this->_channel_name} feed formatter doesn't exist\n";
				throw new Exception("channel {$this->_channel_name} feed formatter doesn't exist");
			}

			$this->create_channel_interaction_record($this->_feed_type);

			// GET DATA
			$this->_payload = array();

			if ($this->_channel_config['aggregate_stores']) {
				foreach ($this->_channel_config['stores'] as $store_id => $store_config) {
					$this->generate_feed_payload($store_id, $store_config);
				}
			} else {
				$store_config = $this->_channel_config['stores'][ $store_id ];
				$this->generate_feed_payload($store_id, $store_config);
			}

			if (count($this->_payload) < 1) {
				die("Stopping, nothing to transmit\n");
			}

			// GENERATE FEED
			$file_prefix = (isset($this->_channel_config['file_name_prefix'][ $this->_feed_type ])) ? $this->_channel_config['file_name_prefix'][ $this->_feed_type ] : null;
			$generated_file = $channel_feed_formatter::render($this->_feed_type, $this->_payload, $file_prefix);

			return $generated_file;
		}

		private function generate_feed_payload( $store_id, $store_config ) {

			print "generating payload for store $store_id / category: {$store_config['category']}	";

			$store_info = Zindigo_Inventory_Manager::get_store_info($store_id);
            $real_storeid = $store_id;

			// check store mirroring settings
			if (isset($store_info['ProdStoreID']) && $store_info['ProdStoreID']) {
				print " [store $store_id mirrors store {$store_info['ProdStoreID']}]";
				// mirror another store
				$store_id = $store_info['ProdStoreID'];
			}

			print "\n";

			switch (strtolower($this->_feed_type)) {
				case 'inventory':
					$this->_payload = array_merge($this->_payload, Zindigo_Inventory_Manager::get_store_inventory($store_id, $store_config['category']));
                    $this->_payload = array_merge($this->_payload, Zindigo_Inventory_Manager::get_store_inventory_removal($store_id));
                    $this->_payload = array_merge($this->_payload, Zindigo_Inventory_Manager::get_store_inventory_removal_deleted($store_id));
                    break;
				case 'product':
					$this->_payload = array_merge($this->_payload, Zindigo_Inventory_Manager::get_store_products($store_id, $store_config['category'], $store_config['disabled'], $store_config['disabled_ids']));
					break;
				case 'price':
					$this->_payload = array_merge($this->_payload, Zindigo_Inventory_Manager::get_store_inventory($store_id, $store_config['category']));
					break;
				case 'order_status':
					if (!isset($this->_order_manager)) {
						$this->_order_manager = new Distribution_Channel_Order_Manager($this->_channel_code);
					}
					$this->_payload = array_merge($this->_payload, $this->_order_manager->find_channel_order_items_with_modified_status($real_storeid));
					break;
				case 'refund_requests':
					$this->_payload = array_merge($this->_payload, Zindigo_Inventory_Manager::get_store_accepted_returns($this->_channel_code, $real_storeid));
                    $this->_payload = array_merge($this->_payload, Zindigo_Inventory_Manager::get_store_rejected_returns($this->_channel_code, $real_storeid));
					break;
                case 'custom_oos':
                    $this->_payload = array_merge($this->_payload, array("EMPTY"));
                    break;
				default:
					print "Cannot push unknown feed type: $this->_feed_type\n";
					print "push [inventory|product|price|order_status|refund_requests]\n\n";
					exit;
					break;
			}

			return true;
		}

		/*
		 * Create and push a feed to a distribution channel
		 *
		 */

		/**
		 * Trigger business logic that should only happen after successfully pushing a feed
		 *
		 * @param type $payload
		 */

		private function after_push() {
			// POST PUSH ACTIONS
			//
			switch (strtolower($this->_feed_type)) {
				case 'inventory':
					break;
				case 'product':
					break;
				case 'price':
					break;
				case 'order_status':

					// we need to update the statuses of channel order items that were sent in the feed
					// this way we prevent ourselves from generating duplicate notifications
					// about the same items to the distribution channels

					$this->_order_manager->update_channel_order_items_using_internal_item_status($this->_payload);
					break;
				case 'refund_requests':
					break;
				default:
					break;
			}
		}

		protected function getChannelType() {
			return 'distribution';
		}
	}
