<?php
namespace ZMVC;

require_once "ZAction.php";

class ZActionFactory {

    public static function create($actionName, $storeId) {
        $actionName = self::formatActionName($actionName);
        $actionFilePath = self::getActionFilePath($actionName, $storeId);
        $actionClassName = self::getActionClassName($actionName);

        if (!file_exists($actionFilePath)) {
            throw new \Exception("Could not find action, {$actionName}", 404);
        }

        try {
            require_once $actionFilePath;

            // If the action file path points to a global store action, use that namespace
            if (false !== strpos($actionFilePath, 'zmvc/store/actions')) {
                $actionClassName = "ZMVC\\Store\\Actions\\{$actionClassName}";
            } else {
                $actionClassName = "\\{$actionClassName}";
            }

            $action = new $actionClassName;
            return $action;
        } catch (\Exception $e) {
            throw new \Exception("Could not load class, {$actionClassName}'", 500);
        }

        return false;
    }

    /**
     * @param $actionName
     * @param $storeId
     *
     * @return bool
     */
    protected static function getActionFilePath($actionName, $storeId) {

        // Check for store action first then, if not found go to the general "{ActionName}" implementation
        $isusingdefault = false;
        //this is for checking the default shop in shop because we havent made the mvc for the regular shop
        try {
            $shopTemplateConfigPath = __DIR__ . "/../../shoptemplates/{$storeId}/default_shop_in_shop.php";

            if (false == file_exists($shopTemplateConfigPath)) {
                throw new \Exception('Could not find mvc default configuration');
            }
            $isusingdefault = true;
        } catch (\Exception $error) {
            $isusingdefault = false;
        }

        $storeActions = (($isusingdefault) ? glob(__DIR__ . "/../../templates/default_shop_in_shop/actions/*") : glob(__DIR__ . "/../../templates/{$storeId}/actions/*"));
        if ($storeActions) {
            $requestedActionFilePath = self::findRequestedActionFilePath($storeActions, $actionName);
            if ($requestedActionFilePath) {
                return $requestedActionFilePath;
            }
        }

        // Check for base implementation of the requested action
        $zStoreActions = glob(__DIR__ . "/store/actions/*");
        if ($zStoreActions) {
            $requestedActionFilePath = self::findRequestedActionFilePath($zStoreActions, $actionName);
            if ($requestedActionFilePath) {
                return $requestedActionFilePath;
            }
        }
        return false;
    }

    /**
     * @param array $actionList
     *
     * @return bool
     */
    protected static function findRequestedActionFilePath(array $actionList, $requestedActionName)
    {
        if (!empty($actionList)) {
            $actionName = self::formatActionName($requestedActionName);
            $actionClassName = strtolower(self::getActionClassName($requestedActionName));

            foreach ($actionList as $actionFilePath) {
                if (strpos(strtolower($actionFilePath), $actionClassName) !== false) {
                    $actionFilePathParts = explode('/', $actionFilePath);
                    $foundActionName = str_replace('.php', '', end($actionFilePathParts));
                    // Require the class names to be the same as well
                    if (strtolower($foundActionName) == strtolower($actionClassName)) {
                        return $actionFilePath;
                    }
                }
            }
        }
        return false;
    }

    protected static function formatActionName($actionName) {
        $actionName = str_replace(array('_'), '', $actionName);
        return $actionName;
    }

    protected static function getActionClassName($actionName) {
        return ucwords($actionName).'Action';
    }
}