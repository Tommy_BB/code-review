<?php
    namespace ZMVC;

    use PDO;
    use PDOException;

    require_once "extensions/autoload.php";

    /**
     * Class ZWebApplication
     */
    class ZWebApplication
    {
        /**
         * @var
         */
        protected static $_instance;

        /**
         * @var int $_storeId
         */
        private $_storeId;

        /**
         *
         */
        protected function __construct()
        {
        }

        /**
         * @return mixed
         */
        public static function getInstance()
        {
            if (null === static::$_instance) {
                self::$_instance = new static;
            }

            return static::$_instance;
        }

        /**
         * @throws \Exception
         *
         * @param array $configuration
         * 'action'           => $_REQUEST['page'],    // this may become deprecated
         * 'storeId'          => $_REQUEST['storeid'], // this may become deprecated
         * 'brandWebStoreId'  => 114,                  // this is the brand's web store id
         * 'layout'           => 'store.twig',
         * 'theme'            => 'store',
         * 'db'               => (database configuration array),
         */
        public function run($configuration = array())
        {
            $actionName     = ($configuration['action']) ? $configuration['action'] : 'home';

            $this->_storeId = $configuration['storeId'];

            if (!empty($configuration['brandWebStoreId'])) {
                try {
                    if ($configuration['use_default']==true && $configuration['isInZindigoShop']==true && $configuration['brandWebStoreId']!=$configuration['storeId']){
                        $action = ZActionFactory::create($actionName, $configuration['storeId']);   
                    } else {
                        $action = ZActionFactory::create($actionName, $configuration['brandWebStoreId']);
                    }
                    
                } catch (\Exception $e) {
                    $action = null;
                }
            }

            if (!$action instanceof ZAction) {
                try {
                    $action = ZActionFactory::create($actionName, $this->_storeId);
                } catch (\Exception $e) {
                    try {
                        $action = ZActionFactory::create('error', $this->_storeId);
                        $action->setException($e);
                    } catch (\Exception $e) {
                        throw new \Exception('Could not locate an action to facilitate error messages.');
                    }
                }
            }

            $this->prepareDatabase($configuration);
            $this->prepareSessionManagement($configuration);
            $this->getController($configuration, $action)->run();
        }

        /**
         * @param $configuration
         *
         * @throws \Exception
         */
        protected function prepareDatabase($configuration)
        {
            \ActiveRecord\Config::initialize(
                function ($cfg) use ($configuration) {
                    $cfg->set_model_directory(__DIR__ . '/store/models');
                    $cfg->set_connections(
                        array(
                            'development' => strtr(
                                'mysql://{username}:{password}@{host}/{database}', array(
                                    '{username}' => $configuration['db']['user'],
                                    '{password}' => $configuration['db']['password'],
                                    '{host}'     => $configuration['db']['host'],
                                    '{database}' => $configuration['db']['database'],
                                )
                            ),
                        )
                    );
                }
            );

            try {
                $dbh = new PDO(
                    $configuration['db']['dsn'], $configuration['db']['user'], $configuration['db']['password']
                );
                $dbh->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
                ZRegistry::getInstance()->add('db', $dbh);
            } catch (PDOException $pErr) {
                throw new \Exception($pErr->getMessage());
            }
        }

        protected function prepareSessionManagement($configuration)
        {
            ZRegistry::getInstance()->add('session', $_SESSION);
        }

        /**
         * @param         $configuration
         *
         * @param ZAction $action
         *
         * @return ZController
         */
        protected function getController($configuration, ZAction $action)
        {
            /** @var ZController $controller */
            $controller = null;

            $controllerBasePath = ($configuration['isInZindigoShop']) ? ((isset($configuration['use_default'])) ? $this->getZindigoShopDefaultBaseTemplatePath() : $this->getZindigoShopBaseTemplatePath())
                : $this->getStoreBaseTemplatePath();

            $storeBaseControllerPath = $controllerBasePath . DIRECTORY_SEPARATOR . 'controllers';
            if (file_exists($storeBaseControllerPath . DIRECTORY_SEPARATOR . 'SiteController.php')) {
                require_once $storeBaseControllerPath . DIRECTORY_SEPARATOR . 'SiteController.php';
                $controller = \SiteController::getInstance();
            }

            $controller
                ->setAction($action)
                ->setLayout($configuration['layout'])
                ->setTheme($configuration['theme'])
                ->setStoreId((($hasDefault) ? 'default_shop_in_shop' : $this->_storeId))
                ->setDebugMode($configuration['debug']);

            /**
             * Sets the brand's web store id if applicable
             */
            if ($controller instanceof ZFacebookController and !empty($configuration['brandWebStoreId'])) {
                /** @var ZFacebookController $controller */
                $controller->setBrandWebStoreId($configuration['brandWebStoreId']);
            }

            /**
             * Sets the brand's default_shop_in_shop
             */
            if ($controller instanceof ZFacebookController and !empty($configuration['use_default'])) {
                /** @var ZFacebookController $controller */
                $controller->setDefaultTemplate();
            }

            /**
             * Sets the brand's default_category to parse
             */
            if ($controller instanceof ZFacebookController and !empty($configuration['default_category'])) {
                /** @var ZFacebookController $controller */
                $controller->setDefaultCategory($configuration['default_category']);
            }

            return $controller;
        }

        /**
         * @return string
         */
        private function getZindigoShopBaseTemplatePath()
        {
            return __DIR__ . "/../../shoptemplates/{$this->_storeId}";
        }

        /**
         * @return string
         */
        private function getZindigoShopDefaultBaseTemplatePath()
        {
            return __DIR__ . "/../../templates/default_shop_in_shop";
        }

        /**
         * @return string
         */
        private function getStoreBaseTemplatePath()
        {
            return __DIR__ . "/../../templates/{$this->_storeId}";
        }
    }