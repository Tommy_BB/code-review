<?php

    /**
     * 
     *         This class is used as the basis for any singleton based component
     */
    abstract class ZSingletonComponent
    {
        static $instance;

        protected function __construct()
        {

        }

        /**
         * @return mixed
         */
        public final static function getInstance()
        {
            if (static::$instance === null) {
                static::$instance = new static();
            }

            return static::$instance;
        }
    }