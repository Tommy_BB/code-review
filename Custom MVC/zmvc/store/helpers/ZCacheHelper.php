<?php
namespace ZMVC\Store\Helpers;

use Exception;

class ZCacheHelper {

    private static $_instance;
    public $dbh;

    protected function __construct() {
        require_once __DIR__ . "/../../../../../classes/classes.php";
        $this->dbh = new \DBH;
    }

    public static function getInstance() {
        if (null === static::$_instance) {
            static::$_instance = new static();
        }

        return static::$_instance;
    }

    public static function getCache($cacheName)
    {

        $memCacheObj = self::getInstance()->dbh->memcache_obj;

        try {
            $cacheObj = memcache_get($memCacheObj, $cacheName);
        } catch (Exception $e) {
            $cacheObj = "";
        }

        return $cacheObj;

    }

    public static function setCache($cacheName, $toCache, $time = 3600)
    {
        $memCacheObj = self::getInstance()->dbh->memcache_obj;

        $result = memcache_replace($memCacheObj, $cacheName, $toCache, 0, $time);
        if ($result == false) {
            memcache_set($memCacheObj, $cacheName, $toCache, 0, $time);
        }

        return true;

    }

}