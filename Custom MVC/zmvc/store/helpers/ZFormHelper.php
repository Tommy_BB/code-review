<?php
    namespace ZMVC\Store\Helpers;

    require_once __DIR__ . "/../../ZSingletonComponent.php";

    /**
     * @package ZMVC\Store\Helpers
     */
    class ZFormHelper extends \ZSingletonComponent
    {
        /**
         * @var
         */
        private $_token;

        /**
         * @param bool $encrypt
         *
         * @return bool|string
         */
        public function generateToken($encrypt = false)
        {
            if ($this->_token === null) {
                $this->_token = date('y-m-d h:i:s');
            }

            return ($encrypt) ? $this->encryptToken($this->_token) : $this->_token;
        }

        /**
         * @param $token
         * @param $tokenCipher
         *
         * @return bool
         */
        public function isValidToken($token, $tokenCipher)
        {
            return ($this->encryptToken($token) === $tokenCipher);
        }

        /**
         * @param string $fieldNamePrefix
         */
        public function tokenHtmlFields($fieldNamePrefix = "")
        {
            $tokenInputName       = "{$fieldNamePrefix}token";
            $tokenCipherInputName = "{$fieldNamePrefix}tokenCipher";

            $hiddenInputTpl = '<input type="hidden" name="%name%" value=""/>';

            echo strtr($hiddenInputTpl, array("%name%" => $tokenInputName));
            echo strtr($hiddenInputTpl, array("%name%" => $tokenCipherInputName));
        }

        /**
         * @param $token
         *
         * @return string
         */
        protected function encryptToken($token)
        {
            return md5($token);
        }
    }