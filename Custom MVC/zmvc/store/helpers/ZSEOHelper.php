<?php
namespace ZMVC\Store\Helpers;

use Exception;

class ZSEOHelper {

    private static $_instance;
    public $dbh;

    protected function __construct() {
        require_once __DIR__ . "/../../../../../classes/classes.php";
        $this->dbh = new \DBH;
    }

    public static function getInstance() {
        if (null === static::$_instance) {
            static::$_instance = new static();
        }
        return static::$_instance;
    }

    public static function findPagesSEO($storeid) {
        $currentPage = self::curPageURL();
        $pageName = self::parseSitemapDom($storeid, $currentPage);
        $tableName = (ISDEVELOPMENT ? "Brand_SEO_Pending" : "Brand_SEO");

        $dbconn = self::getInstance()->dbh->db_cnx;
        if(!isset($pageName) || $pageName == "")
        {
            $homeResult = mysqli_query($dbconn, "SELECT * FROM $tableName WHERE StoreID = '$storeid' AND Page = 'home'");
            $defaultResult = mysqli_query($dbconn, "SELECT * FROM $tableName WHERE StoreID = '$storeid' AND Page = 'DEFAULTSEO'");

            if(mysqli_num_rows($defaultResult) >= 1)
            {
                return mysqli_fetch_assoc($defaultResult);
            } else if(mysqli_num_rows($homeResult) >= 1)
            {
                return mysqli_fetch_assoc($homeResult);
            } else {
                return false;
            }
        } else {

            $pageName = mysqli_escape_string($dbconn,$pageName);
            if(strlen($pageName) > 15) return false;
            $pageResult = mysqli_query($dbconn, "SELECT * FROM $tableName WHERE StoreID = '$storeid' AND Page = '".$pageName."'");
            if(mysqli_num_rows($pageResult) >= 1)
            {
                return mysqli_fetch_assoc($pageResult);
            } else {
                return false;
            }
        }
        return false;

    }

    public static function curPageURL() {
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

    public static function checkPageSEO($storeid) {
        $dbconn = self::getInstance()->dbh->db_cnx;
        $query = <<<EOSQL
            SELECT * FROM Brand_SEO WHERE StoreID = {self::sqlEscape($storeid)}
EOSQL;
        $pageSEO = mysqli_query($dbconn, $query);
        if(mysqli_num_rows($pageSEO) >= 1) {
            $pageSEO = mysqli_fetch_assoc($pageSEO);
            return $pageSEO;
        } else {
            return null;
        }
    }

    public static function sqlEscape($string) {
        $dbconn = self::getInstance()->dbh->db_cnx;
        return mysqli_escape_string($dbconn, $string);
    }

    /*
     * This parses categories and returns the tree string name
     */
    public static function parseDynamic($storeid, $queryString) {
        $dbconn = self::getInstance()->dbh->db_cnx;
        $dynamicId = "";
        $dynamicName = "";
        foreach($queryString as $param => $value)
        {
            $currentParam = strtolower($param);
            if($currentParam == "contentid" && is_numeric($value))
            {
                $dynamicId = $value;
            }
        }
        if($dynamicId != "") {
            $query = "SELECT Name FROM DynamicContent WHERE StoreID = '" . $storeid . "' AND ID = '" .$dynamicId. "'";
            $result = mysqli_query($dbconn, $query);
            $dynamicName = mysqli_fetch_assoc($result);
            $dynamicName = $dynamicName['Name'];
        }
        if(strlen($dynamicName) <= 1)
            $break = 0;

        return $dynamicName;

    }

    /*
     * This parses categories and returns the tree string name
     */
    public static function parseCategories($storeid, $queryString) {
        $dbconn = self::getInstance()->dbh->db_cnx;
        $catid = "";
        $subcatid = "";
        $subsubcatid = "";
        $subsubsubcatid = "";
        $queryIds = "";
        $isKeyword = false;

        foreach($queryString as $param => $value)
        {
            $currentParam = strtolower($param);
            if($currentParam == "categoryid" && is_numeric($value))
            {
                $catid = $value;
                $queryIds .= ($queryIds != "" ? ",".$value : $value);
            }
            if($currentParam == "subcatid" && is_numeric($value))
            {
                $subcatid = $value;
                $queryIds .= ($queryIds != "" ? ",".$value : $value);
            }
            if($currentParam == "subsubcatid" && is_numeric($value))
            {
                $subsubcatid = $value;
                $queryIds .= ($queryIds != "" ? ",".$value : $value);
            }
            if($currentParam == "subsubsubcatid" && is_numeric($value))
            {
                $subsubsubcatid = $value;
                $queryIds .= ($queryIds != "" ? ",".$value : $value);
            }
            if($currentParam == "action" && strtolower($value) == "keywords")
            {
                $isKeyword = true;
            }
        }

        foreach($queryString as $param => $value)
        {
            $currentParam = strtolower($param);
            if($isKeyword == true && $currentParam == "keytitle")
                $currentCat = $value;
        }

        if($queryIds != "") {
            $catTrees = array();
            $query = "SELECT ID, Name FROM StoresCategories WHERE StoreID = $storeid AND ID IN (" . $queryIds . ")";
            $result = mysqli_query($dbconn, $query);
            while ($row = mysqli_fetch_assoc($result)) {
                $catTrees[] = $row;
            }

            $currentCat = "";
            foreach ($catTrees as $cat) {
                if ($cat['ID'] == $catid)
                    $currentCat .= $cat['Name'];
                if ($cat['ID'] == $subcatid || $cat['ID'] == $subsubcatid || $cat['ID'] == $subsubsubcatid)
                    $currentCat .= " -> " . $cat['Name'];;
            }
        }

        if(strlen($currentCat) <= 1)
            $break = 0;

        return $currentCat;
    }

    /*
     * This parses the url structure and returns the page name
     */
    public static function parseSitemapDom($storeid, $url) {

        $parts = parse_url($url);

        if(!isset($parts['query']) || strlen($parts['query']) <= 3) return false;

        parse_str($parts['query'], $queryString);

        if(!isset($queryString['page'])) return false;

        //we need to do special things for categories
        if($queryString['page'] == "categoryproducts")
        {
            return self::parseCategories($storeid, $queryString);
        } else if($queryString['page'] == "dynamic")
        {
            return self::parseDynamic($storeid, $queryString);
        } else {
            return $queryString['page'];
        }
    }

    public static function getCurrentLiveMappedSEO($storeid)
    {
        $dbconn = self::getInstance()->dbh->db_cnx;
        $mappedPages = array();
        $query = "SELECT page FROM Brand_SEO WHERE StoreID ='".$storeid."'";
        $result = mysqli_query($dbconn, $query);
        while($row = mysqli_fetch_assoc($result))
        {
            $mappedPages[] = self::sqlEscape($row['page']);
        }

        return $mappedPages;
    }

    public static function getCurrentPendingMappedSEO($storeid)
    {
        $dbconn = self::getInstance()->dbh->db_cnx;
        $mappedPages = array();
        $query = "SELECT page FROM Brand_SEO_Pending WHERE StoreID ='".$storeid."'";
        $result = mysqli_query($dbconn, $query);
        while($row = mysqli_fetch_assoc($result))
        {
            $mappedPages[] = self::sqlEscape($row['page']);
        }

        return $mappedPages;
    }


    /*
     * read first byte of sitemap
     */
    public static function checkStoreSitemap($storeid) {
        if(false === @file_get_contents("http://stores.zindigo.com/templates/".$storeid."/sitemap.xml",0,null,0,1))
        {
            return false;
        } else {
            return true;
        }
    }

    /*
     * reads all bytes from sitemap
     */
    public static function readStoreSitemap($storeid) {
        return @file_get_contents("http://stores.zindigo.com/templates/".$storeid."/sitemap.xml");
    }



}