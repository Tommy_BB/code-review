<?php
	namespace ZMVC\Store\Helpers;

	use Exception;
	use PDO;
	use ZMVC\Store\Models\StoreProduct;
	use ZMVC\ZRegistry;

	require_once __DIR__ . "/../../ZRegistry.php";
	require_once __DIR__ . "/../models/StoreProduct.php";

	/**
	 * Class ZStoreProductsHelper
	 *
	 * @package ZMVC\Store\Helpers
	 */
	class ZStoreProductsHelper {
		/**
		 * @var
		 */
		private static $_instance;

		/**
		 * @var \DBH
		 */
		public $dbh;

		/**
		 * @var \storeprods
		 */
		public $storeprods;

		/**
		 * @var \products $products
		 */
		public $products;

		/**
		 * @var \discounts
		 */
		public $discounts;

		/**
		 *
		 */
		protected function __construct() {
			require_once __DIR__ . "/../../../../../classes/classes.php";
			$this->dbh = new \DBH;

			require_once __DIR__ . "/../../../../../classes/storeprods.php";
			$this->storeprods = new \storeprods;

			require_once __DIR__ . "/../../../../../classes/products.php";
			$this->products = new \products;

			require_once "../classes/discounts.php";
			$this->discounts = new \discounts;
		}

		public static function getProductCategoryName(
			$categoryId
		) {
			return self::getInstance()->dbh->getStoreCategoryName($categoryId);
		}

		/**
		 * @return mixed
		 */
		public static function getInstance() {
			if (null === static::$_instance) {
				static::$_instance = new static();
			}

			return static::$_instance;
		}

		/**
		 * Returns all categories from a given parent category id
		 * Format $categories[{top level category id}] => [childCategories][{child category id}]
		 *
		 * @param     $storeId
		 * @param int $parentCategoryId
		 *
		 * @return array
		 */
		public static function getProductCategoryTreeByParentId( $storeId, $parentCategoryId = 0 ) {
			$allCategories = array();

			$categories = self::getProductCategoriesByStoreId($storeId, $parentCategoryId);
			if (!empty($categories)) {
				foreach ($categories as $category) {
					if (isset($category['ImageID']) && $category['ImageID'] != "") {
						$catimg = self::getInstance()->dbh->viewImage($category['ImageID']);
						$category['Images'] = $catimg;
					}
					$allCategories[ $category['ID'] ] = $category;
					$children
						= self::getProductCategoryTreeByParentId($storeId,
						$category['ID']);
					if (!empty($children)) {
						$allCategories[ $category['ID'] ]['childCategories']
							= $children;
					}
				}
			}

			return $allCategories;
		}

		/**
		 * @param $storeId
		 * @param $parentCategoryId defaults to top level categories
		 *
		 * @return array
		 * @throws Exception
		 */
		public static function getProductCategoriesByStoreId(
			$storeId, $parentCategoryId = 0
		) {
			$categories = self::getInstance()->dbh->getMainStoreCategoriesCached($storeId, $parentCategoryId, 0);

			return $categories;
		}

		/**
		 * @param $storeId
		 *
		 * @return array
		 * @throws Exception
		 */
		public static function getTopLvlProductCategoriesByStoreId( $storeId ) {
			$productCategoryQry = "Call GetMainStoreCategories({$storeId}, 0, 0);";

			$dbHandler = ZRegistry::getInstance()->get('db');
			try {
				$categories = array();
				foreach (
					$dbHandler->query($productCategoryQry, PDO::FETCH_ASSOC) as
					$categoryRow
				) {
					array_push($categories, $categoryRow);
				}
			} catch (\PDOException $pErr) {
				throw new Exception($pErr->getMessage());
			}

			return $categories;
		}

        public static function getRandomStoreProductsbyCategory(
            $storeId, $categoryID, $numberOfProducts
        ) {

            $randomProductsResult
                = self::getInstance()->storeprods->getRandomStoreProductsbyCategory(
                $storeId,$categoryID, $numberOfProducts, 1
            );

            if (!mysqli_num_rows($randomProductsResult)) {
                return false;
            }

            $randomProducts = array();
            while ($randomProduct = mysqli_fetch_array($randomProductsResult)) {
                array_push($randomProducts, $randomProduct);
            }

            return $randomProducts;
        }

		/**
		 * @param     $storeId
		 * @param     $productId
		 * @param int $numberOfProducts
		 *
		 * @return array|bool
		 */
		public static function getYouMayAlsoLikeProducts(
			$storeId, $productId, $numberOfProducts = 4
		) {
			$youMayAlsoLikeProducts = array();

			$crossProducts = self::getProductCrossSells($storeId, $productId);
			if (!empty($crossProducts)) {
				$youMayAlsoLikeProducts = $crossProducts;
			}

			if (count($youMayAlsoLikeProducts) < $numberOfProducts) {
				$randomProducts = self::getRandomStoreProducts(
					$storeId,
					($numberOfProducts - count($youMayAlsoLikeProducts))
				);
				$youMayAlsoLikeProducts = array_merge($randomProducts,
					$youMayAlsoLikeProducts);
			}

			return $youMayAlsoLikeProducts;
		}

		/**
		 * @param $productId The parent product id
		 * @param $storeId
		 *
		 * @return array|bool
		 */
		public static function getProductCrossSells( $storeId, $productId ) {
			$crossProductsResult
				= self::getInstance()->dbh->getStoreProductCrossSells($productId,
				$storeId);
			if (!mysqli_num_rows($crossProductsResult)) {
				return false;
			}

			$crossProducts = array();
			while ($crossProduct = mysqli_fetch_array($crossProductsResult)) {
				array_push($crossProducts, $crossProduct);
			}

			return $crossProducts;
		}

		/**
		 * @param $storeId
		 * @param $numberOfProducts
		 *
		 * @return array|bool
		 */
		public static function getRandomStoreProducts(
			$storeId, $numberOfProducts
		) {

			$randomProductsResult
				= self::getInstance()->storeprods->getRandomStoreProducts(
				$storeId, $numberOfProducts, 1
			);
			if (!mysqli_num_rows($randomProductsResult)) {
				return false;
			}

			$randomProducts = array();
			while ($randomProduct = mysqli_fetch_array($randomProductsResult)) {
				array_push($randomProducts, $randomProduct);
			}

			return $randomProducts;
		}

		/**
		 * Retrieves "New Arrival" products from store
		 *
		 * @param $storeId
		 * @param $numberOfProducts
		 */
		public static function getNewStoreProducts( $storeId, $limit = 10 ) {
			/** @var \storeprods $storeproducts */
			$storeproducts = self::getInstance()->storeprods;

			$newStoreProdResult = $storeproducts->getStoreNewProducts($storeId);
			if (!mysqli_num_rows($newStoreProdResult)) {
				return false;
			}

			$newProducts = array();
			while ($newProduct = mysqli_fetch_array($newStoreProdResult) and
			       count($newProducts) < $limit) {
				$product = self::getProductDetails($storeId,
					$newProduct['ProductID']);
				array_push($newProducts, $product);
			}

			return $newProducts;
		}

		/**
		 * @param $productId
		 * @param $storeId
		 *
		 * @return array|bool
		 */
		public static function getProductDetails( $storeId, $productId ) {
			$products = self::getInstance()->products;
			$classes = self::getInstance()->dbh;
			$discounts = self::getInstance()->discounts;

			$product = $products->getStoreProduct($productId, $storeId);
			if (empty($product)) {
				return false;
			}

			$salePrice = $discounts->getSalePrice($productId, $storeId);
			if ($salePrice) {
				$product['OrigPrice'] = $product['StorePrice'];
				$product['StorePrice'] = $salePrice;
				$product['SalePrice'] = $salePrice;
			}

			$defaultImages = array();
			if (!empty($product['DefaultImageID'])) {
				$defaultImages = $classes->viewImage($product['DefaultImageID']);
			}

			$alternateImages = array();
			if (count($product['ImageID']) > 0) {
				foreach ($product['ImageID'] as $imageId) {
					if (!empty($defaultImages) and
					    $imageId != $product['DefaultImageID']
					) {
						array_push($alternateImages,
							$classes->viewImage($imageId));
					}
				}
			}

			$imageId = current($product["ImageID"]);
			$images = $classes->viewImage($imageId);

			$propertiesResult = $products->getProductProperties($productId);
			$hasProperties = (mysqli_num_rows($propertiesResult) > 0);

			// Get product properties
			$properties = array();
			$cachedImages = array();
			if ($hasProperties) {
				while ($propertyData = mysqli_fetch_assoc($propertiesResult)) {
					$propertyId = $propertyData['ID'];
					$propertyName
						= strtolower($propertyData['Name']);
					$properties[ $propertyName ] = $propertyData;

					// Get all available property values for the given property
					$availablePropertyValues
						= $products->getAvailablePropertyValuesSV($propertyId,
						$productId);
					if ($availablePropertyValues) {
						foreach ($availablePropertyValues as $propertyValue) {
							$valueName = strtolower($propertyValue['Name']);
							$valueImageId
								= $propertyValue['ImageID'];
							$properties[ $propertyName ]['options'][ $valueName ]
								= $propertyValue;

							// If there is an image id for the property value, add it to the collection
							if ($valueImageId) {
								if (!isset($cachedImages[ $valueImageId ])) {
									$valueImages = $classes->viewImage($valueImageId);
									$cachedImages[ $valueImageId ] = $valueImages;
								} else {
									$valueImages = $cachedImages[ $valueImageId ];
								}

								$properties[ $propertyName ]['options'][ $valueName ]['image']
									= $valueImages;
							}
						}
					}
				}
			}

			// Get product inventory
			$inventoryResult = $products->getProductInventory($productId);
			$hasInventory = (mysqli_num_rows($inventoryResult) > 0);

			$inventory = array();
			if ($hasInventory) {
				while ($inventoryData = mysqli_fetch_assoc($inventoryResult)) {
					if (!empty($properties)) {
						foreach ($properties as $propertyName => $propertyData) {
							$propertyOptions = $propertyData['options'];
							if (empty($propertyOptions)) {
								break;
							}

							// Number of valid value options
							for ($idx = 1; $idx <= 7; $idx ++) {
								$valueName = "Value{$idx}";
								foreach (
									$propertyOptions as $optionName => $optionData
								) {
									if ($optionData['ID']
									    == $inventoryData[ $valueName ]
									) {
										$inventoryData["Value{$idx}Type"]
											= $propertyName;
									}
								}
							}

						}
					}
					array_push($inventory, $inventoryData);
				}
			}

			$productDetails = array(
				'details'         => $product,
				'images'          => $defaultImages,
				'alternateImages' => $alternateImages,
				'properties'      => $properties,
				'inventory'       => $inventory,
			);

			return $productDetails;
		}

		/**
		 * @param     $storeId
		 * @param int $limit
		 *
		 * @return array
		 * @throws Exception
		 */
		public static function getSaleProducts( $storeId, $limit = 10 ) {

			$saleProductsQry = "Call GetStoreSaleProducts({$storeId}, null, {$limit}, 1, 'ASC');";
			/** @var \PDO $dbHandler */
			$dbHandler = ZRegistry::getInstance()->get('db');
			try {
				$products = array();
				foreach (
					$dbHandler->query($saleProductsQry, PDO::FETCH_ASSOC) as
					$productRow
				) {
					$product = new StoreProduct();
					$product->productid = $productRow['productid'];
					array_push($products, $product);
				}

				return $products;
			} catch (\PDOException $pErr) {
				throw new Exception($pErr->getMessage());
			}
		}

		/**
		 * @param     $storeId
		 * @param int $limit
		 *
		 * @return array
		 * @throws Exception
		 */
		public static function getHotProducts( $storeId, $limit = 10 ) {

			$saleProductsQry = "Call GetStoreHotProducts({$storeId}, {$limit}, 1, 'ASC');";
			/** @var \PDO $dbHandler */
			$dbHandler = ZRegistry::getInstance()->get('db');
			try {
				$products = array();
				foreach (
					$dbHandler->query($saleProductsQry, PDO::FETCH_ASSOC) as $productRow) {
					array_push($products, $productRow);
				}
				return $products;
			} catch (\PDOException $pErr) {
				throw new Exception($pErr->getMessage());
			}
		}

		/**
		 * @param     $storeId
		 * @param int $limit
		 *
		 * @return array
		 * @throws Exception
		 */
		public static function getProducts( $storeId, $limit = 25 ) {
			$productQry = "Call GetStoreProducts({$storeId});";

			/** @var \PDO $dbHandler */
			$dbHandler = ZRegistry::getInstance()->get('db');
			try {
				$products = array();
				foreach (
					$dbHandler->query($productQry, PDO::FETCH_ASSOC) as
					$productRow
				) {
					$product = new StoreProduct();
					$product->productid = $productRow['productid'];
					array_push($products, $product);
				}

				if (count($products) > $limit) {
					return array_slice($products, $limit);
				} else {
					return $products;
				}
			} catch (\PDOException $pErr) {
				throw new Exception($pErr->getMessage());
			}
		}

		/**
		 * @param $categoryId
		 *
		 * @return array
		 */
		public static function getProductsByCategoryId( $categoryId ) {
			$classes = self::getInstance()->dbh;

			/** @var \PDO $dbHandler */
			$dbHandler = ZRegistry::getInstance()->get('db');

			$products = array();
			$productResult = $classes->getStoreProductsFromCategory($categoryId);
			while ($productData = mysqli_fetch_assoc($productResult)) {
				$product = self::getProductDetails($productData['StoreID'],
					$productData['ProductID']);
				array_push($products, $product);
			}

			return $products;
		}

		/**
		 * @param $categoryId
		 *
		 * @return array
		 */
		public static function getCategoryProductProperties( $categoryId ) {
			$smartFilterQry =
				"SELECT v.Name, p.ColorFamily, p.Keywords FROM StoresCategories sc JOIN StoresProducts sp ON sp.StoreCategoryID = sc.ID LEFT JOIN Properties pr ON pr.ProductID = sp.ProductID LEFT JOIN `Values` v ON v.PropertyID = pr.ID JOIN Products p ON p.ID = sp.ProductID LEFT JOIN( SELECT DISTINCT `Inventory`.`Value1` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) UNION SELECT DISTINCT `Inventory`.`Value2` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) UNION SELECT DISTINCT `Inventory`.`Value3` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) UNION SELECT DISTINCT `Inventory`.`Value4` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) UNION SELECT DISTINCT `Inventory`.`Value5` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) UNION SELECT DISTINCT `Inventory`.`Value6` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) UNION SELECT DISTINCT `Inventory`.`Value7` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) )AS vwqty ON v.ID = vwqty. VALUE WHERE ( sc.ID = "
				. $categoryId . " OR sc.ParentCategoryID = " . $categoryId . " ) AND(vwqty.Qty > 0 OR vwqty.Qty IS NULL) AND WholesalePrice > 0 AND v.ID IS NOT NULL ORDER BY v.Name ASC";

			$dbHandler = ZRegistry::getInstance()->get('db');

			$colors = array();
			$sizes = array();
			$keywords = array();

			$standard_sizes = array('0','1','2','3','4','5','6','7','8','9','XXL','XL','L','M','S','XS','XXS');
			foreach ($dbHandler->query($smartFilterQry, PDO::FETCH_ASSOC) as $row) {	
				// if (strcspn($row['name'], '0123456789') != strlen($row['name'])) {
				if (in_array($row['name'], $standard_sizes)) {
					
					if (!in_array($row['name'], $sizes)) {
						$sizes[] = $row['name'];
					}
				}
				$keywordstemp = explode(",", $row['keywords']);
				foreach ($keywordstemp as $keyword) {
					$keyword = strtolower(trim($keyword));
					$keyword = str_replace(" ", "_", $keyword);
					if (!in_array($keyword, $keywords) && trim($keyword) != "" && $keyword != null) {
						$keywords[] = $keyword;
					}
				}

				$colorstemp = explode(",", $row['colorfamily']);
				foreach ($colorstemp as $color) {
					if (!in_array($color, $colors) && trim($color) != "" && $color != null) {
						$colors[] = $color;
					}
				}
			}
			sort($colors);
			sort($keywords);
			if (in_array('M', $sizes) || in_array('L', $sizes) || in_array('S', $sizes)){
				$letters_sizes=array('XXS','XS','S','M','L','XL','XXL');
				$result = array();
				foreach ($letters_sizes as $size) {
					if (in_array($size, $sizes)){
						$result[] = $size;
					}
				}
				$sizes=$result;
			} else {
				asort($sizes);
			}

			return array("colors" => $colors, "keywords" => $keywords, "sizes" => $sizes);
		}

        public static function getCategoryProductPropertiesNoVals( $categoryId ) {
            $smartFilterQry =
                "SELECT v.Name, p.ColorFamily, p.Keywords FROM StoresCategories sc JOIN StoresProducts sp ON sp.StoreCategoryID = sc.ID LEFT JOIN Properties pr ON pr.ProductID = sp.ProductID LEFT JOIN `Values` v ON v.PropertyID = pr.ID JOIN Products p ON p.ID = sp.ProductID LEFT JOIN( SELECT DISTINCT `Inventory`.`Value1` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) UNION SELECT DISTINCT `Inventory`.`Value2` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) UNION SELECT DISTINCT `Inventory`.`Value3` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) UNION SELECT DISTINCT `Inventory`.`Value4` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) UNION SELECT DISTINCT `Inventory`.`Value5` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) UNION SELECT DISTINCT `Inventory`.`Value6` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) UNION SELECT DISTINCT `Inventory`.`Value7` AS `Value`, `Inventory`.`Qty` AS `Qty` FROM ( `Inventory` JOIN `ProductStatus` ON( ( `Inventory`.`ProductStatus` = `ProductStatus`.`ID` ) ) ) )AS vwqty ON v.ID = vwqty. VALUE WHERE ( sc.ID = "
                . $categoryId . " OR sc.ParentCategoryID = " . $categoryId . " ) AND(vwqty.Qty > 0 OR vwqty.Qty IS NULL) AND WholesalePrice > 0 ORDER BY v.Name ASC";

            $dbHandler = ZRegistry::getInstance()->get('db');

            $colors = array();
            $sizes = array();
            $keywords = array();

            $standard_sizes = array('0','1','2','3','4','5','6','7','8','9','XXL','XL','L','M','S','XS','XXS');
            foreach ($dbHandler->query($smartFilterQry, PDO::FETCH_ASSOC) as $row) {
                // if (strcspn($row['name'], '0123456789') != strlen($row['name'])) {
                if (in_array($row['name'], $standard_sizes)) {

                    if (!in_array($row['name'], $sizes)) {
                        $sizes[] = $row['name'];
                    }
                }
                $keywordstemp = explode(",", $row['keywords']);
                foreach ($keywordstemp as $keyword) {
                    $keyword = strtolower(trim($keyword));
                    $keyword = str_replace(" ", "_", $keyword);
                    if (!in_array($keyword, $keywords) && trim($keyword) != "" && $keyword != null) {
                        $keywords[] = $keyword;
                    }
                }

                $colorstemp = explode(",", $row['colorfamily']);
                foreach ($colorstemp as $color) {
                    if (!in_array($color, $colors) && trim($color) != "" && $color != null) {
                        $colors[] = $color;
                    }
                }
            }
            sort($colors);
            sort($keywords);
            if (in_array('M', $sizes) || in_array('L', $sizes) || in_array('S', $sizes)){
                $letters_sizes=array('XXS','XS','S','M','L','XL','XXL');
                $result = array();
                foreach ($letters_sizes as $size) {
                    if (in_array($size, $sizes)){
                        $result[] = $size;
                    }
                }
                $sizes=$result;
            } else {
                asort($sizes);
            }

            return array("colors" => $colors, "keywords" => $keywords, "sizes" => $sizes);
        }

		/**
		 * @param $storeid
		 *
		 * @return string
		 */
		public static function getStoreName( $storeid ) {
			$smartFilterQry = "SELECT Name FROM `Stores` WHERE `ID` =".$storeid." LIMIT 1";
			$dbHandler = ZRegistry::getInstance()->get('db');
			foreach ($dbHandler->query($smartFilterQry, PDO::FETCH_ASSOC) as $row) {
				$result=$row['name'];
			}
			return $result;
		}
	}