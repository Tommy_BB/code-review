<?php
    namespace ZMVC\Store\Actions;

    class PrivacyAction extends \ZMVC\Store\Actions\DynamicAction
    {
        public $title = 'Privacy';
        public $pageTitle = 'Privacy';

        /**
         * @return mixed|void
         */
        public function run()
        {
            $cmsContent = $this->fetchDynamicContent($this->getDynamicContentId());

            $content = $this->getController()->render(
                'dynamic.twig', array(
                    'content' => $cmsContent,
                )
            );

            echo $content;
        }
    }