<?php
    namespace ZMVC\Store\Actions;
    use ZMVC\ZRegistry;

    /**
     * Class DynamicAction
     */
    class FindStoreAction extends \ZMVC\Store\Actions\DynamicAction
    {
        public $title = 'Find a Store';
        public $pageTitle = 'Find a Store';

        /**
         * @return mixed|void
         */
        public function run()
        {
            $this->prepareAssets();

            $stores = $this->getStores();
            $states = $this->getStates();

            if (!empty($stores)) {
                $countries = array();
                foreach ($stores as $store) {
                    array_push(
                        $countries, array(
                            'name' => ucwords(strtolower($store['country'])),
                            'code' => $store['country_code'],
                        )
                    );
                }
            }

            $content = $this->getController()->render(
                'findstore.twig', array(
                    'stores'    => $stores,
                    'states'    => $states,
                    'countries' => $countries,
                )
            );

            echo $content;
        }

        /**
         *
         */
        private function prepareAssets()
        {
            $this->addCssFileAsset('views/findstore.min.css');
            $this->addScriptFileAsset('views/findstore.min.js');
        }

        /**
         * @return array
         * @throws Exception
         */
        private final function getStores($countryCode = "code")
        {
            $storeLookupQry = "SELECT *
                FROM StoreLocations sl
                JOIN Countries c ON c.Country_Code = sl.Country
                WHERE sl.StoreID = :storeId
                GROUP by sl.Country Order By c.Country ASC";

            $dbHandler = ZRegistry::getInstance()->get('db');
            try {
                $storesStmt = $dbHandler->prepare($storeLookupQry);
                $stores     = array();
                if (true === $storesStmt->execute(array(':storeId' => $this->getController()->getStoreId()))) {
                    foreach ($storesStmt->fetchAll(PDO::FETCH_ASSOC) as $storeRow) {
                        array_push($stores, $storeRow);
                    }

                    return $stores;
                } else {
                    return false;
                }
            } catch (\PDOException $pErr) {
                throw new \Exception($pErr->getMessage());
            }
        }

        /**
         * @return mixed
         */
        private final function getStates()
        {
            $legacyStoreStatesResult = $this->getController()->getLegacyStoreHelper()->getStoreLocationStates(
                $this->getController()->getStoreId(), null
            );

            $states = array();
            if ($legacyStoreStatesResult) {
                while ($stateRow = mysqli_fetch_assoc($legacyStoreStatesResult)) {
                    array_push($states, array_change_key_case($stateRow));
                }
            }

            return $states;
        }
    }