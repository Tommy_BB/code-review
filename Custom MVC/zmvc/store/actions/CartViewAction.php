<?php
    namespace ZMVC\Store\Actions;

    class CartViewAction extends \ZMVC\ZAction
    {
        public $title = 'Shopping Cart';
        public $pagetitle = 'Shopping Cart';

        public function run()
        {
            $this->prepareAssets();

            echo $this->getController()->render(
                'cart_view.twig', array()
            );

            // Clear any alert messages
            $_SESSION['cart']->clearMessages();
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/cart_view.min.css');
            $this->addScriptFileAsset('views/cart_view.min.js');
        }
    }