<?php
    namespace ZMVC\Store\Actions;

    use ZMVC\Store\Helpers\ZStoreProductsHelper;
    use ZMVC\ZAction;
    use ZMVC\ZRegistry;
	use PDO;
	
    /**
     * Class ProductAction
     */
    class ProductAction extends ZAction
    {
        /**
         * @var string
         */
        private $_recentlyViewedSessionKey = 'recently-viewed';

        /**
         * @var int the maximum number of products stored
         */
        private $_recentlyViewedSessionLimit = 2;

        /**
         * @return mixed|void
         * @throws Exception
         */
        public function run()
        {
            $this->prepareAssets();

            if (isset($_GET['prodid'])) {
                $productId = $_GET['prodid'];
            } else {
                throw new \Exception('Could not find this product.');
            }

            // Update the user's recently viewed product list
            $this->updateRecentlyViewed($productId);
            
            //get out of stock options
            $outofStock = $this->outOfStockOptions($productId);

            // Retrieve the product details
            $storeId        = $this->getController()->getStoreId();
            if(isset($_REQUEST['ostoreid']) && $_REQUEST['ostoreid'] != "")
                $storeId = $_REQUEST['ostoreid'];

            $productDetails = ZStoreProductsHelper::getProductDetails($storeId, $productId);
            if (empty($productDetails)) {
                throw new \Exception('Could not find product details.');
            }

            $fallbackCategories = array(
                19 => 1726,
                54 => 411,
                74 => 826,
                148 => 2243,
                31 => 321,
                63 => 647,
                73 => 869,
                49 => 607,
                109 => 1276,
                77 => 884,
                154 => 2296,
                34 => 2072,
                146 => 2217,
                151 => 2234,
                157 => 2383,
                75 => 2187,
                163 => 2424,
                152 => 2369,
                79 => 892,
                36 => 2192,
                68 => 780,
                26 => 1148,
                16 => 65,
                93 => 1139,
                81 => 2499,
                72 => 1218,
                82 => 988
            );

            $currStore = isset($_REQUEST['storeid']) ? $_REQUEST['storeid'] : $storeId;

            if(isset($fallbackCategories[$currStore]) && isset($productDetails) && (!isset($productDetails['details']['StorePrice']) || (isset($productDetails['details']['StorePrice']) && $productDetails['details']['StorePrice'] <= 0)))
            {
                header("Location: index.php?storeid=".$currStore."&page=products&action=all&categoryId=".$fallbackCategories[$currStore]."&sess=".session_name());
                die("");
            }

            $agentid = $sharedAppId = (isset($pageagent['AgentID'])) ? $pageagent['AgentID'] : $_SESSION['TPAID'];
            $dbHandler = ZRegistry::getInstance()->get('db');

            $agents = $dbHandler->query("SELECT a.FBPageName,a.FBPageID,a.UniqueName, s.FBAPPID, s.ID FROM Agents a JOIN Stores s ON s.ID = a.StoreID WHERE a.AgentID = ".$agentid);
            if($agents) {
                $myagent = $agents->fetch(PDO::FETCH_ASSOC);
                $uniquename = $myagent['uniquename'];
            }
            $papid = $dbHandler->query("SELECT PapUserID FROM zshops.Agents WHERE AgentID='".$agentid."'");
            if($papid) {
                $papids = $papid->fetch(PDO::FETCH_ASSOC);
                $papuserid = $papids["papuserid"];
            } else {
                $papuserid = '';
            }

            // Sets the page title based on the product
            $this->setTitles($productDetails);

            // Product categories
            $youMayAlsoLikeProducts = ZStoreProductsHelper::getYouMayAlsoLikeProducts(
                $storeId, $productId, $numberOfProducts = 4
            );
            $recentlyViewedProducts = $this->getRecentlyViewedProducts();

            // Social
            $facebookUrl  = $this->getFacebookSocialUrl($productDetails);
            $twitterUrl   = $this->getTwitterSocialUrl($productDetails);
            $pinterestUrl = $this->getPinterestSocialUrl($productDetails);

            $socialUrls = array(
                'facebook'  => $facebookUrl,
                'twitter'   => $twitterUrl,
                'pinterest' => $pinterestUrl,
                'producturl' => $this->getProductUrl(),
            );

            // Prev/Next product
            $prevNextProduct = $this->getPrevNextProduct($productId);

            // Determine the prev/next product
            $previousProductId = $nextProductId = null;
            if (!empty($prevNextProduct['prev'])) {
                $previousProductId = $prevNextProduct['prev']['productid'];
            }

            if (!empty($prevNextProduct['next'])) {
                $nextProductId = $prevNextProduct['next']['productid'];
            }

            $storename = ZStoreProductsHelper::getStoreName($storeId);
            
            // This allows us to access the product details from the master layout
            ZRegistry::getInstance()->add('product', $productDetails);
            ZRegistry::getInstance()->add('storename', $storename);

            ZRegistry::getInstance()->add('canseelb',(isset($_SESSION['shopid']) && (isset($_SESSION['ispageOwner']) && $_SESSION['ispageOwner'] !== false)));
            

            $content = $this->getController()->render(
                'product.twig', array(
                    'product'                => $productDetails,
                    'uniquename'             => $uniquename,
                    'papuserid'              => $papuserid,
                    'storename'              => $storename,
                    'youMayAlsoLikeProducts' => $youMayAlsoLikeProducts,
                    'recentlyViewedProducts' => $recentlyViewedProducts,
                    'previousProductId'      => $previousProductId,
                    'nextProductId'          => $nextProductId,
                    'socialUrls'             => $socialUrls,
                    'outofStock'			 => $outofStock,
                    'ostoreid'               => (isset($_REQUEST['ostoreid']) ? $_REQUEST['ostoreid'] : ""),
                    'canseelb'               => (isset($_SESSION['shopid']) && (isset($_SESSION['ispageOwner']) && $_SESSION['ispageOwner'] !== false)),
                )
            );

            echo $content;
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/product.min.css');
            $this->addScriptFileAsset('views/product.min.js');
            $this->addScriptFileAsset('utilities/cloud-zoom.min.js');
        }

        /**
         * @param $productId
         */
        protected function updateRecentlyViewed($productId)
        {
            if (!is_array($_SESSION[$this->_recentlyViewedSessionKey])) {
                $_SESSION[$this->_recentlyViewedSessionKey] = array();
            }
            $recentlyViewedSession =& $_SESSION[$this->_recentlyViewedSessionKey];

            if (!in_array($productId, $recentlyViewedSession)) {
                array_unshift($recentlyViewedSession, $productId);
            }

            if ((count($recentlyViewedSession)) > $this->_recentlyViewedSessionLimit) {
                array_pop($recentlyViewedSession);
            }
        }

        /**
         * @param $productDetails
         */
        private function setTitles($productDetails)
        {
            $this->title = $this->pageTitle = $productDetails['details']['Name'];
        }

        /**
         * @return array|bool
         */
        private function getRecentlyViewedProducts()
        {
            if (!isset($_SESSION[$this->_recentlyViewedSessionKey])) {
                return false;
            }

            $recentlyViewedSession = array_slice($_SESSION[$this->_recentlyViewedSessionKey], 0, $this->_recentlyViewedSessionLimit);

            $recentlyViewedProducts = array();
            foreach ($recentlyViewedSession as $productId) {
                $product = ZStoreProductsHelper::getProductDetails($this->getController()->getStoreId(), $productId);
                array_push($recentlyViewedProducts, $product);
            }

            return $recentlyViewedProducts;
        }

        /**
         * @param $productDetails
         *
         * @return string
         */
        private function getFacebookSocialUrl($productDetails)
        {
            $facebookUrlTpl = "http://www.facebook.com/sharer/sharer.php?u={url}";

            return strtr(
                $facebookUrlTpl, array(
                    '{url}' => urlencode($this->getProductUrl()),
                )
            );
        }

        /**
         * @return string
         */
        public function getProductUrl()
        {
            return $this->getBaseProductUrl() . $_SERVER['REQUEST_URI'];
        }

        /**
         * @return string
         */
        private function getBaseProductUrl()
        {
            $isSecure = (strpos($_SERVER['SERVER_PROTOCOL'], 'HTTPS') !== false);
            $protocol = ($isSecure) ? 'https' : 'http';
            $httpHost = str_replace('local', 'com', $_SERVER['HTTP_HOST']);


            if(isset($_SERVER['HTTP_X_FORWARDED_HOST']) && $_SERVER['HTTP_X_FORWARDED_HOST'] != "" && stripos($_SERVER['HTTP_X_FORWARDED_HOST'],"zindigo") !== true)
            {
                $httpHost = $_SERVER['HTTP_X_FORWARDED_HOST'];
            }

            return "{$protocol}://{$httpHost}";
        }

        /**
         * @param $product
         *
         * @return string
         */
        protected function getTwitterSocialUrl($productDetails)
        {
            $twitterSocialUrlTpl = "http://www.twitter.com/intent/tweet?url={url}&text={productName}";
            $twitterSocialUrl    = strtr(
                $twitterSocialUrlTpl, array(
                    '{url}'         => urlencode($this->getProductUrl()),
                    '{productName}' => urlencode($productDetails['details']['Name']),
                )
            );

            return $twitterSocialUrl;
        }

        /**
         * @param $productDetails
         *
         * @return string
         */
        private function getPinterestSocialUrl($productDetails)
        {   
            $pinterestUrlTpl = "http://pinterest.com/pin/create/button/?url={url}&media={media}&description={description}";

            $pinterestUrl = strtr(
                $pinterestUrlTpl, array(
                    '{url}'         => urlencode($this->getProductUrl()),
                    '{media}'       => urlencode($this->getBaseProductUrl() . '/' . $productDetails['images']['LargePath']),
                    '{description}' => urlencode(strip_tags($productDetails['details']['Name'])),
                )
            );

            return $pinterestUrl;
        }

        private function getPrevNextProduct($currentProductId)
        {
            $storeCategories = ZRegistry::getInstance()->get('productCategories');
            if (empty($storeCategories)) {
                return false;
            }

            $storeCategoryIds = array();
            foreach ($storeCategories as $storeCategory) {
                array_push($storeCategoryIds, (int)$storeCategory['id']);
            }

            $minMaxQry = "SELECT ProductID FROM StoresProducts WHERE StoreCategoryID IN (:categoryIds)";

            /** @var \PDO $dbHandler */
            $dbHandler = ZRegistry::getInstance()->get('db');
            try {
                $stmt = $dbHandler->prepare($minMaxQry);
                $stmt->execute(
                    array(
                        ':categoryIds' => implode(',', $storeCategoryIds)
                    )
                );

                $nextProduct = $prevProduct = null;
                $products    = $stmt->fetchAll();
                foreach ($products as $productIdx => $productRow) {
                    $productId = $productRow['productid'];
                    if ($productId == $currentProductId) {
                        $previousProductId = ($productIdx - 1);
                        $nextProductId     = ($productIdx + 1);

                        if (isset($products[$previousProductId])) {
                            $prevProduct = $products[$previousProductId];
                        }

                        if (isset($products[$nextProductId])) {
                            $nextProduct = $products[$nextProductId];
                        }
                    }
                }

                return array(
                    'prev' => $prevProduct,
                    'next' => $nextProduct,
                );
            } catch (\PDOException $pErr) {
                throw new Exception($pErr->getMessage());
            }
        }
        
        private function outOfStockOptions($productId)
        {
        	$products  = ZStoreProductsHelper::getInstance()->products;
        	$dbHandler = ZRegistry::getInstance()->get('db');
        	$propertiesResult = $products->getProductProperties( $productId );
			$hasProperties    = ( mysqli_num_rows( $propertiesResult ) > 0 );
	
			// Get product properties
			$properties = array();
			if ( $hasProperties ) {
				while ( $propertyData = mysqli_fetch_assoc( $propertiesResult ) ) {
					$propertyId = $propertyData['ID'];
					$propertyName = strtolower( $propertyData['Name'] );
					$properties[ $propertyName ] = $propertyData;
	
					// Get all unavailable property values for the given property
					$unavailablePropertyValues = $products->getUnavailableValues($propertyId, $productId);
					if ( $unavailablePropertyValues ) {
						foreach ( $unavailablePropertyValues as $propertyValue ) {
							$valueName = strtolower($propertyValue['Name']);
							$properties[ $propertyName ]['options'][ $valueName ] = $propertyValue;
						}
					}
				}
			}
			
			//no need to keep going we have all sizes in stock
			if(!isset($properties['size']['options']))
			{
				return array();
			}
			
			// Get product inventory
			$inventoryResult = $products->getProductInventory( $productId );
			$hasInventory    = ( mysqli_num_rows( $inventoryResult ) > 0 );
	
			$inventory = array();
			if ( $hasInventory ) {
				while ( $inventoryData = mysqli_fetch_assoc( $inventoryResult ) ) {
					if ( ! empty( $properties ) ) {
						foreach ( $properties as $propertyName => $propertyData ) {
							$propertyOptions = $propertyData['options'];
							if ( empty( $propertyOptions ) ) {
								break;
							}
	
							// Number of valid value options
							for ( $idx = 1; $idx <= 7; $idx ++ ) {
								$valueName = "Value{$idx}";
								foreach (
									$propertyOptions as $optionName => $optionData
								) {
									if ( $optionData['ID']
									     == $inventoryData[ $valueName ]
									) {
										$inventoryData["Value{$idx}Type"]
											= $propertyName;
									}
								}
							}
	
						}
					}
					array_push( $inventory, $inventoryData );
				}
			}
			
			
			$agentid = "";
			$inzindigo = isset($_SESSION['shopid']) ? 1 : 0;
			$weburl = "";
			
			if(isset($_SESSION['pageid']) && $_SESSION['pageid'] != "")
			{
				$pageagent = $this->getController()->getLegacyStoreHelper()->getPageAgent($_SESSION['pageid'], $_SESSION['storeid']);
				$agentid = $sharedAppId = (isset($pageagent['AgentID'])) ? $pageagent['AgentID'] : $_SESSION['TPAID'];
            } else {
				$getWebAttr = $dbHandler->query("SELECT Web FROM Stores WHERE ID = '".$_SESSION['storeid']."'");
				$webRow = $getWebAttr->fetch(PDO::FETCH_ASSOC);
				
				//$weburl = "http://".$webRow['web'];
				$weburl = "http://".$_SERVER[HTTP_HOST];
            }
            

            
            
            //parse the url to send for oos params
            $cururl = "http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];
            $parsedUrl = parse_url($cururl);
            $parsedQuery = "";
            $removalQuery = array(session_name(),"sess","XDEBUG_PROFILE");
            if(isset($parsedUrl['query']) && $parsedUrl['query'] != "")
            {
            	parse_str($parsedUrl['query'], $parsedQuery);
				foreach($removalQuery as $key=>$val)
				{
					if(isset($parsedQuery[$val]))
						unset($parsedQuery[$val]);
				}
            }
            
            $parsedQuery = $parsedUrl['path'] . "?". http_build_query($parsedQuery);
            
			$oosDetails = array(
				'properties'      => $properties,
				'inventory'       => $inventory,
                'agentID'         => $sharedAgentId,
				'zindigo'		  => $inzindigo,
				'web'			  => $weburl,
				'params'		  => $parsedQuery
			);
			return $oosDetails;
        }
    }