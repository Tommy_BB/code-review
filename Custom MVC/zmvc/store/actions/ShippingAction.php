<?php
    namespace ZMVC\Store\Actions;

    class ShippingAction extends \ZMVC\ZAction
    {
        public $title = 'Shipping';
        public $pageTitle = 'Shipping Method';

        /** @var \DBH */
        private $_dbh;

        public function run()
        {
            $this->prepareAssets();

            /** @var \DBH $ulk */
            $this->_dbh = $this->getController()->getLegacyStoreHelper();

            $isShippingInternational = $this->isShippingInternational();
            $isValidShippingAddress  = $this->isValidShippingAddress();
            $cookieSessionName       = ($_COOKIE[session_name()]) ? '' : '&' . htmlspecialchars(SID);

            echo $this->getController()->render(
                'shipping.twig', array(
                    'isShippingInternational' => $isShippingInternational,
                    'isValidShippingAddress'  => $isValidShippingAddress,
                    'cookieSessionName'       => $cookieSessionName,
                )
            );
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/shipping.min.css');
            $this->addScriptFileAsset('views/shipping.min.js');
        }

        /**
         * @return bool
         */
        private function isValidShippingAddress()
        {
            $isValidShippingAddress = true;

            if (isset($_SESSION)) {
                $vendorShipping = $_SESSION['cart']->shipping();
                foreach ($vendorShipping as $vendorId => $shipData) {
                    if (0 == count($shipData)) {
                        return !$isValidShippingAddress;
                    }
                }
            }

            return $isValidShippingAddress;
        }

        /**
         * @return bool
         */
        private function isShippingInternational()
        {
            $isShippingInternational = false;

            if (isset($_SESSION) && isset($_SESSION['orderinfo'])) {
                $isShippingInternational = $this->_dbh->isInternational($_SESSION['orderinfo']->ship_country);
            }

            return $isShippingInternational;
        }
    }