<?php
    namespace ZMVC\Store\Actions;

    class AboutAction extends \ZMVC\ZAction
    {
        public $title = 'About';
        public $pageTitle = 'About';

        public function run()
        {
            $this->prepareAssets();

            $content = $this->getController()->render(
                'about.twig', array()
            );

            echo $content;
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/about.min.css');
            $this->addScriptFileAsset('views/about.min.js');
        }
    }