<?php
	namespace ZMVC\Store\Actions;
	use Exception;
	use PDO;
	use ZMVC\ZRegistry;
    class OutofstockAction extends \ZMVC\ZAction
    {
        public function run()
        {
        	
			$email = $_REQUEST['oosemail'];
			$name = $_REQUEST['oosname'];
			$size = $_REQUEST['unavailableSizes'];
			$color = $_REQUEST['unavailableColors'];
			$productid = $_REQUEST['productid'];
			$storeid = $_REQUEST['storeid'];
			$agentid = $_REQUEST['agentid'];
			$url = $_REQUEST['weburl'];
			$zindigo = $_REQUEST['zindigo'];
			$infb = $_REQUEST['infb'];
			$params = $_REQUEST['params'];
			$fields = array();
			
			$dbHandler = ZRegistry::getInstance()->get('db');
			
			$oosquery = "INSERT INTO OutofStock (Name,Email,StoreID,ProductID,Requested,ColorValue,SizeValue,InFB,URL,AgentID,Parameters,Zindigo) VALUES ('".$name."','".$email."', '".$storeid."', '".$productid."', NOW(), ".(($color != null && $color != "") ? "'".$color."'" : "NULL").",".(($size != null && $size != "") ? "'".$size."'" : "NULL").",".$infb.",'".$url."','".$agentid."','".$params."',".$zindigo.")";
			
			$oosquerydeploy = $dbHandler->query($oosquery);
			
			
			
			$getproductattr = "SELECT s.Name AS StoreName, p.NAME AS ProductName, s.Email, p.VendorSkuBase AS SKU, i.ThumbPath AS ProductImage,v1.Name AS ColorName,
	v2.Name AS SizeName FROM Products p JOIN Images i ON i.ID = p.ImageID LEFT OUTER JOIN `Values` v1 ON v1.ID = '".$color."'
LEFT OUTER  JOIN `Values` v2 ON v2.ID = '".$size."' JOIN Stores s ON s.ID = '".(($zindigo == 1) ? 55 : $storeid)."' WHERE p.ID = '".$productid."'";
			
			$productattrs = $dbHandler->query($getproductattr);
			if (!($productattrsrow = $productattrs->fetch(PDO::FETCH_ASSOC))) {
            	$this->sendAjaxResponse(array('success' => false));
			}
			
            $data = "PRODUCT ID: ".$productid."<br>CUSTOMER NAME: ".$name."<br>EMAIL: ".$email;
            
            $fields[] = array (
				'name' => 'contactform',
				'type' => 'html',
				'content' => $data,
			);
			$fields[] = array (					
				'name' => 'ProductImage',
				'type' => 'html',
				'content' => 'http://stores.zindigo.com/'.$productattrsrow['productimage'],
			);
			
			
			$fields[] = array (		
				'name' => 'SKU',
				'type' => 'html',
				'content' => $productattrsrow['sku'],
			);
			
			if($productattrsrow['sizename'] != null)
			{				
				$fields[] = array (		
					'name' => 'Size',
					'type' => 'html',
					'content' => $productattrsrow['sizename'],
				);
			}
			
			if($productattrsrow['colorname'] != null)
			{				
				$fields[] = array (		
					'name' => 'Color',
					'type' => 'html',
					'content' => $productattrsrow['colorname'],
				);
			}
			
			$fields[] = array (		
				'name' => 'ProductName',
				'type' => 'html',
				'content' => $productattrsrow['productname'],
			);
			
			$storeHeader = $this->getMailSender()->ReadContentTag('headernew_' . (($zindigo == 1) ? 55 : $storeid));
            $fields[] = array(
                'name' => 'storeheader',
                'type' => 'html',
                'content' => $storeHeader
            );
			
            //get brands email
            $this->getMailSender()->sendaccountemail($productattrsrow['Email'],"0bbb03eb0000000000000000000000170e74",$productattrsrow['storename'],$productattrsrow['Email'],$fields);
            $this->sendAjaxResponse(array('success' => true));
        }

        /**
         * @param $data
         */
        protected function sendAjaxResponse($data)
        {
            echo json_encode($data);
            exit;
        }

        /**
         * @return Bronto
         */
		 protected function getMailSender()
        {
            include_once(__DIR__ . '/../../../../../classes/bronto.php');

            return new \Bronto();
        }
    }