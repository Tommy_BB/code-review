<?php
    namespace ZMVC\Store\Actions;

    class ContactAction extends \ZMVC\ZAction
    {
        public $title = 'Contact Us';
        public $pageTitle = 'Contact Us';

        public function run()
        {
            $this->prepareAssets();

            $content = $this->getController()->render('contact.twig');
            echo $content;
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/contact.min.css');
        }
    }