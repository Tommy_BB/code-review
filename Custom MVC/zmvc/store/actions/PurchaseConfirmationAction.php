<?php
    namespace ZMVC\Store\Actions;

    class PurchaseConfirmationAction extends \ZMVC\ZAction
    {
        public $title = 'Order Confirmation';
        public $pageTitle = 'Order Confirmation';

        public function run()
        {
            $this->prepareAssets();

            //lets calc taxes on order conf page
            $_SESSION['cart']->taxes = $_SESSION['cart']->taxes();
            $_SESSION['cart']->recalc_total();

            if ($this->getController()->getDebugMode()) {
                $baseCheckoutUrl = '//storesdev.zindigo.local/index.php';
            } else {
                $baseCheckoutUrl = '//checkout.zindigo.com/index.php';
            }

            $cookieSessionName = ($_COOKIE[session_name()]) ? '' : '&' . htmlspecialchars(SID);
            echo $this->getController()->render(
                'purchase_confirmation.twig', array(
                    'cookieSessionName' => $cookieSessionName,
                    'baseCheckoutUrl'   => $baseCheckoutUrl,
                )
            );
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/purchase_confirmation.min.css');
        }
    }