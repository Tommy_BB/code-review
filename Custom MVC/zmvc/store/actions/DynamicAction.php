<?php
namespace ZMVC\Store\Actions;
use PDO;
use ZMVC\ZAction;
use ZMVC\ZRegistry;

require_once __DIR__ . "/../../ZAction.php";
require_once __DIR__ . "/../../ZRegistry.php";

/**
 * Class DynamicAction
 *
 * @package ZMVC\Store\Actions
 */
abstract class DynamicAction extends ZAction {

    /**
     * @param $contentId
     *
     * @return null
     * @throws \Exception
     */
    protected function fetchDynamicContent($contentId) {

        if (!is_numeric($contentId)) {
            $contentId = "null";
        }

        $dbHandler = ZRegistry::getInstance()->get('db');

        $dynamicFetchQry = "Call GetDynamicContent({$this->getController()->getStoreId()}, {$contentId});";
        $dynamicStmt = $dbHandler->query($dynamicFetchQry);
        if (!($dynamicRow = $dynamicStmt->fetch(PDO::FETCH_ASSOC))) {
            throw new \Exception('Could not fetch content');
        }

        return $dynamicRow['content'];
    }

    protected function fetchDynamicContentByName($name) {


        $dbHandler = ZRegistry::getInstance()->get('db');

        $dynamicFetchQry = "Select * from DynamicContent where `StoreID` = '".$this->getController()->getStoreId()."' and upper(`Name`) = upper('aboutus') AND `Status` = 1;";
        $dynamicStmt = $dbHandler->query($dynamicFetchQry);
        if (!($dynamicRow = $dynamicStmt->fetch(PDO::FETCH_ASSOC))) {
            //throw new \Exception('Could not fetch content');
            $dynamicRow['content']='Could not fetch content';
        }
        
        return $dynamicRow['content'];
    }
    
    protected function fetchgetCustSvc() {

        if (!is_numeric($contentId)) {
            $contentId = "null";
        }

        $dbHandler = ZRegistry::getInstance()->get('db');

        $dynamicFetchQry = "Call GetCustSvc({$this->getController()->getStoreId()});";
        $dynamicStmt = $dbHandler->query($dynamicFetchQry);
        if (!($dynamicRow = $dynamicStmt->fetch(PDO::FETCH_ASSOC))) {
            throw new \Exception('Could not fetch content');
        }

        return stripslashes($dynamicRow['custsvc']);
    }

    /**
     * @return mixed
     */
    protected function getDynamicContentId()
    {
        return $_REQUEST['content_id'];
    }
}