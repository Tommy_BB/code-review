<?php
    namespace ZMVC\Store\Actions;

    class CompleteOrderAction extends \ZMVC\ZAction
    {
        public $title = 'Complete Order';
        public $pageTitle = 'Complete Order';

        public function run()
        {
            $this->prepareAssets();

            if ($_SESSION['OrderStatus'] == "OnHold")
            {
                $storedata = $this->getController()->getLegacyStoreHelper()->getStore($_SESSION['storeid']);

                if(stripos($_SERVER['SERVER_NAME'], "dev") !== false)
                {
                    if (stripos($_SERVER['SERVER_NAME'], '.local') == false) {
                        $redirect_url = "http://storesdev.zindigo.com/index.php?page=receipt_pass&storeid="
                            . $_SESSION['storeid'] . "&sess=" . session_name() . "&sessionid=" . session_id();
                    } else {
                        $redirect_url = "http://storesdev.zindigo.local/index.php?page=receipt_pass&storeid="
                            . $_SESSION['storeid'] . "&sess=" . session_name() . "&sessionid=" . session_id();
                    }
                } else {
                    $redirect_url =
                        "https://stores.zindigo.com/index.php?page=receipt_pass&storeid="
                        . $_SESSION['storeid'] . "&sess=" . session_name() . "&sessionid=" . session_id();

                    if ($storedata['Web'] <> "") {
                        $redirect_url           =
                            "http://" . $storedata['Web'] . "/index.php?page=receipt_pass&storeid="
                            . $_SESSION['storeid'] . "&sess=" . session_name() . "&sessionid=" . session_id();
                    }
                }
                header("Location: " . $redirect_url);
            } else {

                ob_start();
                $_SESSION['cart']->authorize_payment_form($_SESSION['cart']->grandtotal);
                $paymentForm = ob_get_contents();
                ob_end_clean();


                echo $this->getController()->render(
                    'complete_order.twig', array(
                        'paymentForm' => $paymentForm,
                    )
                );
            }
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/complete_order.min.css');
            $this->addScriptFileAsset('views/complete_order.min.js');
        }
    }