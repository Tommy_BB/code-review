<?php
    namespace ZMVC\Store\Actions;

    class SiteMapAction extends \ZMVC\ZAction
    {
        public $title = 'Site Map';
        public $pageTitle = 'Site Map';

        public function run()
        {
            $this->prepareAssets();

            echo $this->getController()->render('sitemap.twig');
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/sitemap.min.css');
        }
    }