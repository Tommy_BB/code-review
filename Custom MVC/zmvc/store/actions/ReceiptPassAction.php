<?php
    namespace ZMVC\Store\Actions;

    class ReceiptPassAction extends \ZMVC\ZAction
    {
        public $title = 'Review My Order';
        public $pageTitle = 'Review My Order';

        public function run()
        {
            if (empty($_SESSION['cart']->items)) {
                header("Location: index.php?storeid={$this->getController()->getStoreId()}&page=home");
                exit;
            }
            $this->prepareAssets();

            // Retrieve the original order details from the database since the session is lost at this point
            $orderInfo = load_orderinfo();
            $orderDetails = $this->getController()->getLegacyStoreHelper()->getOrder($orderInfo->orderid);

            echo $this->getController()->render(
                'receipt_pass.twig', array(
                    'orderDetails' => $orderDetails
                )
            );

            // Empty the user's cart
            $_SESSION['cart']->emptycart();
            $_SESSION['promo_codes'] = null;
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/receipt_pass.min.css');
            $this->addScriptFileAsset('views/receipt_pass.min.js');
            $this->addScriptFileAsset('bower_components/jQuery.print/jQuery.print.js');
        }
    }