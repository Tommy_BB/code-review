<?php
    namespace ZMVC\Store\Actions;

    class PurchaseNowAction extends \ZMVC\ZAction
    {
        var $title = 'Billing';
        var $pageTitle = 'Billing';

        /** @var \DBH */
        private $_dbh = null;

        public function run()
        {
            /** @var \DBH $ulk */
            $this->_dbh = $this->getController()->getLegacyStoreHelper();

            $this->prepareAssets();

            $countries = $this->getCountries();
            $states    = $this->getStates();

            echo $this->getController()->render(
                'purchase_now.twig', array(
                    'countries' => $countries,
                    'states'    => $states,
                )
            );
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/purchase_now.min.css');
            $this->addScriptFileAsset('views/purchase_now.min.js');
        }

        /**
         * @return array|bool
         * @throws Exception
         */
        private function getCountries()
        {
            $countries = array();

            $countryResult = $this->_dbh->getCountries();
            while ($countryRow = mysqli_fetch_array($countryResult)) {
                array_push(
                    $countries, array(
                        'code' => $countryRow['Country_Code'],
                        'name' => ucwords(strtolower($countryRow['Country'])),
                    )
                );
            }

            return $countries;
        }

        /**
         * @return mixed
         */
        private function getStates()
        {
            $states = array();

            $stateResult = $this->_dbh->getStates();
            while ($stateRow = mysqli_fetch_array($stateResult)) {
                array_push(
                    $states, array(
                        'code' => $stateRow['State_code'],
                        'name' => ucwords(strtolower($stateRow['State'])),
                    )
                );
            }

            return $states;
        }
    }