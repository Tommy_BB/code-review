<?php
    namespace ZMVC\Store\Actions;

    class ReturnsAction extends \ZMVC\Store\Actions\DynamicAction
    {
        public $title = 'Shipping and Returns';
        public $pageTitle = 'Shipping and Returns';

        /**
         * @return mixed|void
         */
        public function run()
        {
            $cmsContent = $this->fetchDynamicContent($this->getDynamicContentId());

            $content = $this->getController()->render(
                'dynamic.twig', array(
                    'content' => $cmsContent,
                )
            );

            echo $content;
        }
    }