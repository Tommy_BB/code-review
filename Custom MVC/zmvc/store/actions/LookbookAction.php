<?php
    namespace ZMVC\Store\Actions;

    use ZMVC\ZRegistry;

    class LookbookAction extends \ZMVC\ZAction
    {
        public $title = 'Lookbook';
        public $pageTitle = 'Lookbook';

        protected $lookbookCategoryId;

        public function run()
        {
            $this->prepareAssets();

            $lookbookCategories = ZRegistry::getInstance()->get('lookbookCategories');

            if ($lookbookCategories) {
                foreach ($lookbookCategories as $categoryId => $categoryData) {
                    $lookbookCategories[$categoryId]['products'] = $categoryData['childCategories'];
                }
            }

            $lookbookCategories = $this->prepareImages($lookbookCategories);

            echo $this->getController()->render(
                'lookbook.twig', array(
                    'collections' => $lookbookCategories,
                )
            );
        }

        protected function prepareAssets()
        {
            $this->addScriptFileAsset('bower_components/jquery.lazyload/jquery.lazyload.js');

            $this->addCssFileAsset('node_modules/slick-carousel/slick/slick.css');
            $this->addScriptFileAsset('node_modules/slick-carousel/slick/slick.min.js');

            $this->addCssFileAsset('views/lookbook.min.css');
            $this->addScriptFileAsset('views/lookbook.min.js');
        }

        protected function prepareImages($lookbookCategories)
        {
            if (empty($lookbookCategories)) {
                return false;
            }

            foreach ($lookbookCategories as $categoryId => $categoryData) {
                foreach ($categoryData['products'] as $productCategoryId => $productData) {
                    $productImages = $this->getController()->getLegacyStoreHelper()->viewImage($productData['ImageID']);
                    $lookbookCategories[$categoryId]['products'][$productCategoryId]['images'] = $productImages;
                }
            }

            return $lookbookCategories;
        }
    }