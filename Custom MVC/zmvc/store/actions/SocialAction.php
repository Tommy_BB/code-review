<?php
    namespace ZMVC\Store\Actions;

    use ZMVC\ZAction;

    class SocialAction extends ZAction
    {
        public $title = 'Social';
        public $pageTitle = 'Social';

        public function run()
        {
            $this->prepareAssets();

            $content = $this->getController()->render(
                'social.twig', array()
            );

            echo $content;
        }

        protected function prepareAssets()
        {
            $storeAssetRoot = '../../../..';
            $this->addCssFileAsset($storeAssetRoot . '/css/social_stream/dcsns_wall.css');
            $this->addScriptFileAsset($storeAssetRoot . '/js/social_stream/jquery.social.stream.wall.1.3.js');
            $this->addScriptFileAsset($storeAssetRoot . '/js/social_stream/jquery.social.stream.1.5.2.min.js');
            $this->addScriptFileAsset('views/social.min.js');
        }
    }
