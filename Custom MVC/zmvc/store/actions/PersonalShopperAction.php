<?php

    namespace ZMVC\Store\Actions;

    use ZMVC\ZAction;

    /**
     * Class PersonalShopperAction
     * A token can be utilized to prevent auto-spammers
     * Simply add the following hidden inputs to your form
     * <input type="hidden" name="token" />
     * <input type="hidden" name="tokenCipher" />
     *
     * The token will then be compared against the cipher via md5 encryption
     *
     * @package ZMVC\Store\Actions
     */
    abstract class PersonalShopperAction extends ZAction
    {
        /**
         * @return mixed|void
         */
        public function run()
        {
            if (isset($_POST) and isset($_POST['email']) and isset($_POST['name'])) {
                // Validate the token if one exists
                if (!$this->isTokenValid()) {
                    echo json_encode(array('success' => false));
                    exit;
                }

                $type      = $_POST['type'];
                $name      = $_POST['name'];
                $email     = $_POST['email'];
                $phone     = $_POST['phone'];
                $country   = $_POST['country'];
                $comments  = $_POST['comments'];
                $storeid   = $_POST['storeid'];
                $productid = $_POST['productid'];

                $data = "TYPE: " . $type . "<br>NAME: " . $name . "<br>EMAIL: " . $email . "<br>PHONE: " . $phone
                    . "<br>COUNTRY: " . $country . "<br>COMMENTS: " . $comments . "<br>STORE ID: " . $storeid
                    . "<br>PRODUCT ID: " . $productid;

                $emailData[] = array(
                    'name'    => 'contactform',
                    'type'    => 'html',
                    'content' => $data,
                );

                $sentEmails = $this->sendEmails($emailData, $_POST);
                echo json_encode(
                    array(
                        'success' => $sentEmails
                    )
                );
                exit;
            }
        }

        /**
         * @return bool
         */
        protected function isTokenValid()
        {
            // We don't require the token, but if it's present then we need to validate it
            if (!$this->isTokenPresent()) {
                return true;
            }

            if (empty($_POST['token']) or empty($_POST['tokenCipher'])) {
                return false;
            }

            return (md5($_POST['token']) == $_POST['tokenCipher']);
        }

        /**
         * @return bool
         */
        protected function isTokenPresent()
        {
            return (isset($_POST) and isset($_POST['token']) and isset($_POST['tokenCipher']));
        }

        /**
         * @param $emailData
         *
         * @return mixed
         */
        abstract protected function sendEmails($emailData, $post);

        /**
         * @return Bronto
         */
        protected function getMailSender()
        {
            include_once(__DIR__ . '/../../../../../classes/bronto.php');

            return new \Bronto();
        }
    }