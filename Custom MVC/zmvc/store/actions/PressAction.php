<?php

namespace ZMVC\Store\Actions;

use ZMVC\ZAction;

class PressAction extends ZAction{

    public $title = 'Press';
    public $pageTitle = 'Press';

    public function run() {
        $this->prepareAssets();

        echo $this->getController()->render('press.twig');
    }

    protected function prepareAssets() {
        $this->addScriptFileAsset('utilities/jsrender.min.js');
        $this->addCssFileAsset('views/press.min.css');
        $this->addScriptFileAsset('views/press.min.js');
    }
} 