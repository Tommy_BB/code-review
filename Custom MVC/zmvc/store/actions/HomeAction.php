<?php
    namespace ZMVC\Store\Actions;

    class HomeAction extends \ZMVC\ZAction
    {
        public $title = 'Home';
        public $pageTitle = 'Home';

        public function run()
        {
            $this->prepareAssets();

            $featuredProducts = \ZMVC\Store\Helpers\ZStoreProductsHelper::getProducts(
                $this->getController()->getStoreId(), 8
            );

            echo $this->getController()->render(
                'home.twig', array(
                    'featuredProducts' => $featuredProducts
                )
            );
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/home.min.css');
            $this->addScriptFileAsset('views/home.min.js');
        }
    }