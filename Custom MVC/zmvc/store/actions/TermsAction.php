<?php
    namespace ZMVC\Store\Actions;

    class TermsAction extends \ZMVC\Store\Actions\DynamicAction
    {
        public $title = 'Terms';
        public $pageTitle = 'Terms';

        /**
         * @return mixed|void
         */
        public function run()
        {
            $cmsContent = $this->fetchDynamicContent($this->getDynamicContentId());

            $content = $this->getController()->render(
                'dynamic.twig', array(
                    'content' => $cmsContent,
                )
            );

            echo $content;
        }
    }