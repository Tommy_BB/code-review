<?php
    namespace ZMVC\Store\Actions;
    use ZMVC\ZAction;

    class ProductsAction extends ZAction
    {
        public $title = 'Products';
        public $pageTitle = 'Products';

        public function run()
        {
            $this->prepareAssets();

            $brandSiteUrl  = 'http://stores.zindigo.com/templates';

            $pageagent = $this->getController()->getLegacyStoreHelper()->getPageAgent($_SESSION['pageid'], $_SESSION['storeid']);
            $sharedAgentId = $sharedAppId = (isset($pageagent['AgentID'])) ? $pageagent['AgentID'] : $_SESSION['TPAID'];

            // Get the top level category if a category id isn't found
            $categories = \ZMVC\Store\Helpers\ZStoreProductsHelper::getTopLvlProductCategoriesByStoreId(
                $this->getController()->getStoreId()
            );
            $topLevelCategory = $categories[0]['id'];
            $categoryId       = $topLevelCategory;

            $content = $this->getController()->render(
                'products.twig', array(
                    'brandSiteUrl'  => $brandSiteUrl,
                    'sharedAgentId' => $sharedAgentId,
                    'sharedAppId'   => $sharedAppId,
                    'categoryId'    => $categoryId,
                )
            );

            echo $content;
        }

        protected function prepareAssets()
        {
            $this->addCssFileAsset('../../defaultshare.css');
            $this->addCssFileAsset('views/products.min.css');
            $this->addScriptFileAsset('utilities/jsrender.min.js');
            $this->addScriptFileAsset('utilities/zindigo-products.min.js');
            $this->addScriptFileAsset('utilities/defaultshare.min.js');
            $this->addScriptFileAsset('views/products.min.js');
        }
    }