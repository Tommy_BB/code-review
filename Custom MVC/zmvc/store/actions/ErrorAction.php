<?php
    namespace ZMVC\Store\Actions;

    /**
     * Class ErrorAction
     */
    class ErrorAction extends \ZMVC\ZAction
    {
        public $title = 'Error';
        public $pageTitle = 'Error';

        /**
         * @var
         */
        private $_exception;

        /**
         * @todo Need to hide the message if on production
         * @return mixed|void
         */
        public function run()
        {
            $errorMessage = $this->getException()->getMessage();
            $content      = $this->getController()->render(
                'error.twig', array(
                    'error' => $errorMessage,
                )
            );

            echo $content;
        }

        protected function getException()
        {
            return $this->_exception;
        }

        /**
         * @param \Exception $exception
         *
         * @return $this
         */
        public function setException(\Exception $exception)
        {
            $this->_exception = $exception;

            return $this;
        }
    }