<?php
namespace ZMVC\Store\Models;

use PDO;
use ZMVC\ZActiveRecord;
use ZMVC\ZRegistry;

require_once "StoreProductImage.php";
require_once __DIR__ . "/../../ZActiveRecord.php";
require_once __DIR__ . "/../../ZRegistry.php";

/**
 * Class StoreProduct
 *
 * @property mixed $productid
 *
 * @package ZMVC\Store\Models
 */
class StoreProduct extends ZActiveRecord {

    static $table_name = 'StoresProducts';

    static $primary_key = 'ProductID';

    /** @var StoreProductImage $_image */
    private $_image;

    private $_details = array();

    public function getDetails() {

        if (empty($this->_details)) {
            $productDetailQry = "Call GetProduct({$this->productid});";

            /** @var PDO $dbHandler */
            $dbHandler = ZRegistry::getInstance()->get('db');
            $pdoStmt = $dbHandler->query($productDetailQry);
            if (!($detailRow = $pdoStmt->fetch(PDO::FETCH_ASSOC))) {
                return null;
            }

            $this->_details = $detailRow;
        }

        return $this->_details;
    }

    /**
     * @return mixed
     */
    public function getName() {
        $details = $this->getDetails();
        return $details['name'];
    }

    /**
     * @return mixed
     */
    public function getPrice() {
        $details = $this->getDetails();
        return number_format($details['wholesaleprice'] + $details['stdmarkup'], 2);
    }

    /**
     * @return null|StoreProductImage
     */
    public function getImage() {

        if (null === $this->_image and $this->productid) {
            $imageQry = "Call GetProductImage({$this->productid});";

            /** @var PDO $dbHandler */
            $dbHandler = ZRegistry::getInstance()->get('db');
            $pdoStmt = $dbHandler->query($imageQry);
            if (!($imageRow = $pdoStmt->fetch(PDO::FETCH_ASSOC))) {
                return null;
            }

            $productImage = new StoreProductImage();
            $productImage
                ->setLargePath($imageRow['largepath'])
                ->setImagePath($imageRow['imagepath']);

            $this->_image = $productImage;
        }

        return $this->_image;
    }
}