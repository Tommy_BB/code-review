<?php
    namespace ZMVC\Store\Models;

    /**
     * Class StoreProductImage
     *
     * @property mixed ID
     * @property mixed ImagePath
     * @property mixed ThumbPath
     * @property mixed LargePath
     *
     * @package ZMVC\Store\Models
     */
    class StoreProductImage
    {

        /**
         * The large path to the image
         *
         * @var string
         */
        private $_largePath;

        /**
         * The regular image path
         *
         * @var string
         */
        private $_imagePath;

        /**
         * @param $largePath
         *
         * @return $this
         */
        public function setLargePath($largePath)
        {
            $this->_largePath = $largePath;

            return $this;
        }

        /**
         * @return string
         */
        public function getLargePath()
        {
            return $this->_largePath;
        }

        /**
         * @return string
         */
        public function getImagePath()
        {
            return $this->_imagePath;
        }

        /**
         * @param $imagePath
         *
         * @return $this
         */
        public function setImagePath($imagePath)
        {
            $this->_imagePath = $imagePath;

            return $this;
        }
    }