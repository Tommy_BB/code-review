<?php
    namespace ZMVC;

    use ActiveRecord;


    /**
     * Simple model abstraction to allow for the mapping of db data
     *
     * 
     */
    abstract class ZActiveRecord extends ActiveRecord\Model
    {

    }
