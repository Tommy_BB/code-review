<?php
    namespace ZMVC;

    /**
     * Class ZAction
     *
     * @package ZMVC
     */
    abstract class ZAction
    {
        /**
         * @var
         */
        public $title;

        /**
         * @var
         */
        public $pageTitle;

        /**
         * @var
         */
        private $_controller;

        /**
         * @var array
         */
        private $_cssAssets = array();

        /**
         * @var array
         */
        private $_jsAssets = array();

        /**
         * @return mixed
         */
        abstract function run();

        /**
         * @return ZController
         */
        public function getController()
        {
            return $this->_controller;
        }

        /**
         * @param ZController $controller
         */
        public function setController(ZController $controller)
        {
            $this->_controller = $controller;
        }

        /**
         * @return array
         */
        public function getScriptFileAssets()
        {
            return $this->_jsAssets;
        }
        
         /**
         * @return array
         */
        public function getCssFileAssets()
        {
            return $this->_cssAssets;
        }

        /**
         * @param $cssFilePath
         *
         * @return $this
         */
        protected function addCssFileAsset($cssFilePath)
        {
            array_push($this->_cssAssets, $cssFilePath);

            return $this;
        }

        /**
         * @param $jsFilePath
         *
         * @return $this
         */
        protected function addScriptFileAsset($jsFilePath)
        {
            array_push($this->_jsAssets, $jsFilePath);

            return $this;
        }
    }