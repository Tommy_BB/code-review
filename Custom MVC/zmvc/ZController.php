<?php

    namespace ZMVC;

    require_once 'extensions/autoload.php';

    define('TEMPLATE_BASE_DIR', __DIR__ . '/../../twig_templates');
    define('STORE_TEMPLATE_BASE_DIR', TEMPLATE_BASE_DIR . '/store');
    define('STORE_DEFAULT_TEMPLATE_BASE_DIR', STORE_TEMPLATE_BASE_DIR . '/themes/default');
    define('BRAND_TEMPLATE_BASE_DIR', __DIR__);

    /**
     * Class ZController
     *
     * @package ZMVC
     */
    class ZController
    {
        /**
         * @var
         */
        private static $_instance;

        /**
         * @var
         */
        protected $storeId;

        /** @var ZAction */
        private $_action;

        /** @var DBH */
        private $_legacyStoreHelper;

        /**
         * @var
         */
        private $_layout;

        /**
         * @var
         */
        private $_isDebugMode;

        /**
         * @var
         */
        private $_theme;

        /**
         * @var $_environment
         */
        private $_environment;

        /**
         * @var
         */
        private $_runOutput;

        /**
         *
         */
        protected function __construct()
        {

        }

        /**
         * @return \DBH
         */
        public function getLegacyStoreHelper()
        {
            if (null === $this->_legacyStoreHelper) {
                // Provides $ulk variable
                include_once(__DIR__ . "/../../../classes/classes.php");

                // Account for the fact the classes file may be included by this point
                if (!isset($ulk)) {
                    global $ulk;
                }
                $this->_legacyStoreHelper = $ulk;
            }

            return $this->_legacyStoreHelper;
        }

        /**
         *
         */
        public function run()
        {
			
			$this->detectFacebookApp();
            $this->init();

            ob_start();
            $this->_action->run();
            $actionOutput = ob_get_clean();

            // Set the output from the buffered run() call
            $this->_runOutput = $actionOutput;

            echo $this->render($this->_layout);
        }

        /**
         *
         */
        protected function init()
        {
			
        }

        /**
         * @param $view
         * @param $viewArguments
         *
         * @return string
         */
        public function render($view, $viewArguments = array())
        {
            $defaultArguments = array(
                'registry'    => \ZMVC\ZRegistry::getInstance(),
                'action'      => $this->_action,
                'controller'  => $this,
                'session'     => $_SESSION,
                'sessionName' => session_name(),
                'sessionId'   => session_id(),
                'input'       => array(
                    'get'     => $_GET,
                    'post'    => $_POST,
                    'request' => $_REQUEST,
                ),
            );
            $viewArguments    = array_merge($defaultArguments, $viewArguments);

            return $this->getViewRenderer()->render($view, $viewArguments);
        }

        /**
         * @return \Twig_Environment
         */
        protected function getViewRenderer()
        {
            $themeTemplateBaseDirPath = __DIR__ . "/../../twig_templates/themes/{$this->_theme}";

            // Base store twig templates, layouts, and macros
            $storeAppTwigTemplates = array(
                "{$themeTemplateBaseDirPath}/layouts",
                "{$themeTemplateBaseDirPath}/views",
                "{$themeTemplateBaseDirPath}/views/macros",
            );

            $templateBaseDirPath = __DIR__ . "/../../templates/{$this->storeId}";

            // Brand specific templates, layouts, and macros
            $brandTwigTemplates    = array(
                "{$templateBaseDirPath}/twig_templates/themes/{$this->_theme}/layouts",
                "{$templateBaseDirPath}/twig_templates/themes/{$this->_theme}/views",
                "{$templateBaseDirPath}/twig_templates/themes/{$this->_theme}/views/macros",
            );
            $storeAppTwigTemplates = array_merge(
                $storeAppTwigTemplates, $brandTwigTemplates
            );

            $loader = new \Twig_Loader_Filesystem($storeAppTwigTemplates);
            $twig   = new \Twig_Environment($loader, array('debug' => true));
            $twig->addExtension(new \Twig_Extensions_Extension_Text());

            return $twig;
        }

        /**
         * @param $isDebugMode
         *
         * @return $this
         */
        public function setDebugMode($isDebugMode)
        {
            $this->_isDebugMode = $isDebugMode;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getDebugMode()
        {
            return $this->_isDebugMode;
        }
		
		/**
		 * Attempt to detect presence of facebook app container
		 * 
		 * @return boolean
		 */
		
		public function detectFacebookApp() {
			// FB passes signed_request on the initial load when an app runs as a facebook app
			//
			if (isset($_REQUEST['signed_request'])) {
				$_SESSION['in_facebook'] = true;
			} 
			
			if (!isset($_SESSION['in_facebook']) || ($_SESSION['in_facebook'] !== true)) {
				$_SESSION['in_facebook'] = false;
			} 
			return $_SESSION['in_facebook'];
		}

        /**
         * @return mixed
         */
        public function getOutput()
        {
            return $this->_runOutput;
        }

        /**
         * @param ZAction $action
         *
         * @return $this
         */
        public function setAction(ZAction $action)
        {
            $this->getInstance()->_action = $action;
            $this->getInstance()->_action->setController($this->getInstance());

            return $this;
        }

        /**
         * @return static
         */
        public static function getInstance()
        {
            if (null === self::$_instance) {
                self::$_instance = new static;
            }

            return self::$_instance;
        }

        /**
         * @return mixed
         */
        public function getStoreId()
        {
            return $this->storeId;
        }

        /**
         * @param $storeId
         *
         * @return $this
         */
        public function setStoreId($storeId)
        {
            $this->storeId = $storeId;

            return $this;
        }

        /**
         * @param $layout
         *
         * @return $this
         */
        public function setLayout($layout)
        {
            $this->_layout = $layout;

            return $this;
        }

        protected function getTheme()
        {
            return $this->_theme;
        }

        /**
         * @param $theme
         *
         * @return $this
         */
        public function setTheme($theme)
        {
            $this->_theme = $theme;

            return $this;
        }
    }