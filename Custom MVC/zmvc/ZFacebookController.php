<?php
    namespace ZMVC;

    /**
     * Class ZFacebookController
     *
     * @package ZMVC
     */
    class ZFacebookController extends ZController
    {
        /**
         * @var
         */
        private $_useDefaultTemplate = false;

        /**
         * @return mixed
         */
        public function getDefaultTemplate()
        {
            return $this->_useDefaultTemplate;
        }

        /**
         * @param $brandWebStoreId
         */
        public function setDefaultTemplate()
        {
            $this->_useDefaultTemplate = true;
        }

        //
        /**
         * @var
         */
        private $_useThisCategory = 0;

        /**
         * @return mixed
         */
        public function getDefaultCategory()
        {
            return $this->_useThisCategory;
        }

        /**
         * @param useThisCategory
         */
        public function setDefaultCategory($dCat)
        {
            $this->_useThisCategory = $dCat;
        }

        /**
         * @var
         */
        private $_brandWebStoreId;

        /**
         * @return mixed
         */
        public function getBrandWebStoreId()
        {
            return $this->_brandWebStoreId;
        }

        /**
         * @param $brandWebStoreId
         */
        public function setBrandWebStoreId($brandWebStoreId)
        {
            $this->_brandWebStoreId = $brandWebStoreId;
        }

        /**
         * If the brand's web store id is set then the application is aware that the store has more than one application instance
         * @return mixed
         */
        public function getStoreId($useWebStoreId=true) {
            return (!empty($this->_brandWebStoreId) and $useWebStoreId) ? $this->_brandWebStoreId : $this->storeId;
        }

        /**
         * @return \Twig_Environment
         */
        protected function getViewRenderer()
        {
            $themeTemplateBaseDirPath = __DIR__ . "/../../twig_templates/themes/{$this->getTheme()}";

            // Base store twig templates, layouts, and macros
            $storeAppTwigTemplates = array(
                "{$themeTemplateBaseDirPath}/layouts",
                "{$themeTemplateBaseDirPath}/views",
                "{$themeTemplateBaseDirPath}/views/macros",
            );

            // Brand specific templates, layouts, and macros
            $templateBaseDirPath = __DIR__ . "/../../templates/{$this->storeId}";
            $brandTwigTemplates    = array(
                "{$templateBaseDirPath}/twig_templates/themes/{$this->getTheme()}/layouts",
                "{$templateBaseDirPath}/twig_templates/themes/{$this->getTheme()}/views",
                "{$templateBaseDirPath}/twig_templates/themes/{$this->getTheme()}/views/macros",
            );

            // Check for the brand's web store for the template first
            $brandWebStoreTwigTemplates = array();
            if ($this->getBrandWebStoreId()) {
                $brandWebStoreTemplateBaseDirPath = __DIR__ . "/../../templates/{$this->getBrandWebStoreId()}";
                array_push($brandWebStoreTwigTemplates, "{$brandWebStoreTemplateBaseDirPath}/twig_templates/themes/{$this->getTheme()}/layouts");
                array_push($brandWebStoreTwigTemplates, "{$brandWebStoreTemplateBaseDirPath}/twig_templates/themes/{$this->getTheme()}/views");
                array_push($brandWebStoreTwigTemplates, "{$brandWebStoreTemplateBaseDirPath}/twig_templates/themes/{$this->getTheme()}/views/macros");
            }

            $storeAppTwigTemplates = array_merge(
                $storeAppTwigTemplates, $brandTwigTemplates, $brandWebStoreTwigTemplates
            );

            $loader = new \Twig_Loader_Filesystem($storeAppTwigTemplates);
            $twig   = new \Twig_Environment($loader, array('debug' => true));
            $twig->addExtension(new \Twig_Extensions_Extension_Text());

            return $twig;
        }
    }