<?php
    namespace ZMVC;

    /**
     * Class ZZindigoShopController
     *
     * @package ZMVC
     */
    class ZZindigoShopController extends ZFacebookController
    {
        /**
         * @return \Twig_Environment
         */
        protected function getViewRenderer()
        {
            $themeTemplateBaseDirPath = __DIR__ . "/../../twig_templates/themes/{$this->getTheme()}";

            // Base store twig templates, layouts, and macros
            $storeAppTwigTemplates = array(
                "{$themeTemplateBaseDirPath}/layouts",
                "{$themeTemplateBaseDirPath}/views",
                "{$themeTemplateBaseDirPath}/views/macros",
            );

            // Check for the brand's default store templates
            $brandDefaultTemplates = array();
            if($this->getDefaultTemplate())
            {
                $defaultOverrideTemplateDirPath = __DIR__ . "/../../shoptemplates/{$this->storeId}";
                array_push($brandDefaultTemplates, "{$defaultOverrideTemplateDirPath}/twig_templates/themes/{$this->getTheme()}/layouts");
                array_push($brandDefaultTemplates, "{$defaultOverrideTemplateDirPath}/twig_templates/themes/{$this->getTheme()}/views");
            }

            // Brand specific templates, layouts, and macros
            $templateBaseDirPath = $this->getDefaultTemplate() ? __DIR__ . "/../../templates/default_shop_in_shop" : __DIR__ . "/../../shoptemplates/{$this->storeId}";
            $brandTwigTemplates    = array(
                "{$templateBaseDirPath}/twig_templates/themes/{$this->getTheme()}/layouts",
                "{$templateBaseDirPath}/twig_templates/themes/{$this->getTheme()}/views",
                "{$templateBaseDirPath}/twig_templates/themes/{$this->getTheme()}/views/macros",
            );

            // Check for the brand's web store for the template first
            $brandWebStoreTwigTemplates = array();
            if ($this->getBrandWebStoreId()) {
                $brandWebStoreTemplateBaseDirPath = $this->getDefaultTemplate() ?  __DIR__ . "/../../templates/default_shop_in_shop" :  __DIR__ . "/../../templates/{$this->getBrandWebStoreId()}";
                array_push($brandWebStoreTwigTemplates, "{$brandWebStoreTemplateBaseDirPath}/twig_templates/themes/{$this->getTheme()}/layouts");
                array_push($brandWebStoreTwigTemplates, "{$brandWebStoreTemplateBaseDirPath}/twig_templates/themes/{$this->getTheme()}/views");
                array_push($brandWebStoreTwigTemplates, "{$brandWebStoreTemplateBaseDirPath}/twig_templates/themes/{$this->getTheme()}/views/macros");
            }

            $storeAppTwigTemplates = array_merge(
                $storeAppTwigTemplates, $brandDefaultTemplates, $brandTwigTemplates, $brandWebStoreTwigTemplates
            );

            $loader = new \Twig_Loader_Filesystem($storeAppTwigTemplates);
            $twig   = new \Twig_Environment($loader, array('debug' => true));
            $twig->addExtension(new \Twig_Extensions_Extension_Text());

            return $twig;
        }
    }