<?php
    namespace ZMVC;

    /**
     * Class ZRegistry
     *
     * 
     * @package ZMVC
     */
    class ZRegistry
    {
        /**
         * @var
         */
        private static $_instance;

        /**
         *
         */
        protected function __construct() {}

        /**
         * @return ZRegistry
         */
        public static function getInstance() {
            if (null === static::$_instance) {
                static::$_instance = new static();
            }

            return static::$_instance;
        }

        /**
         * @var array $_dictionary The collection of all defined data within the registry
         */
        private $_dictionary = array('test' => 'SOME DATA HERE YO');

        /**
         *
         */
        protected function construct()
        {

        }

        /**
         * @param $key
         *
         * @return null
         */
        public function get($key)
        {
            if ($this->dictionaryKeyExists($key)) {
                return $this->_dictionary[$key];
            }

            return null;
        }

        /**
         * @param $key
         * @param $value
         */
        public function add($key, $value)
        {
            $this->_dictionary[$key] = $value;

            return;
        }

        /**
         * @param $key
         */
        public function delete($key)
        {
            if ($this->dictionaryKeyExists($key)) {
                unset($this->_dictionary[$key]);
            }
        }

        /**
         * @return array
         */
        final public function getDictionary()
        {
            return $this->_dictionary;
        }

        /**
         * Returns if the key is in the dictionary
         *
         * @param $key
         *
         * @return bool
         */
        protected function dictionaryKeyExists($key)
        {
            return array_key_exists($key, $this->_dictionary);
        }
    }