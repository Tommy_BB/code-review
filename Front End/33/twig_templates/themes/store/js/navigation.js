$(document).ready(function() {
	var stillinnav = false;
	var hittingnav = 0;
    $(".maincat a").hover(
    	function() {
    		var thisimg = $(this).parent().find("input").val();
    		var curheight = $(this).parent().parent().parent().height();
    		//var curheight = 270;
    		if(thisimg != null && thisimg != "")
    		$(this).parent().parent().parent().find(".catimagepreview").html("<img src=\""+thisimg+"\" style=\"height:"+curheight+"px\" />");
    	}, function() {
    		$(this).parent().parent().parent().find(".catimagepreview").html("");
    	}
    
    );
    
    $(".nav.navbar-nav li>a:first-child").hover(
	    function() {
	    	var thisaleft = Math.round($(this).offset().left);
	    	if($(window).width() < 1024)
	    	{
                try{
	    		//$(".dropdown>a:first-child").parent().children("ul.dropdown-menu").removeStyle('display');
	    		$(".dropdown>a:first-child").parent().children("ul.dropdown-menu").attr('style', function(i, style) {
				    return style.replace(/display[^;]+;?/g, '');
				});
				$(".nav.navbar-nav li>a:first-child").parent().find(".row:first-child").css("left","0px");
                } catch(exc){}
		    } else {
		    
		    //this lines the row to the navigation a
		    $(".nav.navbar-nav li>a:first-child").parent().find(".row:first-child").css("left",thisaleft+"px");
		    
		    //i hate msie...
		    $(this).parent().find(".dropdown-menu").css({"width":$(window).width()+"px"});
		    
		    if($.browser.msie || !!navigator.userAgent.match(/Trident.*rv\:11\./))
		    {
				$(this).parent().find(".dropdown-menu").css({"left" : "-"+$(this).offset().left+"px"});
	    	}
	    	
	    	//end i hate msie
	    	
	    	$(".dropdown>a:first-child").parent().children("ul.dropdown-menu").hide();
		    $(this).parent().find("ul.dropdown-menu").show();
		    
		     $(this).parent().find("ul.dropdown-menu").mouseenter(function () {
		     	stillinnav = true;
		     });
		     $(".navbar").mouseleave(function () {
                if (stillinnav) {
                    $(".navbar ul.dropdown-menu").hide();
                    setTimeout(function () {
                        stillinnav = false;
                    }, 500);
                }
            });
            }
	    }, function() {
	    }
    );
    
    $(".nav.navbar-nav li>a:first-child").click(
	    function() {
	    	
	    	if($(window).width() < 1024)
	    	{
	    		//$(".dropdown>a:first-child").parent().children("ul.dropdown-menu").removeStyle('display');
                try {
                    $(".dropdown>a:first-child").parent().children("ul.dropdown-menu").attr('style', function (i, style) {
                        return style.replace(/display[^;]+;?/g, '');
                    });
                    $(".nav.navbar-nav li>a:first-child").parent().find(".row:first-child").css("left", "0px");
                } catch(exc){}
		    } else {
		    
		    //this lines the row to the navigation a
		    $(".nav.navbar-nav li>a:first-child").parent().find(".row:first-child").css("left",thisaleft+"px");
		    
		    //i hate msie...
		    $(this).parent().find(".dropdown-menu").css({"width":$(window).width()+"px"});
		    
		    if($.browser.msie || !!navigator.userAgent.match(/Trident.*rv\:11\./))
		    {
				$(this).parent().find(".dropdown-menu").css({"left" : "-"+$(this).offset().left+"px"});
	    	}
	    	
	    	//end i hate msie
	    	
	    	$(".dropdown>a:first-child").parent().children("ul.dropdown-menu").hide();
		    $(this).parent().find("ul.dropdown-menu").show();
		    
		     $(this).parent().find("ul.dropdown-menu").mouseenter(function () {
		     	stillinnav = true;
		     });
		     $(".navbar").mouseleave(function () {
                if (stillinnav) {
                    $(".navbar ul.dropdown-menu").hide();
                    setTimeout(function () {
                        stillinnav = false;
                    }, 500);
                }
            });
            }
	    }
    );
    
    $(".navbar-header .navbar-toggle").click(function() {
	    if($(".navbar-collapse").height() <= 0 || hittingnav == 0)
	    {
	    	if(hittingnav == 0) hittingnav++;
		    $(".navbar-header .navbar-toggle .icon-bar").addClass("activated");
	    } else {
		   	$(".navbar-header .navbar-toggle .icon-bar").removeClass("activated");
	    }
    });

    $("input[name=searchtext]").keyup(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            $(this).parent().submit();
        }
    });
    $(".navbarsearch").click(function(event) {
        event.preventDefault();
        $(this).parent().submit();
    });

    $(".searchbutton").click(function(event) {
        event.preventDefault();
        $(this).parent().find("form").submit();
    });
})


