function resizeIframe(){
	var iframeh = $(window).height() - 133 - 21 - 85 - 39 + 100;
    $("#blog").css("height",iframeh);
}

$(document).ready(function() {
    // Do not match height on mobile devices
    if (true == window.matchMedia('(min-width: 480px)').matches) {
        $('.story').matchHeight();
    }

    $('.story').click(function(){
        var key = $(this).attr('rel');
        var href = $('[name="link_'+key+'"]').val();
        window.location.href = href;
    });
});

$( window ).resize(function() {
  resizeIframe();
});