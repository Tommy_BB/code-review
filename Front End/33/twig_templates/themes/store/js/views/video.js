$(document).ready(function() {
    console.log('video module loaded');

    // Video modal processing
    $videoModal = $('#video');

    // Hides the flow player logo on resume (play) and pause
    var $videoPlayer = $videoModal.find('.flowplayer');
    $videoPlayer.bind('resume', hideFlowPlayerLogo);
    $videoPlayer.bind('pause', hideFlowPlayerLogo);

    // Pause the video on modal hide
    $videoModal.on('hidden.bs.modal', function() {
        var videoApi = flowplayer($videoPlayer);
        videoApi.pause();
    })

    /**
     * Hides the flowplayer logo
     * @param event
     * @param api
     */
    function hideFlowPlayerLogo(event, api) {
        $(event.target).find("a[href='http://flowplayer.org']").css('background-image','none');
    }
})