$(document).ready(function () {
    console.log('products module loaded');
    // JSRender view helpers
    $.views.helpers({
        // Convert value to an integer
        Integer: function (val) {
            return val.replace(".00", "");
        },

        round: function (val) {
            val = val + "";
            val = val.replace(".00", "");
            return val;
        },

        Number: function (val) {
            return Number(val);
        },
		checkAvailable : function(val) {
			if(val == "Active")
			return " avble";
			
			return "";
		},
        Truncate: function (val, length, ellipsis) {
            if (typeof length == 'undefined') {
                var length = 100;
            }

            if (typeof ellipsis == 'undefined') {
                var ellipsis = '...';
            }

            if (val.length <= length) {
                return val;
            }

            for (var i = length - 1; val.charAt(i) != ' '; i--) {
                length--;
            }

            return val.substr(0, length - 1) + ellipsis;
       },
       FormatSizesNew: function(val) {
       
            if(val.toLowerCase().indexOf("xs") >= 0 || val.toLowerCase().indexOf("s") >= 0 || val.toLowerCase().indexOf("m") >= 0 || val.toLowerCase().indexOf("k") >= 0 || val.toLowerCase().indexOf("xl") >= 0 || val.toLowerCase().indexOf("xxl") >= 0)
            {
            	var splitarray = val.toLowerCase().split(',');
            	var returnedarray = [];
            	if($.inArray("xs",splitarray) !== -1)
            		returnedarray.push("XS");
            	if($.inArray("s",splitarray) !== -1)
            		returnedarray.push("S");
            	if($.inArray("m",splitarray) !== -1)
            		returnedarray.push("M");
            	if($.inArray("l",splitarray) !== -1)
            		returnedarray.push("L");
            	if($.inArray("xl",splitarray) !== -1)
            		returnedarray.push("XL");
            	if($.inArray("xxl",splitarray) !== -1)
            		returnedarray.push("XXL");
            	
            	val = returnedarray.join(" ");
            }
           
       		val = val.replace(","," ");
       		val = val.replace(/,/g, ' ');
	        return val;
	    }
    });

    var settings = {
        'container': $('#products-container .products'),
        'preloaderContainer': $('.z-preloader'),
        'categoryFilters': $('button.category-filter'),
        'storeid': $('#storeid').val(),
        'catid': $('#catid').val(),
        'action': $('#content-wrap #action').val(),
        'sharedAppId': $('#share-app-id').val(),
        'sharedAgentId': $('#share-agent-id').val(),
        'brandUrl': $('#brand-url').val(),
        'fadeEffect': true,
        'limit': 999,
        'productStepDuration': 200
    }

    // Align the product social popup with the image
    $(document.body).on('mouseenter', '.product:not([class*="product-ad"])', function () {
        var $parentImage = $(this).find('img');
        var $defaultSharer = $(this).find('.defaultsharer');
        $defaultSharer.width($parentImage.width() - 1); // offset by a small amount to make the lines cleaner
    })

    var callbacks = {
        success: function () {
            settings.preloaderContainer.fadeOut();
            $('#Container').mixItUp();
			$('#Container').on('mixEnd', function(e, state){
				$(".sortingby .itemamount").text($("#Container .product:visible").length + " ITEMS");
				$('#Container .product a.gallery').unbind();
				$('#Container .product:visible a.gallery').featherlightGallery({
				    previousIcon: '<i class="fa fa-chevron-left"></i>',
				    nextIcon: '<i class="fa fa-chevron-right"></i>',
				    galleryFadeIn: 300,
				    openSpeed: 300,
					afterOpen:    function(event){
				    	$(".featherlight-inner").first().height($(".featherlight-content").height());
					},
					afterContent : function(event) {
						$(".featherlight-inner").first().height($(".featherlight-content").height());
					},
					afterClose:   function(event){
					}
				});
			});
			
			console.log(settings);
            if (settings.container.children().length > 0) {
                $(".product:hidden").each(function (index) {
                    if (true == settings.fadeEffect) {
                        $(this).delay(settings.productStepDuration * index).fadeIn(300);
                    } else {
                        $(this).delay(settings.productStepDuration * index).show();
                    }
                });

                // Initialize the default share functionality
                var facebookAppId = 1525591217685323;
                //defaultshare.init('.product:not([class*="product-ad"])', 'a', facebookAppId, 'a', '', !in_facebook);
				
                setTimeout(function () {
                    settings.container.zinProducts('more');
                }, 500);
            } else {
                settings.container.find('#status').html("no products were found for this search");
            }
        },

        loader: function (isLoading) {
            if (isLoading) {
            } else {
                settings.preloaderContainer.fadeOut();
            }
        }
    }
    settings.callbacks = callbacks;

    /**
     *
     * @param adNumber
     * @param position
     */
    function placeAd(adNumber, position) {
        if (-1 == settings.container.find('.ad-container.ad-' + adNumber).length) {
            console.log('warning: cannot find ad number: ' + adNumber);
        } else {
            if (settings.container.find('.product-ad-' + adNumber).length == 1) {
                console.log('found ad, skipping');
                return;
            }
            console.log('placing ad ' + adNumber + ' at position ' + position);
            $('.ad-container.ad-' + adNumber + ' div:first-child').clone(true).insertAfter(settings.container.children('div:eq(' + position + ')'));
        }
    }

    /**
     *
     * @param settings
     */
    function getProducts(settings) {
        console.log('retrieving products');

        var productData = {
            storeid: settings.storeid,
            limit: settings.limit,
            catid: settings.catid,
            isurl: encodeURIComponent(settings.brandUrl),
            agentid: settings.sharedAgentId
        }

        if (undefined == settings.action || "" == settings.action) {
            settings.container.zinProducts({
                data: productData,
                itemTemplate: '#product-template',
                preloadTemplate: '#product-loading-template',
                preloadDivContainer: '.preloaded-product',
                callbacks: settings.callbacks
            })
        } else {
            settings.container.zinProducts(settings.action, {
                data: productData,
                itemTemplate: '#product-template',
                preloadTemplate: '#product-loading-template',
                preloadDivContainer: '.preloaded-product',
                callbacks: settings.callbacks
            })
        }
    }

    /**
     * Sets the active category filter based on a category id
     * @param categoryId
     * @returns {boolean}
     */
    function setActiveCategoryFilter(categoryId) {
        // Remove all active classes first
        settings.categoryFilters.removeClass('active');

        var setActive = true;
        settings.categoryFilters.each(function () {
            var $filterButton = $(this);
            if (categoryId == $filterButton.data('category-id')) {
                $filterButton.addClass('active')
                return setActive;
            }
        })

        return !setActive;
    }

    // Handler for the category filters
    settings.categoryFilters.on('click', function () {
        var categoryId = $(this).data('category-id');
        settings.catid = categoryId;

        settings.container.html('');
        settings.preloaderContainer.show();

        setActiveCategoryFilter(categoryId);
        getProducts(settings);
    })

    // Retrieve the products on load
    getProducts(settings);

    // Set the active filter
    if (settings.catid) {
        setActiveCategoryFilter(settings.catid);
    }

    /**
     * Applies a height to all of the ads
     */
    function autoResizeProducts() {
        var $baselineProduct = settings.container.find('div.product:first-child');
        if ($baselineProduct.length != 0) {
            $('[class*="product"]').each(function () {
                var $product = $(this);
                $product.css('height', 'auto');
                $product.height($baselineProduct.height());
            })
        }
    }

    // Auto resize the ads
    if (!in_facebook) {
        setInterval(autoResizeProducts, 500);
        $(window).resize(autoResizeProducts);
    } else {
        autoResizeProducts();
    }

    // Trigger the video modal on click
    $('.watch-video').on('click', function () {
        $videoModal.modal('show');

        return false;
    });
    
    $(".sortavail").click(function() {
    	$(".sortbybutton").removeClass("active");
    	$(this).addClass("active");
    	$('#Container').mixItUp('filter', '.avble');
    	$(".sortingby .itemamount").text($("#Container .product:visible").length + " ITEMS");
    });
    
    $(".sortall").click(function() {
    	$(".sortbybutton").removeClass("active");
    	$(this).addClass("active");
    	$('#Container').mixItUp('filter', 'all');
    	$(".sortingby .itemamount").text($("#Container .product:visible").length + " ITEMS");
    });
})
