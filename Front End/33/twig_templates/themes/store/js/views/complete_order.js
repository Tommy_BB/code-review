$(document).ready(function() {
    console.log('complete order module loaded');

    function resize() {
        var mobileMq = window.matchMedia('all and (max-width: 480px)');
        if (mobileMq.matches) {
            $('.col-xs-2').hide();
            $('.col-xs-8').removeClass('col-xs-12').addClass('col-xs-12');
        } else {
            $('.col-xs-2').show();
            $('#customerinfo').addClass('col-xs-6');
            $('#paymentinfo').addClass('col-xs-6');
        }
    }

    resize();
    $(window).resize(resize);
})