$(document).ready(function() {
    console.log('purchase now module loaded');

    var $billing = $('.billing');
    var $shipping = $('.shipping');
    var $pageTitle = $('.pagetitle');

    // Default the country selector the United States
    $('select[name="country"] option[value="US"]').attr('selected', 'selected');
	
	$('select[name="country"]').change(function(e) {
		
		// Disable zipcode requirement outside of the US
		//
		if (e.currentTarget.value !== "US") {
//			console.log('not us',e.currentTarget.value );
			$('input[name="zipcode"]').removeAttr('required');
			$('span.zipcode').removeClass('required');
			$('span.zipcode').text('');
			
		} else {
			$('input[name="zipcode"]').attr('required','required');
			$('span.zipcode').addClass('required');
			$('span.zipcode').text('*');
		}
	});
	
	
	$('select[name="ship_country"]').change(function(e) {
		
		// Disable zipcode requirement outside of the US
		//
		if (e.currentTarget.value !== "US") {
//			console.log('not us',e.currentTarget.value );
			$('input[name="ship_zip"]').removeAttr('required');
			$('span.ship_zip').removeClass('required');
			$('span.ship_zip').text('');
			
		} else {
			$('input[name="ship_zip"]').attr('required','required');
			$('span.ship_zip').addClass('required');
			$('span.ship_zip').text('*');
		}
	});
	

    $('button.prev').on('click', function() {
        if ($billing.is(':visible')) {
            parent.history.back();
        } else {
            $('html, body').animate({
                scrollTop: $pageTitle.offset().top
            }, 500);

            $shipping.hide();
            $pageTitle.html('Billing');
            $billing.fadeIn();
        }

        return false;
    })

    $('button.next').on('click', function() {
        var billingEmail = $billing.find('[name=email]').val();
        var shippingEmail = $shipping.find('[name=ship_email]').val();
        $shipping.find('[name=ship_contact]').val(shippingEmail);
        $billing.find('[name=contact]').val(billingEmail);

        if (!$shipping.is(':visible')) {
            $('html, body').animate({
                scrollTop: $pageTitle.offset().top
            }, 500);
        }

        $("#billing-form").validate({
            submitHandler: function(form) {
                if ($billing.is(':visible')) {
                    $billing.hide();

                    $pageTitle.html('Shipping');
                    $shipping.fadeIn();
                } else {
                    form.submit();
                }
            }
        })
    })

    jQuery.validator.addMethod("state", function(value, element) {
        var $countrySelector = ($pageTitle.text() == 'Shipping') ? $('select[name="ship_country"]') : $('select[name="country"]');
        var countrySelected = $countrySelector.val();
        return (countrySelected == 'US' && !value) ? false : true;
    })

    billingToShippingInputMap = {
        'company': 'ship_company',
        'fname': 'ship_fname',
        'lname': 'ship_lname',
        'address': 'ship_address1',
        'address2': 'ship_address2',
        'city': 'ship_city',
        'state': 'ship_state',
        'country': 'ship_country',
        'zipcode': 'ship_zip',
        'email': 'ship_email',
        'phone': 'ship_phone',
        'fax': 'ship_fax'
    }

    $('#copy-billing').on('click', function() {
        console.log('copying billing information');

        var isSelected = $(this).is(':checked');

        for (billingInputName in billingToShippingInputMap) {
            var shippingInputName = billingToShippingInputMap[billingInputName];
            var $billingField = $billing.find($('[name="'+ billingInputName +'"]'));
            var $shippingField = $shipping.find($('[name="'+ shippingInputName +'"]'));

            if ($billingField.is('select')) {
                var selectedBillingSelectIdx = (isSelected) ? $billingField.find('option:selected').index() : 0;
                $shippingField.find('option:eq('+ selectedBillingSelectIdx +')').prop('selected', true);
				$shippingField.change();
            } else {
                var shippingFieldValue = (isSelected) ? $billingField.val() : '';
                $shippingField.val(shippingFieldValue);
            }
        }
    })
})