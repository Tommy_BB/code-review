    $(document).ready(function () {
        console.log('social module loaded');

        $('#social-stream').dcSocialStream({
            feeds: {
                twitter: {
                    id: 'kayungernewyork'
                },
                facebook: {
                    id: '104663677628'
                },
                pinterest: {
                    id: 'kayungernewyork'
                },
                instagram: {
					accessToken: '180134371.f014b87.9dd23d8aa11f49339a635d60d0d5f62f',
					redirectUrl: '//www.kayunger.com/index.php?page=social&storeid=33', 
					clientId: 'f014b871e14a4fe294e8936261cae988',
					comments: 3,
                    likes: 10
				},
            },
            rotate: {
                delay: 0
            },
            twitterId: 'kayungernewyork',
            controls: false,
            filter: true,
			order: 'date',
            wall: true,
            cache: false,
            max: 'days',
            days: 60
        });

        $('li[class*="f-"] a img').each(function() {
        	/*
            var $socialImage = $(this);
            $socialImage.attr('data-img-hover', '');
            $socialImage.attr('data-toggle', $socialImage.attr('src').replace('.png', '_on.png'));
            */
            $(this).remove();
            
        })
    });