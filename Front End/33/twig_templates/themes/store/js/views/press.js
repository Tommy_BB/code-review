$(document).ready(function () {
    console.log('press module loaded');
    
function getFeatured(orderby)
{
	if($(".presslideshow").hasClass("slick-initialized"))
		$(".presslideshow").unslick();
	$(".presslideshow").empty();
	var totalpress = 0;
	$.ajax({
		url: "/json/specificpress.php",
		data: "storeid=33&action=moreallpress&orderby="+orderby,
		dataType: 'json',
		async : true,
		success: function(items) { 
			
			$(".presslideshow").empty();
			var pressno = items.length;
			$('.pressitems').html("<span>"+pressno+"</span> ITEMS");
			for(it in items) 
            { 
                    var inside = "";
                    inside += '<div class="press-item">';
                    inside += '<a href="#" class="gallery" data-featherlight="<div><img src=\''+items[it]['mainimage']+'\' /></div><div class=\'collectionstyle\'>'+items[it]['name']+'</div>">';
                    inside += '<img src="'+items[it]['mainimage']+'" />';
                    inside += '</a>';
                    inside += '<div class="hoverstate popup"  data-type="image" data-id='+items[it]['pressid']+'>';
                    inside += '</div>';
                    inside += '</div>';
                    
                    $(".presslideshow").append(inside);
                    $(".loader").hide();
                    totalpress++;
			}
			$(".itemamount").html(totalpress+" Items")
			$(".press-item").imagesLoaded( function() {
				$(".press-item").matchHeight();
			});
			
			$('div.presslideshow').slick({
		        dots: false,
		        slidesToShow: 4,
  				slidesToScroll: 4,
  				prevArrow: '<div class="slick-prev"></div>',
  				nextArrow: '<div class="slick-next"><i class="fa fa-chevron-right"></i></div>',
  				variableWidth: true,
  				draggable: false,
  				responsive: [
			    {
			      breakpoint: 1000,
			      settings: {
			        dots: false,
			        slidesToShow: 3,
	  				slidesToScroll: 3,
	  				prevArrow: '<div class="slick-prev"></div>',
	  				nextArrow: '<div class="slick-next"><i class="fa fa-chevron-right"></i></div>',
	  				variableWidth: true,
	  				draggable: false
			      }
			    },
			    {
			      breakpoint: 800,
			      settings: {
			        dots: false,
			        slidesToShow: 2,
	  				slidesToScroll: 2,
	  				prevArrow: '<div class="slick-prev"></div>',
	  				nextArrow: '<div class="slick-next"><i class="fa fa-chevron-right"></i></div>',
	  				variableWidth: true,
	  				draggable: false
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			        dots: false,
			        slidesToShow: 1,
	  				slidesToScroll: 1,
	  				prevArrow: '<div class="slick-prev"></div>',
	  				nextArrow: '<div class="slick-next"><i class="fa fa-chevron-right"></i></div>',
	  				variableWidth: true,
	  				draggable: false
			      }
			    }
			    ]
		    });
    
			$('.presslideshow .press-item a').featherlightGallery({
			    previousIcon: '<i class="fa fa-chevron-left"></i>',
			    nextIcon: '<i class="fa fa-chevron-right"></i>',
			    galleryFadeIn: 300,
			    openSpeed: 300,
			    afterOpen:    function(event){
			    	$(".featherlight-inner").first().height($(".featherlight-content").height());
				},
				afterContent : function(event) {
					$(".featherlight-inner").first().height($(".featherlight-content").height());
				},
				afterClose:   function(event){
				}
			});
		}
	});
}

getFeatured("");

	$(".suboptionpr").click(function() {
	        var thisval = $(this).find("input").val();
	        var toselecttext = $(this).text();
	        $(this).parent().parent().find(".valtext").text(toselecttext); 
	        getFeatured(thisval);
	        $(this).parent().hide();
	});
});