$(document).ready(function() {
    console.log('find store module loaded');

    var $stores = $('.store');

    $('select[name="country"]').on('change', updateStoreVisibility)
    $('select[name="state"]').on('change', updateStoreVisibility)

    /**
     * Dynamically updates the store visibility based on what the user selects from the location dropdowns
     */
    var hiddenStateOptions = [];
    function updateStoreVisibility() {

        var $countrySelect = $('select[name="country"]');
        var $stateSelect = $('select[name="state"]');

        var selectedCountry = $countrySelect.val();
        var selectedState = $stateSelect.val();

        var selectedCountryText = (selectedCountry == 'all') ? 'all countries' : $countrySelect.find('option:selected').text();
        var selectedStateText = (selectedState == 'all') ? 'all states' : $stateSelect.find('option:selected').text();

        $('.selected-country').html(selectedCountryText);
        $('.selected-state').html(selectedStateText);

        var showAllCountries = (selectedCountry == 'all');
        var showAllStates = (selectedState == 'all');

        for (stateIdx in hiddenStateOptions) {
            var $stateOption = hiddenStateOptions[stateIdx];
            $stateSelect.append($stateOption);
            delete hiddenStateOptions[stateIdx];
        }
        if (false == showAllCountries) {
            $stateSelect.find('option').each(function() {
                var $stateOption = $(this);
                if ($stateOption.data('country') != selectedCountry && $stateOption.val() != 'all') {
                    hiddenStateOptions.push($stateOption);
                    $stateOption.remove();
                    return;
                }
            })
        }

        $stores.each(function() {
            var storeCountry = $(this).data('country');
            var storeState = $(this).data('state');

            if (false == showAllCountries) {
                if (storeCountry != selectedCountry) {
                    $(this).hide();
                    return; //continue;
                }
            }

            if (false == showAllStates) {
                if (storeState != selectedState) {
                    $(this).hide();
                    return; // continue
                }
            }

            $(this).show();
        })
    }

    $('.store').matchHeight();
})
