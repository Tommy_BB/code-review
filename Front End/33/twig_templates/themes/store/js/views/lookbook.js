$(document).ready(function() {
    console.log('lookbook module loaded');

    var settings = {
        $container: $('.lookbook-container'),
        $popup: $('.lookbook-popup'),
        $products: $('.product'),
        $selectedProduct: $('.selectedProduct'),
        $popupCloseBtn: $('.close button'),
        $popupProductName: $('.name span'),
        $popupCollectionName: $('.collection span'),
        $popupImage: $('.image'),
        $paginate: $('.paginate'),
        $paginateLarge: $('.paginate-large'),
        $paginateLeft: $('.left'),
        $paginateRight: $('.right'),
        $paginateIndex: $('.index'),
        $paginateMax: $('.max')
    }

    // All lookbook products
    var products = [];

    // The currently selected product index
    var currentProductIdx;

    // Add the lookbook products to a collection for pagination
    settings.$container.find(settings.$products).each(function() {
        var productName = $(this).find('input[name="name"]').val();
        var productId = $(this).find('input[name="id"]').val();
        var productCollection = $(this).find('input[name="collection"]').val();
        var productImage = $(this).find('img').attr('src');
        products.push({
            id: productId,
            name: productName,
            collection: productCollection,
            image: productImage
        });
    })

    settings.$container.find(settings.$products).on('click', function() {
        console.log('lookbook product clicked');

        // Get the index of the product clicked
        currentProductIdx = $(this).index() + 1;

        // Update the popup data
        updatePopup(currentProductIdx);

        // Toggle the popup
        settings.$container.hide();
        settings.$popup.fadeIn();

        // Resize the pagination div
        resizePagination();

        return false;
    })

    settings.$popup.find(settings.$popupCloseBtn).on('click', function() {
        console.log('lookbook popup closed');

        // Toggle the popup
        settings.$popup.hide();
        settings.$container.fadeIn();

        return false;
    })


    /**
     * @param productIdxOffset
     */
    function processPagination(event) {
        console.log('pagination clicked');
        var productIdxOffset = event.data.offset;

        // Determine the current product index based on which pagination was clicked
        if (currentProductIdx == products.length && productIdxOffset > 0) {
            currentProductIdx = 1;
        } else if (currentProductIdx == 1 && productIdxOffset < 0) {
            currentProductIdx = products.length;
        } else {
            currentProductIdx = (currentProductIdx + productIdxOffset);
        }

        // Update the popup data
        updatePopup(currentProductIdx);

        return false;
    }

    /**
     * Updates the popup data based on a given product idx
     * @param productIdx
     */
    function updatePopup(productIdx) {

        // Update the pagination
        settings.$paginate.find(settings.$paginateIndex).html(productIdx);

        // Update the product info
        productData = products[productIdx - 1];
        settings.$popup.find(settings.$popupImage).attr('src', productData.image);
        settings.$popup.find(settings.$popupProductName).html(productData.name);
        settings.$popup.find(settings.$popupCollectionName).html(productData.collection);
    }

    /**
     * Resizes the pagination based on the height
     */
    function resizePagination() {
        settings.$paginateLarge.height(settings.$popup.find('img').height());
    }

    settings.$paginate.find(settings.$paginateLeft).on('click', {offset: -1}, processPagination);
    settings.$paginate.find(settings.$paginateRight).on('click', {offset: 1}, processPagination);

    // Set the maximum number of scrollable items in the pagination
    settings.$paginate.find(settings.$paginateMax).html(products.length);

    // Keep the pagination vertically aligned with the image
    $(window).resize(resizePagination);
    setInterval(resizePagination, 500);
})