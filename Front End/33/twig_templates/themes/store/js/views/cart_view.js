$(document).ready(function() {
    console.log('cart view module loaded');

    var $form = $('#cart-form');
    var $actionInput = $('#cart-form input[name=action]');

    // Updates and recalculates the cart
    $('.update').on('click', function() {
        $actionInput.val('recalc');
        $form.submit();

        return false;
    })

    // Removes an item from the cart
    $('.delete').on('click', function() {
        var cartItemKey = $(this).data('cart-item-id');
        var deleteUrl = $form.attr('action') + '&action=remove&productID=' + cartItemKey;

        window.location = deleteUrl;
        return false;
    })

    // Apply promotional codes
    $('.apply-promo').on('click', function() {
        $actionInput.val('applypromocodes');
        $form.submit();

        return false;
    })

    $('.continue-shopping').on('click', function() {
        var actionUrl = $form.attr('action');
        var continueShoppingUrl = actionUrl.replace(/page=([^&]+)/gmi, 'page=products');
        window.location = continueShoppingUrl;

        return false;
    })

    $('.checkout').on('click', function() {
        console.log($form);
        var actionUrl = $form.attr('action');
        $actionInput.val('purchase');
        $form.submit();

        return false;
    })
})