var smartajax = "";
var currentcatid = "";
var searchWord = "";
$(document).ready(function () {
    //sessionStorage.clear();
    console.log('products module loaded');
    currentcatid = $('#catid').val();
    searchWord = $('#searchword').val();
    // JSRender view helpers
    $.views.helpers({
        // Convert value to an integer
        Integer: function (val) {
            return val.replace(".00", "");
        },

        round: function (val) {
            val = val + "";
            val = val.replace(".00", "");
            return val;
        },

        Number: function (val) {
            return Number(val);
        },

        Truncate: function (val, length, ellipsis) {
            if (typeof length == 'undefined') {
                var length = 100;
            }

            if (typeof ellipsis == 'undefined') {
                var ellipsis = '...';
            }

            if (val.length <= length) {
                return val;
            }

            for (var i = length - 1; val.charAt(i) != ' '; i--) {
                length--;
            }

            return val.substr(0, length - 1) + ellipsis;
       },
       FormatSizesNew: function(val) {
            if(val == "undefined") return val;

            if(val.toLowerCase().indexOf("xs") >= 0 || val.toLowerCase().indexOf("s") >= 0 || val.toLowerCase().indexOf("m") >= 0 || val.toLowerCase().indexOf("k") >= 0 || val.toLowerCase().indexOf("xl") >= 0 || val.toLowerCase().indexOf("xxl") >= 0)
            {
            	var splitarray = val.toLowerCase().split(',');
            	var returnedarray = [];
            	if($.inArray("xs",splitarray) !== -1)
            		returnedarray.push("XS");
            	if($.inArray("s",splitarray) !== -1)
            		returnedarray.push("S");
            	if($.inArray("m",splitarray) !== -1)
            		returnedarray.push("M");
            	if($.inArray("l",splitarray) !== -1)
            		returnedarray.push("L");
            	if($.inArray("xl",splitarray) !== -1)
            		returnedarray.push("XL");
            	if($.inArray("xxl",splitarray) !== -1)
            		returnedarray.push("XXL");
            	
            	val = returnedarray.join(" ");
            }
            
       		val = val.replace(","," ");
       		val = val.replace(/,/g, ' ');
	        return val;
	    },
	    FixSizes: function(val) {
			val = val.replace(/ /g, '-');
			val = val.replace(/\//g, '-');
			val = val.replace(/,/g, ' SIZE');
			if(val != "" && val != null)
			return "SIZE" + val;
			else return "";
		},
		FixColors: function(val) {
			val = val.replace(/ /g, ' COLOR');
			if(val != "" && val != null)
			return "COLOR" + val;
			else return "";
		}
    });

    if(searchWord != "")
        $('#content-wrap #action').val("search");

    var settings = {
        'container': $('#products-container .products'),
        'preloaderContainer': $('.z-preloader'),
        'categoryFilters': $('button.category-filter'),
        'storeid': $('#storeid').val(),
        'catid': $('#catid').val(),
        'action': $('#content-wrap #action').val(),
        'sharedAppId': $('#share-app-id').val(),
        'sharedAgentId': $('#share-agent-id').val(),
        'brandUrl': $('#brand-url').val(),
        'fadeEffect': true,
        'limit': 11,
        'productStepDuration': 200
    }
    var callbacks = {
        success: function () {
            settings.preloaderContainer.fadeOut();
            if (settings.container.children().length > 0) {
				$('#Container').mixItUp();
				$('#Container').mixItUp('filter', 'all');
				$('#Container').on('mixEnd', function(e, state){
					$(".sortingby .itemamount span").text($("#Container .product:visible").length + " ITEMS");
				});

				if(typeof lookbookHelper !== 'undefined'){
                	lookbookHelper.init(".product","a","div.name");
                }

                setTimeout(function () {
                    settings.container.zinProducts('more');
                }, 500);
            } else {
                $('#status').html("no products were found for this search");
            }
        },
        loader: function (isLoading) {
            if (isLoading) {
            } else {
                settings.preloaderContainer.fadeOut();
            }
        }
    }
    
    settings.callbacks = callbacks;

    /**
     *
     * @param settings
     */
    function getProducts(settings) {
        console.log('retrieving products');

        var productData = {
            storeid: settings.storeid,
            limit: settings.limit,
            catid: settings.catid,
            isurl: encodeURIComponent(settings.brandUrl),
            agentid: settings.sharedAgentId,
            search : searchWord
        }
        if (undefined == settings.action || "" == settings.action) {
            settings.container.zinProducts({
                data: productData,
                itemTemplate: '#product-template',
                preloadTemplate: '#product-loading-template',
                preloadDivContainer: '.preloaded-product',
                callbacks: settings.callbacks
            })
        } else {
            settings.container.zinProducts(settings.action, {
                data: productData,
                itemTemplate: '#product-template',
                preloadTemplate: '#product-loading-template',
                preloadDivContainer: '.preloaded-product',
                callbacks: settings.callbacks
            })
        }
    }

    // Retrieve the products on load
    getProducts(settings);
    
    /**
     * Applies a height to all of the ads
     */
    function autoResizeProducts() {
        var $baselineProduct = settings.container.find('div.product:first-child');
        if ($baselineProduct.length != 0) {
            $('[class*="product"]').each(function () {
                var $product = $(this);
                $product.css('height', 'auto');
                $product.height($baselineProduct.height());
            })
        }
    }

    // Auto resize the ads
    if (!in_facebook) {
        setInterval(autoResizeProducts, 500);
        $(window).resize(autoResizeProducts);
    } else {
        autoResizeProducts();
    }

	$("select[name=color]").change(function (e) {
		if(smartajax && smartajax.readystate != 4)
		smartajax.abort();
		
		//get sizes before we smart filter
		var selbeforesize = $("select[name=size]").val();
		selbeforesize = selbeforesize.replace(".SIZE","");
		
		var selbeforesil = $("select[name=silhouette]").val();
		selbeforesil = selbeforesil.replace(".SIL","");
		
		
		var beforearray = [];
		var afterarray = [];

		$(".size-selector").find(".suboption").each(function() {
			if($(this).css('display') == "block")
				beforearray.push($(this).text());
		});
		
		
		//silhouettes
		var beforearrays = [];
		var afterarrays = [];
		
		$(".silhouette-selector").find(".suboption").each(function() {
			if($(this).css('display') == "block")
				beforearrays.push($(this).text());
		});
		
		//silhouettes
		var beforearrayss = [];
		var afterarrayss = [];
		
		$(".color-selector").find(".suboption").each(function() {
			if($(this).css('display') == "block")
				beforearrayss.push($(this).text());
		});
		
		var thiscolor = $(this).val();
		var beforecolor = $(this).val();
		thiscolor = thiscolor.replace(".COLOR","");
		smartajax = $.ajax({
			url: "/json/smartfilter.php",
			data: "catid="+currentcatid+"&color="+thiscolor+"&size="+selbeforesize+"&sil="+selbeforesil+"&storeid=<?= $storeid; ?>",
			dataType: 'json',
			async : true,
			success: function(items) {
				var allsizes = items['Sizes'];
				for(it in allsizes)
				{
					if($.inArray(allsizes[it],beforearray) < 0)
					{
						$(".size-selector").find(".suboption").each(function() {
							if($(this).text() == allsizes[it])
								$(this).show();

						});
					}
					afterarray.push(allsizes[it]);
				}

				$(".size-selector").find(".suboption").each(function() {
						if($(this).index() == 0)
			    			return;
			    		if($.inArray($(this).text(),afterarray) < 0)
							$(this).hide();
				});
				
				//silhouettes
				var allsil = items['Silhouettes'];
				for(it in allsil)
				{
					if($.inArray(allsil[it],beforearrays) < 0)
					{
						$(".silhouette-selector").find(".suboption").each(function() {
							if($(this).text() == allsil[it])
								$(this).show();

						});
					}
					afterarrays.push(allsil[it]);
				}

				$(".silhouette-selector").find(".suboption").each(function() {
						if($(this).index() == 0)
			    			return;
			    		if($.inArray($(this).text(),afterarrays) < 0)
							$(this).hide();
				});
                                        
			    	if($.inArray(selbeforesize,afterarray) < 0 && $.inArray(selbeforesil,afterarrays) < 0 && beforecolor != "")
					$('#Container').mixItUp('filter',beforecolor);
					else
					if(selbeforesize != "" && selbeforesil != "")
					$('#Container').mixItUp('filter',beforecolor + ".SIZE" + selbeforesize + ".SIL" + selbeforesil);
					else
					if(selbeforesize != "" && selbeforesil == "")
					$('#Container').mixItUp('filter',beforecolor + ".SIZE" + selbeforesize);
					else
					if(selbeforesize == "" && selbeforesil != "")
					$('#Container').mixItUp('filter',beforecolor + ".SIL" + selbeforesil);
					else
					if(selbeforesize == "" && selbeforesil == "" && beforecolor == "")
					$('#Container').mixItUp('filter', 'all');
			}
		});
	});
    
    
	$("select[name=size]").change(function (e) {
		if(smartajax && smartajax.readystate != 4)
		smartajax.abort();
		var selbeforecolor = $("select[name=color]").val();
		selbeforecolor = selbeforecolor.replace(".COLOR","");
		
		var selbeforesil = $("select[name=silhouette]").val();
		selbeforesil = selbeforesil.replace(".SIL","");
		
		
		var beforearray = [];
		var afterarray = [];
		$(".color-selector").find(".suboption").each(function() {
			if($(this).css('display') == "block")
				beforearray.push($(this).text());
		});
		
		var beforearrays = [];
		var afterarrays = [];
		$(".silhouette-selector").find(".suboption").each(function() {
			if($(this).css('display') == "block")
				beforearrays.push($(this).text());
		});
		
		var beforearrayss = [];
		var afterarrayss = [];
		$(".size-selector").find(".suboption").each(function() {
			if($(this).css('display') == "block")
				beforearrayss.push($(this).text());
		});
		
		console.log($(this).val());
		var thissize = $(this).val();
		var beforesize = $(this).val();
		thissize = thissize.replace(".SIZE","");
		smartajax = $.ajax({
		url: "/json/smartfilter.php",
		data: "catid="+currentcatid+"&size="+thissize+"&color="+selbeforecolor+"&sil="+selbeforesil+"&storeid=" + $('#storeid').val(),
		dataType: 'json',
		async : true,
		success: function(items) { 
			var allcolors = items['Colors'];
			for(it in allcolors)
			{
				if($.inArray(allcolors[it],beforearray) < 0)
				{
					$(".color-selector").find(".suboption").each(function() {
						if($(this).text() == allcolors[it])
							$(this).show();
					
					});
				}
				afterarray.push(allcolors[it]);
			}
				$(".color-selector").find(".suboption").each(function() {
					if($(this).index() == 0)
		    			return;
		    		if($.inArray($(this).text(),afterarray) < 0)
						$(this).hide();
				});
				
			var allsils = items['Silhouettes'];
			for(it in allsils)
			{
				if($.inArray(allsils[it],beforearrays) < 0)
				{
					$(".silhouette-selector").find(".suboption").each(function() {
						if($(this).text() == allsils[it])
							$(this).show();
					
					});
				}
				afterarrays.push(allsils[it]);
			}
				$(".silhouette-selector").find(".suboption").each(function() {
					if($(this).index() == 0)
		    			return;
		    		if($.inArray($(this).text(),afterarrays) < 0)
						$(this).hide();
				});
				
                    console.log(beforesize + " is before color");
			    	console.log("--"+beforesize + ".SIZE" + selbeforecolor + ".SIL" + selbeforesil+"--");
			    	
			    	
			    	if($.inArray(selbeforecolor,afterarray) < 0 && $.inArray(selbeforesil,afterarrays) < 0 && beforesize != "")
					$('#Container').mixItUp('filter',beforesize);
					else
					if(selbeforecolor != "" && selbeforesil != "")
					$('#Container').mixItUp('filter',beforesize + ".COLOR" + selbeforecolor + ".SIL" + selbeforesil);
					else
					if(selbeforecolor != "" && selbeforesil == "")
					$('#Container').mixItUp('filter',beforesize + ".COLOR" + selbeforecolor);
					else
					if(selbeforecolor == "" && selbeforesil != "")
					$('#Container').mixItUp('filter',beforesize + ".SIL" + selbeforesil);
					else
					if(selbeforecolor == "" && selbeforesil == "")
					$('#Container').mixItUp('filter', 'all');
		}
		});
	});
	
	
	$("select[name=sorter]").change(function (e) {
		console.log($(this).val() + " is sorter");
		$('#Container').mixItUp('sort', $(this).val());
	});
})
