// JavaScript Document
//Get Products 

(function($){
	$.fn.zinProducts = function() {
		var method = arguments[0];

		if(methods[method]) {
			method = methods[method];
			arguments = Array.prototype.slice.call(arguments, 1);
		} else if( typeof(method) == 'object' || !method ) {
			method = methods.init;
		} else {
			$.error( 'Method ' +  method + ' does not exist in zinProducts' );
			return this;
		}
		return method.apply(this, arguments);
	}
	var _getSessName =  function(init, data) {
		sessName = init;
		for( index in data ) {
			if(index != "start" && index != "limit" && data[index] != null)
			sessName += '_' + data[index].toString().replace(',', '_');
		}
		return sessName;
	}
	var methods = {
		_getInitData : function() {
			$(this).data('isMore', true);
			$(this).data('isLoading', true);
			options = $(this).data('options');
			var container = this.selector;
			try {
				if (window.sessionStorage){
                    console.log(options.sessName);
					items = JSON.parse(sessionStorage.getItem(options.sessName));

					if(typeof items == 'object' && items != null) {
						//current = items.length;
						console.log('From Session',items);
						for(i=0; i< items.length; i++){
							$(container).append( $(options.itemTemplate).render(items[i]) );
						}
						
						options.data.start = items.length+1;
						$(this).data('options', options);
						
						if(typeof options.callbacks.success == 'function') {
							options.callbacks.success.call(this);
						}
						if(typeof options.callbacks.loader == 'function') {
							options.callbacks.loader.call(this , false);
						}
						
						$(this).data('isLoading', false);
						return this;
					} else {
						return methods['_getData'].apply(this);
					}
				} else {
					return methods['_getData'].apply(this);
				}
			} catch (e) {
				return methods['_getData'].apply(this);
			}
		},
		_getData : function() {
			options = $(this).data('options');
			$(this).data('isLoading', true);
			if(typeof options == 'object' && typeof options.callbacks.loader == 'function') {
				options.callbacks.loader.call(this , true);
			} else {
				console.log('No Loader Callback');
			}
		
			var container = this.selector;
			var self = this;
			$.ajax({
				url: options.url,
				data: options.data,
				dataType: 'json',
				async : true,
				
				success: function(items) {
					for(i=0; i< items.length; i++){
						$(container).append( $(options.itemTemplate).render(items[i]) );
					}
					if(items.length < options.data.limit) { self.data('isMore', false); }
                    self.data('isLoading', false);

					if(typeof options.callbacks.success == 'function') {
						options.callbacks.success.call(this);
					} else {
						console.log('No Success Callback');
					}

					if(items.length > 0 && self.data('isMore') == true) {
						try {
							if (window.sessionStorage){
								sessItems = JSON.parse(sessionStorage.getItem(options.sessName));
								//console.log(sessItems);
								if(typeof sessItems == 'object' && sessItems!=null) {
									sessionStorage.setItem(options.sessName, JSON.stringify(sessItems.concat(items)) );
								} else {
									sessionStorage.setItem(options.sessName, JSON.stringify(items) );
								}
								//console.log(sessionStorage.getItem('items<?=$catkey?>'));
							} 
						} catch (e) {
						}
					}
				},
			
				error: function(jqXHR, textStatus, errorThrown) {
					console.log('FAILED: ' + textStatus);
					if(typeof options.callbacks.error == 'function') {
						options.callbacks.error.call(this);
					} else {
						console.log('No Error Callback');
					}
				}
			  
			}).done(function() {
				//console.log('done');

				options.data.start = options.data.start + options.data.limit;
				self.data('options', options);
				
				if(typeof options.callbacks.loader == 'function') {
					options.callbacks.loader.call(this , false);
				} else {
					//console.log('No Success Callback');
				}
			});
			
			return this;
		},
		init :  function(_options) { 
		 	 products_defaults = {
				url : 'json/storecatproducts.php',
				data : {
					catid : null,
                    styleid: null,
					start : 1,
					limit : 9999
				},
				itemTemplate : '#product-template'
			};
			
			options =  $.extend(true, products_defaults, _options);
			options.sessName = _getSessName( 'prods', options.data );
			$(this).data('options', options);
			return methods['_getInitData'].apply(this);
		},
		'all' : function(_options) {
			products_defaults = {
				url : 'json/getstorecatproductsall.php',
				data : {
					catid : null,
					start : 1,
					limit : 9999
				},
				itemTemplate : '#product-template'
			};
			
			options =  $.extend(true, products_defaults, _options)
			options.sessName = _getSessName( 'prodsall', options.data );
			$(this).data('options', options);
			return methods['_getInitData'].apply(this);
		},
		'search' : function(_options) {
			products_defaults = {
				url : 'json/prodsearch.php',
				data : {
					storeid : null,
					start : 1,
					limit : 9999,
					search : ''
				},
				itemTemplate : '#product-template'
			};
			
			options =  $.extend(true, products_defaults, _options)
			options.sessName = _getSessName( 'prodsearch', options.data );
			$(this).data('options', options);
			return methods['_getInitData'].apply(this);
		},		
		'random' : function(_options) {
			products_defaults = {
				url : 'json/storerandomproducts.php',
				data : {
					start : 1,
					limit : 9999
				},
				itemTemplate : '#product-template'
			};
			
			options =  $.extend(true, products_defaults, _options)
			options.sessName = _getSessName( 'prodsrandom', options.data );
			$(this).data('options', options);
			return methods['_getInitData'].apply(this);
		}, 
		'keywords' : function(_options) {
			console.log('Products by Keywords');
			products_defaults = {
				url : 'json/getstoreproductsbykeywords.php',
				data : {
					catid : null,
					storeid : null,
					keywords: null,
					start : 1,
					limit : 9999
				},
				itemTemplate : '#product-template'
			};
			
			options =  $.extend(true, products_defaults, _options);
			options.sessName = _getSessName( 'prodskey', options.data );
			$(this).data('options', options);
			return methods['_getInitData'].apply(this);
		}, 
		'new' : function(_options) {
			console.log('New Products');
			products_defaults = {
				url : 'json/storenewproducts.php',
				data : {
					storeid : null,
					start : 1,
					limit : 9999
				},
				itemTemplate : '#product-template'
			};
			//$(this).data( $.extend(true, productsnew_defaults, _options) );
			options =  $.extend(true, products_defaults, _options);
			options.sessName = _getSessName( 'prodsnew', options.data );
			$(this).data('options', options);
			return methods['_getInitData'].apply(this);
		},
        'collections' : function(_options) {
            console.log('New Products');
            products_defaults = {
                url : 'json/getcollections.php',
                data : {
                    storeid : null,
                    start : 1,
                    limit : 9999
                },
                itemTemplate : '#product-template'
            };
            //$(this).data( $.extend(true, productsnew_defaults, _options) );
            options =  $.extend(true, products_defaults, _options);
            options.sessName = _getSessName( 'prodscollection', options.data );
            $(this).data('options', options);
            return methods['_getInitData'].apply(this);
        },
		sale : function(_options) {
			console.log('Sale Products');
			products_defaults = {
				url : 'json/storesaleproducts.php',
				data : {
					storeid : null,
					start : 1,
					limit : 9999
				},
				itemTemplate : '#product-template'
			};
			options =  $.extend(true, products_defaults, _options);
			options.sessName = _getSessName( 'prodssale', options.data );
			$(this).data('options', options);
			return methods['_getInitData'].apply(this);
		},
		crosssale : function(_options) {
			console.log('Cross Sale Products');
			 products_defaults = {
				url : 'json/crosssells.php',
				data : {
					storeid : null,
					productid : null
				},
				itemTemplate : '#product-template'
			};
			options =  $.extend(true, products_defaults, _options);
			options.sessName = _getSessName( 'prodscross', options.data );
			$(this).data('options', options);
			return methods['_getInitData'].apply(this);
		},
		more : function() {
			if( $(this).data('isMore') && !$(this).data('isLoading') ){
				return methods['_getData'].apply(this);
			} else { 
				//console.log('No More Items');
				return this;
			}
			
		},
		destroy : function( ) {
			$(this.selector).html('');
			return this.each(function(){
				$(this.element).unbind('.zinProducts');
			})
		}
	};
	

})(jQuery);
