(function($) {
    console.log('equal height helper loaded');
    
    Number.prototype.roundTo = function(num) {
	    var resto = this%num;
	    if (resto <= (num/2)) { 
	        return this-resto;
	    } else {
	        return this+num-resto;
	    }
	}
	function autoResizeProducts() {
        var $baselineProduct = $('div.ehh:first-child');
        if ($baselineProduct.length != 0) {
            $('.ehh').each(function () {
                var $product = $(this);
                $product.css('height', 'auto');
                $product.height($baselineProduct.height().roundTo(2));
            })
        }

        if ($baselineProduct.length != 0) {
            $('.ehhi').each(function () {
                var $product = $(this);
                $product.css('height', 'auto');
                $product.height(($baselineProduct.height().roundTo(2) /2));
            })
        }
    }

    // Auto resize the ads
    if (!in_facebook) {
        setInterval(autoResizeProducts, 500);
        $(window).resize(autoResizeProducts);
    } else {
        autoResizeProducts();
    }
})(jQuery);