$(document).ready(function () {
	console.log('out of stock module loaded');
	
	// Get all unavailable color value ids for this product
    var ucolorValueIds = [];
    $('select[name="unavailableColors"] option').each(function() {
        ucolorValueIds.push($(this).val());
    })
    
    //console.log('Unavailable color option values: ' + ucolorValueIds);
	
	// Get all unavailable size value ids for this product
    var usizeValueIds = [];
    $('select[name="unavailableSizes"] option').each(function() {
        usizeValueIds.push($(this).val());
    })
    
    //console.log('Unavailable size option values: ' + usizeValueIds);
    
    var usizeSelectorCloneHtml = $('select[name="unavailableSizes"]').clone(true).wrap('<div/>').parent().html();
	
	$('select[name="unavailableColors"]').on('change', function() {
        $('select[name="unavailableSizes"]').replaceWith(usizeSelectorCloneHtml);
        
        /* REMOVE THIS IF YOU DONT HAVE FANCY SELECT BOXES */
		$('select[name="unavailableSizes"]').parent().find(".valoptions .suboption").each(function() {
			$(this).show();
		});
		/* END */
		
        var selectedColorValueId = $(this).val();
        var invalidSizeValues = [];

        var uinventory = JSON.parse($('#oosinventory').val());
        
        for (inventoryIdx in uinventory) {
            var inventoryData = uinventory[inventoryIdx];
            for (attribute in inventoryData) {
                if (attribute.toLowerCase().indexOf('value') != -1) {
                    var valueId = inventoryData[attribute];
                    $.each(ucolorValueIds, function(key, colorValueId) {
                        if (selectedColorValueId == colorValueId) {
                            if (inventoryData['Value1Type'] == 'size') {
                                var sizeValueId = inventoryData['Value1'];
                            } else {
                                var sizeValueId = inventoryData['Value2'];
                            }
                            if (colorValueId == valueId && (inventoryData['Qty'] <= 0 || inventoryData['ProductStatus'] == 3 && inventoryData['ProductStatus'] == 2 && inventoryData['ProductStatus'] == 5)) {
                                invalidSizeValues.push(sizeValueId);
                                //console.log('Size value: ' + sizeValueId + ' is unavailable.');
                            }
                        }
                    })

                }
            }
        }
        /* REMOVE THIS IF YOU DONT HAVE FANCY SELECT BOXES */
        $('select[name="unavailableSizes"]').parent().find(".valoptions .suboption").each(function() {
        	var thisinput = $(this).find("input").val();
        	console.log(thisinput + " is current val");
        	if (-1 == $.inArray(thisinput, invalidSizeValues) && thisinput != "") {
        		console.log("hidding");
        		console.log($(this));
        		$(this).hide()
        	}
        });
        /* END REMOVE THIS IF YOU DONT HAVE FANCY SELECT BOXES */

        $('select[name="unavailableSizes"] option').each(function() {
            if (-1 == $.inArray($(this).val(), invalidSizeValues) && $(this).val() != "") {
                $(this).remove();
            }
        })
    })
	
    $('#oos-form').validate({
        ignore: "",
    });
    
    jQuery.validator.addMethod("oosname", function(value, element) {
        var oname = $('input[name=oosname]').val();
        return (oname) ? true : false;
    })
    
    jQuery.validator.addMethod("oosemail", function(value, element) {
        var oemail = $('input[name=oosemail]').val();
        return (oemail) ? true : false;
    })
    
    jQuery.validator.addMethod("ooscolor", function(value, element) {
        var color = $('select[name="unavailableColors"]');
        console.log(color);
        return (color.val() || color.length <= 0) ? true : false;
    })
    
    jQuery.validator.addMethod("oossize", function(value, element) {
        var size = $('select[name="unavailableSizes"]');
        return (size.val() || size.length <= 0) ? true : false;
    })
    
	$(".submitoos").on("click", function() {
        
        if($('#oos-form').valid())
        {
        	var url = $('#oos-form').attr('action'); 
			var data = $('#oos-form').serialize() + "&infb="+((window.self === window.top) ? 0 : 1); 
			
            $.ajax({
               type: "POST",
               url: url,
               data: data, 
               success: function(data)
               {
               		$("#outofStock .start-of-modal").hide();
               		$("#outofStock .end-of-modal").show();
               }, 
               error: function(data)
               {
               	alert(data);
               }
            });
		}
		return false;
	});
	
	var beforetextoos = "SIZE NOT AVAILABLE?";
	var aftertextoos = "NOTIFY ME";
	
	$(".oos").hover(function() {
		$(this).text(aftertextoos);
	}, function() {
		$(this).text(beforetextoos);
	});
	
	$(".oos").click(function() {
		$("#outofStock .start-of-modal").show();
        $("#outofStock .end-of-modal").hide();
	});
	
	$(".cont-shop").click(function() {
		$("#outofStock .start-of-modal").show();
        $("#outofStock .end-of-modal").hide();
        $("#outofStock button.close").click();
	})
	
})