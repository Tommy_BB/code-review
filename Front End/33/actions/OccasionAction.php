<?php

class OccasionAction extends \ZMVC\ZAction {
	public $title = 'Occasion';
    public $pageTitle = 'Occasion';
    public function run() {
        $this->prepareAssets();

        echo $this->getController()->render('occasion.twig');
    }

    private function prepareAssets() {
    	
        $this->addCssFileAsset('views/occasion.min.css');
        $this->addScriptFileAsset('utilities/equalEvenHeight.min.js');
        
    }
} 