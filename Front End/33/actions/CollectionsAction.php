<?php

class CollectionsAction extends \ZMVC\ZAction {
	public $title = 'Collections';
    public $pageTitle = 'Collections';
    public function run() {
        $this->prepareAssets();
        $brandSiteUrl  = 'http://stores.zindigo.com/templates';

        $pageagent = $this->getController()->getLegacyStoreHelper()->getPageAgent($_SESSION['pageid'], $_SESSION['storeid']);
        $sharedAgentId = $sharedAppId = (isset($pageagent['AgentID'])) ? $pageagent['AgentID'] : $_SESSION['TPAID'];
        $categoryId       = $_REQUEST['categoryId'];

        $content = $this->getController()->render(
            'collections.twig', array(
                'brandSiteUrl'  => $brandSiteUrl,
                'sharedAgentId' => $sharedAgentId,
                'sharedAppId'   => $sharedAppId,
                'categoryId'    => $categoryId,
            )
        );
        echo $content;
    }

    private function prepareAssets() {
    	
        $this->addCssFileAsset('../../defaultshare.css');
        $this->addCssFileAsset('views/products.min.css');
        $this->addScriptFileAsset('utilities/jsrender.min.js');
        $this->addScriptFileAsset('utilities/zindigo-products.min.js');
        $this->addScriptFileAsset('utilities/defaultshare.min.js');
        
       	$this->addCssFileAsset('bower_components/fontawesome/css/font-awesome.css');
        
        //featherlight
		$this->addScriptFileAsset('bower_components/featherlight/release/featherlight.min.js');
		$this->addCssFileAsset('bower_components/featherlight/release/featherlight.min.css');
		$this->addScriptFileAsset('bower_components/featherlight/release/featherlight.gallery.min.js');
        $this->addCssFileAsset('bower_components/featherlight/release/featherlight.gallery.min.css');
        
        //$this->addScriptFileAsset('views/products.min.js');
        $this->addScriptFileAsset('views/collections.min.js');
        
        $this->addCssFileAsset('views/collections.min.css');
        
    }
} 