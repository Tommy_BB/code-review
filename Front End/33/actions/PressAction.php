<?php

class PressAction extends \ZMVC\Store\Actions\PressAction {

    public $title = 'Press';
    public $pageTitle = 'Press';

    public function run() {
        $this->prepareAssets();

        echo $this->getController()->render('press.twig');
    }
    
    public function prepareAssets() {
		$this->addCssFileAsset('bower_components/fontawesome/css/font-awesome.css');
		$this->addScriptFileAsset('bower_components/featherlight/release/featherlight.min.js');
		$this->addCssFileAsset('bower_components/featherlight/release/featherlight.min.css');
		$this->addScriptFileAsset('bower_components/featherlight/release/featherlight.gallery.min.js');
		$this->addCssFileAsset('bower_components/featherlight/release/featherlight.gallery.min.css');
		
		$this->addCssFileAsset('node_modules/slick-carousel/slick/slick.css');
		$this->addScriptFileAsset('node_modules/slick-carousel/slick/slick.min.js');
		
		$this->addCssFileAsset('views/press.min.css');
		$this->addScriptFileAsset('views/press.min.js');
    }
} 