<?php
use ZMVC\Store\Helpers\ZStoreProductsHelper;
class SearchProductsAction extends \ZMVC\Store\Actions\ProductsAction {

    public function run()
    {
        $this->prepareAssets();

        $brandSiteUrl = 'http://www.kayunger.com';
        $categoryId = $_REQUEST['categoryId'];

        $catprodprops = ZStoreProductsHelper::getCategoryProductProperties($categoryId);
        $pageagent = $this->getController()->getLegacyStoreHelper()->getPageAgent($_SESSION['pageid'], $_SESSION['storeid']);
        $sharedAgentId = $sharedAppId = (isset($pageagent['AgentID'])) ? $pageagent['AgentID'] : $_SESSION['TPAID'];
        $content = $this->getController()->render(
            'products.twig', array(
                'brandSiteUrl'  => $brandSiteUrl,
                'sharedAgentId' => $sharedAgentId,
                'sharedAppId'   => $sharedAppId,
                'categoryId'    => $categoryId,
                'catProdProps'  => $catprodprops,
                'searchword'    => $_REQUEST['searchtext']
            )
        );
        echo $content;

    }
}