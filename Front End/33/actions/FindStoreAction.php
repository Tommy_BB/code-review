<?php
    use ZMVC\ZRegistry;

    /**
     * Class DynamicAction
     */
    class FindStoreAction extends \ZMVC\Store\Actions\DynamicAction
    {
        public $title = 'Find a Store';
        public $pageTitle = 'Find a Store';


	    public function run() {
	        $this->prepareAssets();
	
	        echo $this->getController()->render('findstore.twig');
	    }
	
	    private function prepareAssets() {
	    	
	        $this->addCssFileAsset('views/findstore.min.css');
	        $this->addScriptFileAsset('utilities/equalEvenHeight.min.js');
	        
    }
	}