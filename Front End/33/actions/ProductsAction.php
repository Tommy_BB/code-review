<?php
use ZMVC\Store\Helpers\ZStoreProductsHelper;
class ProductsAction extends \ZMVC\Store\Actions\ProductsAction {

    public function run()
    {
            $this->prepareAssets();

            $brandSiteUrl = 'http://stores.zindigo.com/templates';
            $categoryId = $_REQUEST['categoryId'];
                
			$catprodprops = ZStoreProductsHelper::getCategoryProductProperties($categoryId);
            $pageagent = $this->getController()->getLegacyStoreHelper()->getPageAgent($_SESSION['pageid'], $_SESSION['storeid']);
            $sharedAgentId = $sharedAppId = (isset($pageagent['AgentID'])) ? $pageagent['AgentID'] : $_SESSION['TPAID'];
            $content = $this->getController()->render(
                'products.twig', array(
                    'brandSiteUrl'  => $brandSiteUrl,
                    'sharedAgentId' => $sharedAgentId,
                    'sharedAppId'   => $sharedAppId,
                    'categoryId'    => $categoryId,
                    'catProdProps'  => $catprodprops,
                    'ostoreid'      => (($_REQUEST['categoryId'] == 2097) ?  84 : (($_REQUEST['categoryId'] == 2096) ? 132: "")),
                )
            );
            echo $content;

    }
} 