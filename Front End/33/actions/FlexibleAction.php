<?php

class FlexibleAction extends \ZMVC\ZAction {
	public $title = 'Behind The Scenes';
    public $pageTitle = 'Behind The Scenes';
    public function run() {
        $this->prepareAssets();

        echo $this->getController()->render('flexible.twig');
    }

    private function prepareAssets() {
    	
        $this->addCssFileAsset('views/flexible.min.css');
        $this->addScriptFileAsset('views/flexible.min.js');
        $this->addScriptFileAsset('utilities/equalEvenHeight.min.js');
        
    }
} 