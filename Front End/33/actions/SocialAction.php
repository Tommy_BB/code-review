<?php
	class SocialAction extends \ZMVC\Store\Actions\SocialAction {
        public $title = 'Social';
        public $pageTitle = 'Social';

        public function run()
        {
            $this->prepareAssets();

            $content = $this->getController()->render(
                'social.twig', array()
            );

            echo $content;
        }

        protected function prepareAssets()
        {
            $storeAssetRoot = '../../../..';
            $this->addCssFileAsset($storeAssetRoot . '/css/social_stream/dcsns_wall.css');
            $this->addScriptFileAsset($storeAssetRoot . '/js/social_stream/jquery.social.stream.wall.1.6.js');
            $this->addScriptFileAsset($storeAssetRoot . '/js/social_stream/jquery.social.stream.1.5.13.js');
            $this->addScriptFileAsset('views/social.min.js');
            $this->addCssFileAsset('bower_components/fontawesome/css/font-awesome.css');
            $this->addCssFileAsset('views/social.min.css');
        }
    }
