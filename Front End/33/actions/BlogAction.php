<?php
    use ZMVC\ZAction;

    class BlogAction extends ZAction
    {
        public $title = 'Blog';
        public $pageTitle = 'Blog';
        public $blog_title = '';
        public $blog_desc = '';
        public $blog_image = '';
        public $blog_url = '';

        public function run()
        {
            $this->prepareAssets();

            Feed::$cacheDir = __DIR__ . '/../rss_cache';
            $feed           = Feed::loadRss('http://174.120.254.198/blog/rss');
            //$feed           = Feed::loadRss('http://blogs.zindigo.com/kayunger/feed/');

            $stories = $this->convertFeedToArray($feed);

            $blogurl=(!empty($_GET['blogurl']))?urldecode($_GET['blogurl']):'';
            $url=(!empty($_GET['url']))?urldecode($_GET['url']):'http://blogs.zindigo.com/kayunger/';
            $this->blog_title=(!empty($_GET['blogtitle']))?str_replace('&#8230;', '...', html_entity_decode(urldecode($_GET['blogtitle']))):'';
            $this->blog_desc=(!empty($_GET['blogdesc']))?html_entity_decode(urldecode($_GET['blogdesc'])):$this->blog_title;
            $this->blog_image=(!empty($_GET['blogimage']))?html_entity_decode(urldecode($_GET['blogimage'])):'http://www.kayunger.com/templates/33/images/nav_kayunger_logo_retina.png';
            $this->blog_url='aaaa';
            $backurl=$this->getBackUrl();

            $content = $this->getController()->render(
                'blog.twig', array(
                    'feed'    => $feed,
                    'stories' => $stories,
                    'blogurl' => $blogurl,
                    'backurl' => $backurl,
                    'url' => $url,
                )
            );

            echo $content;
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/blog.min.css');
            $this->addScriptFileAsset('views/blog.min.js');
        }


        /**
        * @return string
        */
        private function getBackUrl()
        {
            
        }

        /**
         * @param Feed $feed
         *
         * @return array
         */
        private function convertFeedToArray(Feed $feed)
        {
            $stories = array();

            foreach ($feed->item as $item) {
                $image = null;

                $encodedContent = $item->{'content:encoded'};
                $encodedContent = str_replace(array('kayunger.com', 'www.kayunger.com'), '174.120.254.198', $encodedContent);

                preg_match_all('/<img[^>]+>/i', $encodedContent, $images);
                $rawContent = preg_replace('/<img[^>]+>/i', '', $encodedContent);
                $rawContent = preg_replace('/<p>&nbsp;<\/p>/i', '', $rawContent);

                if (!empty($images[0][0])) {
                    $image = $images[0][0];
                }

                array_push(
                    $stories, array(
                        'title'      => $item->title,
                        'link'       => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"."&blogurl=".urlencode($item->link),
                        'source'     => $item->link,
                        'image'      => $image,
                        'comments'   => $item->comments,
                        'pubDate'    => $item->pubDate,
                        'creator'    => $item->{'dc:creator'},
                        'rawContent' => $rawContent,
                        'content'    => html_entity_decode(strip_tags($item->{'content:encoded'})),
                    )
                );
            }

            return $stories;
        }
    }