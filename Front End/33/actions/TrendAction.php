<?php

class TrendAction extends \ZMVC\ZAction {

    public function run() {
        $this->prepareAssets();
        $this->title = 'Trends';

        echo $this->getController()->render('trend.twig');
    }

    private function prepareAssets() {
    	
        $this->addCssFileAsset('views/trend.min.css');
        $this->addScriptFileAsset('utilities/equalEvenHeight.min.js');
        
    }
} 