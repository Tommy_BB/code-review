<?php
use ZMVC\ZRegistry;
use ZMVC\Store\Helpers\ZStoreProductsHelper;
class StoreProductsAction extends \ZMVC\Store\Actions\ProductsAction {

    public function run()
    {
        $this->prepareAssets();

        $brandSiteUrl = 'http://stores.zindigo.com/templates';
        $categoryIds = $_REQUEST['categoryIds'];
        $action = $_REQUEST['action'];
        $keywords = $_REQUEST['keywords'];
        $catprodprops = ZStoreProductsHelper::getCategoryProductProperties($categoryId);
        // var_dump($catprodprops);die();
        $pageagent = $this->getController()->getLegacyStoreHelper()->getPageAgent($_SESSION['pageid'], $_SESSION['storeid']);
        $sharedAgentId = $sharedAppId = (isset($pageagent['AgentID'])) ? $pageagent['AgentID'] : $_SESSION['TPAID'];
        $ostoreid="";
        $ostoreids=array();

        // $featuredCategories=ZRegistry::getInstance()->get('featuredCategories');
        $categories=ZRegistry::getInstance()->get('categories');
        // $categories=array_replace($featuredCategories,$categories);
        $category=$categories[$categoryId];

        if (array_key_exists($_REQUEST['categoryId'], $ostoreids)) { $ostoreid = $ostoreids[$_REQUEST['categoryId']]; }
        $content = $this->getController()->render(
            'storeproducts.twig', array(
                'brandSiteUrl'  => $brandSiteUrl,
                'sharedAgentId' => $sharedAgentId,
                'sharedAppId'   => $sharedAppId,
                'categoryIds'    => $categoryIds,
                'action'        => $action,
                'catProdProps'  => $catprodprops,
                'ostoreid'      => $ostoreid,
                'keywords'      => $keywords,
                'categoryDescription' => (!empty($category['Description']))?$category['Description']:'',
                // 'categoryDescription' => (!empty($category['Description']))?$category['Description']:'Introducing spring 2016',
                'categoryBgImage' => (!empty($category['ShareImage']))?'images/uploaded/images/'.$category['ShareImage'].'.jpg':'',
                'categorySmallDescription' => (!empty($category['ShareDescription']))?$category['ShareDescription']:'',

            )
        );

        echo $content;

    }
}