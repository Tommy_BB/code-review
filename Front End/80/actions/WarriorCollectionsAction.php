<?php

use ZMVC\ZRegistry;
use ZMVC\Store\Helpers\ZStoreProductsHelper;

class WarriorCollectionsAction extends \ZMVC\ZAction
{
    public $title = 'Collections';
    public $pageTitle = 'Collections';

    public function run()
    {
        $this->prepareAssets();
        $brandSiteUrl = 'http://stores.zindigo.com/templates';

        $pageagent = $this->getController()->getLegacyStoreHelper()->getPageAgent($_SESSION['pageid'], $_SESSION['storeid']);
        $sharedAgentId = $sharedAppId = (isset($pageagent['AgentID'])) ? $pageagent['AgentID'] : $_SESSION['TPAID'];
        $categoryId = $_REQUEST['categoryId'];

        $collections = $this->getStoreProductCategories($categoryId);

        $categories = ZRegistry::getInstance()->get('wwcategories');
        $category = $categories[$categoryId];

        $iresult = $this->fetchData("https://api.instagram.com/v1/users/self/media/recent?count=10&access_token=2236026172.3b4f146.7b9ed21354af40719a697036ac16e5d3");
        $iresult = json_decode($iresult);
        $igPosts = array();

        $posts = array_slice($iresult->data, 0, 6);
        foreach ($posts as $post) {
            $igPosts[] = array("image" => $post->images->standard_resolution->url);
        }


        $content = $this->getController()->render(
            'warriorcollections.twig', array(
                'brandSiteUrl' => $brandSiteUrl,
                'sharedAgentId' => $sharedAgentId,
                'sharedAppId' => $sharedAppId,
                'categoryId' => $categoryId,
                'collections' => $collections,
                'categoryDescription' => (!empty($category['Description'])) ? $category['Description'] : '',
                'categoryImage' => $category['Images']['ImagePath'],
                'posts' => $igPosts,
            )
        );
        echo $content;
    }

    private function prepareAssets()
    {

        $this->addCssFileAsset('../../defaultshare.css');
        $this->addCssFileAsset('views/products.min.css');
        $this->addScriptFileAsset('utilities/jsrender.min.js');
        $this->addScriptFileAsset('utilities/zindigo-products.min.js');
        $this->addScriptFileAsset('utilities/defaultshare.min.js');
        $this->addScriptFileAsset('utilities/readmore.min.js');
        $this->addCssFileAsset('bower_components/fontawesome/css/font-awesome.css');
        $this->addScriptFileAsset('views/products.min.js');
        $this->addCssFileAsset('views/collections.min.css');

    }

    private final function fetchData($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    private function getStoreProductCategories($cat)
    {
        $productCategories = ZStoreProductsHelper::getProductCategoryTreeByParentId(149, $cat);
        // $productCategories = ZStoreProductsHelper::getProductCategoryTreeByParentId(114, 1734); //Test
        return $productCategories;
    }
}
