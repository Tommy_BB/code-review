<?php
    use ZMVC\ZRegistry;

    /**
     * Class DynamicAction
     */
    class RepsAction extends \ZMVC\Store\Actions\DynamicAction
    {
        public $title = 'Find a Store';
        public $pageTitle = 'Find a Store';


	    public function run() {
	        $this->prepareAssets();
	
	        echo $this->getController()->render('reps.twig');
	    }
	
	    private function prepareAssets() {
	    	
	        $this->addCssFileAsset('views/reps.min.css');
		}
	}