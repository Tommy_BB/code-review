<?php
use ZMVC\Store\Helpers\ZCacheHelper;

class HomeAction extends \ZMVC\Store\Actions\HomeAction {

    public $title = 'Home';
    public $pageTitle = 'Home';

    public function run() {
        $this->prepareAssets();

        //http://stellavalle.com/#access_token=22145322.3b4f146.9874ae68d32042cdabfda681503a65d1

        $instagram = ZCacheHelper::getCache("homePageInstagram");
        if($instagram == "")
        {
            $iresult = $this->fetchData("https://api.instagram.com/v1/users/self/media/recent?count=10&access_token=345514936.4ebf0d2.d0290863a75a46398753e52cead64ed0");
            if($iresult != "") {
                $iresult = json_decode($iresult);
                $igPosts = array();

                $posts = array_slice($iresult->data, 0, 6);
                foreach ($posts as $post) {
                    $igPosts[] = array("image" => $post->images->standard_resolution->url);
                }
                ZCacheHelper::setCache("homePageInstagram",$iresult);
            }
        } else {
            $igPosts = array();
            $posts = array_slice($instagram->data, 0, 6);
            foreach ($posts as $post) {
                $igPosts[] = array("image" => $post->images->standard_resolution->url);
            }
        }


        echo $this->getController()->render(
            'home.twig', array(
                'posts' => $igPosts
            )
        );
    }

    public function prepareAssets() {
        $this->addCssFileAsset('views/home.min.css');
    }

    private final function fetchData($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}