<?php
    use ZMVC\ZRegistry;

    /**
     * Class DynamicAction
     */
    class FindStoreAction extends \ZMVC\Store\Actions\DynamicAction
    {
        public $title = 'Find a Store';
        public $pageTitle = 'Find a Store';

        /**
         * @return mixed|void
         */
        public function run()
        {
            $this->prepareAssets();

            $stores = $this->getStores();
            $states = $this->getStates();
            $countries = $this->getCountries();

            $content = $this->getController()->render(
                'findstore.twig', array(
                    'storesstate' => $stores,
                    'states' => $states,
                    'parsedstates' => $this->parseStates($states),
                    'countries' => $countries,
                )
            );

            echo $content;
        }

        /**
         *
         */
        private function prepareAssets()
        {
            $this->addCssFileAsset('views/findstore.min.css');
            $this->addScriptFileAsset('views/findstore.min.js');
        }

        /**
         * @return array
         * @throws Exception
         */
        private final function getStores($countryCode="code")
        {
            $storeLookupQry = "SELECT *
                FROM StoreLocations sl
                JOIN Countries c ON c.Country_Code = sl.Country
                WHERE sl.StoreID = :storeId
                ORDER BY c.Country ASC";

            $dbHandler = ZRegistry::getInstance()->get('db');
            try {
                $storesStmt = $dbHandler->prepare($storeLookupQry);
                $stores = array();
                if (true === $storesStmt->execute(array(':storeId' => $this->getController()->getStoreId()))) {
                // if (true === $storesStmt->execute(array(':storeId' => 149))) {
                    foreach ($storesStmt->fetchAll(PDO::FETCH_ASSOC) as $storeRow) {
                        array_push($stores, $storeRow);
                    }

                    $stores = $this->reorderByStates($stores);
                    return $stores;
                } else {
                    return false;
                }
            } catch (\PDOException $pErr) {
                throw new \Exception($pErr->getMessage());
            }
        }

        /**
         * @return array
         */
        private final function getCountries() {
            $legacyStoreCountriesResult = $this->getController()->getLegacyStoreHelper()->getStoreLocationCountries($this->getController()->getStoreId());
            // $legacyStoreCountriesResult = $this->getController()->getLegacyStoreHelper()->getStoreLocationCountries(149);
            $legacyCountriesResult = $this->getController()->getLegacyStoreHelper()->getCountries();

            if ($legacyCountriesResult) {
                while ($countryRow = mysqli_fetch_assoc($legacyCountriesResult)) {
                    $countries[$countryRow['Country_Code']] = array_change_key_case($countryRow);
                }
            }

            $storeCountries = array();
            if ($legacyStoreCountriesResult) {
                while ($countryRow = mysqli_fetch_assoc($legacyStoreCountriesResult)) {
                    if (isset($countries[$countryRow['Country']])) {
                        array_push($storeCountries, $countries[$countryRow['Country']]);
                    }
                }
            }

            return $storeCountries;
        }

        /**
         * @return mixed
         */
        private final function getStates()
        {
            $legacyStoreStatesResult = $this->getController()->getLegacyStoreHelper()->getStoreLocationStates(
                $this->getController()->getStoreId(), null
                // 149, null
            );

            $states = array();
            if ($legacyStoreStatesResult) {
                while ($stateRow = mysqli_fetch_assoc($legacyStoreStatesResult)) {
                    array_push($states, array_change_key_case($stateRow));
                }
            }

            return $states;
        }

        private function reorderByStates($stores){
            $results=array();
            foreach ($stores as $store) {
                if (empty($results[$store["state"]])){
                    $results[$store["state"]]=array();
                }
                $results[$store["state"]][]=$store;
            }
            return $results;
        }

        private function parseStates($states){
            $results=array();
            foreach ($states as $state) {
                $results[$state['state']]=$state['statename'];
            }
            return $results;
        }
    }