<?php

use ZMVC\ZRegistry;
use ZMVC\Store\Helpers\ZStoreProductsHelper;

class CollectionsAction extends \ZMVC\ZAction {
	public $title = 'Collections';
    public $pageTitle = 'Collections';
    public function run() {
        $this->prepareAssets();
        $brandSiteUrl  = 'http://stores.zindigo.com/templates';

        $pageagent = $this->getController()->getLegacyStoreHelper()->getPageAgent($_SESSION['pageid'], $_SESSION['storeid']);
        $sharedAgentId = $sharedAppId = (isset($pageagent['AgentID'])) ? $pageagent['AgentID'] : $_SESSION['TPAID'];
        $categoryId = $_REQUEST['categoryId'];

        $collections = $this->getStoreProductCategories($categoryId);

        $categories=ZRegistry::getInstance()->get('svcategories');
        $category=$categories[$categoryId];

        $content = $this->getController()->render(
            'collections.twig', array(
                'brandSiteUrl'  => $brandSiteUrl,
                'sharedAgentId' => $sharedAgentId,
                'sharedAppId'   => $sharedAppId,
                'categoryId'    => $categoryId,
                'collections'      => $collections,
                'categoryDescription' => (!empty($category['Description']))?$category['Description']:'',
            )
        );
        echo $content;
    }

    private function prepareAssets() {
    	
        $this->addCssFileAsset('../../defaultshare.css');
        $this->addCssFileAsset('views/products.min.css');
        $this->addScriptFileAsset('utilities/jsrender.min.js');
        $this->addScriptFileAsset('utilities/zindigo-products.min.js');
        $this->addScriptFileAsset('utilities/defaultshare.min.js');
        $this->addScriptFileAsset('utilities/readmore.min.js');
       	$this->addCssFileAsset('bower_components/fontawesome/css/font-awesome.css');

        $this->addScriptFileAsset('views/products.min.js');
        
        $this->addCssFileAsset('views/collections.min.css');
        
    }

    private function getStoreProductCategories($cat){
        $productCategories = ZStoreProductsHelper::getProductCategoryTreeByParentId(149, $cat);
        // $productCategories = ZStoreProductsHelper::getProductCategoryTreeByParentId(114, 1734); //Test
        return $productCategories;
    }
} 
