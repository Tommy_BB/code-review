<?php
use ZMVC\ZRegistry;

class WarriorStoryAction extends \ZMVC\Store\Actions\DynamicAction {
    public $title = 'Story';
    public $pageTitle = 'Story';

    public function run()
    {
        $this->prepareAssets();

        $storyID = (int)$_REQUEST['storyid'];


        $dbHandler = ZRegistry::getInstance()->get('db');

        $dynamicFetchQry = "Call GetDynamicContent({$this->getController()->getStoreId()}, {$storyID});";
        $dynamicStmt = $dbHandler->query($dynamicFetchQry);
        if (!($dynamicRow = $dynamicStmt->fetch(PDO::FETCH_ASSOC))) {
            throw new \Exception('Could not fetch content');
        }

        if($this->getController()->getStoreId() == (int)$dynamicRow['storeid'])
            $design = $dynamicRow['content'];

        $iresult = $this->fetchData("https://api.instagram.com/v1/users/self/media/recent?count=10&access_token=2236026172.3b4f146.7b9ed21354af40719a697036ac16e5d3");
        $iresult = json_decode($iresult);
        $igPosts = array();

        $posts = array_slice($iresult->data, 0,6);
        foreach($posts as $post)
        {
            $igPosts[] = array("image" => $post->images->standard_resolution->url);
        }


        $content = $this->getController()->render(
            'warriorstory.twig', array(
                'design' => $design,
                'posts' => $igPosts
            )
        );

        echo $content;
    }

    protected function prepareAssets()
    {
        $this->addCssFileAsset('views/story.min.css');
        $this->addScriptFileAsset('views/wwstory.min.js');
    }

    private final function fetchData($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
