<?php
    use ZMVC\ZRegistry;

    /**
     * Class DynamicAction
     */
    class TermsAction extends \ZMVC\Store\Actions\DynamicAction
    {
        public $title = 'Terms';
        public $pageTitle = 'Terms';

        /**
         * @return mixed|void
         */
        public function run()
        {
        	$this->prepareAssets();
            $cmsContent = $this->fetchDynamicContent(415);

            $content = $this->getController()->render(
                'dynamic.twig', array(
                    'content' => $cmsContent,
                )
            );

            echo $content;
        }
        
        protected function prepareAssets()
        {
        	$this->addCssFileAsset('views/privacy.min.css');
        }
    }