<?php
class VideoAction extends \ZMVC\Store\Actions\SocialAction {
    public $title = 'Social';
    public $pageTitle = 'Social';

    public function run()
    {
        $this->prepareAssets();
        $section = $_REQUEST['section'];
        $content = $this->getController()->render(
            'video.twig', array(
               'section' => $section
            )
        );

        echo $content;
    }

    protected function prepareAssets()
    {
        $this->addScriptFileAsset('views/video.min.js');
        $this->addCssFileAsset('views/video.min.css');
    }
}
