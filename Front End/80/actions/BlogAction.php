<?php
    use ZMVC\ZAction;

    class BlogAction extends ZAction
    {
        public $title = 'Blog';
        public $pageTitle = 'Blog';
        public $blog_title = '';
        public $blog_desc = '';
        public $blog_image = '';

        public function run()
        {

            $this->prepareAssets();
            $blog = file_get_contents("http://stellavalleblog.wordpress.com/feed/");
            $xml = simplexml_load_string($blog,"SimpleXMLElement", LIBXML_NOCDATA);

            $endBlog = array();

            $entries = $xml->entry ? $xml->entry : $xml->channel->item;
            $domain = "stellavalleblog";

            foreach($entries as $entry)
            {
			$blogImages = array();
            $date = new DateTime($entry->pubDate);
            foreach ( $entry->children('media',true)->content as $media ) {
                $url =  $media->attributes()->url;
                if(strrpos($url,$domain) === false && strrpos($url,"stellavalle") === false)
                    $url = "";
				if($url != "" && !in_array(((string)$url),blogImages))
					$blogImages[] = (string)$url;
            }

                $link = $entry->link;
                $entry = (array)$entry;

                $entry['date'] = $date->format('M') . " " . $date->format('d');
                $entry['url'] = $blogImages[0];
                $entry['niceurl'] = $this->getNiceURL($link);

                $endBlog[] = (array)$entry;
            }

            $content = $this->getController()->render(
                'blog.twig', array(
                    'blogArticles' => $endBlog
                )
            );

            echo $content;
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/blog.min.css');
            $this->addScriptFileAsset('views/blog.min.js');
        }

        private function getNiceURL($url){

            $str = explode("/",$url);
            return $str[count($str)-2];

        }
    }
