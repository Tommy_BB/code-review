<?php
    use ZMVC\Store\Helpers\ZStoreProductsHelper;
    use ZMVC\ZAction;
    use ZMVC\ZRegistry;

    /**
     * Class ProductAction
     */
    class ProductAction extends ZAction
    {
        /**
         * @var string
         */
        private $_recentlyViewedSessionKey = 'recently-viewed';

        /**
         * @var int the maximum number of products stored
         */
        private $_recentlyViewedSessionLimit = 4;

        /**
         * @return mixed|void
         * @throws Exception
         */
        public function run()
        {
            $this->prepareAssets();

            if (isset($_GET['prodid'])) {
                $productId = $_GET['prodid'];
            } else {
                throw new \Exception('Could not find this product.');
            }

            $currentCategory = isset($_REQUEST['categoryId']) ? $_REQUEST['categoryId'] : 0;


            // Update the user's recently viewed product list
            $this->updateRecentlyViewed($productId);


            // var_dump($outofStock);die();

            // Retrieve the product details
            $storeId        = $this->getController()->getStoreId();
            // if ($productId==16222){
            //     $storeId=114;
            // }
            $ostoreid = "";
            if(isset($_REQUEST['ostoreid']) && $_REQUEST['ostoreid'] != ""){
                $storeId = $_REQUEST['ostoreid'];
                $ostoreid = $_REQUEST['ostoreid'];
            }
            $productDetails = ZStoreProductsHelper::getProductDetails($storeId, $productId);
            if (empty($productDetails)) {
                throw new \Exception('Could not find product details.');
            }

            $agentid = $sharedAppId = (isset($pageagent['AgentID'])) ? $pageagent['AgentID'] : $_SESSION['TPAID'];
            $dbHandler = ZRegistry::getInstance()->get('db');
            $agents = $dbHandler->query("SELECT a.FBPageName,a.FBPageID,a.UniqueName, s.FBAPPID, s.ID FROM Agents a JOIN Stores s ON s.ID = a.StoreID WHERE a.AgentID = ".$agentid);
            if($agents) {
                $myagent = $agents->fetch(PDO::FETCH_ASSOC);
                $uniquename = $myagent['uniquename'];
            }
            $papid = $dbHandler->query("SELECT PapUserID FROM zshops.Agents WHERE AgentID='".$agentid."'");
            if($papid) {
                $papids = $papid->fetch(PDO::FETCH_ASSOC);
                $papuserid = $papids["papuserid"];
            } else {
                $papuserid = '';
            }


            // Sets the page title based on the product
            $this->setTitles($productDetails);

            // Recently Viewed products

            if($currentCategory > 0) {
                $youMayAlsoLikeProducts = ZStoreProductsHelper::getRandomStoreProductsbyCategory(
                    $storeId, $currentCategory, $numberOfProducts = 4
                );
            } else {
                $youMayAlsoLikeProducts = ZStoreProductsHelper::getYouMayAlsoLikeProducts(
                    $storeId, $productId, $numberOfProducts = 4
                );
            }


            // Social
            $facebookUrl = $this->getFacebookSocialUrl($productId);

            // Prev/Next product
            $prevNextProduct = $this->getPrevNextProduct($productId);

            // Determine the prev/next product
            $previousProductId = $nextProductId = null;
            if (!empty($prevNextProduct['prev'])) {
                $previousProductId = $prevNextProduct['prev']['productid'];
            }

            if (!empty($prevNextProduct['next'])) {
                $nextProductId = $prevNextProduct['next']['productid'];
            }

            // Social
            $facebookUrl  = $this->getFacebookSocialUrl($productDetails);
            $twitterUrl   = $this->getTwitterSocialUrl($productDetails);
            $pinterestUrl = $this->getPinterestSocialUrl($productDetails);

            $socialUrls = array(
                'facebook'  => $facebookUrl,
                'twitter'   => $twitterUrl,
                'pinterest' => $pinterestUrl,
                'producturl' => $this->getProductUrl(),
            );

            $storename = ZStoreProductsHelper::getStoreName($storeId);
            
            // This allows us to access the product details from the master layout
            ZRegistry::getInstance()->add('product', $productDetails);
            ZRegistry::getInstance()->add('storename', $storename);

            \ZMVC\ZRegistry::getInstance()->add('product', $productDetails);
            \ZMVC\ZRegistry::getInstance()->add('storename', $storename);

            $storeendArray = $storeArray = array();
            $storeURL = basename($_SERVER['QUERY_STRING']);
            parse_str($storeURL, $storeArray);

            foreach($storeArray as $key => $val)
            {
                if($key == "sess" || $key == "store80" || $key == "store81") continue;
                $storeendArray[$key] = $val;
            }

            $content = $this->getController()->render(
                'product.twig', array(
                    'socialUrls'             => $socialUrls,
                    'product'                => $productDetails,
                    'storename'              => $storename,
                    'uniquename'             => $uniquename,
                    'papuserid'              => $papuserid,
                    'previousProductId'      => $previousProductId,
                    'nextProductId'          => $nextProductId,
                    'youMayAlsoLikeProducts' => $youMayAlsoLikeProducts,
                    'facebookUrl'            => $facebookUrl,
                    'ostoreid'               => $ostoreid,
                    'baseURL'                => "http://stellavalle.com/index.php?".http_build_query($storeendArray)
                )
            );

            echo $content;
        }

        private function prepareAssets()
        {
            $this->addCssFileAsset('views/product.min.css');
            $this->addScriptFileAsset('views/product.min.js');
            $this->addScriptFileAsset('utilities/cloud-zoom.min.js');
        }

        /**
         * @param $productId
         */
        private function updateRecentlyViewed($productId)
        {
            if (!is_array($_SESSION[$this->_recentlyViewedSessionKey])) {
                $_SESSION[$this->_recentlyViewedSessionKey] = array();
            }
            $recentlyViewedSession =& $_SESSION[$this->_recentlyViewedSessionKey];

            if (!in_array($productId, $recentlyViewedSession)) {
                array_unshift($recentlyViewedSession, $productId);
            }

            if ((count($recentlyViewedSession)) > $this->_recentlyViewedSessionLimit) {
                array_pop($recentlyViewedSession);
            }
        }

        /**
         * @param $productDetails
         */
        private function setTitles($productDetails)
        {
            $this->title = $this->pageTitle = $productDetails['details']['Name'];
        }

        /**
         * @return array|bool
         */
        private function getRecentlyViewedProducts()
        {
            if (!isset($_SESSION[$this->_recentlyViewedSessionKey])) {
                return false;
            }

            $recentlyViewedSession = $_SESSION[$this->_recentlyViewedSessionKey];

            $recentlyViewedProducts = array();
            foreach ($recentlyViewedSession as $productId) {
                $product = ZStoreProductsHelper::getProductDetails($this->getController()->getStoreId(), $productId);
                array_push($recentlyViewedProducts, $product);
            }

            return $recentlyViewedProducts;
        }

        /**
         * @param $productDetails
         *
         * @return string
         */
        private function getFacebookSocialUrl($productDetails)
        {
            $facebookUrlTpl = "http://www.facebook.com/sharer/sharer.php?u={url}";

            return strtr(
                $facebookUrlTpl, array(
                    '{url}' => urlencode($this->getProductUrl()),
                )
            );
        }

        /**
         * @return string
         */
        public function getProductUrl()
        {
            return $this->getBaseProductUrl() . $_SERVER['REQUEST_URI'];
        }

        /**
         * @return string
         */
        private function getBaseProductUrl()
        {
            $isSecure = (strpos($_SERVER['SERVER_PROTOCOL'], 'HTTPS') !== false);
            $protocol = ($isSecure) ? 'https' : 'http';
            $httpHost = str_replace('local', 'com', $_SERVER['HTTP_HOST']);

            return "{$protocol}://{$httpHost}";
        }

        /**
         * @param $product
         *
         * @return string
         */
        protected function getTwitterSocialUrl($productDetails)
        {
            $twitterSocialUrlTpl = "http://www.twitter.com/intent/tweet?url={url}&text={productName}";
            $twitterSocialUrl    = strtr(
                $twitterSocialUrlTpl, array(
                    '{url}'         => urlencode($this->getProductUrl()),
                    '{productName}' => urlencode($productDetails['details']['Name']),
                )
            );

            return $twitterSocialUrl;
        }

        /**
         * @param $productDetails
         *
         * @return string
         */
        private function getPinterestSocialUrl($productDetails)
        {
            $pinterestUrlTpl = "http://pinterest.com/pin/create/button/?url={url}&media={media}&description={description}";

            $pinterestUrl = strtr(
                $pinterestUrlTpl, array(
                    '{url}'         => urlencode($this->getProductUrl()),
                    '{media}'       => urlencode($this->getBaseProductUrl() . '/' . $productDetails['images']['LargePath']),
                    '{description}' => urlencode(strip_tags($productDetails['details']['Name'])),
                )
            );

            return $pinterestUrl;
        }

        private function getPrevNextProduct($currentProductId)
        {

            $storeCategories = ZRegistry::getInstance()->get('productCategories');
            if (empty($storeCategories)) {
                return false;
            }

            $storeCategoryIds = array();
            foreach ($storeCategories as $storeCategory) {
                array_push($storeCategoryIds, (int)$storeCategory['id']);
            }

            $minMaxQry = "SELECT ProductID FROM StoresProducts WHERE StoreCategoryID IN (:categoryIds)";

            /** @var \PDO $dbHandler */
            $dbHandler = ZRegistry::getInstance()->get('db');
            try {
                $stmt = $dbHandler->prepare($minMaxQry);
                $stmt->execute(
                    array(
                        ':categoryIds' => implode(',', $storeCategoryIds)
                    )
                );

                $nextProduct = $prevProduct = null;
                $products    = $stmt->fetchAll();
                foreach ($products as $productIdx => $productRow) {
                    $productId = $productRow['productid'];
                    if ($productId == $currentProductId) {
                        $previousProductId = ($productIdx - 1);
                        $nextProductId     = ($productIdx + 1);

                        if (isset($products[$previousProductId])) {
                            $prevProduct = $products[$previousProductId];
                        }

                        if (isset($products[$nextProductId])) {
                            $nextProduct = $products[$nextProductId];
                        }
                    }
                }

                return array(
                    'prev' => $prevProduct,
                    'next' => $nextProduct,
                );
            } catch (\PDOException $pErr) {
                throw new Exception($pErr->getMessage());
            }
        }

        private function outOfStockOptions($productId)
        {
            $products  = ZStoreProductsHelper::getInstance()->products;
            $dbHandler = ZRegistry::getInstance()->get('db');
            $propertiesResult = $products->getProductProperties( $productId );
            $hasProperties    = ( mysqli_num_rows( $propertiesResult ) > 0 );
    
            // Get product properties
            $properties = array();
            if ( $hasProperties ) {
                while ( $propertyData = mysqli_fetch_assoc( $propertiesResult ) ) {
                    $propertyId = $propertyData['ID'];
                    $propertyName = strtolower( $propertyData['Name'] );
                    $properties[ $propertyName ] = $propertyData;
    
                    // Get all unavailable property values for the given property
                    $unavailablePropertyValues = $products->getUnavailableValues($propertyId, $productId);
                    if ( $unavailablePropertyValues ) {
                        foreach ( $unavailablePropertyValues as $propertyValue ) {
                            $valueName = strtolower($propertyValue['Name']);
                            $properties[ $propertyName ]['options'][ $valueName ] = $propertyValue;
                        }
                    }
                }
            }
            
            //no need to keep going we have all sizes in stock
            if(!isset($properties['size']['options']))
            {
                return array();
            }
            
            // Get product inventory
            $inventoryResult = $products->getProductInventory( $productId );
            $hasInventory    = ( mysqli_num_rows( $inventoryResult ) > 0 );
    
            $inventory = array();
            if ( $hasInventory ) {
                while ( $inventoryData = mysqli_fetch_assoc( $inventoryResult ) ) {
                    if ( ! empty( $properties ) ) {
                        foreach ( $properties as $propertyName => $propertyData ) {
                            $propertyOptions = $propertyData['options'];
                            if ( empty( $propertyOptions ) ) {
                                break;
                            }
    
                            // Number of valid value options
                            for ( $idx = 1; $idx <= 7; $idx ++ ) {
                                $valueName = "Value{$idx}";
                                foreach (
                                    $propertyOptions as $optionName => $optionData
                                ) {
                                    if ( $optionData['ID']
                                         == $inventoryData[ $valueName ]
                                    ) {
                                        $inventoryData["Value{$idx}Type"]
                                            = $propertyName;
                                    }
                                }
                            }
    
                        }
                    }
                    array_push( $inventory, $inventoryData );
                }
            }
            
            
            $agentid = "";
            $inzindigo = isset($_SESSION['shopid']) ? 1 : 0;
            $weburl = "";
            
            if(isset($_SESSION['pageid']) && $_SESSION['pageid'] != "")
            {
                $pageagent = $this->getController()->getLegacyStoreHelper()->getPageAgent($_SESSION['pageid'], $_SESSION['storeid']);
                $agentid = $sharedAppId = (isset($pageagent['AgentID'])) ? $pageagent['AgentID'] : $_SESSION['TPAID'];
            } else {
                $getWebAttr = $dbHandler->query("SELECT Web FROM Stores WHERE ID = '".$_SESSION['storeid']."'");
                $webRow = $getWebAttr->fetch(PDO::FETCH_ASSOC);
                
                //$weburl = "http://".$webRow['web'];
                $weburl = "http://".$_SERVER[HTTP_HOST];
            }
            

            
            
            //parse the url to send for oos params
            $cururl = "http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];
            $parsedUrl = parse_url($cururl);
            $parsedQuery = "";
            $removalQuery = array(session_name(),"sess","XDEBUG_PROFILE");
            if(isset($parsedUrl['query']) && $parsedUrl['query'] != "")
            {
                parse_str($parsedUrl['query'], $parsedQuery);
                foreach($removalQuery as $key=>$val)
                {
                    if(isset($parsedQuery[$val]))
                        unset($parsedQuery[$val]);
                }
            }
            
            $parsedQuery = $parsedUrl['path'] . "?". http_build_query($parsedQuery);
            
            $oosDetails = array(
                'properties'      => $properties,
                'inventory'       => $inventory,
                'agentID'         => $sharedAgentId,
                'zindigo'         => $inzindigo,
                'web'             => $weburl,
                'params'          => $parsedQuery
            );
            return $oosDetails;
        }
    }
