<?php
use ZMVC\ZAction;

class BlogArticleAction extends ZAction
{
    public $title = 'Blog Article';
    public $pageTitle = 'Blog Article';
    public $blog_title = '';
    public $blog_desc = '';
    public $blog_image = '';

    public function run()
    {

        $this->prepareAssets();
        $blog = file_get_contents("http://stellavalleblog.wordpress.com/feed/");
        $xml = simplexml_load_string($blog,"SimpleXMLElement", LIBXML_NOCDATA);

        $prevArticle = $nextArticle = $blogArticles = $currentArticle = array();

        $entries = $xml->entry ? $xml->entry : $xml->channel->item;
        $domain = "stellavalleblog";

        $totalArticles = (count($entries) - 1);
        $counter = 0;
        $currentArticleNum = null;

        $whitelistUrls = array(
            "stellavalle.com",
            "etsy.com",
            "rachelgeorge.com",
            "megbiram.com",
            "ikea.com",
            "jonathanadler.com",
            "bxxlght.com",
            "amazon.com",
            "inciteseating.com",
            "jessiedmiller.com",
            "goodhousekeeping.com"
        );

        foreach($entries as $entry)
        {
			$blogImages = array();
            $date = new DateTime($entry->pubDate);
            foreach ( $entry->children('media',true)->content as $media ) {
                $url =  $media->attributes()->url;

                //if($this->strposa((string)$url,$whitelistUrls) === false)
                    //$url = "";

                if(strrpos($url,$domain) === false && strrpos($url,"stellavalle") === false)
                    $url = "";

				if($url != "" && !in_array(((string)$url),$blogImages))
					$blogImages[] = (string)$url;
            }

            $currlink = $entry->link;
            $content = $entry->children('content',true)->encoded;

            $blogentry = (array)$entry;
            $blogentry['date'] = $date->format('M') . " " . $date->format('d');
			
			
            $blogentry['firstImage'] = $blogImages[0];
			$blogentry['secondImage'] = (isset($blogImages[1]) ? $blogImages[1] : $blogImages[0]);
		$blogentry['thirdImage'] = (isset($blogImages[2]) ? $blogImages[2] : null);
$blogentry['fourthImage'] = (isset($blogImages[3]) ? $blogImages[3] : null);	
            $blogentry['niceurl'] = $this->getNiceURL($currlink);
            $blogentry['content'] = strip_tags(html_entity_decode(((string)$content)),'<a>');


            $stringContent = $blogentry['content'];
            $doc = new DOMDocument();
            $doc->loadHTML((string)$stringContent);
            $anchors = $doc->getElementsByTagName('a');
            $len = $anchors->length;
            if ( $len > 0 ) {
                $i = $len-1;
                while ( $i > -1 ) {
                    $anchor = $anchors->item( $i );
                    if ( $anchor->hasAttribute('href') ) {
                        $href = $anchor->getAttribute('href');
                        if($this->strposa($href,$whitelistUrls) !== false)
                        {
                            $i--;
                            continue;
                        }
                        /*
                        if(strrpos($href,"stellavalle.com"))
                        {
                            $i--;
                            continue;
                        }
                        */

                        $fragment = $doc->createDocumentFragment();
                        while($anchor->childNodes->length > 0) {
                            $fragment->appendChild($anchor->childNodes->item(0));
                        }
                        $anchor->parentNode->insertBefore($fragment,$anchor);
                        $anchor->parentNode->removeChild($anchor);
                    }
                    $i--;
                }
            }



            $blogentry['content'] = (string)$doc->saveHTML();
		    //$words = preg_split('/\s+(?![^<>]+>)/m', $blogentry['content']);
            $blogentry['content'] = str_replace('&acirc;&#128;&#153;', "'", $blogentry['content']);
            $blogentry['content'] = str_replace('&Acirc;', "", $blogentry['content']);
            $blogentry['content'] = str_replace( "&acirc;&#128;&brvbar;","…", $blogentry['content']);


            $words = preg_split('/[\s]+/', $blogentry['content']);

            $firstHalf = $secondHalf = "";

            for($i= 0; $i < count($words); $i++)
            {
                if(str_word_count($firstHalf) <= (count($words) / 2))
                {
                    $firstHalf .= " " .$words[$i];
                } else {
                    $secondHalf .= " " .$words[$i];
                }
            }

            $blogentry['firstHalf'] = ltrim($firstHalf);
            $blogentry['secondHalf'] = rtrim($secondHalf);

            $blogArticles[$counter] = $blogentry;

            if ($currentArticleNum == null && strcmp($this->getNiceURL($currlink), $_REQUEST['article']) === 0) {
                $currentArticle = $blogentry;
                $currentArticleNum = $counter;
            }

            $counter++;

        }

        //prev = and next =
        if($currentArticleNum == $totalArticles)
        {
            $nextArticle = $blogArticles[0];
            $prevArticle = $blogArticles[$totalArticles-1];
        } else
        if($currentArticleNum == 0)
        {
                $nextArticle = $blogArticles[1];
                $prevArticle = $blogArticles[$totalArticles];
        }   else {
            $nextArticle = $blogArticles[$currentArticleNum + 1];
            $prevArticle = $blogArticles[$currentArticleNum - 1];
        }


        $content = $this->getController()->render(
            'blogarticle.twig', array(
                'currentArticle' => $currentArticle,
                'nextArticle' => $nextArticle,
                'prevArticle' => $prevArticle
            )
        );

        echo $content;
    }

    private function prepareAssets()
    {
        $this->addCssFileAsset('views/blog.min.css');
        $this->addScriptFileAsset('views/blog.min.js');
    }

    private function getNiceURL($url){

        $str = explode("/",$url);
        return $str[count($str)-2];

    }

    private function strposa($haystack, $needles=array(), $offset=0) {
        $chr = array();
        foreach($needles as $needle) {
            $res = strpos($haystack, $needle, $offset);
            if ($res !== false) $chr[$needle] = $res;
        }
        if(empty($chr)) return false;
        return min($chr);
    }
}
