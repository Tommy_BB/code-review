<?php
use ZMVC\ZRegistry;

class GalleryAction extends \ZMVC\Store\Actions\DynamicAction {
    public $title = 'Story';
    public $pageTitle = 'Story';

    public function run()
    {
        $this->prepareAssets();

        $iresult = $this->fetchData("https://api.instagram.com/v1/users/self/media/recent?count=20&access_token=2236026172.3b4f146.7b9ed21354af40719a697036ac16e5d3");
        $iresult = json_decode($iresult);
        $gallery = array();

        $posts = array_slice($iresult->data, 0,20);
        foreach($posts as $post)
        {
            $maxLength = 200;
            $igtext = substr($post->caption->text,0,$maxLength);
            $igtext = ((strlen($igtext) >= $maxLength) ? $igtext."..." : $igtext);
            $gallery[] = array("image" => $post->images->standard_resolution->url, "from" => "@".$post->caption->from->username, "caption" => $igtext, "time" => $post->created_time);
        }

        $dbHandler = ZRegistry::getInstance()->get('db');

        $dynamicFetchQry = "SELECT * FROM UserUploads WHERE Approved = 1 AND Deleted = 0 ORDER BY Time DESC";

        $stateSmt = $dbHandler->query($dynamicFetchQry);

        while($row =  $stateSmt->fetch(PDO::FETCH_ASSOC))
        {
            $maxLength = 200;
            $desc = substr($row['description'],0,$maxLength);
            $desc = ((strlen($desc) >= $maxLength) ? $desc."..." : $desc);
            $gallery[] = array("image" => "/upimages/".$row['imagepath'], "from" => $row['alias'], "caption" => $desc, "time" => $row['time']);

        }


        function sortFunction( $a, $b ) {
            return $b["time"] - $a["time"];
        }
        usort($gallery, "sortFunction");




        $content = $this->getController()->render(
            'gallery.twig', array(
                'posts' => $gallery
            )
        );

        echo $content;
    }

    protected function prepareAssets()
    {
        $this->addScriptFileAsset('utilities/dropzone.min.js');
        $this->addCssFileAsset('views/gallery.min.css');
        $this->addScriptFileAsset('views/gallery.min.js');
    }

    private final function fetchData($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    private final function date_compare($a, $b)
    {
        $t1 = strtotime($a['time']);
        $t2 = strtotime($b['time']);
        return $t1 - $t2;
    }
}
