<?php
    use ZMVC\ZRegistry;

    /**
     * Class DynamicAction
     */
    class ReturnsAction extends \ZMVC\Store\Actions\DynamicAction
    {
        public $title = 'Shipping and Returns';
        public $pageTitle = 'Shipping and Returns';

        /**
         * @return mixed|void
         */
        public function run()
        {
        	$this->prepareAssets();
            $cmsContent = $this->fetchgetCustSvc();
            $content = $this->getController()->render(
                'dynamic.twig', array(
                    'content' => $cmsContent,
                )
            );

            echo $content;
        }
        
        protected function prepareAssets()
        {
        	$this->addCssFileAsset('views/returns.min.css');
        }
    }