<?php
use ZMVC\ZRegistry;

class StoryAction extends \ZMVC\Store\Actions\DynamicAction {
    public $title = 'Story';
    public $pageTitle = 'Story';

    public function run()
    {
        $this->prepareAssets();

        $storyID = (int)$_REQUEST['storyid'];


        $dbHandler = ZRegistry::getInstance()->get('db');

        $dynamicFetchQry = "Call GetDynamicContent({$this->getController()->getStoreId()}, {$storyID});";
        $dynamicStmt = $dbHandler->query($dynamicFetchQry);
        if (!($dynamicRow = $dynamicStmt->fetch(PDO::FETCH_ASSOC))) {
            throw new \Exception('Could not fetch content');
        }

        if($this->getController()->getStoreId() == (int)$dynamicRow['storeid'])
        $design = $dynamicRow['content'];

        $content = $this->getController()->render(
            'story.twig', array(
                'design' => $design
            )
        );

        echo $content;
    }

    protected function prepareAssets()
    {
        $this->addCssFileAsset('views/story.min.css');
        $this->addScriptFileAsset('views/story.min.js');
    }
}
