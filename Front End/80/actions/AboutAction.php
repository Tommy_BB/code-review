<?php
    use ZMVC\ZRegistry;

    class AboutAction extends \ZMVC\Store\Actions\DynamicAction
    {
        public $title = 'About';
        public $pageTitle = 'About';

        public function run()
        {
            $this->prepareAssets();
            $cmsContent = $this->fetchDynamicContentByName('aboutus');
            $content = $this->getController()->render('about.twig', array(
                'content' => $cmsContent,
            ));

            echo $content;
        }

        private function prepareAssets() {
            $this->addCssFileAsset('views/about.min.css');
            $this->addScriptFileAsset('views/about.min.js');
        }
    }