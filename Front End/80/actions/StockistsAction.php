<?php
use ZMVC\ZRegistry;

class StockistsAction extends \ZMVC\Store\Actions\DynamicAction {
    public $title = 'Stockists';
    public $pageTitle = 'Stockists';

    public function run()
    {
        $this->prepareAssets();
        $states = $countries = array();
        $dbHandler = ZRegistry::getInstance()->get('db');
        $countryStmt = $dbHandler->query("SELECT c.Country AS Country, c.Country_Code AS Country_Code FROM StoreLocations sl JOIN Countries c ON c.Country_Code = sl.Country WHERE sl.StoreID = '".$this->getController()->getStoreId()."' GROUP by sl.Country Order By c.Country ASC");

        while($row =  $countryStmt->fetch(PDO::FETCH_ASSOC))
        {
            $countries[] = array("code" => $row['country_code'], "country" => $row['country']);
        }

        $dynamicFetchQry = "Call GetStoreLocationStates({$this->getController()->getStoreId()}, null);";

        $stateSmt = $dbHandler->query($dynamicFetchQry);

        while($row =  $stateSmt->fetch(PDO::FETCH_ASSOC))
        {
            $states[] = array("state" => $row['state'], "name" => $row['statename']);
        }


        $content = $this->getController()->render(
            'stockists.twig', array(
                'countries' => $countries,
                'states' => $states
            )
        );

        echo $content;
    }

    protected function prepareAssets()
    {
        $this->addScriptFileAsset('utilities/jsrender.min.js');
        $this->addCssFileAsset('views/stockists.min.css');
        $this->addScriptFileAsset('views/stockists.min.js');
    }
}
