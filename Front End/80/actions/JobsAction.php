<?php
    use ZMVC\ZRegistry;
    
    class JobsAction extends \ZMVC\Store\Actions\PersonalShopperAction
    {
        public $title = 'Careers';
        public $pageTitle = 'Careers';

        public function run()
        {
            $this->prepareAssets();
            $email_sent=false;
            
            $cmsContent = $this->fetchDynamicContent(4202);

            if (!empty($_POST['first_name'])){
                
                if (!empty($_FILES['resume'])){
                    include_once "../../../../../classes/cloudfiles.php";
                    $ext = pathinfo($_FILES['resume']['name'], PATHINFO_EXTENSION);
                    $final_filename=strtolower('jcr_'.date('YmdHis').'_'.$_POST['first_name'].'_'.$_POST['last_name'].'.'.$ext);
                    if (move_uploaded_file($_FILES['resume']['tmp_name'], '/tmp/'.$final_filename)) {
                        $result = $cfclass->saveFile("assets", '/tmp/'.$final_filename, $final_filename);
                        $email_url='http://448a68667f881074e230-5c97a0558224501b15fe6d04366a0a1c.r52.cf2.rackcdn.com/'.$final_filename;
                        unlink('/tmp/'.$final_filename);
                    } 
                }

                $email_data ='First Name: '.$_POST['first_name'].'<br/>';
                $email_data.='Last Name: '.$_POST['last_name'].'<br/>';
                $email_data.='Address: '.$_POST['address1'].' '.$_POST['address2'].'<br/>';
                $email_data.='City: '.$_POST['city'].'<br/>';
                $email_data.='State: '.$_POST['state'].'<br/>';
                $email_data.='Postal Code: '.$_POST['zip'].'<br/>';
                $email_data.='Phone: '.$_POST['phone'].'<br/>';
                $email_data.='Email: '.$_POST['email'].'<br/>';
                $email_data.='Comments: '.$_POST['comments'].'<br/>';
                if (!empty($email_url)){
                    $email_data.='Resume: <a href="'.$email_url.'">'.$final_filename.'</a><br/>';
                }

                $emailData[] = array(
                    'name'    => 'contactform',
                    'type'    => 'html',
                    'content' => $email_data,
                );

                $sentEmails = $this->sendEmails($emailData, $_POST['email'], $_POST['first_name'].' '.$_POST['last_name']);
                $email_sent=true;
            }

            $content = $this->getController()->render('jobs.twig', array(
                'content' => $cmsContent,
                'emailsent' => $email_sent,
            ));

            echo $content;
        }

        private function prepareAssets() {
            $this->addCssFileAsset('views/jobs.min.css');
            $this->addScriptFileAsset('views/jobs.min.js');
        }

        /**
         * @param $emailData
         * @todo The ids will be the same for every personal shopper email, should move those ids to the abstract instance
         *
         * @return mixed|void
         */
        protected function sendEmails($emailData, $email){            
            $this->getMailSender()->addOptinContact($email);
            $this->getMailSender()->addOptinContact('julianchang@zindigo.com');

            // Setup the store header
            $storeHeader = $this->getMailSender()->ReadContentTag('headernew_' . $this->getController()->getStoreId());
            $emailData[] = array(
                'name' => 'storeheader',
                'type' => 'html',
                'content' => $storeHeader
            );

            $this->getMailSender()->updateanyfield(
                $email, '0bbb03e900000000000000000000000216e7',
                '@headernew_' . $this->getController()->getStoreId()
            );

            $this->getMailSender()->sendaccountemail(
                $email, '0bbb03eb000000000000000000000016e1f1', 'Julian Chang Careers', 'julianchang@zindigo.com',
                $emailData
            );

            $this->getMailSender()->sendaccountemail(
                'julianchang@zindigo.com', '0bbb03eb000000000000000000000016e1f0', 'Customer Service Contact - Julian Chang',
                'julianchang@zindigo.com', $emailData
            );

            return true;
        }

        protected function fetchDynamicContent($contentId) {

            if (!is_numeric($contentId)) {
                $contentId = "null";
            }

            $dbHandler = ZRegistry::getInstance()->get('db');

            $dynamicFetchQry = "Call GetDynamicContent({$this->getController()->getStoreId()}, {$contentId});";
            $dynamicStmt = $dbHandler->query($dynamicFetchQry);
            if (!($dynamicRow = $dynamicStmt->fetch(PDO::FETCH_ASSOC))) {
                throw new \Exception('Could not fetch content');
            }

            return $dynamicRow['content'];
        }
    }