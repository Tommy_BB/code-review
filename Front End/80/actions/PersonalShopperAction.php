<?php

    /**
     * Class PersonalShopperAction
     */
    class PersonalShopperAction extends \ZMVC\Store\Actions\PersonalShopperAction
    {
        /**
         * @param $emailData
         * @todo The ids will be the same for every personal shopper email, should move those ids to the abstract instance
         *
         * @return mixed|void
         */
        protected function sendEmails($emailData, $post)
        {
        
        	if (!isset($post['email']) or !isset($post['name'])) {
		        die("");
		    }
		
		    $formHelper = \ZMVC\Store\Helpers\ZFormHelper::getInstance();
		    if (!$formHelper->isValidToken($post['token'], $post['tokenCipher'])) {
		        die("");
		    }
		    
            $this->getMailSender()->addOptinContact($post['email']);
            $this->getMailSender()->addOptinContact('stellavalle@zindigo.com');

            // Setup the store header
            $storeHeader = $this->getMailSender()->ReadContentTag('headernew_' . $this->getController()->getStoreId());
            $emailData[] = array(
                'name' => 'storeheader',
                'type' => 'html',
                'content' => $storeHeader
            );

            $this->getMailSender()->updateanyfield(
                $post['email'], '0bbb03e900000000000000000000000216e7',
                '@headernew_' . $this->getController()->getStoreId()
            );

            $this->getMailSender()->sendaccountemail(
                $post['email'], '0bbb03eb000000000000000000000016e1f1', 'Stella Valle Customer Service', 'stellavalle@zindigo.com',
                $emailData
            );

            $this->getMailSender()->sendaccountemail(
                'stellavalle@zindigo.com', '0bbb03eb000000000000000000000016e1f0', 'Customer Service Contact - Stella Valle',
                'stellavalle@zindigo.com', $emailData
            );

            return true;
        }
    }