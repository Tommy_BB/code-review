<?php
use \ZMVC\Store\Helpers\ZStoreProductsHelper;
use ZMVC\ZRegistry;

class CartViewAction extends \ZMVC\ZAction
{
    public $title = 'Shopping Cart';
    public $pageTitle = 'Shopping Cart';

    public function run()
    {
        $this->prepareAssets();

        $dbHandler = ZRegistry::getInstance()->get('db');

        if (isset($_SESSION['promo_codes'])) {
            $this->updateDiscounts($_SESSION['promo_codes']);
        }

        echo $this->getController()->render(
            'cart_view.twig', array()
        );

        // Clear any alert messages
        $_SESSION['cart']->clearMessages();
    }

    private function prepareAssets()
    {
        $this->addCssFileAsset('views/cart_view.min.css');
        $this->addScriptFileAsset('views/cart_view.min.js');
    }

    /**
     * @param $promoCodesToApply
     *
     * @return bool
     */
    private function updateDiscounts($promoCodesToApply)
    {
        /** @var \Cart $cart */
        $cart = $_SESSION['cart'];

        foreach ($promoCodesToApply as $promoCode) {
            // Skip empty promotion codes
            if (empty($promoCode)) {
                continue;
            }

            $promoCode = trim($promoCode);

            $giftCertValidated = $cart->validateGiftCertificate($promoCode);

            // Successfully applied gift certificate
            if ($giftCertValidated) {
                continue;
            }

            $promoCodeValidated = $cart->validateCoupon($promoCode);
            // Successfully applied promotion coupon code
            if ($promoCodeValidated) {
                continue;
            }

            $cart->addError('Could not apply promotional code.');
        }

        return $cart->hasErrors();
    }


}