<?php
    use ZMVC\ZRegistry;

    /**
     * Class DynamicAction
     */
    class PrivacyAction extends \ZMVC\Store\Actions\DynamicAction
    {
        public $title = 'Privacy';
        public $pageTitle = 'Privacy';

        /**
         * @return mixed|void
         */
        public function run()
        {
        	$this->prepareAssets();
            $cmsContent = $this->fetchDynamicContent(416);

            $content = $this->getController()->render(
                'dynamic.twig', array(
                    'content' => $cmsContent,
                )
            );

            echo $content;
        }
        
        protected function prepareAssets()
        {
        	$this->addCssFileAsset('views/privacy.min.css');
        }
    }