<?
function fetchData($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

try {
    $svdataset = 'StellValle_Instagram';
    $iresult = memcache_get($ulk->memcache_obj, $svdataset);
} catch (Exception $e) {
    $iresult = "";
}
$iresult = "";
if($iresult == "")
{
    $iresult = fetchData("https://api.instagram.com/v1/users/self/media/recent?count=10&access_token=22145322.7602433.992992d3796242799b634dbb9e936c09");
    $iresult = json_decode($iresult);
    $result = memcache_replace($ulk->memcache_obj, $svdataset, $iresult, 0, 1800);
    if ($result == false)
        memcache_set($ulk->memcache_obj, $svdataset, $iresult, 0, 1800);
}
?>
<script type="text/javascript" src="/js/<?=$storeid?>/slick.min.js"></script>
<link media="all" rel="stylesheet" href="css/<?=$storeid;?>/slick.css" />
<link type="text/css" rel="stylesheet" href="/css/80/bandw.css" />
<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
<script src="js/fancybox/jquery.fancybox.pack.js"></script>
<script src="/js/80/foundation.interchange.js"></script>

<div class="splitter">
    <div class="whiteside"></div>
    <div class="blackside"></div>
</div>

<div class="hp-container">
    <div class="slick-carousel" style="margin: 0 auto;">
        <div class="slides">
            <a href="index.php?storeid=<?= $storeid; ?>&page=categoryproducts&action=all&categoryID=2064&sess=<?= session_name(); ?>">
                <img src="/images/page/80/bandw/stella_valle_otherhalf_homepage_slider1.jpg" />
            </a>
        </div>
        <div class="slides">
            <a href="index.php?storeid=<?= $storeid; ?>&page=categoryproducts&action=all&categoryID=2064&sess=<?= session_name(); ?>">
                <img src="/images/page/80/bandw/stella_valle_otherhalf_homepage_slider2.jpg" />
            </a>
        </div>
        <div class="slides">
            <a href="index.php?storeid=<?= $storeid; ?>&page=categoryproducts&action=all&categoryID=2064&sess=<?= session_name(); ?>">
                <img src="/images/page/80/bandw/stella_valle_otherhalf_homepage_slider3.jpg" />
            </a>
        </div>
    </div>
</div>

<div class="hp-container">
    <div class="half-side">STELLA VALLE</div>
    <div class="half-side">STYLE BLOG</div>
    <div class="clear"></div>
    <div class="hp-divider black"></div>
    <div class="hp-divider"></div>
    <div class="clear"></div>
    <div class="row collapse">
        <div class="large-4 columns">
            <a href="index.php?storeid=<?= $storeid; ?>&page=blog&sess=<?= session_name(); ?>">
                <img src="images/page/80/bandw/blog_1.jpg" />
            </a>
        </div>
        <div class="large-4 columns show-for-large-only">
            <a href="index.php?storeid=<?= $storeid; ?>&page=blog&sess=<?= session_name(); ?>">
                <img src="images/page/80/bandw/blog_2.jpg" />
            </a>
        </div>
        <div class="large-4 columns show-for-large-only">
            <a href="index.php?storeid=<?= $storeid; ?>&page=blog&sess=<?= session_name(); ?>">
                <img src="images/page/80/bandw/blog_3.jpg" />
            </a>
        </div>
        <div class="clear"></div>
    </div>
</div>


<div class="hp-container" style="margin-top: 50px;">
    <div class="half-side">FOLLOW US</div>
    <div class="half-side">ON INSTAGRAM</div>
    <div class="clear"></div>
    <div class="hp-divider black"></div>
    <div class="hp-divider"></div>
    <div class="clear"></div>
    <div class="row footer-row"">
        <div class="left-half">
        <?
            $countOfPosts = count($iresult->data);
            $posts = array_slice($iresult->data, 0, ($countOfPosts - ($countOfPosts - ($threshold=4))));
            $j = 0;
            foreach ($posts as $post):
        ?>
            <div class="insta-post <?= (($j == 2) ? "break-for-large-only" : null); ?>">
                <a class="recentposts" href="index.php?page=dynamic&ContentID=410&storeid=<?= $storeid; ?>&sess=<?= session_name(); ?><?= $sid; ?>">
                    <img src="<?= $post->images->thumbnail->url; ?>" border="0" />
                </a>
            </div>
        <? $j++; endforeach; ?>
        </div>

    <div class="right-half">
        <?
        $countOfPosts = count($iresult->data);
        $posts = array_slice($iresult->data, 4, ($countOfPosts - ($countOfPosts - ($threshold=4))));
        $j = 0;
        foreach ($posts as $post):
            ?>
            <div class="insta-post <?= (($j == 2) ? "break-for-large-only" : null); ?>">
                <a class="recentposts" href="index.php?page=dynamic&ContentID=410&storeid=<?= $storeid; ?>&sess=<?= session_name(); ?><?= $sid; ?>">
                    <img src="<?= $post->images->thumbnail->url; ?>" border="0" />
                </a>
            </div>
        <? $j++; endforeach; ?>
    </div>
    </div>
    <div class="clear"></div>
</div>

<input type="hidden" name="storeid" value="<?=$storeid;?>"/>
<div class="footer-padding"></div>

<div id="Featured-Product" class="home-modal">
    <a href="index.php?storeid=<?= $storeid; ?>&page=categoryproducts&action=all&categoryID=1013&sess=<?= session_name(); ?>">
        <img src="/images/page/80/trendreport_target.png" border="0" alt="Trend" width="700" height="500" />
    </a>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        <? if ($_SESSION['contestStatus'] == false) { ?>

        $("body").imagesLoaded(function () {
            $.fancybox.open({
                href: '#Featured-Product',
                type: 'inline',
                padding: 5
            });
        });

        <? } ?>

        $('.slick-carousel').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            draggable: false,
            slides: ".slides",
            prevArrow: '<div class="slick-prev"></div>',
            nextArrow: '<div class="slick-next"></div>',
            arrows: true,
            infinite: true,
            fade: true,
            autoplay: true,
            autoplaySpeed: 3000,
            useCSS : false,
            speed: 500
        });
        $('.slick-carousel').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            $('.slick-carousel[data-slick-index="' + currentSlide + '"]').hide().css("opacity","0");
        });

        $('.fancybox').fancybox();

        $(".svmodalexit").click(function() {
            $(this).parent().parent().fadeOut();
        });
        $("input[name=svoptin]").focusin(function() {
            if($(this).val() == "Enter Your Email")
                $(this).val("");
        });

        $("input[name=svoptin]").focusout(function() {
            if($(this).val() == "")
                $(this).val("Enter Your Email");
        });

        $(".svsubmitter").click(function() {
            var svemail = $(this).parent().find("input[name=svoptin]").val();
            if(svemail != "" && svemail != "ENTER YOUR EMAIL" && svemail != null)
            {
                $.ajax({
                    async: true,
                    type: "POST",
                    url: "//marketing.zindigo.com/ajax.php",
                    data: "action=optincode&sid=<?= $storeid; ?>&email="+ encodeURI(svemail),
                    dataType: "json",
                    success: function(msg){
                        localStorage.setItem("svsignedup","true");
                        $("input[name=svoptin]").val("Thank You!");
                        setTimeout(function() { $(".svmodal").fadeOut(); },500);
                    }
                });
            }
        });
    });
</script>

<? if ($_SESSION['contestStatus'] !== false && isset($_SESSION["cart"]->items) && !isset($_SESSION["cart"]->items['77_RaiseMidiRing-G-g-3'])) { ?>
    <style>
        .contest-modal
        {
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0px;
            left: 0px;
            background: #000;
            background: rgba(0,0,0,0.8);
            z-index: 9999999999999999;
        }

        .contest-modal .modal-holder
        {
            width: 700px;
            height: 500px;
            position: absolute;
            left: 0px;
            top: 0px;
            right: 0px;
            bottom: 0px;
            margin: auto;
            background-color: #FFF;
        }

        .contest-modal .modal-holder .first-modal
        {
            display: block;
            cursor: pointer;
            height: 100%;
            text-align: center;
        }


        .contest-modal .modal-holder .second-modal
        {
            display: none;
        }

        .contest-modal .modal-holder .second-modal .svtarget
        {
            text-align: center;
            padding-top: 5px;
        }

        .contest-modal .modal-holder .second-modal .select-social-text
        {
            text-align: center;
            font-family: 'AvenirNextLTPro-Regular';
            margin-top: 50px;
            font-size: 18px;
            line-height: 20px;
        }

        .contest-modal .modal-holder .second-modal .select-social-text-small
        {
            text-align: center;
            font-family: 'AvenirNextLTPro-Regular';
            font-size: 14px;
            line-height: 20px;
        }

        .submit-contest
        {
            background-color: #000;
            color: #FFF;
            text-align: center;
            font-family: 'AvenirNextLTPro-Regular';
            height: 35px;
            line-height: 35px;
            font-size: 16px;
            widht: 100%;
            cursor: pointer;
        }
        .contest-modal .modal-holder .modal-exit
        {
            background:url('/images/page/80/futuristic/trendclose.png') no-repeat top center transparent;
            position: absolute;
            top: 5px;
            right: 5px;
            width: 20px;
            height: 23px;
            z-index: 99;
            cursor: pointer;
        }
        .select-error, .input-error
        {
            text-align: center;
            color: #da1a33;
            font-size: 12px;
            font-family: 'AvenirNextLTPro-Regular';
            display: none;
        }

        @media (max-width: 700px) {
            .contest-modal .modal-holder
            {
                width: 100%;
                padding: 10px;
                box-sizing: border-box;
                height: inherit;
                margin: 0px;
            }
            .contest-modal .modal-holder .large-3
            {
                display: none;
            }
            .contest-modal .modal-holder .first-modal img
            {
                max-height: 100%;
                width: 100%;
            }
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".first-modal").click(function() {
                $(this).hide();
                $(".second-modal").show();
            });
            $(".contest-modal .modal-holder .modal-exit").click(function() {
                $(".contest-modal").fadeOut();
            });

            $(".submit-contest").click(function() {
                if($("select[name=socialchoose]").val() == "")
                {
                    $(".select-error").show();
                    return true;
                } else {
                    $(".select-error").hide();
                }

                if($("input[name=social-handle]").val() == "")
                {
                    $(".input-error").show();
                    return true;
                } else {
                    $(".input-error").hide();
                }

                if($("input[name=social-handle]").val() != "" && $("select[name=socialchoose]").val() != "")
                {
                    location.href = "index.php?page=contest&storeid=<?= $storeid; ?>&sess=<?= session_name(); ?>&handle="+encodeURI($("input[name=social-handle]").val())+"&social="+encodeURI($("select[name=socialchoose]").val());
                }

            });

            $(".contest-modal form").submit(function (e) {
                e.preventDefault();
            });
        })
    </script>
    <div class="contest-modal">
        <div class="modal-holder">
            <div class="modal-exit"></div>
            <div class="first-modal">
                <img data-interchange="[/images/page/80/trendreport_mobile.png, (default)],[/images/page/80/svtrendreport07302015.png, (only screen and (min-width: 700px))]" />
            </div>
            <div class="second-modal">
                <div class="svtarget">
                    <img src="/images/page/80/svtarget.png" />
                </div>
                <div class="select-social-text">Select the social media platform you shared the photo on:</div>
                <form class="custom">
                    <div class="select-social-media">
                        <div class="row collapse">
                            <div class="large-3 columns">&nbsp;</div>
                            <div class="large-6 columns" style="margin-top: 15px;">
                                <select name="socialchoose" class="large choose">
                                    <option value="" selected>select one</option>
                                    <option value="facebook">Facebook</option>
                                    <option value="twitter">Twitter</option>
                                    <option value="instagram" >Instagram</option>
                                </select>
                            </div>
                            <div class="large-3 columns">&nbsp;</div>
                        </div>
                        <div class="select-error">REQUIRED</div>
                    </div>
                    <div class="select-social-text" style="margin-top: 20px;">Enter your username, Handle, or URL:</div>
                    <div class="select-social-text-small">(for sharing verification only)</div>
                    <div class="row collapse">
                        <div class="large-3 columns">&nbsp;</div>
                        <div class="large-6 columns" style="margin-top: 10px;">
                            <input type="text" name="social-handle" value="" placeholder="" style="width: 100%" />
                        </div>
                        <div class="large-3 columns" >&nbsp;</div>
                    </div>
                    <div class="input-error">REQUIRED</div>
                    <div class="row collapse">
                        <div class="large-3 columns">&nbsp;</div>
                        <div class="large-6 columns" style="margin-top: 10px;">
                            <div class="submit-contest">SUBMIT</div>
                        </div>
                        <div class="large-3 columns" >&nbsp;</div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<? } ?>