<?php //include_once __DIR__ . "/../../includes/modal_optin.php"; ?>
<!-- Footer -->
<footer>

    <div class="row collapse show-for-mobile social-footer">
        <div class="large-12 columns centered-column">
            <div class="large-6 small-7 columns">
                <form class="footer-newsletter right" name="input1" action="http://marketing.zindigo.com/ajax.php" method="get" onSubmit="giveThanks1();">

                    <input type="hidden" name="action" value="optin">
                    <input type="hidden" name="sid" value="<?php echo $storeid; ?>">

                    <input type="text" name="email" value="Email Sign-up" onFocus="this.value='';this.style.textAlign='left';">
                    <a href="javascript: document.input1.submit();" class=" go-button button prefix"></a>
                </form>
            </div>
            <div class="large-6 small-5 columns">
                <ul class="social-links">
                    <li>
                        <a href="https://www.facebook.com/pages/stella-valle/113196925361707" class="facebook">Facebook</a>
                    </li>
                    <li><a href="http://instagram.com/stellavalle/" class="instagram">Instagram</a></li>
                    <li><a href="http://pinterest.com/stellavalle/" class="pinterest">Pinterest</a></li>
                    <li><a href="https://twitter.com/stellavallenyc" class="twitter">Twitter</a></li>
                </ul>
            </div>

            <script>
                function giveThanks1() {
                    document.input1.email.value = ">Thanks for joining";
                }
            </script>
        </div>
    </div>


    <div class="row show-for-dektop-only  social-footer">
        <div class="large-6 columns desktop-footer footer-links" style="padding-left: 6px; padding-right: 0px;">
            
            <a href="http://cfda.com/" target="_blank"><img src="/images/page/80/CFDA_logo.png" style="height:18px;"></a>

            <a href="index.php?page=dynamic&ContentID=422&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(
            ); ?>">CONTACT US</a>
            <a href="index.php?page=contactus&storeid=<?= $storeid ?>&sess=<?= session_name() ?>">SHIPPING &amp;
                                                                                                  RETURNS</a>
            <a href="index.php?page=dynamic&ContentID=415&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(
            ); ?>">TERMS &amp; CONDITIONS</a>
            <a href="index.php?page=dynamic&ContentID=416&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(
            ); ?>">PRIVACY POLICY</a>


        </div>

        <div class="large-6 columns desktop-footer newsletter_holder text-right" style="padding-left: 0px; padding-right: 0px;">

            <form class="footer-newsletter" name="input" action="http://marketing.zindigo.com/ajax.php" method="get" onSubmit="giveThanks();">

                <input type="hidden" name="sid" value="<?php echo $storeid; ?>">

                <input type="text" name="email" value="Email Sign-up" onFocus="this.value='';this.style.textAlign='left';">
                <a href="#" class="go-button button prefix"></a>
            </form>

            <script>
                function giveThanks() {
                    document.input.email.value = ">Thanks for joining";
                }

                $('input[name="email"]').keydown(function(event) {
                    if (event.keyCode == 13) {
                        $(this).parent().find('.go-button').click();
                        return false;
                    }
                })

                $(function() {
                    $('.go-button').click(function () {
                        var theemail = $(this).parent().find("input[name='email']").val();
                        if (theemail != "" && theemail != null) {
                            var storeid = <?= $storeid; ?>;
                            $.ajax({
                                url: "http://marketing.zindigo.com/ajax.php",
                                dataType: 'json',
                                crossDomain: true,
                                data: {
                                    sid: storeid,
                                    action: "optincode20",
                                    email: theemail
                                }
                            }).done(function () {
                                $("input[name='email']").val("THANK YOU!");
                                showSweepstakesModal(theemail);
                            });

                            return false;
                        }
                    });
                })
            </script>

            <ul class="social-links">
                <li><a href="https://www.facebook.com/pages/stella-valle/113196925361707" class="facebook"></a></li>
                <li><a href="http://instagram.com/stellavalle/" class="instagram"></a></li>
                <li><a href="http://pinterest.com/stellavalle/" class="pinterest"></a></li>
                <li><a href="https://twitter.com/stellavallenyc" class="twitter"></a></li>
            </ul>


            <a href="#" class="empowered_by">
                <span style="font-size:11px; font-family: 'Open Sans',sans-serif !important; font-weight:300; margin-right: 3px;">EMPOWERED BY</span>
                <img src="<?= $storeimagecloud; ?>80_zlogosnew.png" alt="Powered by Zindigo" style="max-width:130px;">
            </a>


        </div>
    </div>


    <div class="row show-for-mobile">
        <div class="small-7 columns footer-links">
            <a href="http://cfda.com/" target="_blank"><img src="/images/page/80/CFDA_logo.png" style="height:18px;"></a><br/>
            <a href="index.php?page=dynamic&ContentID=422&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(
            ); ?>">Contact</a>
            <span>|</span>
            <a href="index.php?page=contactus&storeid=<?= $storeid ?>&sess=<?= session_name() ?>">Shipping</a>
            <span>|</span>
            <a href="index.php?page=dynamic&ContentID=415&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(
            ); ?>">Terms</a>
        </div>
        <div class="small-5 columns empowered-container">
            <a href="#" class="empowered_by">
                EMPOWERED BY ZINDIGO
            </a>
        </div>
    </div>


</footer>
<!-- Footer -->
<div id="shopper-modal" style="min-height: 200px" class="reveal-modal">
    <div id="shopper-modal-close"></div>
    <div class="pstitle">OUR PERSONAL SHOPPERS ARE HERE TO HELP</div>
    <div class="thankyou-message"></div>
    <div class="psthanks">
        <div class="floatleft">
            <span style="font-weight:700;margin-bottom: 21px;">WE CAN ASSIST YOU WITH</span>
            <ol class="personal-shopper-modal-ul" style="margin-left: 19px;font-weight: normal; font-size: 13px; margin-top: 10px; list-style-type: disc;">
                <li>CHOOSING THE LATEST MUST HAVE PIECES FOR YOU</li>
                <li>FINDING THE PERFECT GIFT</li>
                <li>SIZING ADVICE</li>
                <li>PRODUCT INFORMATION</li>
                <li>ORDERING AND SHIPPING INFORMATION</li>
            </ol>
        </div>
        <div class="floatright">
            <div style="margin-bottom:10px;font-weight: 800;">CALL <strong>(877) 212 8450 (US)</strong></div>
            <div style="margin-bottom:20px;"><span style="font-weight: 800;">EMAIL </span> stellavalle@zindigo.com</div>
            <div style="margin-bottom:10px; font-weight: 800;">Or, Submit a query below:</div>
            <p />

            <form id="personalshopperform">
                <input type='text' placeholder="NAME *" name='name' id='name' data-msg-required="Please enter your first name" required/>
                <input type='text' placeholder="EMAIL *" name='email' id='email' required/>
                <input type='text' placeholder="TELEPHONE" name='phone' id='phone' />
                <textarea placeholder="COMMENTS" name='comments' id='comments'></textarea>
                <input type='hidden' name='storeid' value='<?= $storeid ?>'>
                <input type='hidden' name='productid' value='<?= $_GET['prodid'] ?>'>

                <div class="pssubmit submit">Submit</div>
            </form>
        </div>
        <br clear="all" />

        <div style="text-align:right;margin: 3px;font-size: 10px;">* REQUIRED</div>
    </div>

</div>

<script>
    $(document).foundation();
</script>
<script>
    $(document).ready(function () {
        $(".header-nav-personal-shopper").click(function () {
            $('#shopper-modal').foundation('reveal', 'open');
        });

        $("#nav-personal-shopper").click(function () {
            $('#shopper-modal').foundation('reveal', 'open');
        });

        $("#shopper-modal-close").click(function () {
            $('#shopper-modal').foundation('reveal', 'close');
        });
    });
</script>
<script>
    $(".pssubmit").click(function () {
        $("#personalshopperform").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                email: {
                    required: true,
                    minlength: 2,
                    email: true
                }
            }
        });

        if ($('#personalshopperform').valid()) {
            var url = "/index.php?storeid=<?=$storeid?>&page=contactsubmit";
            var data = $("#personalshopperform").serialize();

            //alert(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    if (data == 'ok') {
                        $('#shopper-modal .thankyou-message').html("<div style='font-size: 14px; text-transform: uppercase; text-align: center;'>Thanks so much for reaching out!<br>We will get back to you shortly.<br /><br />Best Regards,<br>Customer Service.<br><Br><div class='contshop-ps'>Continue shopping</div></div>");
                        $('.psthanks').hide();
                        $('.contshop-ps').click(function () {
                            $('#shopper-modal').foundation('reveal', 'close');
                        });
                    }
                },
                error: function (data) {
                    $('.messagebox').show();
                    $('.psthanks').show();
                }
            });

            return false;
        }
    });
</script>

<style>
    .free-shipping { border: 0 !important }
</style>


<style>

    .contest-modal .modal-holder .modal-exit
    {
        background:url('/images/page/80/futuristic/trendclose.png') no-repeat top center transparent;
        position: absolute;
        top: 5px;
        right: 5px;
        width: 20px;
        height: 23px;
        z-index: 99;
        cursor: pointer;
    }
</style>
