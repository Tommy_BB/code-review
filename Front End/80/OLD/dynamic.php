<?php if($content['ID'] == '410' ) {
	$flag = "stellavalle"
	?>
<link href="css/<?php echo $storeid; ?>/dcsns_wall.css" rel="stylesheet" />
<script src='js/social_stream_standard/jquery.social.stream.wall.1.6.js'></script>
<script src='js/social_stream_standard/jquery.social.stream.1.5.13.js'></script>

<script type="text/javascript">
$(document).ready(function () {
    $('#social-stream').dcSocialStream({
    	<?= stripslashes($content['Content']); ?>
	});
});
</script>

    <div class="row collapse our-story">
          <div class="large-12 columns">
              <h1>#STELLAVALLE Social</h1>
          </div>
    </div>

	<div class="row spacing-fix">
        <div class="small-12 large-11 large-centered columns hashtag">
<!-- Start Content
				<div id="content" >
<div id="cloud-masonry-wrapper" class="cloud-masonry-<?=$content['ID']?>">
	<div id="cloud-header">
		<div id="cloud-title"><?= stripslashes($content['Name']); ?></div>
	</div>
	<div id="cloud-container">
        <div id="cloud-content"></div>
	</div>
</div>
				</div> End Content -->
				<div id="social-stream"></div>

        </div>
	</div>

<?php
} elseif($content['ID'] == '411' || $content['ID'] == '4190') { ?>
    <div class="row collapse our-story">
          <div class="large-12 columns">
              <h1>STOCKIST</h1>
          </div>
    </div>

	<div class="row stockist spacing-fix">
	<?= stripslashes($content['Content']); ?>
	</div>
<?php
} elseif($content['ID'] == '409' ) { ?>

	<?= stripslashes($content['Content']); ?>

<?php } elseif($content['ID'] == '4194' || $content['ID'] == '4195') { ?>
<script type="text/javascript" src="/min/b=js&amp;f=jquery.flexisel.js"></script>
<style>
  .blacktop{
    background-color: #000;
    height: 30px;
    margin-top: 180px;
  }

  .ww_logo{
    max-height: 200px;
    margin-top: 30px;
  }
  .ww_logo_container{
    text-align: center;
  }
  .ww_text_container{
    font-size: 13px;
      line-height: 20px;
      margin-top: 30px;
  }
  .ww_video_container{
    margin: 20px 0;
  }

  .ww_sub_container{
    border-top:1px solid #000;
    margin-top:40px;
  }

  .fullwidth{
    max-width: none;
  }
  .block_quote {
    font-family: "aw-conqueror-didot",serif;
    font-size: 34px;
    line-height: 44px;
    margin-top: 40px;
    padding: 0 20px;
    text-align: center;
  }
  @media screen and (max-width: 940px) {
    .blacktop{
      margin-top: auto;
    }

    .block_quote {
      font-size: 28px;
      line-height: 34px;
      margin-top: 10px;
    }
  }
</style>
<div class="row fullwidth blacktop"></div>
<div class="row fullwidth">
  <div class="medium-6 columns" style="padding:0;">
    <?php if ($content['ID'] == '4194'){ ?>
    <img src="/images/page/80/armed_w_insp_landing_feature.jpg" style="width:100%;">
    <?php } ?>
    <?php if ($content['ID'] == '4195'){ ?>
    <img src="/images/page/80/thecorps_collection_landing_feature.jpg" style="width:100%;">
    <?php } ?>
  </div>
  <div class="medium-6 columns">
    <div class="row fullwidth">
      <div class="medium-12 columns ww_logo_container">
        <img class="ww_logo" src="/images/page/80/ww_logo.png">
      </div>
    </div>
    <div class="row fullwidth">
      <div class="medium-12 columns">
        <div class="ww_text_container">
          <?php echo stripslashes($content['Content']); ?>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row fullwidth" style="margin-bottom:80px;">
  <div class="small-12 large-11 large-centered columns">
    <ul id="Collections">
      <?php
      require_once "../classes/storeprods.php";
      if ($content['ID'] == '4194'){
        $thiscatid = 2280;
      }
      if ($content['ID'] == '4195'){
        $thiscatid = 2013;
      }
      $thiscatprods = $storeprods->getStoreActiveProductsAllFromCategoryCached($thiscatid);
      foreach($thiscatprods as $thisprod) {
        $thisprod['OrigPrice'] = $thisprod['StorePrice'];
        $thisprod['StorePrice'] = number_format($discounts->getSalePrice($thisprod['ProductID'],$prodstoreid),2);
        $thisprod['OrigPrice'] = str_replace(".00","",number_format($thisprod['OrigPrice'],2));
        $thisprod['StorePrice'] = str_replace(".00","",number_format($thisprod['StorePrice'],2));
        $phref = "index.php?page=productslist&storeid=".$storeid."&categoryID=".$_GET['categoryID']."&subcatID=".$_GET['subcatID']."&subsubcatID=".$_GET['subsubcatID']."&prodid=".$thisprod['ProductID']."&sess=". session_name() . $sid;?>
          <li><a href="<?php echo $phref;?>"><img src="<?php echo $thisprod['ImagePath'];?>" <? if(isset($thisprod['AltImagePath'])) { ?>onmouseover="this.src='<?php echo $thisprod['AltImagePath'];?>'"  <? } ?>onmouseout="this.src='<?php echo $thisprod['ImagePath'];?>'"><br><?php echo $thisprod['ProductName'];?> <br><?php if($thisprod['OrigPrice']>$thisprod['StorePrice']){?>$<span style="text-decoration:line-through;"><?php echo $thisprod['OrigPrice'];?></span>&nbsp;$<?= $thisprod['StorePrice']; ?><? }else{ ?>$<?= $thisprod['StorePrice']; ?><? } ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div>

<script type="text/javascript">
  /*** Carousel Plugin ***/
  $(window).load(function() {
  $("#Collections").flexisel({
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: {
            portrait: {
                changePoint:480,
                visibleItems: 1
            },
            landscape: {
                changePoint:640,
                visibleItems: 2
            },
            tablet: {
                changePoint:768,
                visibleItems: 3
            }
        }
    });
  });
</script>

<?php } else { ?>
	    <div class="row collapse our-story">
          <div class="large-12 columns">
              <h1><?= stripslashes($content['Name']); ?></h1>
          </div>
    </div>
	<div class="row spacing-fix">
    	<div class="small-12 large-10 large-centered columns" id="container">
 <?php       	echo stripslashes($content['Content']); ?>
</div> </div>
<?php } ?>