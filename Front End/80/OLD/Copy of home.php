<?php include ("../classes/cloudfiles.php"); ?>
	<div class="row">
		<div class="large-12 columns"  style="position:relative;">


            <div class="slideshow-wrapper">
  				<div class="preloader"></div>
                   <ul data-orbit data-options="variable_height: true;">
<?php
	$storeimages = $cfclass->getAssetFiles($storeid . "_","storeimages");
	foreach ($storeimages as $filename) {
		if (stristr($filename,'logo') === false)  { ?>
                          <li><a href="index.php?storeid=80&page=categoryproducts&action=all&categoryID=1013&sess=<?=session_name()?>" ><img src="<?php echo $filename;?>" data-interchange="[<?php echo $filename;?>, (default)], [<?php echo $filename;?>, (small)], [<?php echo $filename;?>, (medium)]"></a></li>
<?php } }  ?>

                    </ul>
            </div>

        </div>
	</div>

	<script type="text/javascript">
	if(!jQuery().imagesLoaded) {
		$.ajax({
		  url: "js/jquery.imagesloaded.min.js",
		  dataType: "script",
		  async: false
		});
	}
	</script>

<?php
	$contents = $classes->getDynamicContent(80,408);
	$content = mysqli_fetch_array($contents);
	echo stripslashes($content['Content']);
?>


  <script src="js/foundation/foundation.interchange.js"></script>
  <script src="js/foundation/foundation.orbit.js"></script>
  <script src="js/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
  <script src="js/jquery.flexisel.js"></script>

  <script>
 /* 	$(document).foundation();*/

	/*** Comment this lines to disable the popup ***/
	$("body").imagesLoaded(function() {
	$.fancybox.open({
		href : '#Featured-Product',
		type : 'inline',
		padding : 5
	});
	$(window).load(function() {
	$("#Featured-Carousel").flexisel({
        enableResponsiveBreakpoints: false,
        responsiveBreakpoints: {
            portrait: {
                changePoint:480,
                visibleItems: 3
            },
            landscape: {
                changePoint:640,
                visibleItems: 3
            },
            tablet: {
                changePoint:768,
                visibleItems: 3
            }
        }
    });
	});
	});

	$(document).ready(function() {
	 if ($.browser.msie )
	 {
		$(".has-dropdown").hover(
			function() {
				$(".orbit-container img").each(function() {
					$(this).hide();
				});
			},
			function() {
				$(".orbit-container img").each(function() {
					$(this).show();
				});
			}
		);
	}
	});
  </script>

