<?php
set_time_limit(1200);
include_once("../classes/mobiledetect.php");
$detect = new Mobile_Detect();
?>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="/js/<?php echo $storeid;?>/touchpunch.min.js"></script>
<link rel="stylesheet" href="/css/<?=$storeid?>/createyourown.css" />
<script src="/js/jquery.flexisel.js"></script>
<script src="/js/<?php echo $storeid;?>/jquery.scrollto.js"></script>
<script src="/js/<?php echo $storeid;?>/jquery.fancybox.pack.js?v=2.1.5"></script>
<script src="js/jquery.exposure.js"></script>

<style>
	.moresecurepopup
	{
		background-color: #000;
		background: rgba(0,0,0,0.8);
		display: table;
		height: 100%;
		position: fixed;
		overflow: hidden;
		width: 100%;
		z-index: 999999999999;
		visibility: hidden;
	}

	.moresecurepopup .popupcontainer
	{
		#position: absolute;
		#top: 50%;
		display: table-cell;
		vertical-align: middle;
	}

	.moresecurepopup .popupcontainer .popupad
	{
		width: 400px;
		height: 200px;
		position: relative;
		#top: -50%;
		margin:0 auto;
		text-align: center;
		color: #FFF;
		font-size: 16px;
	}

	.yesiwould
	{
		background: #000;
		border: 1px solid #FFF;
		display: block;
		padding: 10px;
		color: #FFF;
		text-align: center;
		margin-right: 10px;
		width: 200px;
	}

	.noiwouldnt
	{
		background: #000;
		border: 1px solid #FFF;
		display: block;
		padding: 10px;
		color: #FFF;
		text-align: center;
		margin-top: 10px;
		width: 200px;
	}

	.gluetext
	{
		color: #FFF;
		margin-top: 30px;
		margin-bottom: 30px;
	}

	.bigglue
	{
		font-size: 40px;
		color: #FFF;
	}
</style>

<script type="text/javascript">
	var lastposition = "";
	var cartitems = [];
	var tosend = "";
	var commentoutput = "";
	var checkoutcart = function() {
		if(cartitems.length > 0)
		{
			var newarray = new Object();
			$.each(cartitems, function (i, item) {
				if(newarray[cartitems[i]])
				{
					newarray[cartitems[i]] = newarray[cartitems[i]] + 1;
				} else {
				    newarray[cartitems[i]+""] = 1;
				}
			});

			var i = 0;
			$.each(newarray, function(key,valueObj){
			    if(i != 0)
			    	tosend += ","+key+"|"+valueObj;
			    else
			    	tosend += key+"|"+valueObj;
			    i++;
			});

			//THIS IS WHERE WE SAVE THE LEFT TO RIGHT TOP TO BOTTOM
				var theorderarray = [];
				$(".iniitedscrew").each(function() {
				    var thissum = $(this).position().top + $(this).position().left;
				    var prodid = $(this).find("input[name=prodid]").val();
				    var prodsku = $(this).find("input[name=sku]").val();
				    var prodname = $(this).find("input[name=prodname]").val();
				    obj = {};
				    obj["prodid"] = prodid;
				    obj["sum"] = thissum;
				    obj["prodsku"] = prodsku;
				    obj["prodname"] = prodname;
				    theorderarray.push(obj);
				});

				theorderarray.sort(function(obj1, obj2) {
					return  obj1.sum - obj2.sum;
				});

				//console.log(theorderarray);

				commentoutput = "Please glue from left to right top to bottom";

				$.each(theorderarray, function () {
				    //console.log($(this)[0])
					commentoutput += "[ SKU:" + $(this)[0].prodsku + " ProdName:" + $(this)[0].prodname + "]";
				});

				$(".moresecurepopup").css("visibility","visible");

			//END LEFT TO RIGHT TOP TO BOTTOM

			/*
			issavingcart = true;
			$(".savingcartpopup").css("visibility","visible");
			$.ajax({
				  type: "POST",
				  url: "/json/addmultipletocart.php",
				  data: "toadd="+tosend+"&storeid=<?= $storeid; ?>&sess=<?= session_name(); ?>&customcom="+encodeURI(commentoutput),
				  dataType: 'json',
				  success : function(data) {
				  	location.href = "index.php?page=cart_view&storeid=<?=$storeid?>&sess=<?=session_name()?>";
				  }
			});
			*/

		}
	};

	var gemcontwidth = 0;
	var gemcontheight = 0;

	var issavingcart = false;

	$(document).ready(function() {

		$(".noiwouldnt").click(function() {
			$(".moresecurepopup").css("visibility","hidden");
			issavingcart = true;
			$(".savingcartpopup").css("visibility","visible");
			$.ajax({
				  type: "POST",
				  url: "/json/addmultipletocart.php",
				  data: "toadd="+tosend+"&storeid=<?= $storeid; ?>&sess=<?= session_name(); ?>",
				  dataType: 'json',
				  success : function(data) {
				  	location.href = "index.php?page=cart_view&storeid=<?=$storeid?>&sess=<?=session_name()?>";
				  }
			});
		});

		$(".yesiwould").click(function() {
			$(".moresecurepopup").css("visibility","hidden");
			issavingcart = true;
			$(".savingcartpopup").css("visibility","visible");
			$.ajax({
				  type: "POST",
				  url: "/json/addmultipletocart.php",
				  data: "toadd="+tosend+"&storeid=<?= $storeid; ?>&sess=<?= session_name(); ?>&customcom="+encodeURI(commentoutput),
				  dataType: 'json',
				  success : function(data) {
				  	location.href = "index.php?page=cart_view&storeid=<?=$storeid?>&sess=<?=session_name()?>";
				  }
			});
		});

		window.setInterval(function(){
			if(issavingcart)
			$(".dotdotdot").append(".");
		}, 750);



		var addtocart = function(itemid) {
			cartitems.push(itemid);
		};

		var removecart = function(itemid) {
			$.each(cartitems, function (i, item) {
				if(cartitems[i] == itemid)
				{
					cartitems.splice(i,1);
				}
			});

		};

		var clearcart = function() {
			cartitems = [];
		};

		$("#accessoryul").flexisel({
	        enableResponsiveBreakpoints: true,
	        responsiveBreakpoints: {
	            portrait: {
	                changePoint:480,
	                visibleItems: 1
	            },
	            landscape: {
	                changePoint:640,
	                visibleItems: 2
	            },
	            tablet: {
	                changePoint:768,
	                visibleItems: 3
	            }
	        }
	    });

	    $("#gemul").flexisel({
	        enableResponsiveBreakpoints: true,
	        responsiveBreakpoints: {
	            portrait: {
	                changePoint:480,
	                visibleItems: 1
	            },
	            landscape: {
	                changePoint:640,
	                visibleItems: 2
	            },
	            tablet: {
	                changePoint:768,
	                visibleItems: 3
	            }
	        }
	    });

		var lastoffset;
		var iniitedacc = false;

		var thetotal = 0.00;

		$(".startover").click(function() {
			$(".tocreate").html("");
			iniitedacc = false;
			thetotal = 0.00;
			$(".totalamt").text(thetotal);
			clearcart();
			$(".tocreate").css("visibility","hidden");
			<? if ($detect->isMobile() || $detect->isTablet()) { ?>
				$(".accessory").show();
			<? } ?>

		});

		$(".addtocart").click(function() {
			checkoutcart();
		});

		$(".accitem").draggable({
			revert: "invalid",
			containment: ".createapage",
			helper: "clone",
			cursor: "move",
			appendTo: ".selectpiece",
			snap: ".tocreate",
			snapMode: "inner"
		});

		 $(".screwitem").draggable({
			revert: "invalid", // when not dropped, the item will revert back to its initial position
			appendTo: ".thegems",
			helper: "clone",
			cursor: "move",
			drag: function(event, ui) {
				lastposition = ui.position;
			},
			stop: function(event, ui) {
			}
		});

		$( ".tocreate" ).droppable({
			drop: function(event, ui) {
				//Drop accessory
				if($(ui.draggable).hasClass("accitem") && !iniitedacc)
				{
					$(".tocreate").css("visibility","visible");
					var thisamt = parseFloat(ui.draggable.find("input[name=accamt]").val()).toFixed(2);

					thetotal += parseFloat(thisamt);
					$(".totalamt").text(thetotal);

					addtocart(ui.draggable.find("input[name=prodid]").val());

					var toclone = ui.draggable.find("img");
					var toinside = "";
					toinside += '<div class="inittiedacc">';
					toinside += '<span class="gemcontainer">';
					toinside += '<img src="'+toclone.attr("src")+'" />';
					toinside += "<div class=\"thegems\"></div>";
					toinside += "</span>";
					toinside += "</div>";
					$(toinside).appendTo(this);
					iniitedacc = true;
					reinitacc();
					gemcontwidth = $(".gemcontainer").width();
					gemcontheight = $(".gemcontainer").height();
					<? if ($detect->isMobile() || $detect->isTablet()) { ?>
						$(".accessory").hide();
					<? } ?>
				}

			}

		});

		var reinit = function() {
			$(".iniitedscrew").draggable({
				containment: ".maincontain",
				cursor: "move",
				stop : function(event, ui) {
				<? if (!$detect->isMobile() || $detect->isTablet()) { ?>
					if(ui.offset.left > $(".createapage").width() * .74 || ui.offset.top > $(".createapage").height() * 7)
					{
						var thisamt = parseFloat(ui.helper.find("input[name=gemamt]").val()).toFixed(2);
						thetotal -= Math.floor(thisamt);
						$(".totalamt").text(thetotal);

						$(this).remove();
						removecart(ui.helper.find("input[name=prodid]").val());
					}
					<? } ?>
				}
			});
		}

		var reinitacc = function() {

	$( ".gemcontainer" ).droppable({
			drop: function(event, ui) {
				//screw drop
				if($(ui.draggable).hasClass("screwitem") && iniitedacc)
				{
					var thisamt = parseFloat(ui.draggable.find("input[name=gemamt]").val()).toFixed(2);
					thetotal += Math.floor(thisamt);
					$(".totalamt").text(thetotal);

					addtocart(ui.draggable.find("input[name=prodid]").val());

					lastoffset = ui.offset;
					var toclone = ui.draggable.clone();
					//toclone.css({"top":(lastoffset.top - (lastoffset.top * .2)), "left" : (lastoffset.left/2) - (lastoffset.left * .2)});

					var thepercentleft = (lastposition.left / $(".gemcontainer").width())  * 100;
					var thepercenttop = (lastposition.top / $(".gemcontainer").height())  * 100;

					toclone.css({"top":thepercenttop+"%", "left" : thepercentleft.toFixed(2)+"%"});

					$(toclone).appendTo(".thegems");
					$(toclone).addClass("iniitedscrew");
					$(toclone).removeClass("screwitem");
					reinit();
				}
			}
		});

		$(".inittiedacc").draggable({
			containment: ".createapage",
			helper: "clone",
			cursor: "move",
			snap: ".tocreate",
			snapMode: "inner",
				stop : function(event, ui) {
				<? if (!$detect->isMobile() || $detect->isTablet()) { ?>
					if(ui.offset.left > $(".createapage").width() * .4/* || ui.offset.top > $(".createapage").height() * 7*/)
					{
						$(".tocreate").html("");
						$(".tocreate").css("visibility","hidden");
						iniitedacc = false;
						thetotal = 0.00;
						clearcart();
						$(".totalamt").text(thetotal);
					}
					<? } ?>
				}
			});
		}
		$(".theproduct").disableSelection();
	});
</script>

<? if ($detect->isMobile() || $detect->isTablet()) { ?>
<style>

.createapage {
min-height: 800px;
}
.createpiece
{
	width: 100%;
	height: 52%;
	left: 0px;
	top: -20px;
}
.selectpiece
{
	width: 69%;
	height: 50%;
	top: 52%;
	right: 17%;
}

.selectpiece .accitem img
{
	max-height: 80px;
	max-width: 80px;
}

.createpiece .topnav .startover
{
	width: auto;
	font-size: 10px;
	padding: 7px;
}

.createpiece .topnav .addtocart
{
	width: auto;
	font-size: 10px;
	padding: 7px;
}

.createpiece .createdown
{
	font-size: 26px;
}

.selectpiece .screws
{
	margin-top: 2%;
}

.selectpiece .screwitem img
{
	max-height: 35px;
    max-width: 35px;
    min-height: 35px;
}

.createpiece .tocreate .inittiedacc .gemcontainer .thegems .screwitem
{
	top:0px;
	right: 0px;
}

</style>
<? } ?>

<? if ($detect->isTablet()) { ?>
<style>
.createpiece .tocreate .inittiedacc .gemcontainer .thegems .iniitedscrew
{
	min-width: 4%;
	width: 4%;
	max-width: 4%;
}
.createpiece .tocreate .inittiedacc .gemcontainer .thegems .screwitem
{
	min-width: 4%;
	width: 4%;
	max-width: 4%;
}
</style>
<? } ?>

<div class="savingcartpopup">
	<div class="popupcontainer">
		<div class="popupad">
			<img src="/images/page/80/ajax-loader.gif" /><br/>
			<div class="savingit">Saving Cart<span class="dotdotdot"></span></div>
		</div>
	</div>
</div>

<div class="moresecurepopup">
	<div class="popupcontainer">
		<div class="popupad">
			<div class="bigglue">screw it?</div>
			<div class="gluetext">Would you like stella valle to affix your screws permanently to your accessory?<br/><br/>If not, be sure to tighten your screws before wearing.</div>
			<div style="margin:0 auto; margin-top: 15px; width:212px;">
			<div class="yesiwould">Yes, make my screws permanent</div>
			<div class="noiwouldnt">No, I want to interchange my screws</div>
			</div>
		</div>
	</div>
</div>

<div class="row collapse our-story">
	<div class="large-12 columns"></div>
</div>

<div class="row shop collapse spacing-fix show-for-dektop-only">
  <div class="large-12 columns text-center"></div>
</div>
<? if (!$detect->isMobile() && !$detect->isTablet()) { ?>
<div style="margin-top: 40px;"></div>
<? } ?>


<div class="row">
	<div class="createapage large-12 large-centered columns">
		<div class="createpiece" id="togotoiphone">
			<div class="topnav">
				<div class="startover">Start Over</div>
				<div class="addtocart">Add To Cart</div>
			</div>
			<div class="thetotal">Total = $<span class="totalamt">0.00</span></div>
			<? /*
			<div class="createdown">CREATE YOURSELF</div>
			<div class="createdowndesc"><div style="">"Life isn't about finding yourself,<br/>It's about creating youself."</div><br/><div style="font-family: 'Open Sans',sans-serif !important; font-weight:300; font-size:14px;">Create your custom piece by first dragging your accessory here.<br/>Then adding your screws.</div></div>
			*/ ?>

			<div class="createbg">
				<img src="/images/page/80/createyourself.png" />
			</div>
			<div class="tocreate">
			</div>
		</div>

		<div class="selectpiece">
			<div class="accessory">
				<div class="acchead">ACCESSORY</div>
				<ul id="accessoryul">
				<?
				require_once "/classes/discounts.php";
				$discounts = NEW discounts;
//								error_log("getting gStoreActiveProductsAllFromCategory1053");
					$thiscatprods = $storeprods->getStoreActiveProductsAllFromCategoryCached(1053);
//					 while ( $thisprod = mysqli_fetch_array($thiscatprods)) {
					 foreach($thiscatprods as $thisprod) {
					$thisprod['OrigPrice'] = $thisprod['StorePrice'];

					$thisprod['StorePrice'] = number_format($discounts->getSalePrice($thisprod['ProductID'],$prodstoreid),2);

					$thisprod['OrigPrice'] = str_replace(".00","",$thisprod['OrigPrice']);
					$thisprod['StorePrice'] = str_replace(".00","",$thisprod['StorePrice']);
					$thisprod['OrigPrice'] = floatval($thisprod['OrigPrice']);
					$thisprod['StorePrice'] = floatval($thisprod['StorePrice']);

					 ?>

						<li>
							<div class="accitem">
								<input type="hidden" name="accamt" value="<?= $thisprod['StorePrice']; ?>" />
								<input type="hidden" name="prodid" value="<?= $thisprod['ProductID']; ?>" />
								<img src="<?= $thisprod['ImagePath']; ?>" class="accimg" />
							</div>
							<?= $thisprod['ProductName']; ?><br/>
							<? if($thisprod['OrigPrice']>$thisprod['StorePrice']){ ?>
							<span style="text-decoration:line-through;">$<?= $thisprod['OrigPrice']; ?></span>&nbsp;<span>$<?= $thisprod['StorePrice']; ?></span>
							<? } else { ?>
							<span>$<?= $thisprod['StorePrice']; ?></span>
							<? } ?>
						</li>
					<? } ?>
				</ul>
				<? /*
				<div class="accitem">
					<input type="hidden" name="accamt" value="159.99" />
					<img src="http://storesdev.zindigo.com/images/uploaded/images/15282.jpg" class="accimg" />
				</div>
				*/ ?>
			</div>
			<div class="screws">
				<div class="screwhead">SCREWS</div>
				<ul id="gemul">
				<?
//				error_log("getting gStoreActiveProductsAllFromCategory1025");
					$thiscatprods = $storeprods->getStoreActiveProductsAllFromCategoryCached(1025);
					$i = 0;
					$totalamt = count($thiscatprods);
//					 while ( $thisprod = mysqli_fetch_array($thiscatprods)) {
					 foreach($thiscatprods as $thisprod) {
					$thisprod['OrigPrice'] = $thisprod['StorePrice'];
					$thisprod['StorePrice'] = number_format($discounts->getSalePrice($thisprod['ProductID'],$prodstoreid),2);
					 $thisprod['OrigPrice'] = str_replace(".00","",$thisprod['OrigPrice']);
					 $thisprod['StorePrice'] = str_replace(".00","",$thisprod['StorePrice']);
					 $thisprod['OrigPrice'] = floatval($thisprod['OrigPrice']);
					 $thisprod['StorePrice'] = floatval($thisprod['StorePrice']);
					 ?>
					 	<? if($i == 0 || $i%2 == 0) echo "<li id=\"".$i."\">"; ?>
							<div class="ascrew <?= $i; ?>">
								<div class="screwitem">
									<input type="hidden" name="gemamt" value="<?= $thisprod['StorePrice']; ?>" />
									<input type="hidden" name="prodid" value="<?= $thisprod['ProductID']; ?>" />
									<input type="hidden" name="oprice" value="<?= $thisprod['OrigPrice']; ?>" />
									<input type="hidden" name="prodname" value="<?= $thisprod['ProductName']; ?>" />
									<input type="hidden" name="sku" value="<?= $thisprod['StyleNumber']; ?>" />

									<img src="<?= $thisprod['ImagePath']; ?>" class="accimg" />
								</div>
								<?= $thisprod['ProductName']; ?><br/>
								<? if($thisprod['OrigPrice']>$thisprod['StorePrice']){ ?>
							<span style="text-decoration:line-through;">$<?= $thisprod['OrigPrice']; ?></span>&nbsp;<span>$<?= $thisprod['StorePrice']; ?></span>
							<? } else { ?>
							<span>$<?= $thisprod['StorePrice']; ?></span>
							<? } ?>
							</div>
							<? $i++; ?>
						<? if($i == $totalamt || ($i%2 == 0 && $i != 0)) echo "</li>"; ?>
					<? } ?>
				</ul>
				<? /*
				<div class="screwitem">
					<input type="hidden" name="gemamt" value="19.99" />
					<img src="http://storesdev.zindigo.com/images/uploaded/images/15619.png" class="screwimg" />
				</div>
				*/
//				error_log("done");
				?>
			</div>
		</div>
	</div>
</div>

<? if($detect->isTablet()) { ?>
	<script type="text/javascript">
	$(document).ready(function() {
		$(window).load(function() {
			var heightfourty = $(window).height() * .3;
			$(".createpiece").css("height",heightfourty+"px");
		});
	});
	</script>
<? } ?>

<? if($detect->isMobile()) { ?>
	<script type="text/javascript">
	$(document).ready(function() {
		$(window).load(function() {
			var pos = $('#togotoiphone').offset();
    		$('body').animate({ scrollTop: pos.top });
		});
	});
	</script>
<? } ?>
