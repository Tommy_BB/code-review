<?

include_once("../classes/mobiledetect.php");
$detect = new Mobile_Detect();

?>
<link type="text/css" rel="stylesheet" href="/min/b=css&amp;f=cyberpayment.css,80/checkout.css,80/foundation.css,80/app.css,80/component.css,80/carousel.css" />

<style>
    .onlineret {
        display: block;
        margin: 60px 20px;
    }

    .storeresults {
        height: 430px;
        overflow: auto;
    }

    .storelist {
        margin-top: 15px;
        text-transform: uppercase;
        line-height: 20px;
        font-size: 12px;
        font-family: 'AvenirNextLTPro-Regular';
    }

    .storename {
        text-transform: uppercase;
        font-family: 'AvenirNextLTPro-Bold';
        margin: 0;
    }

    #map-canvas {
        height: 500px;
        margin: 0px;
        padding: 0px
    }

    .storesubmit {
        width: 192px;
        float: left;
        font-size: 12px;
        text-align: center;
        background: #000;
        color: #fff;
        padding: 5px;
    }

    h1 {
        font-size: 21px;
        font-family: 'AvenirNextLTPro-Regular';
    }

    .stores a {
        text-decoration: underline;
    }
</style>
<div class='stores'>
    <div class="row spacing-fix" style="max-width: 1180px;">

        <div class="large-3 columns" style="max-width: 192px; box-sizing: content-box;">
            <h1 class="sls">Store Locator</h1>

            <form id="searchstores" class="custom" name="storelocator">
                <div class="clear"></div>
                <div class="signupinput">
                    <select name="country" id='country' class="large choose">
                        <option value="">COUNTRY</option>
                        <? $storecountires = mysqli_query($ulk->db_cnx,"SELECT c.Country AS Country, c.Country_Code AS Country_Code FROM StoreLocations sl JOIN Countries c ON c.Country_Code = sl.Country WHERE sl.StoreID = '".(isset($storeidoverride) ? $storeidoverride : $storeid)."' GROUP by sl.Country Order By c.Country ASC");
                        while ($row = mysqli_fetch_array($storecountires)) { ?>
                            <option class='country' value="<?=$row['Country_Code']?>"><?=$row['Country']?></option>
                        <?	} ?>
                    </select>
                </div>
                <div class="signupinput">
                    <select name="state" id="state" class="large choose">
                        <option value="">STATE</option>
                        <? $storestates = $ulk->getStoreLocationStates((isset($storeidoverride) ? $storeidoverride : $storeid), null);
                        while ($row = mysqli_fetch_assoc($storestates)) { ?>
                            <option class="states" value=<?=$row['State']?>><?=$row['State']?></option>
                        <?	} ?>
                    </select>
                </div>
                <input type='hidden' name='storeid' value='<?=$storeid?>'>
                <div class="clear"></div>
                <div class="storesubmit" style="cursor: pointer;">SUBMIT</div>
            </form>
            <div style="clear:both"></div>
        </div>
        <div class="large-3 columns">
            <div id='storeresults' class="storeresults"></div>
        </div>
        <div class="large-6 columns map">
            <img class="holderimg" src="/images/page/80/storelocator.jpg">
            <div style="width: 100%; height: 100%;">
                <div style='display:none;' id="map-canvas"></div>
            </div>
        </div>
    </div>
</div>

<script>


    //on country change, see if it is US. if so, do the state search and put results in state submit dropdown
    $("#country").change(function() {
        var country = $(this).val();
        console.log(country);
        if (country === "US") {
            $('.state-selector').show();
            $('#states').show();

        } else {
            $("#state option:selected").prop("selected", false);
            $('#states').hide();
            $('.state-selector').hide();
        }
    });

    //on submit, search db for selected criteria.
    $('.storesubmit').click(function() {

        var url = "/index.php?storeid=<?=$storeid?>&page=searchstores&sess=<?session_name()?>";
        var data = $("#searchstores").serialize();

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(data)
            {
                $('.storeresults').html(data);
                /*
                $(".storeresults").mCustomScrollbar({
                    mouseWheel:true,
                    contentTouchScroll: true,
                    scrollButtons:{
                        enable:false
                    },
                    autoDraggerLength: true
                });
                $(".storeresults").mCustomScrollbar("update");
                */
                <? if ($detect -> isMobile()) { ?>
                $('html,body').animate({
                    scrollTop: $("#storeresults").offset().top
                });
                <? } ?>
            },
            error: function(data)
            {
                $('.messagebox').show();
            }
        });

    });
</script>

<script type="text/javascript" src="//maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript">
    //on select of "view map", show google map on right page
    var geocoder;
    var map;
    function initialize() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(-34.397, 150.644);
        var mapOptions = {
            zoom: 15,
            center: latlng
        }
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    }

    function codeAddress(a) {
        $('.holderimg').hide();
        $('#map-canvas').show();
        initialize();
        var address = document.getElementById(a).value;
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

</script>