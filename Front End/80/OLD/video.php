<?php $contents = $classes->getDynamicContent($storeid,$_GET['ContentID']);
$content = mysqli_fetch_array($contents);
?>
<style>
.videosmall {
text-transform: uppercase; 
}
.spacing-fix {
margin-top: 160px;
margin-bottom: 15px;
}

.nbs-flexisel-nav-left {
display:none; 
}

.nbs-flexisel-nav-right {
display:none; 
}
</style>
<script type="text/javascript" src="/js/80/flowplayer.js"></script>
<link rel="stylesheet" href="//releases.flowplayer.org/5.4.3/skin/minimalist.css">
	<div class="row spacing-fix collapse" style="max-width: 1170px;">
		<div class="small-12 large-11 large-centered columns" style="width: 70%;">
            <ul id="Collections">
                <li class="videosmall" name='videos/80_Entrepreneur.mp4'>
					<img src="videos/80_SharkTank.png"><br>Shark Tank
				</li>
				<li>
					<a style="text-transform: uppercase" target="_blank" onclick="api.pause();" href="http://abcnews.go.com/Entertainment/video/shark-tank-entrepreneurs-give-back-23577170"><img src="videos/80_video_0002_3.jpg"><br>Shark Tank: Swimming with the Sharks Update</a>
				</li>
            </ul>
		</div>
	</div>
	<div class="row collapse" style="max-width: 1170px;">
	<div class="large-12 columns"  style=" border-top: 1px solid #000;">
	<div style="font-family: 'GFS Didot', serif; text-transform: uppercase; font-size: 16px; margin: 15px 0;" class="titlename">Shark Tank</div>
	<div class="player">
	   <video autoplay="autoplay" poster="videos/80_SharkTank.png" controls="false">
	   	<source src="videos/80_80_shark_tank_final_Broadband.m4v" type="video/mp4">
		<source src="videos/80_shark_tank_final_Broadband.webm" type="video/webm">
	<embed
          src="YTPlayer.swf"
          flashvars="movieName=videos/80_shark%20tank%20final%20-%20Broadband.mp4.mp4&autoStart=true"
          width=480
          height=390
          allowFullScreen=true
          type="application/x-shockwave-flash"
          pluginspage="http://get.adobe.com/flashplayer" />
	   </video>
	</div>

	<script type="text/javascript">
		var api;
		$(function () {
		if($.browser.mozilla == true)
		{
			$(".splashpage").css("paddingLeft","10px");
			$(".splashpage").css("paddingRight","11px");
			$(".player").css("paddingLeft","10px");
			$(".player").css("paddingRight","11px");
			//$(".player video").css("paddingLeft","10px");
			//$(".player video").css("paddingRight","11px");
		}
		
		   // install flowplayer to an element with CSS class "player"
		   $(".player").flowplayer({ 
		   		swf: "/js/80/flowplayer.swf",
		   		tooltip : false,
		   		fullscreen : false
		   
		   });
		   $("a[href='http://flowplayer.org']").css("visibility","hidden");
		api = flowplayer();
		 
		});
	</script>
	
	</div>
</div>
	<script src="/js/jquery.flexisel.js"></script>
	<script>
	/*** Carousel Plugin ***/
	$(window).load(function() {
		$("#Collections").flexisel({
		visibleItems: 2,
			enableResponsiveBreakpoints: true,
			responsiveBreakpoints: {
				portrait: {
					changePoint:480,
					visibleItems: 1
				},
				landscape: {
					changePoint:640,
					visibleItems: 2
				},
				tablet: {
					changePoint:768,
					visibleItems: 2
				}
			}
		});
	});
	
		$(".videosmall").on("click", function(event) {
			  event.preventDefault();
				var vidname = $(this).attr("name");
				console.log(vidname);
				api.load(vidname);
		});
	</script>