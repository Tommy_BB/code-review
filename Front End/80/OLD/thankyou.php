
<link rel="stylesheet" type="text/css" href="css/checkout.css"/>
<?php
if( file_exists("css/".$storeid."/checkout.css") ){
	?><link rel="stylesheet" type="text/css" href="css/<?=$storeid?>/checkout.css"/><?php
}
?>

<?
require_once("../classes/products.php");
$info = load_orderinfo();
$orderarray = $ulk->getOrder($info->orderid);

$giftCertAmount = $orderarray['AmountCharged'];
?>


<script type='text/javascript' src='/js/jquery.printElement.min.js'></script>
<div id="checkout_container">

<div id="checkouttitle">Thank You</div>

       <div id="orderinfo">
            <div id="ordernumber">Order Number: <?= $info->order_num; ?></div>
            <div id="orderdate">Date: <?php echo date("F j, Y, g:i a"); ?></div>
       </div>


<table class="cartitems cartheader" border="0">
<thead>
    	<th class="cart-header cart-img">&nbsp;</th>
        <th class="cart-header cart-desc">Product Description</th>
        <th class="cart-header cart-qty">Qty</th>
        <th class="cart-header cart-price">Price</th>
    </thead>
    <tbody></tbody>
</table>
<div id="cartitemscont">
<table class="cartitems" border="0">
    <tbody>
<?php
	foreach($_SESSION["cart"]->items as $productid => $item){
		$price = $item->get_price();
		$qty = $item->get_qty();
		$total = $item->item_total();
		$name = $item->get_name();
		$imgthumb = $products->getProductImage($item->get_id());
		$propnames = $_SESSION["cart"]->propertynames();
?>
    	<tr class="cartitem">
        	<td class="cart-img">
            	<img src="<?= $imagedir.$imgthumb['ThumbPath']; ?>" />
            	<?
            		$gettracking = mysqli_query($ulk->db_cnx,"SELECT * FROM StoresProducts sp JOIN StoresCategories sc ON sc.ID = sp.StoreCategoryID WHERE sp.ProductID = '".$item->get_id()."' and sc.ID IN (1248,1249,1250,1251,1252)");
            		if(mysqli_num_rows($gettracking) >= 1)
					{
						echo '<iframe src="//givve.go2cloud.org/SLDe?adv_sub='.$info->order_num.'&amount='.$_SESSION['cart']->grandtotal.'" scrolling="no" frameborder="0" width="1" height="1"></iframe>';
					}
            	?>
            </td>
            <td class="cart-desc">
            	<p class="productname"><?= $name; ?></p>
                <p class="productdesc">
                <?php
				if($propnames){
                	foreach($propnames as $key=>$propname) {
						$properties = $item->get_properties();
                        foreach($properties as $property=>$valueid) {
                        	if ($property == $propname) {
                        		$value = $classes->getValue($valueid);
                        		echo $property . " : " . $value['Name'] . "; ";
                        	}
                        }

                     }
                }?>
                </p>
            </td>
            <td class="cart-qty">
            	<?php echo $qty?>
            </td>
            <td class="cart-price">
            	$<?= number_format($total, 2)?>
            </td>
        </tr>
<?php } ?>

    </tbody>
</table>
</div>


<table id="info">
	<tr>
        <td id="billto">
            Bill To:
        	<div class="address">
			<?php echo stripslashes($info->fname)." ".stripslashes($info->lname);?><br/>
				<?php pv($info->company); ?><br/>
				<?php pv($info->address); ?><br/>
				<?php if ($info->address2 <> "") {
				pv($info->address2);
				echo "<br/>";
				}?>
				<?php pv($info->city);
				echo ", ";
				pv($info->state);
				echo " ";
				echo $info->zipcode."<br/>";?>
				<?php if ($info->country <> "US") {
				pv($info->country);
				} ?>
      		</div>
        </td>
        <td id="shipto">
            Ship To:
            <div class="address"><?php echo $info->ship_fname." ".$info->ship_lname; ?><br>
                <?php pv($info->company); ?><br>
                <?php pv($info->ship_address1) ;?><br>
                <?php if ($info->ship_address2 <> "") {
                pv($info->ship_address2);
                echo "<br>";
                }?>
                <?php pv($info->ship_city);
                echo ", ";
                pv($info->ship_state);
                echo " ";
                echo $info->ship_zip."<br>";
                ?>
                <?php if ($info->ship_country <> "US") { pv($info->ship_country); } ?>
           </div>
        </td>
        <td id="commentsbox">
        	<?php pv($info->customercomments); ?>
        </td>
        <td id="costs">
        	Sub-total: $<?= number_format( $_SESSION["cart"]->total, 2) ?><br />
            Tax: $<?= number_format( $_SESSION["cart"]->taxes(), 2) ?><br />
			<?
            $discount = $_SESSION["cart"]->coupondiscount + $_SESSION["cart"]->purchasediscount;
            if ($discount > .01){ ?>
           		Discount: $<?= number_format( $discount , 2); ?><br />
            <? } else if ($discount < -.01) { ?>
            	Fee: $<?= number_format( abs($discount) ,2 ); ?><br />
            <? } ?>
            Shipping: $<?= number_format($_SESSION["cart"]->totshipping, 2); ?><br />
            <span id="total">Total: $<?= number_format( $_SESSION['cart']->grandtotal , 2); ?>
        </td>
    </tr>
</table>
<div style="width:80%; margin: 0 auto;" class="latetext">
 Thank you for your order!
 </div>
<div id="cartbuttons">
               <div class="printthis" style="cursor:pointer;"><img src="/images/print_icon_label.jpg" /></div>
            </div>

    <!-- <div>
        <p style="padding-top: 20px; font-size: 14px;">
            We typically ship all orders within 1-3 days, however due to high volume from airing on Shark Tank, please allow up to 4-6 weeks (approximately 12/10/2014) for delivery on your order.  We will make sure that all products ship for Holiday delivery. You will receive a shipping confirmation email once your order has shipped. We appreciate your patience as we do our best to get the order to you as soon as possible.  Please don't hesitate to contact us with any questions about your order at <a href="mailto:stellavalle@zindigo.com" target="_blank">stellavalle@zindigo.com</a>.  Again, thank you so much for your patronage! <br><br>Lots of Love, Ashley &amp; Paige
        </p>
    </div> -->
</div>

<script type="text/javascript">
	$(function(){
		$('#checkout_container').click(function() {
			$(".checkout").printElement({
			overrideElementCSS:['css/<?=$_SESSION['shopid'];?>/checkout.css','css/<?=$_SESSION['shopid'];?>/style.css']
			});
		});
	});
</script>

<img src="http://shareasale.com/sale.cfm?amount=<?= number_format( $_SESSION['cart']->grandtotal , 2); ?>&tracking=<?= $info->order_num; ?>&transtype=sale&merchantID=61619" width="1" height="1">
