<?php $contents = $classes->getDynamicContent($storeid,$_GET['ContentID']);
$content = mysqli_fetch_array($contents);
?>
<style>
.videosmall {
text-transform: uppercase; 
}
.spacing-fix {
margin-top: 160px;
margin-bottom: 15px;
}
.nbs-flexisel-nav-left {
display:none; 
}

.nbs-flexisel-nav-right {
display:none; 
}
</style>
<script type="text/javascript" src="/js/80/flowplayer.js"></script>
<link rel="stylesheet" href="//releases.flowplayer.org/5.4.3/skin/minimalist.css">
	<div class="row spacing-fix collapse" style="max-width: 1170px;">
		<div class="small-12 large-11 large-centered columns" style="width: 70%;">
            <ul id="Collections">
                <li class="videosmall" name='videos/80_Entrepreneur.mp4' title="To succeed in Fashion: Find Mentors, Vendors and Business Partners You Trust">
					<img src="videos/80_sv_thumb_1_to_succeed_in_fashion.jpg"><br><div style="padding: 0 5px">To succeed in Fashion: Find Mentors, Vendors and Business Partners You Trust</div>
				</li>
				<li class="videosmall" name='videos/80_Entrepreneur2_232787.mp4' title="Breaking into Fashion? Network and don't give up">
					<img src="videos/80_sv_thumb_2_breaking_into_fashion.jpg"><br><div style="padding: 0 5px">Breaking into Fashion? Network and don't give up</div>
				</li>
				<li class="videosmall" name='videos/80_Entrepreneur_233017.mp4' title="Pitching a Fashion Line to Investors? Brush Up on Your Storytelling Skills">
					<img src="videos/80_sv_thumb_3_pitching_a_fashion_line.jpg"><br><div style="padding: 0 5px">Pitching a Fashion Line to Investors? Brush Up on Your Storytelling Skills</div>
				</li>
            </ul>
		</div>
	</div>
	<div class="row collapse" style="max-width: 1170px;">
	<div class="large-12 columns"  style=" border-top: 1px solid #000; padding: 0;">
	<div style="font-family: 'GFS Didot', serif; text-transform: uppercase; font-size: 16px; margin: 15px 0;" class="titlename">To succeed in Fashion: Find Mentors, Vendors and Business Partners You Trust </div>
	<div class="player">
	   <video autoplay="autoplay" poster="images/dynamic/80_4077_entrepreneur.png" controls="false">
	   	<source type="video/mp4" src="videos/80_Entrepreneur.mp4">
	      <source type="video/webm" src="videos/80_Entrepreneur.webm">
	      <source type="video/ogg" src="videos/80_Entrepreneur.ogv">
	   </video>
	   <div class="skipvideo"></div>
	</div>

	<script type="text/javascript">
		var api;
		$(function () {
		if($.browser.mozilla == true)
		{
			$(".splashpage").css("paddingLeft","10px");
			$(".splashpage").css("paddingRight","11px");
			$(".player").css("paddingLeft","10px");
			$(".player").css("paddingRight","11px");
			//$(".player video").css("paddingLeft","10px");
			//$(".player video").css("paddingRight","11px");
		}
		
		   // install flowplayer to an element with CSS class "player"
		   $(".player").flowplayer({ 
		   		swf: "/js/80/flowplayer.swf",
		   		tooltip : false,
		   		fullscreen : false
		   
		   });
		   $("a[href='http://flowplayer.org']").css("visibility","hidden");
		api = flowplayer();

		});
	</script>
	
	</div>
</div>
	<script src="/js/jquery.flexisel.js"></script>
	<script>
	/*** Carousel Plugin ***/
	$(window).load(function() {
		$("#Collections").flexisel({
		visibleItems: 3,
		close: false,
			enableResponsiveBreakpoints: true,
			responsiveBreakpoints: {
				portrait: {
					changePoint:480,
					visibleItems: 1
				},
				landscape: {
					changePoint:640,
					visibleItems: 2
				},
				tablet: {
					changePoint:768,
					visibleItems: 3
				}
			}
		});
	});
	
		$(".videosmall").on("click", function(event) {
			  event.preventDefault();
				var vidname = $(this).attr("name");
				var titlename = $(this).attr("title");
				$('.titlename').html(titlename);
				
				api.load(vidname);
		});
	</script>