<link rel="stylesheet" type="text/css" href="css/checkout.css"/>
<?php
//77_RaiseMidiRing-G-g-3

if(isset($_SESSION["cart"]->items) && isset($_SESSION["cart"]->items['77_RaiseMidiRing-G-g-3']))
{
    //make sure there is only 1....
    $origprice = $_SESSION["cart"]->items['77_RaiseMidiRing-G-g-3']->oprice;
    $mykey = '77_RaiseMidiRing-G-g-3';
    $_SESSION["cart"]->recalcitem($mykey, 1, $origprice);

    if($_SESSION["cart"]->itemcount() == 1)
    {
        if(isset( $_SESSION['promo_codes']))
        {
            unset($_SESSION['promo_codes']);
            $_SESSION["cart"]->coupondiscount = 0;
            $_SESSION["cart"]->giftCertAmount = 0;
        }
    }

    $_SESSION["cart"]->cleanup();
    $_SESSION["cart"]->calc_total();
}

if(isset( $_SESSION['promo_codes']))
{
    $cart                    =& $_SESSION['cart'];
    $cart->clearMessages();

    $promoCodesToApply = $_SESSION['promo_codes'];
    foreach ($promoCodesToApply as $promoCode) {
        // Skip empty promotion codes
        if (empty($promoCode)) {
            continue;
        }

        $promoCode = trim($promoCode);

        $giftCertValidated = $cart->validateGiftCertificate($promoCode);
        // Successfully applied gift certificate
        if ($giftCertValidated) {
            $cart->addSuccess('Successfully applied gift certificate.');
            continue;
        }

        $promoCodeValidated = $cart->validateCoupon($promoCode);
        // Successfully applied promotion coupon code
        if ($promoCodeValidated) {
            $cart->addSuccess('Successfully applied promotional code.');
            continue;
        }

        $cart->addError('Could not apply promotional code.');
    }
}
                            
                            
if( file_exists("css/".$storeid."/checkout.css") ){
	?><link rel="stylesheet" type="text/css" href="css/<?=$storeid?>/checkout.css"/><?php
}


if( isset($_SESSION['cartback']) && $_SESSION['cartback'] != "") { $cartback = $_SESSION['cartback']; }
else { $cartback = "index.php?storeid=".$storeid."&sess=".session_name()."&page=home"; }

?>

<script language="Javascript">
needcalc=0;
function frmsubmit(action) {
	frm = document.entryform;
	frm.action.value = action;
	frm.submit();
}
function purchase() {
	if (needcalc == 1) {
	alert("Please recalculate the cart");
//	return true;
	} else {
frmsubmit('purchase');
//	self.location.href='index.php?page=purchase_now&storeid=<?php echo $storeid;?>&sess=<?php echo session_name(); if ( ! isset( $_COOKIE[session_name()] ) ) { echo "&".htmlspecialchars(SID); } ?>';
//	return false;
	}
}
</script>

<div id="checkout_container">
<div id="checkouttitle">Shopping Cart</div>


<?php if ($_SESSION["cart"]->itemcount() == 0) { ?>
<div class="labels">
    <div class="labels" style="width: 248px">
        <p style="width:750px" class="lblempty">Your Shopping Cart is Empty</p>
        <p style="width: 750px; padding-top: 10px; margin-bottom: 10px;" class="lbldescription">We invite you to continue shopping <br/></p>
    </div>

        <div class="cartbutton cartbtncont"><a href="<?=$cartback?>">Back</a></div>
</div>

</div>
<?php return false; ?>

<?php } else {


?>
<form name="entryform" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>?page=cart_view&storeid=<?php echo $storeid;?>&sess=<?php echo session_name();?>">
<input type="hidden" name="action" id="actionfield" value="">
<table class="cartitems cartheader" border="0">
	<thead>
    	<th class="cart-header cart-img">&nbsp;</th>
        <th class="cart-header cart-desc">Product Description</th>
        <th class="cart-header cart-qty">Qty</th>
        <th class="cart-header cart-price">Price</th>
    </thead>
    <tbody></tbody>
</table>
<div id="cartitemscont">
<table class="cartitems" border="0">
    <tbody>
<?php
	foreach($_SESSION["cart"]->items as $productid => $item){
		$price = $item->get_price();
		$qty = $item->get_qty();
		$total = $item->item_total();
		$name = $item->get_name();

		$imgthumb = $products->getProductImage($item->get_id());
?>
    	<tr class="cartitem">
        	<td class="cart-img">
            	<img src="<?= $imagedir.$imgthumb['ThumbPath']; ?>" />
            	<input type="hidden" name="id[]" value="<?php echo urlencode($productid); ?>"/>

            </td>
            <td class="cart-desc">
            	<p class="productname"><?= $name; ?></p>
                <p class="productdesc">
                <?php
				if($propnames){
                	foreach($propnames as $key=>$propname) {
						$properties = $item->get_properties();
                        foreach($properties as $property=>$valueid) {
                        	if ($property == $propname) {
                        		$value = $classes->getValue($valueid);
                        		echo $property . " : " . $value['Name'] . "; ";
                        	}
                        }
                     }
                }?>
                </p>
            </td>
            <td class="cart-qty">
            	<input type="text" class="qib" size="3" name="qty[]" value="<?php echo $qty?>" onChange="needcalc=1;" style="text-align:center">

                <div class="cart-update"><a href="javascript: frmsubmit('recalc');">Update</a></div>
            </td>
            <td class="cart-price">
            	$<?= number_format($total, 2)?><input type="hidden" name="price[]" value="<?php echo $price;?>">
                <div class="cart-update"><a href="index.php?page=cart_view&action=remove&productID=<?= htmlspecialchars($productid); ?>&sess=<?= session_name() ?>&storeid=<?= $storeid ?>">Delete</a></div>
            </td>
        </tr>
<?php } ?>

    </tbody>
</table>
</div>

<table id="info">
	<tr>
    	<td id="disclaimer">
        	Our merchandise subtotal does not yet include shipping and handling, tax, or promotional pricing offers, which will be reflected at Checkout. Unless otherwise stated.
        </td>
        <td></td>
        <td id="costs">
       <?
	      $discount = $_SESSION["cart"]->coupondiscount + $_SESSION["cart"]->purchasediscount;
			if ($discount > .01){ ?>
	         <div>Discount: -$<?php printf("%.2f", $discount ); ?></div>
	       <? } else if ($discount < -.01) { ?>
 			  <div>Fee: $<?php printf("%.2f", abs($discount) ); ?></div>
 		<? } ?>
 		
 			<?php if ($_SESSION['cart']->giftCertAmount > .01){ ?>
            	<div>Gift Certificate: -$<?php echo number_format($_SESSION['cart']->giftCertAmount,2);?></div>
            <?php } ?>

        	<div id="total">Total: $<?php printf("%.2f", ($_SESSION["cart"]->recalc_total())); ?></div>
        </td>
    </tr>
</table>

<div style="text-align:right; float:right; display:inline;" id="cartbuttons">
	<? echo "<span style='color: red; font-weight: bold; display: block; margin-bottom: 20px; margin-top: 20px; text-align: right;'>".$msg."</span>"; ?>
	<div class="cartpromo" style="margin-bottom: 5px; float:right;">
		<label for="code_prom" style="float:left; line-height: 25px;"> Gift Card or Promo Code: &nbsp;</label>
		<input type="text" id="gift_certificate_code" name="promo_codes[]" value="<?= (is_array($_SESSION['promo_codes']) && isset($_SESSION['promo_codes'][0])) ? $_SESSION['promo_codes'][0] : '' ?>" style="padding:0px; margin: 0px; border: none; height:25px; float:left; margin-right: 5px; width: 200px; border:1px solid #000;">
		<div class="cartbtncont" style="float:left;">
		<a class="cartbutton cartpromo" onClick="frmsubmit('applypromocodes')" style="width: auto; padding-left: 5px; padding-right: 5px; height: 25px; display:inline-block; line-height: 25px; cursor:pointer; margin: 0px; outline: none; border:none; float:left;">Apply</a>
		</div>
			<div style="clear:both;"></div>
	</div>
	<div style="clear:both;"></div>
	<div class="cartpromo" style="float:right;">
		<label for="gift_certificate_code" style="float:left; line-height: 25px;">Gift Card or Promo Code: &nbsp;</label>
		<input type="text" id="gift_certificate_codes" name="promo_codes[]" value="<?= (is_array($_SESSION['promo_codes']) && isset($_SESSION['promo_codes'][1])) ? $_SESSION['promo_codes'][1] : '' ?>" style="padding:0px; margin: 0px; border: none; height:25px; float:left; margin-right: 5px; width: 200px; border:1px solid #000;" />
		<div class="cartbtncont" style="float:left;">
		<a class="cartbutton cartpromo" onClick="frmsubmit('applypromocodes')" style="width: auto; padding-left: 5px; padding-right: 5px; height: 25px; display:inline-block; line-height: 25px; cursor:pointer; margin: 0px; outline: none; border:none; float:left;">Apply</a>
		</div>
	</div>
</div>
<div style="clear:both;"></div>


<div id="cartbuttons">
    <div class="cartbutton cartbtncont"><a href="<?=$cartback?>">Continue Shopping</a></div>
    <div class="cartbutton cartbtncheckout"><a href="javascript: purchase();" >Checkout</a></div>
</div>
<?php } ?>
</form>
</div>