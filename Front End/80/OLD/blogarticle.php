<?
	$domain = "stellavallezindigo";
	$count=0;
    $limit=50;
	include("../feeds/getFeed.php");
	$thisfeed = new CachedFeed();
	$blog = $thisfeed->getFeed("http://stellavallezindigo.wordpress.com/feed/","StellaValleZ-BLOG");
	$xml = simplexml_load_string($blog,null, LIBXML_NOCDATA);
	$entries = $xml->entry ? $xml->entry : $xml->channel->item;

    function getNiceURL($url){

        $str = explode("/",$url);
        return $str[count($str)-2];
    }
	?>

<div class="row collapse our-story">
    <div class="large-12 columns">
        <h1>#STELLAVALLE Blog</h1>
    </div>
</div>

<div class="row blog spacing-fix">
    <div class="small-12 large-10 large-centered columns" id="container" style="padding-left:0px; padding-right:0px;">
        <?php
        foreach($entries as $entry)
        {
            if (strcmp(getNiceURL($entry->link), $_REQUEST['article']) !== 0) continue;
            $date = new DateTime($entry->pubDate);
            ?>
            <div class="row" id="blog-row-<?php echo $count;?>" style="border-bottom:1px solid #000;padding-top:10px;">
                <div style="text-align: center;">
                    <h1 class="articlename"><?php echo $entry->title ; ?></h1>
                    <div style="min-width: 80px; display: block; margin: 0 auto; padding-bottom: 15px;">
                        <p style="background: url(/images/page/<?php echo $storeid;?>/blog_date.png);width:123px;height:66px;background-position: -50px -50px;text-transform: uppercase; margin: 0 auto;">
                            <span style="padding-top: 13px;line-height:1em;"><?php echo $date->format('M');?></span>
                            <span style="color: white;padding-left: 40px;"><?php echo $date->format('d');?></span>
                        </p>
                    </div>
                </div>
        	    <div class="row" style="padding-bottom: 20px">
        	        <div class="small-12 columns">
        	            <div id="blog" style="padding: 5px 5px 5px 5px;border: 1px solid black">
                            <?
                                $article = $entry->children('content',true)->encoded;

                                echo $article;

                            ?>
                        </div>
        	        </div>
                </div>
            </div>
            <?php
                break;
        } ?>

    </div>
</div>

<style>
    #blog a
    {
        position: relative;
        display: inline-block;
    }

    #blog a .imgsharer
    {
        position: absolute;
        background-color: #000;
        background: rgba(0,0,0,0.8);
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        z-index: 999;
    }

    #blog a .imgsharer a
    {
        color: #FFF;
    }

    #blog p{
        text-align: center;
    }

    #blog a .imgsharer .social
    {
        width: 98px;
        height: 36px;
        text-decoration: none;
        display: inline-block;
        overflow: hidden;
        text-indent: -9999px;
        background: url('/images/page/80/social_spritesheet.png') no-repeat;
        cursor: pointer;
    }

    #blog a .imgsharer .blogsocial
    {
        text-align: center;
        position: absolute;
        height: 36px;
        width: 100%;
        top: 0px;
        bottom: 0px;
        left: 0px;
        right: 0px;
        margin: auto;
    }

    #blog a .imgsharer .social
    {
        margin-right: 5px;
    }

    #blog a .imgsharer .social:last-child
    {
        margin-right: 0px;
    }

    #blog a .imgsharer .social.fb
    {
        background-position: -5px -5px;
    }

    #blog a .imgsharer .social.twitter
    {
        background-position: -114px -5px;
    }

    #blog a .imgsharer .social.pinterest
    {
        background-position: -5px -97px;
    }
    @media only screen and (max-width: 500px) {
        #blog a .imgsharer .social
        {
            width: 36px;
        }
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        $("#blog a").hover(
            function() {
                if($(this).find("img").length <= 0) return true;
                if($(this).attr("rel") == "nofollow") return true;

                $(this).attr("href","javascript:void(0)");

                var thisimgurl = $(this).find("img").attr("src");
                console.log(thisimgurl);

                var articlename = $(".articlename").text();

                var inside = "";
                inside += '<div class="imgsharer">';
                inside += '<div class="blogsocial">';
                inside += '<a class="social fb" href="http://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(window.location.href)+'" target="_blank">Facebook</a>';
                inside += '<a class="social twitter" href="https://twitter.com/intent/tweet?url='+encodeURIComponent(window.location.href)+'&text='+encodeURIComponent(articlename)+'" target="_blank">Twitter</a>';
                inside += '<a class="social pinterest" href="http://pinterest.com/pin/create/button/?url='+encodeURIComponent(window.location.href)+'&media='+encodeURIComponent(thisimgurl)+'&description='+encodeURIComponent(articlename)+'" target="_blank">Pinterest</a>';
                inside += '</div>';
                inside += '</div>';
                $(this).find(".imgsharer").remove();
                $(this).append(inside);
            }, function() {
                if($(this).find("img").length <= 0) return true;
                if($(this).attr("rel") == "nofollow") return true;
                $(this).find(".imgsharer").remove();
            }
        )
    });
</script>

