<link type="text/css" rel="stylesheet" href="/min/b=css&amp;f=cyberpayment.css,80/checkout.css,80/foundation.css,80/app.css,80/component.css,80/carousel.css" />
<?php
$flag = "shop";
if(!isset($isFacebook)) { $isFacebook = true; }
if(!isset($setCrossSale)) { $setCrossSale = true; }
if(!isset($lblAltView)) { $lblAltView = ""; }
if(!isset($lblCrossSale)) { $lblCrossSale = "You may also like"; }
if(!isset($lblBack)) { $lblBack = "<img src=\"/images/page/80/back.png\" />"; }
if(!isset($lblPrev)) { $lblPrev = "<img src=\"/images/page/80/previous.png\" />"; }
if(!isset($lblNext)) { $lblNext = "<img src=\"/images/page/80/next.png\" />"; }

$inshops = false;
if (strtolower(substr($_SERVER['SERVER_NAME'], 0, 5)) == "shops") {
	$inshops = true;
}

if(!isset($lblXProds)) { $lblXProds = "You may also like"; }


require_once("../classes/storeprods.php");
if (!isset($products)) {
	include_once ("../classes/products.php");
	$products = new products;
}

$properties = $products->getProductProperties($productid);
$numprops = mysqli_num_rows($properties);
$prodinv = $products->getProductInventory($productid);
$numitems = mysqli_num_rows($prodinv);

if(!isset($prod['Name']))
	header("Location: http://stellavalle.com/index.php?storeid=".$storeid."&page=categoryproducts&action=all&categoryID=1013&sess=".session_name().$sid);
?>

				<?
$prod['OrigPrice'] = str_replace(".00","",number_format($prod['OrigPrice'],2));
$prod['StorePrice'] = str_replace(".00","",number_format($prod['StorePrice'],2));
$prod['OrigPrice'] = floatval($prod['OrigPrice']);
$prod['StorePrice'] = floatval($prod['StorePrice']);
?>


<script language="JavaScript">
	function ValidatePurchase(prodids) {
		if($('prod-btn-cart').is(':hidden')){ return false; }
		document.getElementById("addtobag").submit();
		//checkThirdPatyCookies();
	}


	var invitems = new Array(<?php echo $numitems; ?>);
	<?php $i = 0;
	while($item = mysqli_fetch_array($prodinv)) {
		$itemkey = $item['Value1'].":".$item['Value2'].":".$item['Value3'].":".$item['Value4'].":".$item['Value5'].":".$item['Value6'].":".$item['Value7'];
		?>
			invitems['<?php echo $itemkey; ?>'] = new Object();
			invitems['<?php echo $itemkey; ?>'].qty = <?php echo $item['Qty']; ?>;
			invitems['<?php echo $itemkey; ?>'].status = "<?php echo $item['StatusText']; ?>";
			invitems['<?php echo $itemkey; ?>'].etadate = "<?php echo $item['ETAFormatted']; ?>";
		<?php $i = $i +1;
	} ?>


	$(document).ready(function(){
		getItemAvailable();
	});

	function getItemAvailable() {

		var value1 = document.getElementById('val1');
		if (value1 == null) { value1 = '0';} else { value1 = value1.options[value1.selectedIndex].value; }
		var value2 = document.getElementById('val2');
		if (value2 == null) { value2 = '0';} else { value2 = value2.options[value2.selectedIndex].value; }
		var value3 = document.getElementById('val3');
		if (value3 == null) { value3 = '0';} else { value3 = value3.options[value3.selectedIndex].value; }
		var value4 = document.getElementById('val4');
		if (value4 == null) { value4 = '0';} else { value4 = value4.options[value4.selectedIndex].value; }
		var value5 = document.getElementById('val5');
		if (value5 == null) { value5 = '0';} else { value5 = value5.options[value5.selectedIndex].value; }
		var value6 = document.getElementById('val6');
		if (value6 == null) { value6 = '0';} else { value6 = value6.options[value6.selectedIndex].value; }
		var value7 = document.getElementById('val7');
		if (value7 == null) { value7 = '0';} else { value7 = value7.options[value7.selectedIndex].value; }

		var arrKey = value1+':'+value2+':'+value3+':'+value4+':'+value5+':'+value6+':'+value7;
		var istatus = document.getElementById('prod-dtl-ship');
		var addcart = document.getElementById('prod-btn-cart');

<?php if ($prod['Custom']) { ?>
	 	invitems[arrKey] = undefined;
	 	<?php } ?>

		if(invitems[arrKey] == undefined) {
			istatus.innerHTML = 'Usually ships in <?php echo $prod['Ships']; ?>';
			addcart.style.visibility = 'visible';
		} else {
			switch (invitems[arrKey].status){
				case "Active":
					if(invitems[arrKey].qty > 0) {
						istatus.innerHTML = 'Usually ships in <?php echo $prod['Ships']; ?>';
						addcart.style.visibility = 'visible';
						validateQty();
					} else {
						istatus.innerHTML = 'This item is out of stock. Please choose different options.';
						addcart.style.visibility = 'hidden';
					}
					break;
				case "Active but can't add":
					istatus.innerHTML = 'This item is temporarily unavailable. Please choose different options.';
					addcart.style.visibility = 'hidden';
					break;
				case "Inactive":
					istatus.innerHTML = 'This item is unavailable. Please choose different options.';
					addcart.style.visibility = 'hidden';
					break;
				case "On Backorder":
					istatus.innerHTML = 'This item is temporarily out of stock. The expected ship date is ' + invitems[arrKey].etadate;
					if(invitems[arrKey].qty > 0) {
						addcart.style.visibility = 'visible';
					} else {
						addcart.style.visibility = 'hidden';
					}
					break;
				case "Obsolete":
					istatus.innerHTML = 'This item is unavailable. Please choose different options.';
					addcart.style.visibility = 'hidden';
					break;
				default:
					alert(invitems[arrKey].status);
					istatus.innerHTML = 'Usually ships in <?php echo $prod['Ships']; ?>';
					addcart.style.visibility = 'visible';
				}
			}
		<?php if (stripslashes($prod['ProductStatusName']) == "Active but can't add"){?>addcart.style.visibility = 'hidden';<?php  } ?>
		<?php if ($prod['ProductStatusName'] == "On Backorder") {?>istatus.innerHTML = 'This item is temporarily out of stock. The expected ship date is <?php echo Date('m/d/Y',strtotime($prod['BackorderETA']))?>';<?php } ?>

		}

		function validateQty() {
			var currQty = document.getElementById('itemqty');
			var value1 = document.getElementById('val1');
			if (value1 == null) { value1 = '0';} else { value1 = value1.options[value1.selectedIndex].value; }
			var value2 = document.getElementById('val2');
			if (value2 == null) { value2 = '0';} else { value2 = value2.options[value2.selectedIndex].value; }
			var value3 = document.getElementById('val3');
			if (value3 == null) { value3 = '0';} else { value3 = value3.options[value3.selectedIndex].value; }
			var value4 = document.getElementById('val4');
			if (value4 == null) { value4 = '0';} else { value4 = value4.options[value4.selectedIndex].value; }
			var value5 = document.getElementById('val5');
			if (value5 == null) { value5 = '0';} else { value5 = value5.options[value5.selectedIndex].value; }
			var value6 = document.getElementById('val6');
			if (value6 == null) { value6 = '0';} else { value6 = value6.options[value6.selectedIndex].value; }
			var value7 = document.getElementById('val7');
			if (value7 == null) { value7 = '0';} else { value7 = value7.options[value7.selectedIndex].value; }

			var arrKey = value1+':'+value2+':'+value3+':'+value4+':'+value5+':'+value6+':'+value7;


			if(isNaN(currQty.value) || jQuery.trim(currQty.value) == '') { currQty.value = 1; }
			if(invitems[arrKey] != undefined) {
				if(currQty.value > invitems[arrKey].qty) {
				currQty.value = invitems[arrKey].qty;
				alert('Only ' + invitems[arrKey].qty + ' of these items are available');
			}
		}

	}
</script>

<?
if(isset($_GET['ImageID']))
	$imageid = $_GET['ImageID'];
//print 'alert(ImageID: ' .$imageid. ' )';
//print 'alert(CatID: ' .$_GET['categoryID']. ' )';
if($_GET['categoryID'] != "" && $_GET['categoryID'] != null)
{
	if ($_GET['subsubcatID'] <> "" && $_GET['subsubcatID'] <> 0) {
		$catname=$classes->getStoreCategoryNameCached($_GET['subsubcatID']);
		$thiscatid = $_GET['subsubcatID'];
		//print 'alert("subsubcatID' .$imageid. ' ")';
	}elseif ($_GET['subcatID'] <> "" && $_GET['subcatID'] <> 0) {
		$catname=$classes->getStoreCategoryNameCached($_GET['subcatID']);
		$thiscatid = $_GET['subcatID'];
		$subcatid = $_GET['subcatID'];
		///print 'alert("subcatID' .$imageid. ' ")';
	} elseif ($_GET['categoryID'] <> "" && $_GET['categoryID'] <> 0) {
		$catname=$classes->getStoreCategoryNameCached($_GET['categoryID']);
		$thiscatid = $_GET['categoryID'];
	}
	$img = $products->viewImage($imageid);
}

if($isFacebook) {
	$appid= $store['FBAPPID'];
	//		if (!isset($store['FBAPPID'])) {
	//			$appid = 127229660667006;
	//		}
	$page = $_SESSION['pageid'];
	$fbpageid = $_SESSION['pageid'];
	//		if (!isset($_SESSION['pageid'])) {
	//			$fbpageid = 12312312;
	//		}
	if(isset($_SESSION['shopid']))
		$insidestore = $storeid;
}
$catid = $_GET['categoryID'];
$subcatid = $_GET['subcatID'];
$subsubcatid = $_GET['subsubcatID'];
if ($catid == "") { $catid = 0; }
if ($subcatid == "") { $subcatid = 0; }
if ($subsubcatid == "") { $subsubcatid = 0; }

$fburl = $httppath."index.php/productslist/".$storeid."/".$catid."/".$productid."/".$subcatid."/".$subsubcatid."/";
if($inshops) {$fburl ="https://shops.zindigo.com/".$_SESSION['uname']."/productslist/".$storeid."/".$catid."/".$productid."/".$subcatid."/".$subsubcatid."/"; }
?>
<style>
#cloud-zoom-big
{
	min-width: 100%;
	max-width: 100%;
	width: 100%;
}
</style>
<?php if($isFacebook) { ?>
	<script type="text/javascript">
    $(document).ready(function(e) {
        $('.prod-dtl-social').hover(function(e) {
            $('.prod-dtl-social').height(300).css('z-index', '99999999').width(470);
            $('.prod-dtl-social iframe').height(300).css('z-index', '99999999').width(470);
        }, function(e) {
            $('.prod-dtl-social').height(35).css('z-index', '9').width(287);
            $('.prod-dtl-social iframe').height(35).css('z-index', '9').width(287);
        });
    });
    </script>
<? } ?>
<?
if($storeid == 72 && isset($_REQUEST['cartback']) && $_REQUEST['cartback'] != "")
	$linkBack = $_REQUEST['cartback'];
?>
<script type="text/javascript" src="/js/cloud-zoom.1.0.3.js"></script>
<div class="row collapse spacing-fix">
          <div class="large-12 columns">
          </div>
    </div>
<div id="prod-dtls" class="row shop-detail">
<? /*
<div id="prod-dtl-nav">
<?php if(isset($linkBack)) {?><div id="prod-dtl-back" class="prod-dtl-navlink"><a href="<?= $linkBack ?>"><?= $lblBack ?></a></div><?php } ?>
<?php if(isset($linkPrev)) {?><div id="prod-dtl-prev" class="prod-dtl-navlink"><a href="<?= $linkPrev ?>"><?= $lblPrev ?></a></div><?php } ?>
<?php if(isset($linkNext)) {?><div id="prod-dtl-next" class="prod-dtl-navlink"><a href="<?= $linkNext ?>"><?= $lblNext ?></a></div><?php } ?>
</div>
*/ ?>
	<?php
if($isFacebook && !$inshops) {
	?>
            <div class="prod-dtl-social">
                <iframe allowtransparency='true' style="z-index:99999;" frameborder='0' width='470' scrolling='no' height='35' src='https://zindigo.com/facebook/likebutton.php/<?=$fbpageid?>/<?=$appid?>/<?=$productid?>/<?=$catid?>/<?=$subcatid?>/<?=$subsubcatid?>/productslist/<?=$imageid?>/light<?= (isset($_SESSION['shopid']) ? "/" . $_SESSION['shopid'] : null); ?><?= (isset($insidestore) ? "/" . $insidestore : null); ?>'></iframe>
             </div>

            <?php
} elseif($isFacebook && $inshops) { ?>
		<div class="prod-dtl-social">
          <iframe allowtransparency='true' style="z-index:99999;" frameborder='0' width='470' scrolling='no' height='35' src='https://zindigo.com/facebook/shopslikebutton.php/<?=$fbpageid?>/<?=$appid?>/<?=$productid?>/<?=$catid?>/<?=$subcatid?>/<?=$subsubcatid?>/productslist/<?=$imageid?>/light/<?= $_SESSION['uniquename']; ?><?= (isset($_SESSION['shopid']) ? "/" . $_SESSION['shopid'] : null); ?><?= (isset($insidestore) ? "/" . $insidestore : null); ?>'></iframe>
          <? /*
          <iframe src="//www.facebook.com/plugins/like.php?href=<?= urlencode($fburl) ?>&show_faces=false&layout=button_count&action=like&colorscheme=light&font&width=50&height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:50px; height:21px;" allowTransparency="true"></iframe>
          */ ?>

        </div>

		<? }
		$img=$products->viewImage($imageid); ?>


        <div class="small-12 large-6 columns">

                <div class="row collapse">

					<div class="small-12 large-8 columns">
                    	<div id="big-image"> <a href='<?php echo $imagedir.$img['LargePath']?>' class ='cloud-zoom' id='zoom1' rel="softFocus:false, position:'inside', zoomWidth:'auto', zoomHeight:'auto', tintOpacity:'1', lensOpacity:'1'"><img src="<?= $imagedir.$img['LargePath']?>"></a></div>
                    </div>
<?php
if(count($imgarray) > 1 ) { ?>
                    <div class="small-12 large-4 columns"><!--<p><?=$lblAltView?></p>-->
                        <ul id="images-shop-thumbails">

	<?php
foreach($imgarray as $thumb) {
	$imgthumb = $products->viewImage($thumb);
	?>
				<li><a href="<?=$imagedir.$imgthumb['LargePath']?>" class="cloud-zoom-gallery" rel="useZoom: 'zoom1', smallImage: '<?=$imagedir.$imgthumb['LargePath']?>'">
					<img src="<?=$imagedir.$imgthumb['ImagePath']?>" alt="" >
				</a></li>
				<?php
} ?>



                        </ul>
                    </div>
	<?php	} ?>
                </div>
        </div>

        <div class="small-12 large-6 columns shop-detail-padding">
                <div class="row collapse row-price show-for-mobile">
                	<div class="small-8 large-8 columns"><?= $productname; ?></div>
                    <div class="small-4 large-4 columns text-right"><span>					<?php if($prod['OrigPrice']>0){?>
                        <span class="origprice" style="text-decoration:line-through;">$<?= number_format($prod['OrigPrice'],2)?></span>
                        <span class="saleprice">$<?= number_format($prod['StorePrice'],2)?></span>
                   		<img src='/images/sale.gif' align='absmiddle'>
                    <?php } else {?>
                   		$<?= number_format($prod['StorePrice'], 2)?>
                    	<?php if($prod['IsOnSale']){ echo "<img src='/images/sale.gif' align='absmiddle'>";}?>
                    <?php } ?>
                    <? if(isset($prod['FinalSale']) && $prod['FinalSale'] == 1) { ?>
                    	<img src="/images/page/<?= $storeid; ?>/finalsale.png" alt="Final Sale No Returns" title="Final Sale No Returns" align="absmiddle" />
                    <? } ?>

                    <?php if($prod['IsNew']){ echo "<img src='/images/icon_new.gif' align='absmiddle'>";} ?>
                    <?php if($prod['IsHot']){ echo "<img src='/images/icon_hot.gif' align='absmiddle'>";} ?>
                    <?php if($freeshipping){ echo "<img src='/images/icon_free_shipping.gif' align='absmiddle' alt='Free Shipping'>";}?>
                    </span></div>
                </div>

                <div class="product-description">

                    <!-- Only for Desktop -->
                	<h1><?php echo $productname;?></h1>
					<?= $prod['Description']; ?>

                    <span>					<?php if($prod['OrigPrice']>0){?>
                        <span class="origprice" style="text-decoration:line-through;">$<?= $prod['OrigPrice']?></span>
                        <span class="saleprice">$<?= $prod['StorePrice']?></span>
                   		<img src='/images/sale.gif' align='absmiddle'>
                    <?php } else {?>
                   		$<?= number_format($prod['StorePrice'], 2)?>
                    	<?php if($prod['IsOnSale']){ echo "<img src='/images/sale.gif' align='absmiddle'>";}?>
                    <?php } ?>
                    <? if(isset($prod['FinalSale']) && $prod['FinalSale'] == 1) { ?>
                    	<img src="/images/page/<?= $storeid; ?>/finalsale.png" alt="Final Sale No Returns" title="Final Sale No Returns" align="absmiddle" />
                    <? } ?>

                    <?php if($prod['IsNew']){ echo "<img src='/images/icon_new.gif' align='absmiddle'>";} ?>
                    <?php if($prod['IsHot']){ echo "<img src='/images/icon_hot.gif' align='absmiddle'>";} ?>
                    <?php if($freeshipping){ echo "<img src='/images/icon_free_shipping.gif' align='absmiddle' alt='Free Shipping'>";}?>
                    </span>
                </div>

                <form id="addtobag" name="AddProduct<?=$productid?>" method="Get" action="/index.php" class="custom">
                	<input type="hidden" name="page" value="cart_add">
                	<input type="hidden" name="productID" value="<?=$productid?>">
                	<input type="hidden" name="sess" value="<?=session_name()?>">
                	<input type="hidden" name="storeid" value="<?php echo $storeid; ?>">
                <? if(isset($_REQUEST['ostoreid'])){ ?>
                	<input type="hidden" name="ostoreid" value="<?= $_REQUEST['ostoreid']; ?>">
                <? } ?>
                      <div class="row collapse">
                      	<div class="small-12 large-5 columns shop-forms-spacing">
              <?php  mysqli_data_seek($properties, 0);
              $v=1;
              while ($property = mysqli_fetch_array($properties)){
	              $values=$products->getAvailablePropertyValuesSV($property['ID'],$productid);?>
                    <span class="tpdfix"><?php echo stripslashes($property['Name'])?>:</span>
	                <select name="<?php echo stripslashes($property['Name'])?>" id="val<?php echo $v;?>" onChange="getItemAvailable();" class="medium choose">
	                <!--	<option value="">CHOOSE <?php echo stripslashes($property['Name'])?></option> -->
	                
	                <?php // while ($value = mysqli_fetch_array($values))
foreach ($values as $value)
	                { ?>
	                    <option value="<?php echo $value['ID'];?>">
	                    <?php echo stripslashes($value['Name']);?>
	                    <?php if($value['Price'] > 0.00) { echo " (Add $".sprintf('%.2f',$value['Price']).")";} elseif ($value['Price'] < 0.00) { echo " (Subtract $".sprintf('%.2f',abs($value['Price'])).")";} ?></option>
	                    <?php }?>
	                </select>
	        			<? if($property['Name'] == "Size" && $sizechart <> "") { ?>
				<a class="sizechart" onclick="window.open('<?= $sizechart; ?>','','width=800,height=400');" style="cursor:pointer;">Size Guide</a>
			<? } ?>
            <?php
$v = $v + 1;
}?>

                      	</div>






                        <div class="small-12 large-6" style="float:left;">
                    <!--      <select id="customDropdown1" class="medium quantity">
                            <option selected>Qty.</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                          </select> -->
                          <span class="tpdfix">Quantity:</span><input type="text" class="medium quantity" name="qty" id="itemqty" value="1" size="5" onchange="validateQty();" style="text-align:center">
                      	</div>




                      </div>

    			<?php if ($prod['Customizable']== 1) {?>
    	        <div class="row collapse">
                      	<div class="small-12 large-12 columns shop-forms-spacing">
                <?php echo stripslashes($prod['CustomPrompt'])?>:
                <div class="small-6 large-6 columns shop-forms-spacing"><span class="tpdfix">INITIALS:</span><textarea id="CustomComments" name="CustomComments" style="font-family:Didot;font-size:18px;width:100px;"></textarea></div> <div class="small-6 large-6 columns shop-forms-spacing"><span class="tpdfix">Style:</span><select name="Custom[Custom_Other]" onChange="CustomComments.style.fontFamily=this.options[this.selectedIndex].value;" class="medium choose"><option value="Didot">normal</option><option value="cursive">script</option></select></div>

			<table>
				<?php if ($prod['CustomName']){echo '<tr><td>Name: </td><td nowrap><input name="Custom[Custom_Name]" maxlength="'.$prod['Name_Chars'].'">(Max. Chars '.$prod['Name_Chars'].')</td></tr>';} else { echo '<input type="hidden" name="Custom[Custom_Name]" value="">';}?>
				<?php if ($prod['CustomAddress1']){echo '<tr><td>Address1: </td><td nowrap><input name="Custom[Custom_Address1]" maxlength="'.$prod['Address1_Chars'].'">(Max. Chars '.$prod['Address1_Chars'].')</td></tr>';} else { echo '<input type="hidden" name="Custom[Custom_Address1]" value="">';}?>
				<?php if ($prod['CustomAddress2']){echo '<tr><td>Address2:</td><td nowrap><input name="Custom[Custom_Address2]" maxlength="'.$prod['Address2_Chars'].'">(Max. Chars '.$prod['Address2_Chars'].')</td></tr>';} else { echo '<input type="hidden" name="Custom[Custom_Address2]" value="">';}?>
				<?php if ($prod['CustomCity']){echo '<tr><td>City:</td><td nowrap><input name="Custom[Custom_City]" maxlength="'.$prod['City_Chars'].'">(Max. Chars '.$prod['City_Chars'].')</td></tr>';} else { echo '<input type="hidden" name="Custom[Custom_City]" value="">';}?>
				<?php if ($prod['CustomState']){echo '<tr><td>State:</td><td nowrap><input name="Custom[Custom_State]" maxlength="'.$prod['State_Chars'].'">(Max. Chars '.$prod['State_Chars'].')</td></tr>';} else { echo '<input type="hidden" name="Custom[Custom_State]" value="">';}?>
				<?php if ($prod['CustomZip']){echo '<tr><td>Zip:</td><td nowrap><input name="Custom[Custom_Zip]" maxlength="'.$prod['Zip_Chars'].'">(Max. Chars '.$prod['Zip_Chars'].')</td></tr>';} else { echo '<input type="hidden" name="Custom[Custom_Zip]" value="">';}?>
				<?php if ($prod['CustomPhone']){echo '<tr><td>Phone:</td><td nowrap><input name="Custom[Custom_Phone]" maxlength="'.$prod['Phone_Chars'].'">(Max. Chars '.$prod['Phone_Chars'].')</td></tr>';} else { echo '<input type="hidden" name="Custom[Custom_Phone]" value="">';}?>
				<?php if ($prod['CustomFax']){echo '<tr><td>Fax:</td><td nowrap><input name="Custom[Custom_Fax]" maxlength="'.$prod['Fax_Chars'].'">(Max. Chars '.$prod['Fax_Chars'].')</td></tr>';} else { echo '<input type="hidden" name="Custom[Custom_Fax]" value="">';}?>
				<?php if ($prod['CustomEmail']){echo '<tr><td>Email:</td><td nowrap><input name="Custom[Custom_Email]" maxlength="'.$prod['Email_Chars'].'">(Max. Chars '.$prod['Email_Chars'].')</td></tr>';} else { echo '<input type="hidden" name="Custom[Custom_Email]" value="">';}?>
				<?php if ($prod['CustomWeb']){echo '<tr><td>Web:</td><td nowrap><input name="Custom[Custom_Web]" maxlength="'.$prod['Web_Chars'].'">(Max. Chars '.$prod['Web_Chars'].')</td></tr>';} else { echo '<input type="hidden" name="Custom[Custom_Web]" value="">';}?>
<!--				<?php if ($prod['CustomOther']){echo '<tr><td>'.$prod['CustomOtherName'].':</td><td nowrap><input name="Custom[Custom_Other]" maxlength="'.$prod['Other_Chars'].'">(Max. Chars '.$prod['Other_Chars'].')</td></tr>';} else { echo '<input type="hidden" name="Custom[Custom_Other]" value="">';}?>-->
</table>
				</div></div>
			<?php } else { ?>
			<input type="hidden" name="CustomComments" value="">
			<input type="hidden" name="Custom[Custom_Name]" value="">
			<input type="hidden" name="Custom[Custom_Address1]" value="">
			<input type="hidden" name="Custom[Custom_Address2]" value="">
			<input type="hidden" name="Custom[Custom_City]" value="">
			<input type="hidden" name="Custom[Custom_State]" value="">
			<input type="hidden" name="Custom[Custom_Zip]" value="">
			<input type="hidden" name="Custom[Custom_Phone]" value="">
			<input type="hidden" name="Custom[Custom_Fax]" value="">
			<input type="hidden" name="Custom[Custom_Email]" value="">
			<input type="hidden" name="Custom[Custom_Web]" value="">
			<input type="hidden" name="Custom[Custom_Other]" value="">
								       <?php } ?>
						<div class="row collapse">
							<div class="small-12 large-8">
                      		<a href="Javascript: ValidatePurchase(<?= $productid; ?>)" id="prod-btn-cart" class="large button expand add-cart" style="width:100%;">ADD TO CART</a>
                      		</div>
						</div>
					<div class="clear"></div>
                      <div class="row collapse">
						<style>
							.button-container{
								float:left;
								width: 20px;
								height: 30px;
								color:#666;
							}
							.button-container img{
								width: 100%;
							}
							.button-container a{
								color:#666;
							}

							.button-container a:hover{
								color:#8c8c8c;
							}
							.topspace{
								padding-top:5px;
							}
							.reset{
								clear: both;
							}
						</style>
						<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
						<div class="custom-prod-dtl-socials" style='font-size:20px;color:#666 !important;'>
							<div class='button-container topspace' style='margin-right: 18px;'><a target="_top" href="http://www.facebook.com/sharer/sharer.php?u=<?= urlencode($fburl) ?>"><i class="fa fa-facebook"></i></a></div>
							<div class='button-container topspace' style='margin-right: 20px;'><a target="_top" href="https://twitter.com/intent/tweet?original_referer=<?= urlencode($fburl) ?>&source=tweetbutton&text=<?=urlencode($productname)?>&url=<?= urlencode($fburl) ?>"><i class="fa fa-twitter"></i></a></div>
							<div class='button-container'><a href="#" id="prod-btn-share"><img src='/images/share_icon_retina.png'></a></div>
							<div class='reset'></div>
						</div>
						<script type="text/javascript">
							$('#prod-btn-share img').hover(
								function() {
									$(this).attr('src','/images/share_icon_retina_hover.png');
								},function() {
									$(this).attr('src','/images/share_icon_retina.png');
								}
							);
						</script>
						<style type="text/css">
						    #modal_share .share_button{
						        height:37px !important;
						    }
						</style>
						
                      </div>
                </form>

					<?php if($prod['Custom']){ ?>
                    	<strong style="margin-top:10px;">
               			<div id="prod-dtl-ship">
							<?php if ($prod['ProductStatusName'] == "On Backorder") {
								echo "<b>On Backorder - Estimated Ship Date is " . Date('m/d/Y',strtotime($prod['BackorderETA'])) ."</b><br/><br/>";
							} else if(trim($prod['Ships']) <> "") { ?>
                                Usually ships in <?php echo $prod['Ships']; ?>
                            <?php } ?>
                        </div>
                        <div class="prod-dtl-ship">
                        Please Note: This is a made to order and your credit card will be charged at the time of checkout.
                        </div>
                        </strong>
                    <?php } else { ?>
               			<div id="prod-dtl-ship" style="margin-top:10px;">
                        <?php if ($prod['ProductStatusName'] == "On Backorder") {
	                        echo "<b>On Backorder - Estimated Ship Date is " . Date('m/d/Y',strtotime($prod['BackorderETA'])) ."</b><br/><br/>";
                        } else if(trim($prod['Ships']) <> "") { ?>
                            Usually ships in <?php echo $prod['Ships']; ?>
                        <?php } ?>
                        </div>
                    <?php } ?>

        </div>
</div>

<?
	//include_once(__DIR__."/../../includes/inventoryHelper.php");
	//$iHelper = new inventoryHelper($productid,$s);
?>
<?php include_once __DIR__ . "/../../includes/modal_share.php"; ?>