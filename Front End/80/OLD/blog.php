<?php
$domain = "stellavallezindigo";
$count=0;
$limit=50;
include("../feeds/getFeed.php");
$thisfeed = new CachedFeed();
$blog = $thisfeed->getFeed("http://stellavallezindigo.wordpress.com/feed/","StellaValleZ-BLOG");
$xml = simplexml_load_string($blog,null, LIBXML_NOCDATA);
$entries = $xml->entry ? $xml->entry : $xml->channel->item;

function getNiceURL($url){

    $str = explode("/",$url);
    echo $str[count($str)-2];

}
?>

<div class="row collapse our-story">
    <div class="large-12 columns">
        <h1>#STELLAVALLE Blog</h1>
    </div>
</div>

<div class="row blog spacing-fix">
    <div class="small-12 large-10 large-centered columns" id="container" style="padding-left:0px; padding-right:0px;">
        <?php
        include_once("../classes/mobiledetect.php");
        $detect = new Mobile_Detect();
        foreach($entries as $entry)
        {
            if($limit && ($count>=$limit)) break;
            $date = new DateTime($entry->pubDate);
            foreach ( $entry->children('media',true)->content as $media ) {
                $url =  $media->attributes()->url;
                if(strrpos($url,$domain) === false && strrpos($url,"stellavalle") === false)
                    $url = "";
            }
            ?>

            <div class="row" id="blog-row-<?php echo $count;?>" style="border-bottom:1px solid #000;padding-top:10px;">
                <div id="blog-image-<?php echo $count;?>" class="small-12 large-5 columns blog-image" style="padding-left:0px; padding-right:13px;">
                    <img src="<?php echo $url;?>">
                </div>
                <div class="small-12 large-2 columns" style="padding-left:2em;">
                    <p style="background: url(/images/page/<?php echo $storeid;?>/blog_date.png);width:123px;height:66px;background-position: -50px -50px;text-transform: uppercase;"><span style="padding-top: 13px;line-height:1em;"><?php echo $date->format('M');?></span><span style="color: white;padding-left: 40px;"><?php echo $date->format('d');?></span></p>
                </div>
                <div class="small-12 large-5 columns" style="padding-right: 0px; padding-left: 0px; position: relative; <?php  if (!$detect->isMobile() && !$detect->isTablet()) { ?>left: -25px;<? } ?>">
                    <h1><?php echo $entry->title ; ?></h1>
                    <p><?php echo strip_tags($entry->description );?></p>

                    <div class="row collapse">
                        <div class="small-12 large-12 columns">
                            <ul class="social-links">
                                <li style="width:84px;text-align:center;"><a href="/blog/<?= getNiceURL($entry->link) . "?sess=".session_name(); ?>" class="readmore">READ MORE</a></li>
                                <li><a target="_new" href="http://www.facebook.com/sharer.php?u=<?php echo $entry->link;?>&amp;t=<?php echo urlencode($entry->title);?>" original-title="Share on Facebook" class="facebook">Facebook</a></li>
                                <li><a target="_new" href="http://twitter.com/share?url=<?php echo $entry->link;?>&text=<?php echo urlencode($entry->title);?>" original-title="Share on Twitter" class="twitter">Twitter</a></li>
                            </ul>
                            <? if($entry->title == "New! Women Warriors by Stella Valle Inspirational Video") { ?>
                                <div style="clear:both;"></div>
                                <div class="player" style="width:100%;">
                                    <video controls="false" id="video_small" poster="/images/page/80/ww_poster.png" style="width:100%;">
                                        <source src="videos/80_WWbyStellaValle.mp4" type="video/mp4">
                                    </video>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Post Blog -->


            <?php
            $count++;
        } ?>

    </div>
</div>

