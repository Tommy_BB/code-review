<link rel="stylesheet" type="text/css" href="css/checkout.css"/>
<?php
if( file_exists("css/".$storeid."/checkout.css") ){
	?><link rel="stylesheet" type="text/css" href="css/<?=$storeid?>/checkout.css"/><?php
}
?>
        <div id="content" class="checkout">
             <form name="entryform" action="index.php?page=purchase_confirmation&storeid=<?=$storeid;?>&sess=<?php echo session_name();?>" method="post">
        <div id="checkout_container">
        	<div id="checkouttitle">Shipping Method</div>

            <div id="orderinfo">
            </div>
    <!-- begin shipping method snippet - please use jquery below  ---->
<input type=hidden name=form value="send">
     <div id="shippingmethod">

     <?php
    require_once("../classes/products.php");
	$propnames = $_SESSION["cart"]->propertynames();
	$_SESSION['lowbalance'] = 'no';
	$storename=$ulk->getStoreName($_SESSION['storeid']);

	$vendorshipping = $_SESSION["cart"]->shipping();
	$canship = true;
	foreach($vendorshipping as $vendorid=>$shiparray){
		if(count($shiparray) == 0){ ?>
	<tr><td colspan="2">We're sorry, but we cannot ship to that address. Please go back and try entering a different address.</td></tr>
		<?php
		$canship = false;
		break;
		}
	}

?>


<?php
if($canship) {
foreach($vendorshipping as $vendorid=>$shiparray){

	$vitems = $_SESSION['cart']->vendoritems($vendorid);
//	print_r($vitems);
?>

<table id="shippingmethod"><tbody>
<?php
$row = 1;
$hasasteric = false;
foreach($shiparray as $ship) {
	if($ship['servicecode'] <> '3 Day Select' && $ship['servicecode'] <> 'Next Day Air Saver' && $ship['servicecode'] <> 'Next Day Air A.M.' && $ship['servicecode'] <> '2nd Day Air A.M.' && $ship['servicecode'] <>'Priority Overnight' && $ship['servicecode'] <>'First Overnight' ) {
//		if($ship['servicecode'] == "Ground" || $ship['servicecode'] == "FedEx Express Saver" || $ship['servicecode'] == "FedEx Ground" || $ship['servicecode'] == "Freight") {$selected="checked";} else { $selected="";}

		if($row == 1){ $selected="checked";} else { $selected = "";}

		if(isset($ship['ShipAmount'])) {
			echo'<tr class="method"><td class="methodsel"><input style="text-indent: 0px; margin: 3px 0px 0px;" class="radioWidth" type="radio" name="shipmethod['.$vendorid.']" value="'.$ship['servicecode'].':'.$ship['ShipAmount'].':'.$ship['transittime'].'" '.$selected.'></td><td class="methodname">'.$ship['ServiceName'].''.(($row == 3) ? " *" : null).'</td><td class="methodamt">$'.number_format($ship['ShipAmount'], 2).'</td>';
			if($row == 3)
			echo '<td class="methodtrns">Ships Next Business Day</td></tr>';
			else
			echo '<td class="methodtrns">Transit Time: up to '.$ship['DeliveryDaysMax'].' business days</td></tr>';
		//echo '<div id="standard" class="method" ><ul><li id="ship1" class="scheckbox"><input style="text-indent: 0px; margin: 3px 0px 0px;" class="radioWidth" type="radio" name="shipmethod['.$vendorid.']" value="'.$ship['servicecode'].':'.$ship['ShipAmount'].':'.$ship['transittime'].'" '.$selected.'></li><li class="shiptype">'.$ship['ServiceName'].'</li>';
		//echo "<li class='sprice'>$"; printf("%.2f",$ship['ShipAmount']); echo "</li><li class='delivery'>Transit Time: up to ".$ship['DeliveryDaysMax']." business days</li></ul></div>";
		} else {
			echo '<tr class="method"><td class="methodsel"><input class="radioWidth" type="radio" name="shipmethod['.$vendorid.']" value="'.$ship['servicecode'].':'.$ship['amount'].':'.$ship['transittime'].'" '.$selected.'>'.$ship['servicecode'].'</td><td>$'.number_format($ship['amount'], 2).'</td><td class="methodtrns">Transit Time:  '.$ship['transittime'].' business days</td></tr>';
		//echo '<div id="standard" class="method" ><ul><li id="ship1" class="scheckbox"><input class="radioWidth" type="radio" name="shipmethod['.$vendorid.']" value="'.$ship['servicecode'].':'.$ship['amount'].':'.$ship['transittime'].'" '.$selected.'>'.$ship['servicecode'].':</li>';
		//echo "<li class='sprice'>$";printf("%.2f",$ship['amount']); echo"</li><li class='delivery'>Transit Time:  ".$ship['transittime']." business days</li></ul></div>";
		}
		$hasasteric = ($row == 3) ? true : false;
		$row = $row + 1;
	}

}?>
</tbody></table>
<?php
}
	if($hasasteric) { ?>
		<div style="font-size:14px; text-align:center; margin-bottom:10px;">*Overnight orders usually ship the following business day.</div>
	<? }
}
?>


<table id="info">
	<tr>
        <td id="costs">
        	Sub-total: $<?php printf("%.2f", $_SESSION["cart"]->recalc_total()); ?> <span>USD</span>
        </td>
    </tr>
</table>



            <div id="cartbuttons">
            	<a href="index.php?page=cart_view&storeid=<?=$storeid?>&sess=<?= session_name() ?>"><div class="cartbutton cartbtncont" >Back</div></a>
                
                <?php if($canship){ ?>
                	<input type="submit" class="cartbutton cartbtncheckout" value="Next" id="shipButton">
                <?php } ?>
            </div>

        </div>
    </div>
<input type="hidden" id="shipMethod" />

</form>
</div>

<?php include 'templates/'.$storeid.'/footer.php';?>

