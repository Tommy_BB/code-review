<link rel="stylesheet" type="text/css" href="css/checkout.css"/>
<?php
if( file_exists("css/".$storeid."/checkout.css") ){
	?><link rel="stylesheet" type="text/css" href="css/<?=$storeid?>/checkout.css"/><?php
}
if(!$isFacebook) {
$checkoutactionroot = (($isdevelopment) ? "http://storesdev.zindigo.local/" : "https://checkout.zindigo.com/");
 } else {
 	$checkoutactionroot = "";
 }
?>

<script language="Javascript">
needcalc=0;
function frmsubmit(action) {
	frm = document.entryform;
	frm.action.value = action;
	frm.submit();
}
function purchase() {
	if (needcalc == 1) {
	alert("Please recalculate the cart");
//	return true;
	} else {
	self.location.href='<?=$checkoutactionroot?>index.php?page=purchase_now&storeid=<?php echo $storeid;?>&sess=<?php echo session_name(); if ( ! isset( $_COOKIE[session_name()] ) ) { echo "&".htmlspecialchars(SID); } ?>';
//	return false;
	}
}
</script>

<div id="checkout_container">

<div id="checkouttitle">Review Your Order</div>


<?php if ($_SESSION["cart"]->itemcount() == 0) { ?>
<div class="labels">
    <div class="labels" style="width: 248px">
        <p style="width:750px" class="lblempty">Your Shopping Cart is Empty</p>
        <p style="width: 750px; padding-top: 10px; margin-bottom: 10px;" class="lbldescription">We invite you to continue shopping <br/></p>
    </div>

    <div id="contShopButton" style="margin:auto; margin-left:310px; margin-bottom:20px;">
        <p>
     	   <a style="color:black" href="index.php?page=home&storeid=<?php echo $storeid?>&sess=<?= session_name(); ?>">BACK</a>
        </p>
    </div>
</div>
<?php return false; ?>

<?php } else {



$ordertotal=$_SESSION["cart"]->recalc_total();


if(($_SESSION['orderinfo']->cardno == ''  && $_SESSION['orderinfo']->method <> "po") || ($_SESSION['orderinfo']->PONum == ''  && $_SESSION['orderinfo']->method == "po"))
{
	if($_SESSION['orderinfo']->gift_certificate_code != '')
	{
		$orderGrandTotal1 = ($_SESSION['cart']->giftCertAmount + $_SESSION['cart']->grandtotal);
		$orderGrandTotal1 = number_format($orderGrandTotal1,2);


		if($orderGrandTotal1 > number_format($_SESSION['cart']->giftCertAmount,2))
		{
			$_SESSION['lowbalance'] = 'yes';
			//redirect("index.php?page=purchase_now");
			//die;

		}
	}
}


?>


<table class="cartitems cartheader" border="0">
<thead>
    	<th class="cart-header cart-img">&nbsp;</th>
        <th class="cart-header cart-desc">Product Description</th>
        <th class="cart-header cart-qty">Qty</th>
        <th class="cart-header cart-price">Price</th>
    </thead>
    <tbody></tbody>
</table>
<div id="cartitemscont">
<table class="cartitems" border="0">
    <tbody>
<?php
	foreach($_SESSION["cart"]->items as $productid => $item){
		$price = $item->get_price();
		$qty = $item->get_qty();
		$total = $item->item_total();
		$name = $item->get_name();

		$imgthumb = $products->getProductImage($item->get_id());
?>
    	<tr class="cartitem">
        	<td class="cart-img">
            	<img src="<?= $imagedir.$imgthumb['ThumbPath']; ?>" />
            </td>
            <td class="cart-desc">
            	<p class="productname"><?= $name; ?></p>
                <p class="productdesc">
                <?php
				if($propnames){
                	foreach($propnames as $key=>$propname) {
						$properties = $item->get_properties();
                        foreach($properties as $property=>$valueid) {
                        	if ($property == $propname) {
                        		$value = $classes->getValue($valueid);
                        		echo $property . " : " . $value['Name'] . "; ";
                        	}
                        }

                     }
                }?>
                </p>
            </td>
            <td class="cart-qty">
            	<?php echo $qty?>
            </td>
            <td class="cart-price">
            	$<?= number_format($total, 2) ?>
            </td>
        </tr>
<?php } ?>

    </tbody>
</table>
</div>



<form name="entryform" action="<?=$checkoutactionroot?>index.php?page=complete_order&sess=<?php echo session_name();?>&storeid=<?php echo $storeid;?>" method="post" onsubmit="submitForm()">

<table id="info">
	<tr>
        <td id="billto">
            Bill To:
            <div class="address">
				<?php pv($customer);?><br/>
                <?php pv($company); ?><br/>
                <?php pv($address); ?><br/>
                <?php if ($address2 <> "") {
                pv($address2);
                echo "<br/>";
                }?>
                <?php pv($city);
                echo ", ";
                pv($state);
                echo " ";
                echo $zipcode."<br/>";?>
                <?php if ($country <> "US") {
               		pv($country);
                } ?>
            </div>
        </td>
        <td id="shipto">
            Ship To:
            <div class="address"><?php echo $ship_fname." ".$ship_lname; ?><br>
                <?php pv($company); ?><br>
                <?php pv($ship_address1) ;?><br>
                <?php if ($ship_address2 <> "") {
                pv($ship_address2);
                echo "<br>";
                }?>
                <?php pv($ship_city);
                echo ", ";
                pv($ship_state);
                echo " ";
                echo $ship_zip."<br>";
                ?>
                <?php if ($ship_country <> "US") { pv($ship_country); } ?>
            </div>
        </td>
        <td id="commentsbox">
            <? if(isset($_SESSION['sv-contest-handle']) && isset($_SESSION['sv-contest-social'])) { ?>
        	<textarea name="CustomerComments" id="CustomerComments" style="visibility: hidden;"><? echo "Shared On : ".$_SESSION['sv-contest-social'] ." - with handle : " .  $_SESSION['sv-contest-handle']; ?></textarea>
            <? } else { ?>
                <textarea name="CustomerComments" id="CustomerComments" onfocus="this.value=''; this.onfocus=null;">Comments</textarea>
            <? } ?>
        </td>
        <td id="costs">
        	Sub-total: $<?= number_format($_SESSION["cart"]->total , 2); ?><br />
            Tax: $<?= number_format( $_SESSION["cart"]->taxes() , 2);?><br />
			<?
            $discount = $_SESSION["cart"]->coupondiscount + $_SESSION["cart"]->purchasediscount;
            if ($discount > .01){ ?>
           		Discount: -$<?= number_format($discount,2 ); ?><br />
            <? } else if ($discount < -.01) { ?>
            	Fee: $<?= number_format( abs($discount), 2 ); ?><br />
            <? } ?>
            Shipping: $<?= number_format( $_SESSION["cart"]->totshipping , 2) ?><br />
            <?php if ($_SESSION['cart']->giftCertAmount > .01){ ?>
            Gift Certificate: -$<?php echo number_format($_SESSION['cart']->giftCertAmount,2);?><br/>
            <?php } ?>
            <span id="total">Total: $<?= number_format($_SESSION["cart"]->recalc_total() , 2); ?>
        </td>
    </tr>
</table>

<div id="cartbuttons">
    <div class="cartbutton cartbtncont"><a href="index.php?page=cart_view&storeid=<?= $storeid ?>&sess=<?= session_name() ?>">Continue Shopping</a></div>
	<div class="cartbutton cartbtncheckout" style='cursor: pointer;'><a style="text-decoration:none" onclick="document.getElementsByName('entryform')[0].submit(); return false;" id="submitdiv">Submit</a></div>

<script type="text/javascript">
function submitForm() {
	document.getElementById('submitdiv').innerHTML='<p class=textbig><img src=\'images/indicator.white.gif\' align=middle>&nbsp;Please Wait</p>';
}
</script>
</div>
<input type='hidden' name='sessionid' value='<?=session_id()?>'>
</form>
<?php } ?>

</div>
