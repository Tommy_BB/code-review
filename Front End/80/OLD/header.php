<?php
require_once "../classes/products.php";
require_once "../classes/discounts.php";
$isFacebook = false;

$storeimagecloud = (((isset($_SERVER['HTTPS'])) && ($_SERVER['HTTPS'] == 'on')) ? "https://c14031392.ssl.cf2.rackcdn.com/" : "http://c14031392.r92.cf2.rackcdn.com/");

if (isset($_GET['prodid']) && $_GET['prodid'] != "") {
	$prod = $products->getStoreProduct($_GET['prodid'], $prodstoreid);
	$saleprice = $discounts->getSalePrice($_GET['prodid'], $storeid);
	$imgarray = $prod["ImageID"];
	$imageid = $imgarray[0];
	$img=$classes->viewImage($imageid);
}

$cats=$classes->getMainStoreCategoriesCached($prodstoreid,1013);
$catcount = count($cats);

if(!isset($catid)) {
	if(isset($_GET['categoryID'])) { $catid = $_GET['categoryID']; }
	if(isset($_GET['catid'])) { $catid = $_GET['catid']; }
}
if(!isset($subcatid)) {
	if(isset($_GET['subcatID'])) { $subcatid = $_GET['subcatID']; }
}
if(!isset($subsubcatid)) {
	if(isset($_GET['subsubcatID'])) { $subsubcatid = $_GET['subsubcatID']; }
}


$httppath = (($isdevelopment) ? $httppath = $protocol . "storesdev.zindigo.com/" : $httppath = "http://www.stellavalle.com/");
//$fburl = $httppath."/index.php/".$page."/".$storeid."/".$catid."/".$_GET['prodid']."/".$subcatid."/".$subsubcatid."/";

if ( isset( $_COOKIE[session_name()] ) ) {
	$sid = '';
} else {
	$sid = '&'.htmlspecialchars(SID);
}

//cap 50
//build query to check for all order that are marked as complete
$checkContest = "SELECT o.ID FROM Orders o JOIN OrderDetails od ON od.OrderID = o.ID WHERE od.ProductID = 14470 AND o.OrderStatus IN (3,5,6) GROUP BY o.ID;";
$checkResult = mysqli_query($ulk->db_cnx,$checkContest);
    if(mysqli_num_rows($checkResult) >= 50)
    {
        $_SESSION['contestStatus'] = false;
    } else {
        $_SESSION['contestStatus'] = true;
    }

?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head prefix="og: http://ogp.me/ns#">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<!--<base href="<?php echo $httppath;?>" />-->

	<meta charset="UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<?php if(isset($prod['Name'])) { ?>
	<meta property="og:title" content="<?php echo htmlspecialchars($prod['Name']." from ".$storename,ENT_QUOTES);?>" />
	<meta property="og:type" content="product" />
	<meta property="og:url" content="<?php echo $fburl;?>" />
	<meta property="og:site_name" content="<?php echo htmlspecialchars($storename,ENT_QUOTES);?>" />
	<meta property="og:image" content="<?php echo $protocol.$_SERVER['SERVER_NAME']."/".$img['ImagePath'];?>" />
	<meta property="og:description" content="<?php echo htmlspecialchars(strip_tags($prod['Description']),ENT_QUOTES);?>" />
	<meta property="fb:app_id" content="<?php echo $store['FBAPPID'];?>"/>

	<!-- Pinterest OpenGraph Product Rich Pin -->
	<? if (number_format($saleprice, 2) != number_format($prod['StorePrice'], 2)): ?>
		<meta property="product:price:amount" content="<?= number_format($saleprice, 2); ?>"  />
		<meta property="og:price:standard_amount" content="<?= number_format($prod['StorePrice'], 2); ?>"  />
	<? else: ?>
		<meta property="product:price:amount" content="<?= number_format($prod['StorePrice'], 2); ?>"  />
	<? endif; ?>
	<meta property="product:color" content="<?= ucwords(strtolower($prod['ColorFamily'])); ?>" >
	<meta property="og:price:currency" content="USD" />
	<title><?php echo htmlspecialchars($prod['Name']." from ".$storename,ENT_QUOTES);?></title>
<?php } else { ?>
	<meta property="og:title" content="<?php echo htmlspecialchars(" from ".$storename,ENT_QUOTES);?>" />
	<meta property="og:type" content="website" />
	<meta property="fb:app_id" content="<?php echo $store['FBAPPID'];?>"/>
	<title><?php echo htmlspecialchars($storename);?></title>
<?php } ?>
	<meta name="author" content="Zindigo" />
	<meta name="viewport" content="width=device-width"/>
<link rel="shortcut icon" href="/css/<?php echo $storeid; ?>/favicon.ico" type="image/x-icon" />

	<link type="text/css" rel="stylesheet" href="/min/b=css&amp;f=cyberpayment.css,80/checkout.css,80/foundation.css,80/app.css,80/component.css,80/carousel.css" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="/js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
	<link type="text/css" rel="stylesheet" href="/css/<?=$prodstoreid;?>/version_warrior.css" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-42769314-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
	//console.log("<?= $_SERVER['SERVER_ADDR']; ?>");
</script>
<style type="text/css">
.social-footer .button{
	z-index: 0;
}
</style>
</head>
<body>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
<script type="text/javascript" src="/min/b=js&amp;f=80/custom.modernizr.js,foundation.min.js,foundation/foundation.topbar.js,foundation/foundation.forms.js,jsrender.js"></script>
<script type="text/javascript" src="/js/foundation/foundation.interchange.js"></script>
<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/js/<?=$prodstoreid;?>/version_warrior.js"></script>
<script src="https://use.typekit.net/ivb4qup.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<!-- Header -->
 	 <header class="row">

     <div class="container">

     	<div class="position-fixed">
     	  
 			<div class="row" <?= (($page == "home") ? "style=\"display:none;\"" : null); ?>>
 	  			<div class="small-12 large-5 columns free-shipping">
      	            <strong><i>Free Shipping On Orders Over $100</i></strong>
       		 	</div>
  	<!-- Shopping Cart -->
 				<div class="small-12 large-7 columns thetop">
					<div class="pstopnav">
						<div class="ps">
							<div class="text header-nav-personal-shopper">Personal Shopper</div>
							<div class="text line">|</div>
							<div class="text header-nav-personal-shopper">Contact Us&nbsp;<i class="fa fa-phone-square"></i></div>
							<div class="text line">|</div>
							<a class="shopping-cart view-cart" href="/index.php?page=cart_view&storeid=<?=$storeid?>&sess=<?=session_name()?>" class="link">View Cart&nbsp;<i class="fa fa-shopping-cart"></i></a>
						</div>
					</div>
				</div>
    		</div>
    	<?php
$shopcount = count($_SESSION["cart"]->items);
if($shopcount > 0 && ($page != "cart_view" && $page != "purchase_now" && $page != "purchase_confirmation" && $page != "shipping")){?>
<? } ?>
	<!-- /Shopping Cart -->


	<div class="row collapse">
		<div class="large-12 columns">


				<nav class="top-bar">
				  <ul class="title-area show-for-mobile">
				    <!-- Title Area -->
				    <li class="name">
                      <a href="/index.php?page=home&storeid=<?= $storeid; ?>">
                          <img src="<?= $storeimagecloud; ?>80_sv_logo_square.png" alt="Stella Valle" class="logo_square">
                          <img src="<?= $storeimagecloud; ?>80_sv_logo_text.png" alt="Stella Valle" class="logo_text">
                      </a>
				    </li>
				    <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
				    <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
				  </ul>

				  <section class="top-bar-section">
				    <!-- Left Nav Section -->
				    <ul class="left">
                    	<li><a <?php if ($flag == "our-story"): echo 'class="current-section"'; endif; ?> href="index.php?page=dynamic&ContentID=409&storeid=<?php echo $storeid;?>&sess=<?php echo session_name(); ?>">OUR STORY</a></li>
						<li class="divider"></li>
						<li class="has-dropdown"><a <?php if ($flag == "shop"): echo 'class="current-section"'; endif; ?> href="index.php?storeid=<?=$storeid?>&page=categoryproducts&action=all&categoryID=1013&sess=<?=session_name()?>">SHOP</a>
							<ul class="dropdown">
								<li><a  href="/index.php?storeid=<?=$storeid?>&page=categoryproducts&action=all&categoryID=1013&sess=<?=session_name()?>">ALL PRODUCTS</a></li>
								<li class="divider"></li>
								<li><a  href="/index.php?storeid=<?=$storeid?>&page=categoryproducts&action=keywords&keywords=cat_necklaces&keytitle=Necklaces&sess=<?=session_name()?>">NECKLACES</a></li>
								<li class="divider"></li>
								<li><a href="/index.php?storeid=<?=$storeid?>&page=categoryproducts&action=keywords&keywords=cat_bracelets&keytitle=Bracelets&sess=<?=session_name()?>">BRACELETS</a></li>
								<li class="divider"></li>
								<li><a  href="/index.php?storeid=<?=$storeid?>&page=categoryproducts&action=keywords&keywords=cat_earrings&keytitle=Earrings&sess=<?=session_name()?>">EARRINGS</a></li>
								<li class="divider"></li>
								<li><a  href="/index.php?storeid=<?=$storeid?>&page=categoryproducts&action=keywords&keywords=cat_rings&keytitle=Rings&sess=<?=session_name()?>">RINGS</a></li>
							</ul>
						</li>
                      <li class="divider"></li>
                      <?
                    //  mysqli_data_seek($cats, 0);
                      $hreftouse = $cats[0];
                      ?>
				      <li class="has-dropdown"><a <?php if ($flag == "collections"): echo 'class="current-section"'; endif; ?> href="index.php?storeid=<?=$storeid?>&page=categoryproducts&action=all&categoryID=<?=$hreftouse['ID']?>&sess=<?=session_name()?>">COLLECTIONS</a>
				        <ul class="dropdown" id="collections-menu">
							<?php
							//mysqli_data_seek($cats, 0);
						//	while ( $cat = mysqli_fetch_array($cats)){
						foreach($cats as $cat){
							// Do not show women warrior
							if ($cat['ID'] == 2013 || $cat['ID'] == 2280 || $cat['ID'] == 2287) {
								continue;
							}
							?>
									<li>
										<a href="/index.php?storeid=<?=$storeid?>&page=categoryproducts&action=all&categoryID=<?=$cat['ID']?>&sess=<?=session_name()?>"><?php echo $cat['Name']; ?></a>
									</li>
									<li class="divider"></li>
								<?php
							} ?>
				        </ul>
				      </li>
                      <li class="divider"></li>
						<li class="has-dropdown women-warrior"><a style="padding-top: 35px !important;" <?php if ($flag == "women-warriors"): echo 'class="current-section"'; endif; ?> href="/index.php?page=warrior&storeid=<?php echo $storeid;?>&sess=<?php echo session_name(); ?>"><span>UNDER $100</span>WOMEN <br />WARRIORS BY <br />STELLA VALLE</a>
						<ul class="dropdown">
				          <li><a class="reset-women-warrior" href="/index.php?page=warrior&storeid=<?php echo $storeid;?>&sess=<?php echo session_name(); ?>">ABOUT WOMEN WARRIORS</a></li>
                          <li class="divider"></li>
                          <li><a class="reset-women-warrior" href="/index.php?page=dynamic&ContentID=4194&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(); ?>">ARMED WITH INSPIRATION</a></li>
                          <li class="divider"></li>
                          <li><a class="reset-women-warrior" href="/index.php?page=dynamic&ContentID=4195&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(); ?>">THE CORPS</a></li>
                          <li class="divider"></li>
				        </ul>
					  </li>

                      <li class="divider show-for-dektop-only"></li>
                      <li class="show-for-dektop-only desktop-logo">
                          <a href="/index.php?page=home&storeid=<?= $storeid; ?>&sess=<?= session_name(); ?>"><img src="<?= $storeimagecloud; ?>80_svlogonewest.png" alt="Stella Valle"></a>
                      </li>
                      <li class="divider"></li>
                        <li class="">
                            <a <?php if ($flag == "storelocator"): echo 'class="current-section"'; endif; ?> href="/index.php?page=storelocator&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(); ?>">STOCKISTS</a>
                        </li>
					  
                      
                      <li class="divider"></li>
                      <li class="has-dropdown"><a <?php if ($flag == "press"): echo 'class="current-section"'; endif; ?> href="/index.php?page=press&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(); ?>">PRESS</a>
                                            	<ul class="dropdown">
				          <li><a href="/index.php?page=press&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(); ?>">EDITORIAL</a></li>
                          <li class="divider"></li>
                          <li><a href="/index.php?page=press&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(); ?>&subsection=blog">BLOGGERS AND CELEBRITY</a></li>
                          <li class="divider"></li>
				          <li><a href="/index.php?page=video&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(); ?>">SHARK TANK</a></li>
						  <li class="divider"></li>
						  <li><a href="/index.php?page=press&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(); ?>&subsection=target">STELLA VALLE FOR TARGET</a></li>
						  <li class="divider"></li>
						   <li><a href="/index.php?page=entrepreneur&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(); ?>">ENTREPRENEUR</a></li>
				        </ul>
				        </li>
					
					  <li class="divider"></li>
					  <li><a <?php if ($flag == "blog"): echo 'class="current-section"'; endif; ?> href="/blog?sess=<?php echo session_name(); ?>">BLOG</a></li>
					  
                      <li class="divider"></li>
                      <li class=""><a <?php if ($flag == "stellavalle"): echo 'class="current-section"'; endif; ?> href="/index.php?page=dynamic&ContentID=410&storeid=<?php echo $storeid; ?>&sess=<?php echo session_name(); ?>">#STELLAVALLE</a>
                      </li>
          
				    </ul>
				  </section>
				</nav>

      </div>
    </div>

    	</div>
    </div>
    </header>
