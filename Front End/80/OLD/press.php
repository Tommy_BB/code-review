	<div class="row collapse our-story">
		<div class="large-12 columns">
			<h1>PRESS</h1>
		</div>
	</div>
	<div class="row spacing-fix">
    	<div class="large-12 columns">
                <ul class="grid cs-style-4" style="max-width: 1170px">
                </ul>
        </div>
	</div>
	<script src="js/toucheffects.js"></script>
	<script src="js/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
	<? /*
	<script src="/js/115/jquery.fancybox.js"></script>
	*/ ?>
  	<script src="js/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

  	<script>
	/*** Lightbox for Images ***/
	$(document).ready(function() {

		$('.fancybox').fancybox();

		$('.fancybox-media')
		  .attr('rel', 'media-gallery')
		  .fancybox({
			  openEffect : 'none',
			  closeEffect : 'none',
			  prevEffect : 'none',
			  nextEffect : 'none',
			  arrows : false,
			  helpers : {
				  media : {},
				  buttons : {}
			  }
		  });

	});
	
var lastdate = "";
var lastCover = "";
var firstImage = "";
$.views.helpers({
	getThumb: function(val) {
		return val.replace("<?php echo "/".$prodstoreid."_"; ?>", "/th_<?php echo $prodstoreid."_"; ?>");
	},
	setLastDate: function(val) {
		lastdate = val;
	},
	getLastDate: function() {
		return lastdate;
	},
	setLastCover: function(val) {
		lastCover = val;
	},
	getLastCover: function() {
		return lastCover;
	},
	setFirstImage: function(val) {
		if(firstImage == "")
		firstImage = val;
	},
	getFirstImage: function() {
		return firstImage;
	},
	unsetFirstImage: function() {
		firstImage = "";
	}
});

  </script>

  <script type="text/javascript">
	//window.sessionStorage.clear();
var current = 0;
var isMore = true;
var isLoading = false;
var init = true;

$(document).ready(function(e) {
try {
	if (window.sessionStorage){
		items = JSON.parse(sessionStorage.getItem('press<?=$pressStoreID?>_<?=$_GET["subsection"]?>'));

		if(typeof items == 'object' && items != null) {
			for (i = 0; i < items.length; i++) {
				$('.grid').append( $('#presstmpl').render(items[i]) );
				console.log(items[i]);
			}
			current = items.length;
			isLoading = false;
			$(".nfancybox").click(function() {
				$(this).parent().find("a.fancybox:first").click();
			});
		} else {
			getPress();
		}
	} else {
		getPress();
	}
} catch (e) {
	getPress();
}
});


function getPress() {
	if(isMore && !isLoading) {
		//console.log('Getting Press');
		isLoading = true;
		$.ajax({
					url : 'json/pressajaxcaptions.php',
					data : {
						limit : 99,
						storeid : 80,
						action : 'morepress',
            current : current,
						subsection : '<?php if (!empty($_GET["subsection"])) { echo $_GET["subsection"]; } else { echo ""; } ?>'
					},
			dataType:'json',
			success : function(items) {
				if(items.length > 0) {
					for (i = 0; i < items.length; i++) {
						$('.grid').append( $('#presstmpl').render(items[i]));
					}


					try {
						if (window.sessionStorage && items.length > 0){
							sessItems = JSON.parse(sessionStorage.getItem('press<?=$pressStoreID?>_<?=$_GET["subsection"]?>'));
							if(typeof sessItems == 'object' && sessItems!=null) {
								sessionStorage.setItem('press<?=$pressStoreID?>_<?=$_GET["subsection"]?>', JSON.stringify(sessItems.concat(items)) );
							} else {
								sessionStorage.setItem('press<?=$pressStoreID?>_<?=$_GET["subsection"]?>', JSON.stringify(items) );
							}
						}
					} catch (e) {
					}

				} else {
					isMore = false;
				}
				
				$(".nfancybox").click(function() {
					$(this).parent().find("a.fancybox:first").click();
				});
			}
		}).done(function() {
			isLoading = false;
			init = false;
		});
		current++;
	}
}
</script>

<style>
.press-gallery
{
	display: none;
}
</style>

  	<style>
  		.grid li
  		{
  			width: 33%;
  			margin: 0px 0px 15px 0px;
  			padding: 10px;
  			outline: none;
  			border: none;
  			position: relative;
			max-height: 520px;
			overflow: hidden;
  		}
  		
  		.nfancybox .hoverpart
  		{
  			position: absolute;
  			z-index: 999;
  			display: none;
  			background: rgba(255,255,255,0.7);
  			width: 100%;
  			height: 100%;
  			top: 0px;
  			left: 0px;
			padding: 10px;
  		}
		
		.nfancybox img {
			width: 100%;
		}
  		
  		.grid li .nfancybox .hoverpart .articlestext
  		{
  			/*position: absolute;*/
  			margin-top: 45%;
			font-size: 16px;
  			line-height: 22px;
  		}
  		
  		.grid li .nfancybox .hoverpart .articlestext .articletitle
  		{
  			font-size: 18px;
  			line-height: 24px;
  			text-align: center;
  			font-family: 'GFS Didot',serif;
  			text-transform: uppercase;
  		}
  		
  		.nfancybox:hover .hoverpart
  		{
  			display: block;
  		}
  		
  		.grid li .nfancybox
  		{
  			display: block;
  			position: relative;
  			cursor: pointer;
			
  		}
  		
  		.firstcaption
  		{
  			text-align: center;
  			font-family: 'Open Sans', sans-serif;
  		}
  		
  		.articledate
  		{
  			text-align: center;
  			font-family: 'Open Sans', sans-serif;
  		}
  		
  		
  		.nfancybox:hover
  		{
  			color: #000;
  		}
  		
  		
  		.grid figcaption
  		{
  			display: none;
  		}
  		
  		.fancybox-skin
  		{
  			padding: 20px !important;
  			padding-top: 75px !important;
			background-color: white;
  		}
  		
  		.fancybox-title-float-wrap
  		{
  			top: 0px;
  			right: auto;
  			left: 0px;
  			bottom: auto;
  			margin-bottom: 0px;
  			width: 100%;
  		}
  		
  		.fancybox-title-float-wrap .child
  		{
  			margin: 0px;
  			display: block;
  			text-align: left;
  			font-weight: normal;
  		}
  		
  		.popuptitle
  		{
  			font-size: 15px;
  			line-height: 18px;
  			font-family: 'GFS Didot',serif;
  			text-transform: uppercase;
			white-space: normal;
			width: 85%;
			max-height: 40px;
			overflow: auto;
			word-break: break-all;
			word-wrap: break-word;
			margin-top: 16px;
  		}
  		
  		.popuprest
  		{
  			font-size: 14px;
  			line-height: 16px;
  			font-family: 'Open Sans', sans-serif;
			white-space: normal;
			width: 95%;
			word-break: break-all;
			word-wrap: break-word;
  		}
  		
  		.fancybox-nav
  		{
  			position: initial;
  		}
  		
  		.fancybox-nav span
  		{
  			visibility: visible;
  			top: 0px;
  			margin-top: -55px;
  		}
  		
  		.fancybox-prev span
  		{
  			right: 30px;
  			left: auto;
  			background-image: url('/images/page/108/fancybox_prev.png');
  			background-position: 0px 0px;
  			background-repeat: no-repeat;
  			background-size: 25px auto;
  			height: 25px;
  			width: 25px;
  			z-index: 999999999;
  		}
  		
  		.fancybox-next span
  		{
  			left: auto;
  			right: 0px;
  			background-image: url('/images/page/108/fancybox_next.png');
  			background-position: 0px 0px;
  			background-repeat: no-repeat;
  			background-size: 25px auto;
  			height: 25px;
  			width: 25px;
  			z-index: 99999999999;
  		}
  		
  		#fancybox-loading, .fancybox-close, .fancybox-prev span, .fancybox-next span, .fancybox-next-arrow
  		{
		}
  	</style>

<script type='text/x-jsrender' id="presstmpl">
			{{:~setLastDate(date)}}
			{{:~setLastCover(name)}}
			
            <li>
            <figure>
				<div class="nfancybox">
					<img src="{{:mainimage}}" alt="{{:name}}">
					<div class="hoverpart">
	            		<div class="articlestext">
	            			<div class="articletitle">{{:name}}</div>
	            			<div class="firstcaption">{{:caption}}</div>
	            			<div class="articledate">{{>date}}</div>
	            		</div>
	            	</div>
				</div>
				<figcaption>
					<div class="press-gallery">
						{{for subimages ~mainimage=mainimage}}
							{{if (subimages != ~mainimage) }}
								<a class="fancybox" rel="{{:~mainimage}}" href="{{:subimages}}" title="<div class='popuptitle'>{{:~getLastCover()}}</div><div class='popuprest'>{{:subcaption}} | {{:~getLastDate()}}</div>"></a>
							{{/if}}
						{{/for}}
					</div>
				</figcaption>
            </figure>
            </li>
</script>