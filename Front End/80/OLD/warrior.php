<?php
require_once "../classes/storeprods.php";
require_once "../classes/products.php";
?>
<link type="text/css" rel="stylesheet" href="/css/80/octobersvhp.css" />
<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
<script src="/js/jquery.flexisel.js"></script>
<script src="js/fancybox/jquery.fancybox.pack.js"></script>

<style>
	.blacktop{
		background-color: #000;
		height: 30px;
		margin-top: 180px;
	}

	.ww_logo{
		max-height: 200px;
		margin-top: 30px;
	}
	.ww_logo_container{
		text-align: center;
	}
	.ww_text_container{
		font-size: 13px;
	    line-height: 20px;
	    margin-top: 30px;
	}
	.ww_video_container{
		margin: 20px 0;
	}

	.ww_sub_container{
		border-top:1px solid #000;
		margin-top:40px;
	}

	.fullwidth{
		max-width: none;
	}

	@media screen and (max-width: 940px) {
	    .blacktop{
			margin-top: auto;
		}
	}
</style>
<div class="row blacktop fullwidth"></div>
<div class="row fullwidth">
	<div class="medium-6 columns">
		<div class="row fullwidth">
			<div class="medium-12 columns ww_logo_container">
				<img class="ww_logo" src="/images/page/80/ww_logo.png">
			</div>
		</div>
		<div class="row fullwidth">
			<div class="medium-12 columns">
				<div class="ww_text_container">
					Paige and Ashley, the sister-duo behind the jewelry collection Stella Valle are West Point Graduates and former U.S. Army Officers turned jewelry designers. After partnering with Lori Greiner and Mark Cuban, from ABC’s Shark Tank, the team designed Women Warriors by Stella Valle. Drawing from the military adornments that the sisters once wore, the line will inspire you to be a warrior for what you want in life. 
					<br/><br/>Women Warriors is proud to partner with Fatigues to Fabulous by providing jewelry to homeless or at-risk Veteran women to help them re-acclimate back to everyday life and feel beautiful!
				</div>
			</div>
		</div>
		<div class="row fullwidth">
			<div class="medium-12 columns">
				<div class="ww_video_container">
					<div class="player" >
						<video controls="false" id="video_big" poster="/images/page/80/ww_play_button.png" style="width:100%;">
							<source src="videos/80_WWbyStellaValle.mp4" type="video/mp4">
						</video>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="medium-6 columns" style="padding:0;">
		<img src="/images/page/80/ww_landing_feature.jpg" style="width:100%;">
	</div>
</div>

<div class="row fullwidth ww_sub_container">
	<div class="medium-12 columns" style="padding:0;">
		<a href="index.php?page=dynamic&ContentID=4194&storeid=<?= $storeid; ?>&sess=<?= session_name(); ?>"><img src="/images/page/80/ww_landing_sub.jpg" style="width:100%;"></a>
	</div>
</div>

<div class="row fullwidth" style="margin-bottom:80px;">
  <div class="small-12 large-11 large-centered columns">
    <ul id="Collections">
      <?php
      require_once "../classes/storeprods.php";
      $thiscatid = 2279;
      $thiscatprods = $storeprods->getStoreActiveProductsAllFromCategoryCached($thiscatid);
      foreach($thiscatprods as $thisprod) {
        $thisprod['OrigPrice'] = $thisprod['StorePrice'];
        $thisprod['StorePrice'] = number_format($discounts->getSalePrice($thisprod['ProductID'],$prodstoreid),2);
        $thisprod['OrigPrice'] = str_replace(".00","",number_format($thisprod['OrigPrice'],2));
        $thisprod['StorePrice'] = str_replace(".00","",number_format($thisprod['StorePrice'],2));
        $phref = "index.php?page=productslist&storeid=".$storeid."&categoryID=".$_GET['categoryID']."&subcatID=".$_GET['subcatID']."&subsubcatID=".$_GET['subsubcatID']."&prodid=".$thisprod['ProductID']."&sess=". session_name() . $sid;?>
          <li><a href="<?php echo $phref;?>"><img src="<?php echo $thisprod['ImagePath'];?>" <? if(isset($thisprod['AltImagePath'])) { ?>onmouseover="this.src='<?php echo $thisprod['AltImagePath'];?>'"  <? } ?>onmouseout="this.src='<?php echo $thisprod['ImagePath'];?>'"><br><?php echo $thisprod['ProductName'];?> <br><?php if($thisprod['OrigPrice']>$thisprod['StorePrice']){?>$<span style="text-decoration:line-through;"><?php echo $thisprod['OrigPrice'];?></span>&nbsp;$<?= $thisprod['StorePrice']; ?><? }else{ ?>$<?= $thisprod['StorePrice']; ?><? } ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div>

<script type="text/javascript">
	if ($('#video_big').is(':visible')){
		$('#video_big').get(0).play();
	}

	$('#video_big').on('ended',function(){
      $('#video_big').get(0).load();
    });

    /*** Carousel Plugin ***/
	$(window).load(function() {
	$("#Collections").flexisel({
	    enableResponsiveBreakpoints: true,
	    responsiveBreakpoints: {
	        portrait: {
	            changePoint:480,
	            visibleItems: 1
	        },
	        landscape: {
	            changePoint:640,
	            visibleItems: 2
	        },
	        tablet: {
	            changePoint:768,
	            visibleItems: 3
	        }
	    }
	});
	});
</script>