var storeId = 80;

var storeBaseAssetPath = '../../twig_templates/themes/store';
var storeGeneratedCssAssetPath = '../../css/' + storeId + '/generated';
var storeGeneratedJsAssetPath = '../../js/' + storeId + '/generated';

module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        /**
         * Environment configurations
         */
        config: {
            dev: {
                options: {
                    variables: {
                        uglifyOptions: {
                            preserveComments : 'all',
                            drop_console: ''
                        }
                    }
                }
            },
            staging: {
                options: {
                    variables: {
                        uglifyOptions: {
                            preserveComments : false,
                            drop_console: true
                        }
                    }
                }
            },
            prod: {
                options: {
                    variables: {
                        uglifyOptions: {
                            preserveComments : false,
                            drop_console: true
                        }
                    }
                }
            }
        },

        compass: {
            brand: {
                options: {
                    sassDir: 'twig_templates/themes/store/scss',
                    cssDir: storeGeneratedCssAssetPath + '',
                    outputStyle: 'expanded',
                    force: true
                }
            }
        },

        /**
         * Add resources that you wish to include into the brand's site
         */
        concat: {
            bootstrapJs: {
                src: [
                    storeBaseAssetPath + '/js/vendors/bootstrap/*',
                ],
                dest: storeGeneratedJsAssetPath + '/frontend.min.js'
            },
            brandJs: {
                src: [
                    storeGeneratedJsAssetPath + '/bower_components/respond/dest/respond.src.js',
                    storeGeneratedJsAssetPath + '/bower_components/matchHeight/jquery.matchHeight.js',
                    storeGeneratedJsAssetPath + '/bower_components/mixitup/build/jquery.mixitup.min.js',
                    storeGeneratedJsAssetPath + '/bower_components/imagesloaded/imagesloaded.pkgd.min.js',
                    storeGeneratedJsAssetPath + '/bower_components/jquery.cookie/jquery.cookie.js',
                    storeGeneratedJsAssetPath + '/bower_components/jquery-validation/dist/jquery.validate.js',
                    storeGeneratedJsAssetPath + '/bower_components/ladda-bootstrap/dist/spin.js',
                    storeGeneratedJsAssetPath + '/bower_components/ladda-bootstrap/dist/ladda.js',
                    storeGeneratedJsAssetPath + '/bower_components/cryptojslib/rollups/md5.js',
                    storeGeneratedJsAssetPath + '/bower_components/jquery.browser/dist/jquery.browser.js',
                    //storeGeneratedJsAssetPath + '/bower_components/smartmenus/dist/addons/bootstrap/jquery.smartmenus.bootstrap.js',
                    'twig_templates/themes/store/js/*'
                ],
                dest: storeGeneratedJsAssetPath + '/app.min.js'
            },
            bootstrapCss: {
                src: [
                    storeGeneratedCssAssetPath + '/vendors/bootstrap/stylesheets/bootstrap.css',
                    storeBaseAssetPath + '/css/vendors/yamma3/yamm/yamm.css'
                ],
                dest: storeGeneratedCssAssetPath + '/frontend.min.css'
            },
            brandCss: {
                src: [
                    storeGeneratedCssAssetPath + '/app.css',
                    storeGeneratedCssAssetPath + '/bower_components/ladda-bootstrap/dist/ladda-themeless.css',
                    //storeGeneratedCssAssetPath + '/bower_components/smartmenus/dist/addons/bootstrap/jquery.smartmenus.bootstrap.css',
                ],
                dest: storeGeneratedCssAssetPath + '/app.min.css'
            },
            zindigoCss: {
                src: [
                    storeGeneratedCssAssetPath + '/app-zindigo.css',
                ],
                dest: storeGeneratedCssAssetPath + '/app-zindigo.min.css'
            },
            facebookCss: {
                src: [
                    storeGeneratedCssAssetPath + '/app-facebook.css',
                ],
                dest: storeGeneratedCssAssetPath + '/app-facebook.min.css'
            }
        },

        cssmin : {
            frontend: {
                src: storeGeneratedCssAssetPath + '/frontend.min.css',
                dest: storeGeneratedCssAssetPath + '/frontend.min.css'
            },
            brand: {
                expand: true,
                cwd: storeGeneratedCssAssetPath + '/views',
                src: ['*.css', '!*.min.css'],
                dest: storeGeneratedCssAssetPath + '/views',
                ext: '.min.css'
            }
        },

        uglify: {
            options: {
                preserveComments: '<%= grunt.config.get("uglifyOptions").preserveComments %>',
                compress: {
                    drop_console: '<%= grunt.config.get("uglifyOptions").drop_console %>'
                }
            },
            frontend: {
                files: [{
                    expand: true,
                    cwd: storeGeneratedJsAssetPath,
                    src: '*.js',
                    dest: storeGeneratedJsAssetPath,
                    ext: '.min.js'
                }]
            },
            brand: {
                files: [{
                    expand: true,
                    cwd: 'twig_templates/themes/store/js/views',
                    src: ['*.js', '!*.min.js'],
                    dest: storeGeneratedJsAssetPath + '/views',
                    ext: '.min.js'
                }, {
                    expand: true,
                    cwd: 'twig_templates/themes/store/js/utilities',
                    src: ['*.js', '!*.min.js'],
                    dest: storeGeneratedJsAssetPath + '/utilities',
                    ext: '.min.js'
                }]
            }
        },

        copy: {
            // Copies bootstrap from the main theme into the brand's for customization
            bootstrap: {
                files: [
                    { // base
                        expand: true,
                        flatten: true,
                        src: [storeBaseAssetPath + '/sass/vendors/bootstrap/stylesheets/*'],
                        dest: 'twig_templates/themes/store/scss/vendors/bootstrap/stylesheets/',
                        filter: function(src) {
                            return (src.indexOf('.scss') != -1);
                        }
                    },
                    { // partials
                        expand: true,
                        flatten: true,
                        src: [storeBaseAssetPath + '/sass/vendors/bootstrap/stylesheets/bootstrap/*'],
                        dest: 'twig_templates/themes/store/scss/vendors/bootstrap/stylesheets/bootstrap/',
                        filter: function(src) {
                            return (src.indexOf('.scss') != -1);
                        }
                    },
                    { // mixins
                        expand: true,
                        flatten: true,
                        src: [storeBaseAssetPath + '/sass/vendors/bootstrap/stylesheets/bootstrap/mixins/*'],
                        dest: 'twig_templates/themes/store/scss/vendors/bootstrap/stylesheets/bootstrap/mixins/',
                        filter: function(src) {
                            return (src.indexOf('.scss') != -1);
                        }
                    },
                    {
                        expand: true,
                        flatten: true,
                        src: [storeBaseAssetPath + '/sass/vendors/bootstrap/fonts/*'],
                        dest: 'twig_templates/themes/store/scss/vendors/bootstrap/fonts/',
                        filter: 'isFile',
                    },
                ]
            },
            // Copies vendor modules installed through npm/bower to the respective store css and js folders
            node_vendor_modules: {
                files: [
                    {
                        expand: true,
                        src: ['node_modules/**/*'],
                        dest: storeGeneratedJsAssetPath,
                        filter: function(src) {
                            return (src.indexOf('.js') != -1 && src.indexOf('grunt') == -1 && src.indexOf('.json') == -1);
                        }
                    },
                    { // copy over the ajax loader for the slick carousel
                        expand: true,
                        src: ['node_modules/slick-carousel/slick/*'],
                        dest: storeGeneratedCssAssetPath,
                        filter: function(src) {
                            return (src.indexOf('.gif') != -1 && src.indexOf('grunt') == -1 && src.indexOf('.json') == -1);
                        }
                    },
                    {
                        expand: true,
                        src: ['node_modules/**/*'],
                        dest: storeGeneratedCssAssetPath,
                        filter: function(src) {
                            return ((src.indexOf('fonts') != -1 || src.indexOf('img') != -1 || src.indexOf('.css') != -1) && src.indexOf('grunt') == -1);
                        }
                    },
                    {
                        expand: true,
                        src: ['bower_components/**/*'],
                        dest: storeGeneratedJsAssetPath,
                        filter: function(src) {
                            return (src.indexOf('.js') != -1 && src.indexOf('grunt') == -1 && src.indexOf('.json') == -1);
                        }
                    },
                    {
                        expand: true,
                        src: ['bower_components/**/*'],
                        dest: storeGeneratedCssAssetPath,
                        filter: function(src) {
                            return ((src.indexOf('fonts') != -1 || src.indexOf('img') != -1 || src.indexOf('.css') != -1));
                        }
                    },
                    { // bootstrap fonts
                        expand: true,
                        flatten: true,
                        src: ['twig_templates/themes/store/scss/vendors/bootstrap/fonts/*'],
                        dest: storeGeneratedCssAssetPath + '/bootstrap/',
                        filter: function(src) {
                            return (src.indexOf('.eot') != -1 || src.indexOf('.svg') != -1 || src.indexOf('.ttf') != -1 || src.indexOf('.woff') != -1);
                        }
                    },
                    {
                        expand: true,
                        flatten: true,
                        src: ['twig_templates/themes/store/fonts/**/*'],
                        dest: storeGeneratedCssAssetPath + '/fonts/',
                        filter: function(src) {
                            return ((src.indexOf('eot') != -1 || src.indexOf('ttf') != -1 || src.indexOf('woff') != -1));
                        }
                    },
                    {
                        expand: true,
                        flatten: true,
                        src: ['twig_templates/themes/store/js/utilities/*'],
                        dest: storeGeneratedJsAssetPath + '/utilities/',
                        filter: function(src) {
                            return (src.indexOf('min.js') != -1 || src.indexOf('blank.png') != -1);
                        }
                    },
                ]
            }
        },

        // Removes the node vendor folders
        clean: {
            options: {
                force: true
            },
            node_vendor_modules: {
                src: [storeGeneratedCssAssetPath + '/node_modules', storeGeneratedJsAssetPath + '/node_modules',
                storeGeneratedCssAssetPath + '/bower_components', storeGeneratedJsAssetPath + '/bower_components',
                storeGeneratedCssAssetPath + '/fonts', storeGeneratedJsAssetPath + '/utilities']
            }
        },

        /**
         * Watches the brand's scss and html for changes and allows for live reload via extension
         */
        watch: {
            scss: {
                files: '**/*.scss',
                options: {
                    livereload: true
                },
                tasks: ['config:dev', 'compass:brand', 'concat', 'cssmin', 'uglify']
            },
            html: {
                files: '**/*.twig',
                options: {
                    livereload: true
                }
            },
            js: {
                files: 'twig_templates/themes/store/js/**/*.js',
                options: {
                    livereload: true
                },
                tasks: ['config:dev', 'concat', 'uglify']
            },
            php: {
                files: 'actions/*.php',
                options: {
                    livereload: true
                },
                tasks: ['config:dev', 'build']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-config');

    // Initial build
    grunt.registerTask('init', ['config:dev', 'copy:bootstrap', 'build'])

    // Build
    grunt.registerTask('build', ['compass', 'clean', 'copy:node_vendor_modules', 'concat', 'cssmin', 'uglify']);

    // Build and watch
    grunt.registerTask('default', ['config:dev', 'build', 'watch']);

    // Environment builds
    grunt.registerTask('dev', ['config:dev', 'build']);
    grunt.registerTask('staging', ['config:staging', 'build']);
    grunt.registerTask('prod', ['config:prod', 'build']);
}
