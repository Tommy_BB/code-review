function blackbackground(){
    $('.newslettercontainer').css('height','auto');
    if ($(document).width()>991){
        $('.newslettercontainer').height($('.footercontainer .col-md-3:nth-child(1)').height());
    }
}

$(document).ready(function() {
    var optedinforCode = false;
    console.log('app module loaded');

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-42769314-1', 'auto');
    ga('send', 'pageview');

    // Hover effect for footer links
    $(document).on({
        mouseover: processDataHover,
        mouseleave: processDataHover
    }, '[data-img-hover][data-toggle]');

    blackbackground();

    /**
     * Toggles the [data-img-hover] images on hover
     */
    function processDataHover() {
        var tempSrc = $(this).attr('src');
        $(this).attr('src', $(this).data('toggle'));
        $(this).data('toggle', tempSrc);
    }

    // Hide the mobile menu if a non-followable link is clicked
    $('.mobile-right a[href="#"]').on('click', function() {
        $('.navbar-collapse').collapse('toggle');
    })

    // Activate validation on the personal shopper modal form
    var $personalShopperModal = $('#personalShopperModal');
    $personalShopperModal.find('form').validate();

    $personalShopperModal.find('form button[type=submit]').on('click', function() {
        console.log('submitting personal shopper request');

        var laddaButton = Ladda.create(this);
        laddaButton.start();

        if (false == $personalShopperModal.find('form').valid()) {
            setTimeout(laddaButton.stop, 500);
            return false;
        }

        // If the form submission was successfully, then set the token that the personal shopper action will authenticate
        var dateString = (new Date()).toISOString().replace(/-/g,"");
        var tokenCipherString = CryptoJS.MD5(dateString);
        $('input[name="tokenCipher"]').val(tokenCipherString);
        $('input[name="token"]').val(dateString);

        $.ajax({
            type: "POST",
            url: $personalShopperModal.find('form').attr('action'),
            data: $personalShopperModal.find('form').serialize(),
            dataType: 'json',
            success: function(result)
            {
                console.log('personal shopper form results ' + result);
                laddaButton.stop();
                if (result.success == true) {
                    $personalShopperModal.find('.row-form').addClass('hidden');
                    $personalShopperModal.find('.row-thankyou').removeClass('hidden');
                    setTimeout(function() {
                        console.log('hiding personal shopper modal');
                        $personalShopperModal.modal('hide');
                    }, 2000);
                }
            },
            error: function(result)
            {
            	console.log(result);
                console.log('could not send personal shopper email');
                laddaButton.stop();
            }
        });

        return false;
    })

    // Size guide modal
    $('#sizeGuide .navigation a').on('click', function() {
        // reset all active navigation items
        $(this).parent().parent().find('> div').removeClass('active');

        var $parent = $(this).parent();
        $parent.addClass('active');

        var targetId = $(this).data('target');
        var $target = $('#' + targetId);
        if (0 == $target.length) {
            console.log('could not locate target');
            return false;
        }

        // hide all of the content pages before showing the new target
        $('.content').hide();

        $target.show().removeClass('hidden');

        return false;
    })
    
    var abc_keys = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65];
	var abc_index = 0;
	$(document).keydown(function(e){
	    if(e.keyCode === abc_keys[abc_index++]){
	        if(abc_index === abc_keys.length){
	            $(document).unbind('keydown', arguments.callee);
	            $("body").find("img").each(function() {
		            $(this).attr("src","/images/page/33/thelax.png");
		 	    })
	        }
	    }else{
	        abc_index = 0;
	    }
	});

    // Newsletter popup centering
    var $newsletterModal = $('#newsletter');
    var $sweepstakesModal = $('#sweepstakes');
    var $oosModal = $('#outofStock');

    var pageLocation = $(location).attr('href');
    if (!pageLocation.match(/checkout/)) {
        console.log('newsletter plugin loaded');

        // Only show the newsletter modal on the first visit to the site
        var visitedSiteKey = 'visited_site_sv';
		try {
            if (typeof $.cookie(visitedSiteKey) === 'undefined' || $.cookie(visitedSiteKey) === null) {
                console.log("showing popup cookie not set");
                $newsletterModal.modal('show');
            } else {
                console.log(visitedSiteKey + " cookie is set");
                console.log($.cookie(visitedSiteKey));
            }
        } catch (e) {
            console.log(e);
            if ('true' != window.sessionStorage.getItem(visitedSiteKey)) {
                $newsletterModal.modal('show');
                window.sessionStorage.setItem(visitedSiteKey, 'true');
            }
		}


        $('#newsletter').on('hidden.bs.modal', function () {
            console.log("hiding newsletter");
            if(!optedinforCode) {
                console.log("setting cookie for 30 days because they didnt opt in");
                $.cookie(visitedSiteKey, "true", { expires : 30 });
            }
        });
    }

    $newsletterModal.on('shown.bs.modal', centerModal);
    $sweepstakesModal.on('shown.bs.modal', centerModal);
    $oosModal.on('shown.bs.modal', centerModal);
    $(window).on('resize', function () {
        $('#newsletter:visible').each(centerModal);
        $('#sweepstakes:visible').each(centerModal);
        $('#outofStock:visible').each(centerModal);
    })

    // Centers a bootstrap modal
    function centerModal() {
        $(this).css('display', 'block');
        var $dialog = $(this).find(".modal-dialog");
        var offset = ($(window).height() - $dialog.height()) / 2;
        // Center modal vertically in window
        $dialog.css("margin-top", offset);
    }

    $newsletterModal.find('input[name="email"]').keydown(function(event) {
        if (event.keyCode == 13) {
            $newsletterModal.find('.signup').click();
            return false;
        }
    })

    // Newsletter processing
    $newsletterModal.find('.signup').click(function () {
        var $parentForm = $(this).parents('form');

        if ($parentForm.validate() && $parentForm.valid()) {

            $.cookie("visited_site_sv", "true", { expires : 999 });

            var storeId = $parentForm.find('input[name="sid"]').val();
            var email = $parentForm.find('input[name="email"]').val();
            $.ajax({
                url: $parentForm.attr('action'),
                dataType: 'json',
                crossDomain: true,
                data: {
                    sid: storeId,
                    action: "optinlist",
                    email: email
                }
            }).done(function () {
                optedinforCode = true;
                console.log('newsletter successfully submitted');
                $newsletterModal.find('.form').hide();
                $newsletterModal.find('.thankyou').removeClass('hidden');
            });
        } else {
            console.log('newsletter form not valid');
        }

        return false;
    });

    $newsletterModal.on('hidden.bs.modal', function (e) {
    	/*
        $sweepstakesModal.find('input[name="email"]').val(email);
        $sweepstakesModal.modal('show');
        */
    })

    // Sweepstake processing
    $sweepstakesModal.find('button[type="submit"]').click(function () {
        var $parentForm = $(this).parents('form');
        console.log($parentForm);

        if ($parentForm.validate() && $parentForm.valid()) {
            $.ajax({
                url: $parentForm.attr('action') + "&" + $parentForm.serialize(),
                dataType: 'json',
                crossDomain: true
            }).done(function (data) {
                if (data.success) {
                    console.log('sweepstakes successfully submitted');
                    $sweepstakesModal.find('.form').hide();
                    $sweepstakesModal.find('.thankyou').removeClass('hidden');
                    setTimeout(function() {
                        $sweepstakesModal.modal('hide');
                    }, 1000);
                } else {
                    console.log('error submitting sweepstakes');
                }
            });
        } else {
            console.log('sweepstakes form not valid');
        }

        return false;
    });

    $('input[name="join"]').keydown(function(event) {
        if (event.keyCode == 13) {
            return $('.signup-footer').click();
        }
    });

    $('.signup-footer').click(function () {
        var email = $("input[name='join']").val();
        if (email != "" && email != null) {
            var storeId = $('input[name="storeId"]').val();
            $.ajax({
                url: "//marketing.zindigo.com/ajax.php",
                dataType: 'json',
                crossDomain: true,
                data: {
                    sid: storeId,
                    action: "optinlist",
                    email: email
                }
            }).done(function () {
                $("input[name='join']").val("THANK YOU!");
                if (!$sweepstakesModal.find('.thankyou').is('hidden')) {
                    $sweepstakesModal.find('input[name="email"]').val(email);
                    $sweepstakesModal.modal('show');
                }
            });
        }

        return false;
    });

    $("#newstter_form").submit(function(){
        var url=$("#newstter_form").attr('action');
        var sid=$("#sid").val();
        var email=$("#newsletter_email").val();
        var data={action:'optinlist',email:email,sid:sid}
        $.ajax({
            url: url,
            dataType: 'json',
            crossDomain: true,
            data: data
        }).done(function () {
            $("#newsletter_email").val("THANK YOU!");
            setTimeout(function() {$("#newsletter_email").val("");},1500);
        });
        return false;
    });
    
    $(".prodvalcont").click(function() {
        var hidethis = false;
        if($(this).parent().find(".valoptions").css('display') == "block") {
            hidethis = true;
        }
        $(".valoptions").hide();
        if(!hidethis)
        $(this).parent().find(".valoptions").toggle();
	});
	
	$(".suboptionpr").click(function() {
        var thisval = $(this).find("input").val();
        var toselecttext = $(this).text();
        $(this).parent().parent().find(".valtext").text(toselecttext);
        $(this).parent().parent().find("select").val(thisval).trigger('change');
        $(this).parent().hide();
	});
	
	$(".val-selector").mouseleave(function () {
	    $(this).find(".valoptions").hide();
	});

});

$(window).on('resize', function () {
    blackbackground();
});

jQuery.extend({
    compare : function (a,b) {
        var obj_str = '[object Object]',
            arr_str = '[object Array]',
            a_type  = Object.prototype.toString.apply(a),
            b_type  = Object.prototype.toString.apply(b);

        if ( a_type !== b_type) { return false; }
        else if (a_type === obj_str) {
            return $.compareObject(a,b);
        }
        else if (a_type === arr_str) {
            return $.compareArray(a,b);
        }
        return (a === b);
    }
});

jQuery.extend({
    compareArray: function (arrayA, arrayB) {
        var a,b,i,a_type,b_type;
        // References to each other?
        if (arrayA === arrayB) { return true;}

        if (arrayA.length != arrayB.length) { return false; }
        // sort modifies original array
        // (which are passed by reference to our method!)
        // so clone the arrays before sorting
        a = jQuery.extend(true, [], arrayA);
        b = jQuery.extend(true, [], arrayB);
        a.sort();
        b.sort();
        for (i = 0, l = a.length; i < l; i+=1) {
            a_type = Object.prototype.toString.apply(a[i]);
            b_type = Object.prototype.toString.apply(b[i]);

            if (a_type !== b_type) {
                return false;
            }

            if ($.compare(a[i],b[i]) === false) {
                return false;
            }
        }
        return true;
    }
});

jQuery.extend({
    compareObject : function(objA,objB) {

        var i,a_type,b_type;

        // Compare if they are references to each other
        if (objA === objB) { return true;}

        if (Object.keys(objA).length !== Object.keys(objB).length) { return false;}
        for (i in objA) {
            if (objA.hasOwnProperty(i)) {
                if (typeof objB[i] === 'undefined') {
                    return false;
                }
                else {
                    a_type = Object.prototype.toString.apply(objA[i]);
                    b_type = Object.prototype.toString.apply(objB[i]);

                    if (a_type !== b_type) {
                        return false;
                    }
                }
            }
            if ($.compare(objA[i],objB[i]) === false){
                return false;
            }
        }
        return true;
    }
});