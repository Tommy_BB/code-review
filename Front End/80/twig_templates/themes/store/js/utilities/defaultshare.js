var defaultshare = (function() {
	var options = {
		themainelement : "",
		inittiedfb : false
	};
	
	var init = function(theprodelement, descelement, appid,prodhref,agentid, isurl) {
		if(typeof(isurl)==='undefined') isurl = "";
		options.themainelement = theprodelement;
		//console.log("defaultshareinit");
		$(options.themainelement).each(function() {
			if ($(this).hasClass("dsinited")) return true;
			
			if($(this).find(".ads-img").length >=1) return true;
			if($(this).hasClass("ad")) return true;
			if($(this).hasClass("ad-jbox")) return true;
			
			//console.log($(this).find("a"));
			$(this).find(".defaultsharer").remove();
			var thisimg = $(this).find("img").first().attr("src");
			var thisname = $(this).find($(descelement)).text().substring(0,$(this).find($(descelement)).text().indexOf('$'));
			thisname = thisname.replace(/^\s+|\s+$/g,'');
			
			var thisurls = $(this).find("input[name=yourls]").val();

			var inside = "";
			inside += '<div class="defaultsharer">';
			if($(this).find("input[name=stylenumber]").length >= 1)
			{
				inside += '<div style="color:#FFF;font-family: \'AvenirNextLTPro-Regular\'; font-size: 12px;">STYLE NUMBER : '+$(this).find("input[name=stylenumber]").val()+'</div>';
			}
			inside += '<div class="moreinfo">MORE DETAILS</div>';
			if (isurl) {
				inside += '<div class="defaultcenter">';
				inside += '<div class="shareit">SHARE IT:</div>';
				inside += '<a class="defshare deffb" href="http://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(thisurls)+'" target="_blank"></a>';
				inside += '<a class="defshare deffbmess" href="javascript:void(0)"></a>';
				inside += '<a class="defshare deftwitter" href="https://twitter.com/intent/tweet?url='+encodeURIComponent(thisurls)+'&text='+thisname+'" target="_blank"></a>';
				inside += '<a class="defshare defpin" href="http://pinterest.com/pin/create/button/?url='+encodeURIComponent(thisurls)+'&media='+encodeURIComponent("https://stores.zindigo.com/"+thisimg)+'&description='+thisname+'" target="_blank"></a>';
				inside += '<div style="clear:both;"></div>';
			inside += '</div>';
			}
			inside += '</div>';
			
			$(this).append(inside);
			
			try {
			if (typeof FB == 'undefined' && !options.inittiedfb)
			{
				//console.log("initting fb...");
				options.inittiedfb = true;
		    	$("body").prepend('<div id="fb-root"></div>');
		    	 window.fbAsyncInit = function() {
				    FB.init({
				      appId : appid, 
				      status : true,
				      cookie : true,
				      xfbml : true
				    });
  				};
			  (function(d){
			     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
			     if (d.getElementById(id)) {return;}
			     js = d.createElement('script'); js.id = id; js.async = true;
			     js.src = "//connect.facebook.net/en_US/all.js";
			     ref.parentNode.insertBefore(js, ref);
			   }(document));
			}
			} catch(e) 
			{}
			$(this).find(".deffbmess").click(function() {
				//console.log(thisurls + " isthisurls");
				FB.ui({
		          method: 'send',
		          name: thisname,
		          link: thisurls
		    	});
			});
			
			$(this).find(".moreinfo").click(function() {
				if(isurl == 78)
				{
					$(this).parent().parent().find(".details").click();
				} else
				if(isurl == 23)
				{
					$(this).parent().parent().find(".lnk-prodinfo").click();
				}else {
					var thelinkto = $(this).parent().parent().find(prodhref).attr("href");
					location.href = thelinkto;
				}
			});
			
			$(this).hover(
				function() {
					if($(this).find(".fb-like").length <= 0) {
                        $(this).find(".defaultsharer").append('<div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true" data-href="'+thisurls+'"></div>');
                    }
					try {
						FB.XFBML.parse();
					} catch(e) 
					{
					}
					$(this).find(".defaultsharer").show();
				},
				function() {
					$(this).find(".defaultsharer").hide();
					$(this).find(".fb-like").remove();
				}
			);
			
			$(this).addClass("dsinited");
		});
	};
	
	return {
		options : options,
		init: init
	};

})();

