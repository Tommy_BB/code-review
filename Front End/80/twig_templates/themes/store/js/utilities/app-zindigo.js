$(document).ready(function() {
    window.setInterval(function () {
        $("#content-wrap").height($(window).height() - ($("nav").outerHeight(true) + $(".top-nav").outerHeight(true)));
        $("#content-wrap").css('overflow-y', 'auto');
    }, 1000);
});