var geocoder;
var map;
$(document).ready(function() {
    var bgImg = $(".stockists").find(".bg-img").first().attr("src");
    console.log(bgImg + " was found");
    $(".stockists").css("background-image","url('"+bgImg+"')");

function fixStockists()
    {
        var $baselineProduct = $('.current-stores .store:first-child');
        if ($baselineProduct.length != 0) {
            $('.current-stores .store').each(function () {
                var $article = $(this);
                $article.css('height', 'auto');
                $article.height($baselineProduct.height());
		$article.css('min-height', '160px');
            });
        }
    }

    function fixStoryText()
    {
        $(".stockist-holder").height($(".stockists").find(".bg-img").first().height());
        $(".map-holder:visible .map-canvas").height($(".stockists").find(".bg-img").first().height());
fixStockists();
    }

    setInterval(fixStoryText, 500);
    $(window).resize(fixStoryText);

    $("select[name=country]").change(function() {
        var country = $(this).val();
        console.log(country);
        if (country === "US") {
            $('.state-selector').show();
        } else {
            $("select[name=state] option:selected").prop("selected", false);
            $('.state-selector').hide();
        }
    });


    $(".submit-stockist").click(function() {
        queryStores();
    });


    function queryStores()
    {
        var country = $("select[name=country] option:selected").val();
        var state = ((country == "US") ? $("select[name=state] option:selected").val() : "");
        $.ajax({
            url: "/json/storelocator.php",
            data: "storeid=80&country="+country+"&state="+state,
            dataType: 'json',
            async: true,
            success: function (items) {
                $(".current-stores").empty();
                for(i=0; i< items.length; i++){
                    var searchaddress = "";
                    if (items[i]['Country'] == 'US'){
                        searchaddress = items[i]['Address'] + " " + items[i]['City'] +", " + items[i]['State'] + " " + items[i]['Zipcode'];
                    } else {
                        searchaddress = items[i]['Address'] + " " + items[i]['City'] + ", " + items[i]['Country'] + " " + items[i]['Zipcode'];
                    }
                    items[i]['curCount'] = i;
                    items[i]['searchAddress'] = searchaddress;
                    $(".current-stores").append( $("#stockist-template").render(items[i]));
                }
            }
        });
    }

});
function codeAddress(a) {
    $(".map-holder:visible .map-canvas").show();
    initialize();
    var address = document.getElementById(a).value;
    geocoder.geocode( { 'address': address}, function(results, status) {
        console.log(results);
        console.log(status);
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function initialize() {
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(-34.397, 150.644);
    var mapOptions = {
        zoom: 15,
        center: latlng
    }
    map = new google.maps.Map($(".map-holder:visible .map-canvas")[0], mapOptions);
}


google.maps.event.addDomListener(window, 'load', initialize);
