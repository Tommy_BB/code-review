$(document).ready(function() {
    console.log('purchase now module loaded');
    $('.pagetitle').html('Billing Address')

    var $billing = $('.billing');
    var $shipping = $('.shipping');
    var $pageTitle = $('.pagetitle');

    var updateOrderInformation = function(serializable) {
        var inforMation = $(serializable).serialize();
        $.ajax({
            url: "/json/orderActions.php",
            data: inforMation+"&action=billing&XDEBUG_PROFILE=1",
            dataType: 'json',
            async: true,
            type: "POST",
            success: function (msg) {
                $billing.hide();
                $pageTitle.html('Shipping Address');
                $shipping.fadeIn();
            }, error: function(msg) {
                $billing.hide();
                $pageTitle.html('Shipping Address');
                $shipping.fadeIn();
            }


        });
    };


    $('button.prev').on('click', function() {
        if ($billing.is(':visible')) {
            parent.history.back();
        } else {
            $shipping.hide();
            $pageTitle.html('Billing Address');
            $billing.fadeIn();
        }

        return false;
    })

    $('button.next').on('click', function() {
        var billingEmail = $billing.find('[name=email]').val();
        var shippingEmail = $shipping.find('[name=ship_email]').val();
        $shipping.find('[name=ship_contact]').val(shippingEmail);
        $billing.find('[name=contact]').val(billingEmail);
        $("#billing-form").validate({
            submitHandler: function(form) {
                if ($billing.is(':visible')) {
                    updateOrderInformation($("#billing-form"));
                } else {
                    form.submit();
                }
            }
        })
    })

    
    billingToShippingInputMap = {
        'company': 'ship_company',
        'fname': 'ship_fname',
        'lname': 'ship_lname',
        'address': 'ship_address1',
        'address2': 'ship_address2',
        'city': 'ship_city',
        'state': 'ship_state',
        'country': 'ship_country',
        'zipcode': 'ship_zip',
        'email': 'ship_email',
        'phone': 'ship_phone',
        'fax': 'ship_fax'
    }

    $('#copy-billing').on('click', function() {
        console.log('copying billing information');

        var isSelected = $(this).is(':checked');

        for (billingInputName in billingToShippingInputMap) {
            var shippingInputName = billingToShippingInputMap[billingInputName];
            var $billingField = $billing.find($('[name="'+ billingInputName +'"]'));
            var $shippingField = $shipping.find($('[name="'+ shippingInputName +'"]'));

            if ($billingField.is('select')) {
                var selectedBillingSelectIdx = (isSelected) ? $billingField.find('option:selected').index() : 0;
                $shippingField.find('option:eq('+ selectedBillingSelectIdx +')').prop('selected', true);
            } else {
                var shippingFieldValue = (isSelected) ? $billingField.val() : '';
                $shippingField.val(shippingFieldValue);
            }
        }
    })
})