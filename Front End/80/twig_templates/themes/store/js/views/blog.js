$(document).ready(function() {
    console.log("loaded story");
    var bgImg = $(".story").find(".story-image").attr("src");

    console.log(bgImg + " was found");

    $(".story").css("background-image","url('"+bgImg+"')");



    function fixArticles()
    {
        var $baselineProduct = $('div.blog-article:first-child');
        if ($baselineProduct.length != 0) {
            $('.blog-article').each(function () {
                var $article = $(this);
                $(this).find(".article-info").css('height', 'auto');
                $article.css('height', 'auto');
                $article.height($baselineProduct.height());
                $(this).find(".article-info").height($baselineProduct.height());
            });
        }
    }

    setInterval(fixArticles, 500);
    $(window).resize(fixArticles);
});