$(document).ready(function () {
    $('.alternate-view-gallery:first').css('opacity','1');

    $('.alternate-view-gallery').click(function(){
        $('.alternate-view-gallery').css('opacity','0.5');
        $(this).css('opacity','1');
    });

    // Attach handler for back button
    // $('a.back').on('click', function() {
    // self.history.back();
    // return false;
    // });

    //Fixed for Safari
    if (window.top.location.href == document.referrer){
        $('a.back').hide();
    } else {
        $('a.back').attr('href',document.referrer);
    }

    $(".suboptionpr").unbind();
    $(".suboptionpr").click(function() {
        var toselecttext = $(this).text();
        var thisval = $(this).find("input").val();
        $(this).parent().parent().find(".valtext").text(toselecttext);
        $(this).parent().parent().find("select").val(thisval).trigger('change');
        $(this).parent().hide();
    });

    // Alternate image gallery
    $('a.cloud-zoom-gallery').on('click', function() {
        $('a.cloud-zoom-gallery').removeClass("active");
        $(this).addClass("active");
        var $alternateImage = $(this).find('img');
        var $largeProductImage = $('#large-product-image');
        var largePath = $alternateImage.data('large');
        console.log('Showing alternate image: ' + largePath);

        $largeProductImage.attr('href', largePath);

        return false;
    })
    $('a.cloud-zoom-gallery').first().click();


    // Alternate image gallery (mobile)
    $('a.alternate-view-gallery').on('click', function() {
        var $alternateImage = $(this).find('img');
        var $largeProductImage = $('.large-product-image');
        var largePath = $alternateImage.data('large');
        console.log('Showing alternate image: ' + largePath);

        $largeProductImage.attr('src', largePath);

        // Focus on the product image
        $('html, body').animate({
            scrollTop: $largeProductImage.offset().top
        }, 1000);

        return false;
    })

    // Setup validation for the product form
    var $productForm = $('#add-product');
    $productForm.validate({
        ignore: ""
    });

    // Show a message if a user does not select a finish
    jQuery.validator.addMethod("finish", function(value, element) {
        var finish = $('select[name=Finish]');
        return (finish.val() || finish.length <= 0) ? true : false;
    })

    // Show a message if a user does not select a color
    jQuery.validator.addMethod("color", function(value, element) {
        var color = $('select[name=Color]');
        return (color.val() || color.length <= 0) ? true : false;
    })

    // Show a message if a user does not select a size
    jQuery.validator.addMethod("size", function(value, element) {
        var size = $('select[name=Size]');
        return (size.val() || size.length <= 0) ? true : false;
    })

    // Validate the quantity before allowing a user to add a product to their cart
    jQuery.validator.addMethod("quantity", function(value, element) {
        var size = $('select[name="Size"] option:selected').val();
        var color = $('select[name=Color]').val();
        return isProductAvailable(value, size, color);
    })


    // Get all available color value ids for this product
    var colorValueIds = [];
    $('select[name=Color] option').each(function() {
        if($(this).val() != "")
            colorValueIds.push($(this).val());
    })
    console.log('Available color option values: ' + colorValueIds);

    var sizeValueIds = [];
    $('select[name="Size"] option').each(function() {
        if($(this).val() != "")
            sizeValueIds.push($(this).val());
    })
    console.log('Available size option values: ' + sizeValueIds);

    var finishValueIds = [];
    $('select[name="Finish"] option').each(function() {
        if($(this).val() != "")
            finishValueIds.push($(this).val());
    })
    console.log('Available finish option values: ' + sizeValueIds);

    var sizeSelectorCloneHtml = $('select[name="Size"]').clone(true).wrap('<div/>').parent().html();

    $('select[name="Color"]').on('change', function() {
        $('select[name="Size"]').replaceWith(sizeSelectorCloneHtml);

        /* REMOVE THIS IF YOU DONT HAVE FANCY SELECT BOXES */
        $('select[name="Size"]').parent().find(".valoptions .suboption:first").click();
        $('select[name="Size"]').parent().find(".valoptions .suboption").each(function() {
            $(this).show();
        });
        /* END */

        var selectedColorValueId = $(this).val();
        var validSizeValues = [];

        var uinventory = JSON.parse($('#inventory').val());

        for (inventoryIdx in uinventory) {
            var inventoryData = uinventory[inventoryIdx];
            for (attribute in inventoryData) {
                if (attribute.toLowerCase().indexOf('value') != -1) {
                    var valueId = inventoryData[attribute];

                    $.each(colorValueIds, function(key, colorValueId) {
                        if (selectedColorValueId == colorValueId) {
                            if (inventoryData['Value1Type'] == 'size') {
                                var sizeValueId = inventoryData['Value1'];
                            } else {
                                var sizeValueId = inventoryData['Value2'];
                            }

                            if (colorValueId == valueId && inventoryData['Qty'] > 0 && sizeValueId > 0) {
                                validSizeValues.push(sizeValueId);
                                console.log('Size value: ' + sizeValueId + ' is available.');
                            }
                        }
                    })


                }
            }
        }

        console.log("Valid Sizes Below");
        console.log(validSizeValues);
        console.log("_______");
        /* REMOVE THIS IF YOU DONT HAVE FANCY SELECT BOXES */
        $('select[name="Size"]').parent().find(".valoptions .suboption").each(function() {
            var thisinput = $(this).find("input").val();
            if (-1 == $.inArray(thisinput, validSizeValues) && thisinput != "") {
                $(this).hide()
            }
        });
        /* END REMOVE THIS IF YOU DONT HAVE FANCY SELECT BOXES */

        $('select[name="Size"] option').each(function() {
            if (-1 == $.inArray($(this).val(), validSizeValues) && $(this).val() != "") {
                $(this).remove();
            }
        });

        /* REMOVE THIS IF YOU DONT HAVE FANCY SELECT BOXES */
        $('select[name="Finish"]').parent().find(".valoptions .suboption").each(function() {
            var thisinput = $(this).find("input").val();
            if (thisinput != "") {
                // $(this).show();
                // console.log($('select[name="Color"]').val());
                if ($('select[name="Color"]').val()!=''){
                    $(this).show();
                } else {
                    $(this).hide();
                }

            }
        });

    })

    //click the first color to reset the sizes on pretty selects
    $('select[name="Color"]').parent().find(".valoptions .suboption:first").click();

    /**
     * Determines product availability by comparing combinations of size and color value ids against a product's inventory
     *
     * @author Chris Willard
     * @param quantityDesired the quantity desired for a particular product
     * @param sizeValueId the size value property id
     * @param colorValueId the color value property id
     * @returns {boolean}
     */
    function isProductAvailable(quantityDesired, sizeValueId, colorValueId) {
        var inventory = JSON.parse($('#inventory').val());
		console.log('inventory',inventory);
		console.log('searching for color:',colorValueId);
		console.log('searching for size:',sizeValueId);

        var sizeIsAvailable = false;
        var colorIsAvailable = false;

        // If the product does not have colors then no need to validate the colors
        var productHasColors = !(colorValueId == undefined || colorValueId == '');
        if (!productHasColors) {
            colorIsAvailable = true;
        }

        var productHasSizes = !(sizeValueId == undefined || sizeValueId == '');
        if (!productHasSizes) {
            sizeIsAvailable = true;
        }

        console.log("HERE : " + productHasSizes + " - " + productHasColors);

        console.log("HERE2 : " + sizeIsAvailable + " - " + colorIsAvailable);

        if (0 == inventory.length) {
            return true;
        }

        for (inventoryIdx in inventory) {
            var inventoryData = inventory[inventoryIdx];
            for (attribute in inventoryData) {

                if (attribute.toLowerCase().indexOf('value') != -1) {
                    var valueId = inventoryData[attribute];
                    switch (valueId) {
                        case sizeValueId:
                            if (false == sizeIsAvailable) {
                                console.log('size is available');
                                sizeIsAvailable = true;
                            }
                            break;

                        case colorValueId:
                            if (false == colorIsAvailable) {
                                console.log('color is available');
                                colorIsAvailable = true;
                            }
                            break;
                    }
                }


            }

            console.log(sizeIsAvailable + " -- " + colorIsAvailable + " for ");
            console.log(inventoryData);
            // If both values are available then the product is available, just need to validate quantity
            if (true == sizeIsAvailable && true == colorIsAvailable) {
                console.log('size and color is available, checking quantity');
                console.log('comparing quantity (desired: ' + quantityDesired + ') (stock: ' + inventoryData['Qty'] + ')');
                console.log('looking at inventoryData ',inventory[inventoryIdx]);
                return (parseInt(quantityDesired) <= parseInt(inventoryData['Qty']));
            } else {

                if (!productHasSizes) {
                    sizeIsAvailable = true;
                }
                if (!productHasColors) {
                    colorIsAvailable = true;
                }
            }
        }

        return false;
    }
})

