$(document).ready(function () {
    console.log('press module loaded');
    function getFeatured(subsection)
    {
        $.ajax({
            url: "//stores.zindigo.com/json/pressajaxcaptions.php",
            data: "storeid=80&action=morepress&limit=999&current=0&orderby=&subsection="+subsection,
            dataType: 'json',
            async : true,
            success: function(items) {
                for(it in items)
                {
                    var inside = "";
                    inside += '<div class="press-item col-xs-12 col-sm-4">';
                    inside += '<a href="#" class="gallery" data-featherlight="<div class=\'about-press\'>'+items[it]['name']+'<br/>'+items[it]['caption']+items[it]['date']+'</div><div class=\'press-image\'><img src=\''+items[it]['mainimage']+'\' /></div>">';
                    inside += '<img src="'+items[it]['mainimage']+'" />';
                    inside += '</a>';
                    for(those in items[it]['subimages']) {
                        if (items[it]['subimages'][those]['subimages'] != items[it]['mainimage']) {
                            inside += '<a href="#" style="display: none;" class="gallery" data-featherlight="<div class=\'about-press\'>' + items[it]['name'] + '<br/>' + items[it]['caption'] + items[it]['date'] + '</div><div class=\'press-image\'><img src=\'' + items[it]['subimages'][those]['subimages'] + '\' /></div>"></a>';
                        }
                    }
                    inside += '<div class="hoverpart"> <div class="articlestext"> <div class="articletitle">'+items[it]['name']+'</div> <div class="firstcaption">'+items[it]['caption']+'</div> <div class="articledate">'+items[it]['date']+'</div></div> </div>';
                    inside += '</div>';
                    $(".presslideshow").append(inside);
                }

                $('.presslideshow .press-item a').featherlightGallery({
                    previousIcon: '<img src="/templates/80/images/white-arrow-left.png" />',
                    nextIcon: '<img src="/templates/80/images/white-arrow-right.png" />',
                    closeIcon : '<img src="/templates/80/images/white-close.png" />',
                    galleryFadeIn: 300,
                    openSpeed: 300,
                    afterOpen:    function(event){
                        $(".featherlight-close").click(function() {
                            $(".featherlight").hide();
                        });
                    },
                    afterContent : function(event) {
                    },
                    afterClose:   function(event){
                    }
                });

                $(".press-item .hoverpart").click(function() {
                   $(this).parent().find('a:visible').first().click();
                });

            }
        });
    }
    getFeatured($("input[name=subsection]").val());
});