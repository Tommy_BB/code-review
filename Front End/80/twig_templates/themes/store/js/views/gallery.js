
$(document).ready(function () {

    function autoResizeProducts() {
        var biggestHeight = 0;
        $('.igposts .igpost').each(function () {
            var $product = $(this);
            if($(this).height() > biggestHeight) {
                biggestHeight = $(this).height();
            }
        });

        $('.igposts .igpost').each(function () {
            var $product = $(this);
            $product.css('height', 'auto');
            $product.height(biggestHeight);
        })

    }
    // Auto resize the ads

    if (!in_facebook) {
        setInterval(autoResizeProducts, 500);
        $(window).resize(autoResizeProducts);
    } else {
        autoResizeProducts();
    }

    Dropzone.options.myAwesomeDropzone = {
        url: "/json/uploadimage.php",
        autoProcessQueue: false,
        uploadMultiple: false,
        parallelUploads: 1,
        maxFiles: 1,
        acceptedFiles: 'image/*',
        previewTemplate: $("#preview-template").html(),
        init: function() {
            var myDropzone = this;
            this.on("addedfile", function(file) {
                console.log(file);
                setTimeout(function() {
                    var theImage = $("#my-awesome-dropzone .dz-image-preview").find("img").first().attr("src");
                    $("#my-awesome-dropzone .dz-message").find("img").first().attr("src", theImage);
                    console.log(theImage + " is the src");
                },1000);

            });

            $(".post-photo").click(function (e) {
                if($(".agree-terms").find(".fa-check-square-o").length <= 0) {
                    $(".uploading-error").text("Please agree to the terms & conditions");
                    return true;
                }

                if($(".other-user-vals").find("input[name=nickname]").val().length <= 0) {
                    $(".uploading-error").text("Please enter your nickname.");
                    return true;
                }

                if($(".other-user-vals").find("textarea[name=description]").val().length <= 0) {
                    $(".uploading-error").text("Please enter your description for this post.");
                    return true;
                }
                    $(".uploading-error").text("");
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();

            });
        },
        complete: function(file, responseText) {
            $(".upload-row").hide();
            $(".finished-row").show();

        },
        sending: function(file, xhr, formData) {
            formData.append("alias", encodeURIComponent($(".other-user-vals").find("input[name=nickname]").val()));
            formData.append("description", encodeURIComponent($(".other-user-vals").find("textarea[name=description]").val()));
        }

    }

    $(".agree-terms").click(function() {
        //uncheck
        if($(this).find(".fa-check-square-o").length > 0)
        {
            $(this).find("i").remove();
            $(this).prepend('<i class="fa fa-square-o" aria-hidden="true"></i>');
        } else {
            $(this).find("i").remove();
            $(this).prepend('<i class="fa fa-check-square-o" aria-hidden="true"></i>');
        }
    });

    $("#gallery-upload .cancel-photo").click(function() {
        $("#gallery-upload .close").click();
    });
});