var api;
$(function () {
    if($.browser.mozilla == true)
    {
        $(".splashpage").css("paddingLeft","10px");
        $(".splashpage").css("paddingRight","11px");
        $(".player").css("paddingLeft","10px");
        $(".player").css("paddingRight","11px");
        //$(".player video").css("paddingLeft","10px");
        //$(".player video").css("paddingRight","11px");
    }

    // install flowplayer to an element with CSS class "player"
    $(".player").flowplayer({
        swf: "/js/80/flowplayer.swf",
        tooltip : false,
        fullscreen : false

    });
    $("a[href='http://flowplayer.org']").css("visibility","hidden");
    api = flowplayer();

    $(".videosmall").on("click", function(event) {
        event.preventDefault();
        var vidname = $(this).attr("name");
        console.log(vidname);
        api.load(vidname);
    });

});