var smartajax = "";
var currentcatid = "";
var searchWord = "";

var prodOptions = {
    'totalProds': 0,
    'currentView': 24
};

$(document).ready(function () {

    console.log("loaded collections");

    sessionStorage.clear();
    console.log('products module loaded');
    currentcatid = $('#catid').val();
    searchWord = $('#searchword').val();
    // JSRender view helpers
    $.views.helpers({
        // Convert value to an integer
        Integer: function (val) {
            return val.replace(".00", "");
        },

        round: function (val) {
            val = val + "";
            val = val.replace(".00", "");
            return val;
        },

        Number: function (val) {
            return Number(val);
        },

        Truncate: function (val, length, ellipsis) {
            if (typeof length == 'undefined') {
                var length = 100;
            }

            if (typeof ellipsis == 'undefined') {
                var ellipsis = '...';
            }

            if (val.length <= length) {
                return val;
            }

            for (var i = length - 1; val.charAt(i) != ' '; i--) {
                length--;
            }

            return val.substr(0, length - 1) + ellipsis;
        },
        FormatSizesNew: function (val) {
            if (val == "undefined") return val;

            if (val.toLowerCase().indexOf("xs") >= 0 || val.toLowerCase().indexOf("s") >= 0 || val.toLowerCase().indexOf("m") >= 0 || val.toLowerCase().indexOf("k") >= 0 || val.toLowerCase().indexOf("xl") >= 0 || val.toLowerCase().indexOf("xxl") >= 0) {
                var splitarray = val.toLowerCase().split(',');
                var returnedarray = [];
                if ($.inArray("xs", splitarray) !== -1)
                    returnedarray.push("XS");
                if ($.inArray("s", splitarray) !== -1)
                    returnedarray.push("S");
                if ($.inArray("m", splitarray) !== -1)
                    returnedarray.push("M");
                if ($.inArray("l", splitarray) !== -1)
                    returnedarray.push("L");
                if ($.inArray("xl", splitarray) !== -1)
                    returnedarray.push("XL");
                if ($.inArray("xxl", splitarray) !== -1)
                    returnedarray.push("XXL");

                val = returnedarray.join(" ");
            }

            val = val.replace(",", " ");
            val = val.replace(/,/g, ' ');
            return val;
        },
        FixSizes: function (val) {
            val = val.replace(/ /g, '-');
            val = val.replace(/\//g, '-');
            val = val.replace(/,/g, ' SIZE');
            if (val != "" && val != null)
                return "SIZE" + val;
            else return "";
        },
        FixColors: function (val) {
            val = val.replace(/ /g, ' COLOR');
            if (val != "" && val != null)
                return "COLOR" + val;
            else return "";
        }
    });

    if (searchWord.length > 0) {
        console.log("search word length is " + searchWord.length);
        $('#content-wrap #action').last().val("search");
    }

    var settings = {
        'container': $('.products'),
        'preloaderContainer': $('.z-preloader'),
        'categoryFilters': $('button.category-filter'),
        'storeid': $('#storeid').val(),
        'catid': $('#catid').val(),
        'action': $('#content-wrap #action').last().val(),
        'sharedAppId': $('#share-app-id').val(),
        'sharedAgentId': $('#share-agent-id').val(),
        'brandUrl': $('#brand-url').val(),
        'fadeEffect': true,
        'limit': 999,
        'productStepDuration': 200
    }
    var callbacks = {
        success: function () {
            settings.preloaderContainer.fadeOut();
            $('.loader_product').remove();
            if (settings.container.children().length > 0) {
                $('#Container').mixItUp();
                $('#Container').mixItUp('filter', 'all');
                prodOptions.totalProds = $("#Container .product").length;
                $('#Container').on('mixEnd', function (e, state) {
                    buildpagination();
                });
                $(".product").hover(function () {
                        var replacer = $(this).find("img").first().attr("data-valueover");
                        $(this).find("img").attr("src", replacer);
                    },
                    function () {
                        var replacer = $(this).find("img").first().attr("data-valueout");
                        $(this).find("img").attr("src", replacer);
                    });


                setTimeout(function () {
                    settings.container.zinProducts('more');
                }, 500);
            } else {
                $('#status').html("no products were found for this search");
            }
        },
        loader: function (isLoading) {
            if (isLoading) {
                if ($('.products').html() != '') {
                    $('.loader_product').remove();
                    var div_loader = '<div style="display: inline-block;" class="product col-lg-4 col-md-4 col-sm-4 col-xs-12 mix loader_product"><img src="/templates/144/images/ajax-loader.gif" style="margin-top:45%;"><br/>Loading</div>';
                    $(".products").append(div_loader);
                }
            } else {
                settings.preloaderContainer.fadeOut();
                $('.loader_product').remove();
            }
        }
    }

    settings.callbacks = callbacks;


    /**
     *
     * @param settings
     */
    function getProducts(settings) {
        console.log('retrieving products');

        var productData = {
            storeid: settings.storeid,
            limit: settings.limit,
            catid: settings.catid,
            isurl: encodeURIComponent(settings.brandUrl),
            agentid: settings.sharedAgentId
        }
        if (undefined == settings.action || "" == settings.action) {
            settings.container.zinProducts({
                data: productData,
                itemTemplate: '#product-template',
                preloadTemplate: '#product-loading-template',
                preloadDivContainer: '.preloaded-product',
                callbacks: settings.callbacks
            })
        } else {
            settings.container.zinProducts(settings.action, {
                data: productData,
                itemTemplate: '#product-template',
                preloadTemplate: '#product-loading-template',
                preloadDivContainer: '.preloaded-product',
                callbacks: settings.callbacks
            })
        }
    }

    // Retrieve the products on load
    getProducts(settings);

    function autoResizeProducts() {
        var $baselineProduct = settings.container.find('div.product:first-child');
        if ($baselineProduct.length != 0) {
            $('.products .product').each(function () {
                var $product = $(this);
                $(this).find(".phover a div:first").css('height', 'auto');
                $product.css('height', 'auto');
                $product.height($baselineProduct.height());
                $(this).find(".phover a div:first").height($baselineProduct.height());
            })
        }
    }


    if ($("article").text().length > 0) {
        $('article').readmore({
            speed: 150,
            lessLink: '<a href="#" class="col-md-offset-4 col-md-8 col-sx-12 articlelink">READ LESS<i class="fa fa-angle-up" aria-hidden="true"></i></a>',
            moreLink: '<a href="#" class="col-md-offset-4 col-md-8 col-sx-12 articlelink">READ MORE<i class="fa fa-angle-down" aria-hidden="true"></i></a>',
            blockCSS: ''
        });
    }

    function autosizeDesc() {
        if ($("article").text().length > 0 && $(".ww-collections").length <= 0) {
            $(".title-holder").height($(".desc-holder").height());
        }
        if ($("article").text().length > 0 && $(".ultimate-center").length > 0) {
            $(".ultimate-center").height($("article").height() + 35);
        }

        if($(".ww-desc-holder").length > 0)
        {
            $(".ww-desc-holder").css('height', 'auto');
            if($(".ultimate-center").height() > $(".title-holder").height())
            {
                $(".ww-desc-holder").height($(".ultimate-center").height());
            } else {
                $(".ww-desc-holder").height($(".title-holder").height());
            }
        }
        autoResizeProducts();
    }

    // Auto resize the ads

    if (!in_facebook) {
        setInterval(autosizeDesc, 500);
        $(window).resize(autosizeDesc);
    } else {
        autosizeDesc();
    }

    $("select[name=items-to-show]").change(function (e) {
        var paginateVal = $(this).val();
        console.log($(this).val() + " is items to show");
        prodOptions.currentView = parseInt(paginateVal);
        buildpagination();
        //$('#Container').mixItUp('paginate', {limit: paginateVal});
    });


    $("select[name=sorter]").change(function (e) {
        console.log($(this).val() + " is sorter");
        $('#Container').mixItUp('sort', $(this).val());
        $('#Container').on('mixEnd', function (e, state) {
            buildpagination();
        });

    });


    var buildpagination = function () {
        $(".paginator").each(function () {
            $(this).remove();
        });
        $(".pagor").each(function () {
            $(this).remove();
        });
        $(".holder").remove();
        $(".pagination").append('<div class="holder"></div>');
        pagination(1);
    };

    var pagination = function (selected) {
        console.log("Building pagination for page number" + selected);
        var pages = Math.ceil(prodOptions.totalProds / prodOptions.currentView);
        $(".holder").html('');
        $(".pagor").each(function () {
            $(this).remove();
        });


        //logic here
        if (pages <= 3) {
            var inside = '<div class="paginator">';
            inside += '<div class="paginatorleft"><i class="fa fa-angle-left" aria-hidden="true"></i></div>';
            for (var i = 1; i <= pages; i++) {
                if (i == selected) {
                    inside += "<a href='javascript:void(0)' class='pagor selected'>" + i + "</a>";
                } else {
                    inside += "<a href='javascript:void(0)' class='pagor'>" + i + "</a>";
                }
            }
            inside += '<div class="paginatorright"><i class="fa fa-angle-right" aria-hidden="true"></i></div>';
            inside += "</div>";
            if (selected < 3 && pages != selected) {
                //inside += '<div class="next paginatorright"><i class="fa fa-angle-right" aria-hidden="true"></i></div>';
            }
        } else {
            if (selected < 3) {
                var inside = "<div class='paginator'>";
                inside += '<div class="paginatorleft"><i class="fa fa-angle-left" aria-hidden="true"></i></div>';
                for (i = 1; i <= 3; i++) {
                    if (i == selected) {
                        inside += "<a href='javascript:void(0)' class='pagor selected'>" + i + "</a>";
                    } else {
                        inside += "<a href='javascript:void(0)' class='pagor'>" + i + "</a>";
                    }
                }
                inside += "<div class='dotdotdot'>...</div><a href='javascript:void(0)' class='pagor'>" + Number(pages) + "</a><div class='paginatorright'><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></div><div style='clear:both;'></div></div>";
            } else if (selected > (Number(pages) - 2)) {
                var inside = "<div class='paginator'><div class='paginatorleft'><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></div><a href='javascript:void(0)' class='pagor'>1</a><div class='dotdotdot'>...</div>";
                for (i = (Number(pages) - 2); i <= Number(pages); i++) {
                    if (i == selected) {
                        inside += "<a href='javascript:void(0)' class='pagor selected'>" + i + "</a>";
                    } else {
                        inside += "<a href='javascript:void(0)' class='pagor'>" + i + "</a>";
                    }
                }
                inside += "<div class='paginatorright'><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></div><div style='clear:both;'></div></div>";
                if (selected < (Number(pages))) {
                    //inside += '<div class="next paginatorright"><i class="fa fa-angle-right" aria-hidden="true"></i></div>';
                }
            } else {
                var inside = "<div class='paginator'><div class='paginatorleft'><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></div><a href='javascript:void(0)' class='pagor'>1</a><div class='dotdotdot'>...</div>";
                for (i = (Number(selected) - 1); i <= (Number(selected) + 1); i++) {
                    if (i == selected) {
                        inside += "<a href='javascript:void(0)' class='pagor selected'>" + i + "</a>";
                    } else {
                        inside += "<a href='javascript:void(0)' class='pagor'>" + i + "</a>";
                    }
                }
                inside += "<div class='dotdotdot'>...</div><a href='javascript:void(0)' class='pagor'>" + pages + "</a><div class='paginatorright'><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></div><div style='clear:both;'></div></div>";
                if (selected < (Number(selected) + 1)) {
                    //inside += '<div class="next paginatorright"><i class="fa fa-angle-right" aria-hidden="true"></i></div>';
                }
            }
        }

        $(".paginator").remove();
        $(".viewdividernext").remove();
        $(".next").remove();
        $(".holder").after(inside);

        $(".pagor").click(function () {
            updatePage(this);
        });
        $(".paginatorleft").click(function () {
            if (Number(selected) - 1 > 1)
                updatePagearrow(Number(selected) - 1);
            else
                updatePagearrow(1);
        });

        $(".paginatorright").click(function () {
            if (selected < pages)
                updatePagearrow(Number(selected) + 1);
            else
                updatePagearrow(pages);
        });

        cleanupProds(selected);
    };

    var updatePagearrow = function (selx) {
        pagination(selx);
    };

    var updatePage = function (elem) {
        pagination($(elem).text());
    };

    var cleanupProds = function (curpage) {
        curpage = parseInt(curpage);
        console.log("cleaning prods");
        var begProd = (curpage * prodOptions.currentView) - prodOptions.currentView;
        var endProd = ((begProd + prodOptions.currentView) - 1);
        console.log("Showing prods " + begProd + " - " + endProd);
        $("#Container .product").hide();
        for (var i = 0; i < $("#Container .product").length; i++) {
            if (i >= begProd && i <= endProd) {
                var curProd = $("#Container .product")[i];
                $(curProd).show();
            }
        }
    }
})
