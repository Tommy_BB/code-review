var setlastnav = function(params) {
    try {
        console.log(params);
        sessionStorage.setItem("navInfo", params);
    } catch(err) {
       // console.log(err);
    }
};
var getlastNav = function() {
    try {
        return sessionStorage.getItem("navInfo");
    } catch(err) {
       // console.log(err);
    }
};

var cloneNav = function() {
    var dropDown;
    var params={};
    window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str,key,value) {
        if(key == "sess" || key == "store80" || key == "store81" || key == "storeid") return;
        params[key] = value;
    });

    if ((($(location).attr('href').match(/page=product/) && !$(location).attr('href').match(/page=products/)) || $(location).attr('href').match(/page=blogarticle/))) {
        var oldwindow = getlastNav();
        if(oldwindow != null && (typeof oldwindow != 'undefined') && oldwindow.length > 0) {
            var params = {};
            oldwindow.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (str, key, value) {
                if (key == "sess" || key == "store80" || key == "store81" || key == "storeid") return;
                params[key] = value;
            });
        }
    }
    if ($(location).attr('href').match(/page=/)) {
        $("nav").find("a").each(function() {
            var thelink = $(this).attr("href");
            var paramer = {};

            thelink.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str,key,value) {
                if(key == "sess" || key == "store80" || key == "store81" || key == "storeid") return;
                paramer[key] = value;
            });


            if($.compare(params,paramer))
            {
                $(this).closest(".maincat").find(".cattitle").addClass("active");
                dropDown = $(this).closest(".top-drop");
                $(this).closest(".top-drop").find("a").first().addClass("alwaysopen");
            }
        });
    }

    if(typeof dropDown != 'undefined')
    {
        setlastnav(window.location.search.toString());
        if($(dropDown).index() > 0)
        {
            $(".wwlogo").css('display','block');
        } else {
            $(".svlogo").css('display','block');
        }
    } else {
            $(".svlogo").css('display', 'block');

    }


    $("#content-wrap").prepend("<div style=\"clear:both;\"></div>");
    $("#content-wrap").prepend($(dropDown).find("ul.dropdown-menu").first().clone());
};
$(document).ready(function() {
    cloneNav();
	$("a.topnav").click(function(event) {
        if($(window).width() > 1024){
        	var url = $(this).attr('href');
        	if (url != '#'){
        		window.location.href = url;
        	}
        }
    });

	var stillinnav = false;
	var hittingnav = 0;

    $(".nav.navbar-nav li>a.topnav:first-child").hover(
	    function() {
	    	var thisaleft = Math.round($(this).offset().left)-20;
	    	if($(window).width() < 1024)
	    	{
                try{
	    		//$(".dropdown>a:first-child").parent().children("ul.dropdown-menu").removeStyle('display');
	    		$(".dropdown>a:first-child").parent().children("ul.dropdown-menu").attr('style', function(i, style) {
				    return style.replace(/display[^;]+;?/g, '');
				});
				$(".nav.navbar-nav li>a:first-child").parent().find(".row:first-child").css("left","0px");
                } catch(exc){}
		    } else {
		    
		    //this lines the row to the navigation a
		    //$(".nav.navbar-nav li>a:first-child").parent().find(".row:first-child").css("left",thisaleft+"px");
		    
		    //i hate msie...
		    $(this).parent().find(".top-menu").css({"width":$(window).width()+"px"});

		    if($.browser.msie || !!navigator.userAgent.match(/Trident.*rv\:11\./))
		    {
				$(this).parent().find(".top-menu").css({"left" : "-"+$(this).offset().left+"px"});
	    	}

	    	//end i hate msie
	    	
	    	$(".dropdown>a:first-child").parent().children("ul.top-menu").hide();
		    $(this).parent().find("ul.top-menu").show();
		    
		     $(this).parent().find("ul.top-menu").mouseenter(function () {
		     	stillinnav = true;
		     });
		     $(".navbar").mouseleave(function () {
                if (stillinnav) {
                    $(".navbar ul.top-menu").hide();
                    setTimeout(function () {
                        stillinnav = false;
                    }, 500);
                }
            });
            }
	    }, function() {
	    }
    );


    $(".nav.navbar-nav li>a:first-child").click(
	    function() {
	    	if($(window).width() < 1024)
	    	{
	    		//$(".dropdown>a:first-child").parent().children("ul.dropdown-menu").removeStyle('display');
                try {
                    $(".dropdown>a:first-child").parent().children("ul.dropdown-menu").attr('style', function (i, style) {
                        return style.replace(/display[^;]+;?/g, '');
                    });
                    $(".nav.navbar-nav li>a:first-child").parent().find(".row:first-child").css("left", "0px");
                } catch(exc){}
		    } else {
		    
		    //this lines the row to the navigation a
		    //$(".nav.navbar-nav li>a:first-child").parent().find(".row:first-child").css("left",thisaleft+"px");
		    
		    //i hate msie...
		    $(this).parent().find(".top-menu").css({"width":$(window).width()+"px"});
		    
		    if($.browser.msie || !!navigator.userAgent.match(/Trident.*rv\:11\./))
		    {
				$(this).parent().find(".top-menu").css({"left" : "-"+$(this).offset().left+"px"});
	    	}
	    	
	    	//end i hate msie
	    	
	    	$(".dropdown>a:first-child").parent().children("ul.dropdown-menu").hide();
		    $(this).parent().find("ul.top-menu").show();
		    
		     $(this).parent().find("ul.top-menu").mouseenter(function () {
		     	stillinnav = true;
		     });
		     $(".navbar").mouseleave(function () {
                if (stillinnav) {
                    $(".navbar ul.top-menu").hide();
                    setTimeout(function () {
                        stillinnav = false;
                    }, 500);
                }
            });
            }
	    }
    );



    $(".navbar-header .navbar-toggle").click(function() {
	    if($(".navbar-collapse").height() <= 0 || hittingnav == 0)
	    {
	    	if(hittingnav == 0) hittingnav++;
		    $(".navbar-header .navbar-toggle .icon-bar").addClass("activated");
	    } else {
		   	$(".navbar-header .navbar-toggle .icon-bar").removeClass("activated");
	    }
    });

    //sub menu shit
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $(this).parent().siblings().removeClass('open');
        $(this).parent().toggleClass('open');
    });
});

