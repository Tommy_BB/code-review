<?php
    error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_STRICT);
    require_once __DIR__ . '/../../packages/zmvc/ZWebApplication.php';

    try {
        //$applicationEnvironment = "staging";
		if(stripos($_SERVER['SERVER_NAME'], "dev") !== false)
			$applicationEnvironment = "staging";
		
        if ($applicationEnvironment == 'development') {
            $db = require_once __DIR__ . '/config/development.php';
            $isDebugMode = true;
        } else if ($applicationEnvironment == 'staging') {
            $db = require_once __DIR__ . '/config/production-non-internal.php';
            $isDebugMode = true;
        } else {
            $db = require_once __DIR__ . '/config/production.php';
            $isDebugMode = false;
        }

        $configuration = array(
            'action'  => $_REQUEST['page'],
            'storeId' => $_REQUEST['storeid'],
            'layout'  => 'store.twig',
            'theme'   => 'store',
            'debug'   => $isDebugMode,
            'db'      => $db,
        );
        \ZMVC\ZWebApplication::getInstance()->run($configuration);
    } catch (Exception $err) {
        // This is a fallback if there is not a valid ErrorAction
        echo "An error occured: {$err->getMessage()}";
    }


