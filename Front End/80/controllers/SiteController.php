<?php
    use ZMVC\Store\Helpers\ZStoreProductsHelper;
    use ZMVC\ZController;
    use ZMVC\ZRegistry;
    use ZMVC\Store\Helpers\ZSEOHelper;

    /**
     * Class SiteController
     */
    class SiteController extends ZController
    {
        /**
         *
         */
        protected function init()
        {

            $categories = $this->getStoreProductCategories(2376);
            ZRegistry::getInstance()->add('categories', $categories);

            $svcategories = $this->getStoreProductCategories(2432);
            ZRegistry::getInstance()->add('svcategories', $svcategories);

            $svcategories = $this->getStoreProductCategories(2442);
            ZRegistry::getInstance()->add('wwcategories', $svcategories);


            $featuredcategories = $this->getStoreProductCategories(2373);
            ZRegistry::getInstance()->add('featuredcategories', $featuredcategories);

            $storename = ZStoreProductsHelper::getStoreName($this->getStoreId());
            ZRegistry::getInstance()->add('storename', $storename);

            if(isset($_REQUEST['categoryId']))
            {
            	$currentCatName = $this->getProductCategoryName($_REQUEST['categoryId']);
            	ZRegistry::getInstance()->add('currentCatName', $currentCatName);
            }

            if (isset($_SESSION['cart'])) {
                $cartItems = $this->prepareCartItems($_SESSION['cart']);
                ZRegistry::getInstance()->add('cartItems', $cartItems);
            }

            if(($overridePageseo = ZSEOHelper::findPagesSEO($this->getStoreId())) !== false)
            {
                $seoTitle = $overridePageseo['metatitle'];
                $seoDescription = $overridePageseo['metadescription'];
                $seoImage = $overridePageseo['metaimage'];
                $seohone = $overridePageseo['h1'];
                $seohonedesc = $overridePageseo['h1description'];
            } else {
                $seoTitle = $storename;
                $seoDescription  = "";
                $seoImage = "";
                $seohone = "";
                $seohonedesc = "";
            }

            ZRegistry::getInstance()->add('seo', array("title" => $seoTitle, "desc" => $seoDescription, "image" => $seoImage, "hone" => $seohone, "honedesc" => $seohonedesc));

            $countries = $this->getCountries();
            ZRegistry::getInstance()->add('countries', $countries);
        }
        
        private final function getProductCategoryName(
			$categoryId
		) {
			return ZStoreProductsHelper::getProductCategoryName($categoryId);
		}

        /**
         * @return array
         * @throws Exception
         */
        private final function getCountries()
        {
            $countryLookupQry = "SELECT *
                FROM Countries c
                GROUP BY c.Country_Code
                ORDER BY c.Country ASC";

            $dbHandler = ZRegistry::getInstance()->get('db');
            try {
                $countryStmt = $dbHandler->prepare($countryLookupQry);
                $countries = array();
                if (true === $countryStmt->execute()) {
                    foreach ($countryStmt->fetchAll(PDO::FETCH_ASSOC) as $countryRow) {
                        array_push($countries, $countryRow);
                    }

                    return $countries;
                } else {
                    return false;
                }
            } catch (\PDOException $pErr) {
                throw new \Exception($pErr->getMessage());
            }
        }

        /**
         * @return array
         */
        private function getStoreProductCategories($cat)
        {
            $productCategories = ZStoreProductsHelper::getProductCategoryTreeByParentId($this->getStoreId(), $cat);
            return $productCategories;
        }

        /**
         * @param Cart $cart
         *
         * @return array
         */
        private function prepareCartItems(Cart $cart) {
            if ($cart->itemcount() == 0) {
                return;
            }

            /** @var Products $products */
            require_once __DIR__ . "/../../../../classes/products.php";
            if (!isset($products)) {
                $products = new products;
            }

            $cartItems = array();
            foreach ($cart->items as $cartItemKey => $item) {
                $productImage = $products->getProductImage($item->get_id());
                $cartItem['image'] = $productImage['ImagePath'];
                $cartItem['details'] = $item;
                $cartItem['itemKey'] = $cartItemKey;

                /**
                 * Used to retrieve the properties for a particular product in a user's cart
                 * @todo should refactor this to not include the overhead of calling in all known product details
                 */
                $productDetails = ZMVC\Store\Helpers\ZStoreProductsHelper::getProductDetails($this->getStoreId(), $item->get_id());

                // Set the cart item's properties
                $cartItem['properties'] = array();
                foreach ($productDetails['properties'] as $propertyType => $propertyTypeData) {
                    // If there are no options for the property type, then we don't care
                    if (!isset($propertyTypeData['options']) or empty($propertyTypeData['options'])) {
                        continue;
                    }

                    foreach ($propertyTypeData['options'] as $optionData) {
                        foreach ($item->properties as $name => $propertyValueId) {
                            if ($propertyValueId == $optionData['ID']) {
                                $cartItem['properties'][strtolower($propertyType)] = $optionData['Name'];
                                break;
                            }
                        }
                    }
                }

                array_push($cartItems, $cartItem);
            }

            return $cartItems;
        }
    }